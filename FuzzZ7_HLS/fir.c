
#include <stdint.h>

typedef int32_t s32;
typedef int64_t s64;

const s32 taps = 31;
// Les coefficients sont en virgule fixe 0.40
const s64 coeffs[taps] = {161516059097ll, 13957200602ll, 14541041277ll, 14962154230ll, 15394262300ll, 15794484533ll, 16148527277ll, 16512465625ll, 16812632300ll, 17127092625ll, 17362388114ll, 17608678718ll, 17760411323ll, 17915442462ll, 17968219021ll, 18020995579ll, 17968219021ll, 17915442462ll, 17760411323ll, 17608678718ll, 17362388114ll, 17127092625ll, 16812632300ll, 16512465625ll, 16148527277ll, 15794484533ll, 15394262300ll, 14962154230ll, 14541041277ll, 13957200602ll, 161516059097ll, }; // @Param

void fir(s32 *y, s32 x[2]) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis register both port=y
#pragma HLS INTERFACE axis register both port=x

  static s32 shift_reg0[taps]; // Virgule fixe 24.0
  static s32 shift_reg1[taps]; // Virgule fixe 24.0
  s32 x0 = x[0]; // Virgule fixe 24.0
  s32 x1 = x[1]; // Virgule fixe 24.0
  s64 acc0 = 0; // Virgule fixe 24.40
  s64 acc1 = 0; // Virgule fixe 24.40

  for(s32 i = taps-1; i >= 1; i--) {
	s32 data0 = shift_reg0[i-1];
	s32 data1 = shift_reg1[i-1];

	shift_reg0[i] = data0;
	shift_reg1[i] = data1;

	acc0 += data0 * coeffs[i];
	acc1 += data1 * coeffs[i];
  }

  shift_reg0[0] = x0;
  shift_reg1[0] = x1;

  acc0 += x0 * coeffs[0];
  acc1 += x1 * coeffs[0];

  y[0] = ((s32)(acc0 >> 40)); // Troncation en virgule fixe 24.0
  y[1] = ((s32)(acc1 >> 40)); // Troncation en virgule fixe 24.0
}
