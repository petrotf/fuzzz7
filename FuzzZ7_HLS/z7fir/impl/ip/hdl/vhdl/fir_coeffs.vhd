-- ==============================================================
-- Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2020.1 (64-bit)
-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- ==============================================================
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity fir_coeffs_rom is 
    generic(
             DWIDTH     : integer := 38; 
             AWIDTH     : integer := 5; 
             MEM_SIZE    : integer := 31
    ); 
    port (
          addr0      : in std_logic_vector(AWIDTH-1 downto 0); 
          ce0       : in std_logic; 
          q0         : out std_logic_vector(DWIDTH-1 downto 0);
          clk       : in std_logic
    ); 
end entity; 


architecture rtl of fir_coeffs_rom is 

signal addr0_tmp : std_logic_vector(AWIDTH-1 downto 0); 
type mem_array is array (0 to MEM_SIZE-1) of std_logic_vector (DWIDTH-1 downto 0); 
signal mem : mem_array := (
    0 => "10010110011011000110110111100111011001", 
    1 => "00001100111111111010011111101011011010", 
    2 => "00001101100010101101101010111001111101", 
    3 => "00001101111011110100000101101011110110", 
    4 => "00001110010101100100011100110100011100", 
    5 => "00001110101101011011001011010100110101", 
    6 => "00001111000010100001101111100010101101", 
    7 => "00001111011000001110000011101011011001", 
    8 => "00001111101010000111000110100011101100", 
    9 => "00001111111100110110101011000110010001", 
    10 => "00010000001010111000010000010010010010", 
    11 => "00010000011001100011110001110100111110", 
    12 => "00010000100010100110100101111010111011", 
    13 => "00010000101011110101111111010100011110", 
    14 => "00010000101110111111010100001110001101", 
    15 => "00010000110010001000101001000111111011", 
    16 => "00010000101110111111010100001110001101", 
    17 => "00010000101011110101111111010100011110", 
    18 => "00010000100010100110100101111010111011", 
    19 => "00010000011001100011110001110100111110", 
    20 => "00010000001010111000010000010010010010", 
    21 => "00001111111100110110101011000110010001", 
    22 => "00001111101010000111000110100011101100", 
    23 => "00001111011000001110000011101011011001", 
    24 => "00001111000010100001101111100010101101", 
    25 => "00001110101101011011001011010100110101", 
    26 => "00001110010101100100011100110100011100", 
    27 => "00001101111011110100000101101011110110", 
    28 => "00001101100010101101101010111001111101", 
    29 => "00001100111111111010011111101011011010", 
    30 => "10010110011011000110110111100111011001" );


begin 


memory_access_guard_0: process (addr0) 
begin
      addr0_tmp <= addr0;
--synthesis translate_off
      if (CONV_INTEGER(addr0) > mem_size-1) then
           addr0_tmp <= (others => '0');
      else 
           addr0_tmp <= addr0;
      end if;
--synthesis translate_on
end process;

p_rom_access: process (clk)  
begin 
    if (clk'event and clk = '1') then
        if (ce0 = '1') then 
            q0 <= mem(CONV_INTEGER(addr0_tmp)); 
        end if;
    end if;
end process;

end rtl;

Library IEEE;
use IEEE.std_logic_1164.all;

entity fir_coeffs is
    generic (
        DataWidth : INTEGER := 38;
        AddressRange : INTEGER := 31;
        AddressWidth : INTEGER := 5);
    port (
        reset : IN STD_LOGIC;
        clk : IN STD_LOGIC;
        address0 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce0 : IN STD_LOGIC;
        q0 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0));
end entity;

architecture arch of fir_coeffs is
    component fir_coeffs_rom is
        port (
            clk : IN STD_LOGIC;
            addr0 : IN STD_LOGIC_VECTOR;
            ce0 : IN STD_LOGIC;
            q0 : OUT STD_LOGIC_VECTOR);
    end component;



begin
    fir_coeffs_rom_U :  component fir_coeffs_rom
    port map (
        clk => clk,
        addr0 => address0,
        ce0 => ce0,
        q0 => q0);

end architecture;


