// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2020.1
// Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _fir_HH_
#define _fir_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "fir_shift_reg0.h"
#include "fir_coeffs.h"

namespace ap_rtl {

struct fir : public sc_module {
    // Port declarations 8
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst_n;
    sc_out< sc_lv<32> > y_TDATA;
    sc_out< sc_logic > y_TVALID;
    sc_in< sc_logic > y_TREADY;
    sc_in< sc_lv<32> > x_TDATA;
    sc_in< sc_logic > x_TVALID;
    sc_out< sc_logic > x_TREADY;


    // Module declarations
    fir(sc_module_name name);
    SC_HAS_PROCESS(fir);

    ~fir();

    sc_trace_file* mVcdFile;

    ofstream mHdltvinHandle;
    ofstream mHdltvoutHandle;
    fir_shift_reg0* shift_reg0_U;
    fir_shift_reg0* shift_reg1_U;
    fir_coeffs* coeffs_U;
    regslice_both<32>* regslice_both_y_U;
    regslice_both<32>* regslice_both_x_U;
    sc_signal< sc_logic > ap_rst_n_inv;
    sc_signal< sc_lv<5> > shift_reg0_address0;
    sc_signal< sc_logic > shift_reg0_ce0;
    sc_signal< sc_logic > shift_reg0_we0;
    sc_signal< sc_lv<32> > shift_reg0_d0;
    sc_signal< sc_lv<32> > shift_reg0_q0;
    sc_signal< sc_lv<5> > shift_reg1_address0;
    sc_signal< sc_logic > shift_reg1_ce0;
    sc_signal< sc_logic > shift_reg1_we0;
    sc_signal< sc_lv<32> > shift_reg1_d0;
    sc_signal< sc_lv<32> > shift_reg1_q0;
    sc_signal< sc_lv<5> > coeffs_address0;
    sc_signal< sc_logic > coeffs_ce0;
    sc_signal< sc_lv<38> > coeffs_q0;
    sc_signal< sc_logic > y_TDATA_blk_n;
    sc_signal< sc_lv<6> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state3;
    sc_signal< sc_lv<1> > icmp_ln66_fu_165_p2;
    sc_signal< sc_logic > ap_CS_fsm_state5;
    sc_signal< sc_logic > ap_CS_fsm_state6;
    sc_signal< sc_logic > x_TDATA_blk_n;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_logic > ap_CS_fsm_state2;
    sc_signal< sc_lv<32> > x0_reg_283;
    sc_signal< sc_lv<32> > x1_reg_289;
    sc_signal< sc_lv<5> > i_fu_171_p2;
    sc_signal< sc_lv<5> > i_reg_298;
    sc_signal< bool > ap_block_state3_io;
    sc_signal< sc_lv<64> > zext_ln70_fu_183_p1;
    sc_signal< sc_lv<64> > zext_ln70_reg_313;
    sc_signal< sc_lv<32> > sext_ln83_fu_228_p1;
    sc_signal< sc_lv<24> > trunc_ln1_reg_329;
    sc_signal< sc_lv<64> > acc0_fu_257_p2;
    sc_signal< sc_logic > ap_CS_fsm_state4;
    sc_signal< sc_lv<64> > acc1_fu_273_p2;
    sc_signal< sc_lv<32> > sext_ln84_fu_279_p1;
    sc_signal< sc_lv<64> > acc0_0_reg_130;
    sc_signal< sc_lv<64> > acc1_0_reg_142;
    sc_signal< sc_lv<5> > i_0_reg_154;
    sc_signal< sc_lv<64> > zext_ln67_fu_177_p1;
    sc_signal< sc_lv<32> > mul_ln80_fu_191_p0;
    sc_signal< sc_lv<64> > mul_ln80_fu_191_p2;
    sc_signal< sc_lv<32> > mul_ln81_fu_206_p0;
    sc_signal< sc_lv<64> > mul_ln81_fu_206_p2;
    sc_signal< sc_lv<64> > acc0_1_fu_197_p2;
    sc_signal< sc_lv<24> > trunc_ln_fu_218_p4;
    sc_signal< sc_lv<64> > acc1_1_fu_212_p2;
    sc_signal< sc_lv<32> > sext_ln73_fu_243_p0;
    sc_signal< sc_lv<38> > mul_ln73_fu_251_p0;
    sc_signal< sc_lv<64> > zext_ln73_fu_247_p1;
    sc_signal< sc_lv<32> > mul_ln73_fu_251_p1;
    sc_signal< sc_lv<64> > mul_ln73_fu_251_p2;
    sc_signal< sc_lv<32> > sext_ln74_fu_263_p0;
    sc_signal< sc_lv<38> > mul_ln74_fu_267_p0;
    sc_signal< sc_lv<32> > mul_ln74_fu_267_p1;
    sc_signal< sc_lv<64> > mul_ln74_fu_267_p2;
    sc_signal< sc_lv<6> > ap_NS_fsm;
    sc_signal< sc_logic > regslice_both_y_U_apdone_blk;
    sc_signal< sc_lv<32> > y_TDATA_int;
    sc_signal< sc_logic > y_TVALID_int;
    sc_signal< sc_logic > y_TREADY_int;
    sc_signal< sc_logic > regslice_both_y_U_vld_out;
    sc_signal< sc_logic > regslice_both_x_U_apdone_blk;
    sc_signal< sc_lv<32> > x_TDATA_int;
    sc_signal< sc_logic > x_TVALID_int;
    sc_signal< sc_logic > x_TREADY_int;
    sc_signal< sc_logic > regslice_both_x_U_ack_in;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<6> ap_ST_fsm_state1;
    static const sc_lv<6> ap_ST_fsm_state2;
    static const sc_lv<6> ap_ST_fsm_state3;
    static const sc_lv<6> ap_ST_fsm_state4;
    static const sc_lv<6> ap_ST_fsm_state5;
    static const sc_lv<6> ap_ST_fsm_state6;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<32> ap_const_lv32_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<64> ap_const_lv64_0;
    static const sc_lv<5> ap_const_lv5_1E;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<5> ap_const_lv5_1F;
    static const sc_lv<64> ap_const_lv64_259B1B79D9;
    static const sc_lv<32> ap_const_lv32_28;
    static const sc_lv<32> ap_const_lv32_3F;
    static const bool ap_const_boolean_1;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_acc0_1_fu_197_p2();
    void thread_acc0_fu_257_p2();
    void thread_acc1_1_fu_212_p2();
    void thread_acc1_fu_273_p2();
    void thread_ap_CS_fsm_state1();
    void thread_ap_CS_fsm_state2();
    void thread_ap_CS_fsm_state3();
    void thread_ap_CS_fsm_state4();
    void thread_ap_CS_fsm_state5();
    void thread_ap_CS_fsm_state6();
    void thread_ap_block_state3_io();
    void thread_ap_rst_n_inv();
    void thread_coeffs_address0();
    void thread_coeffs_ce0();
    void thread_i_fu_171_p2();
    void thread_icmp_ln66_fu_165_p2();
    void thread_mul_ln73_fu_251_p0();
    void thread_mul_ln73_fu_251_p1();
    void thread_mul_ln73_fu_251_p2();
    void thread_mul_ln74_fu_267_p0();
    void thread_mul_ln74_fu_267_p1();
    void thread_mul_ln74_fu_267_p2();
    void thread_mul_ln80_fu_191_p0();
    void thread_mul_ln80_fu_191_p2();
    void thread_mul_ln81_fu_206_p0();
    void thread_mul_ln81_fu_206_p2();
    void thread_sext_ln73_fu_243_p0();
    void thread_sext_ln74_fu_263_p0();
    void thread_sext_ln83_fu_228_p1();
    void thread_sext_ln84_fu_279_p1();
    void thread_shift_reg0_address0();
    void thread_shift_reg0_ce0();
    void thread_shift_reg0_d0();
    void thread_shift_reg0_we0();
    void thread_shift_reg1_address0();
    void thread_shift_reg1_ce0();
    void thread_shift_reg1_d0();
    void thread_shift_reg1_we0();
    void thread_trunc_ln_fu_218_p4();
    void thread_x_TDATA_blk_n();
    void thread_x_TREADY();
    void thread_x_TREADY_int();
    void thread_y_TDATA_blk_n();
    void thread_y_TDATA_int();
    void thread_y_TVALID();
    void thread_y_TVALID_int();
    void thread_zext_ln67_fu_177_p1();
    void thread_zext_ln70_fu_183_p1();
    void thread_zext_ln73_fu_247_p1();
    void thread_ap_NS_fsm();
    void thread_hdltv_gen();
};

}

using namespace ap_rtl;

#endif
