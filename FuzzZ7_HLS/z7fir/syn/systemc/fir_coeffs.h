// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2020.1 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __fir_coeffs_H__
#define __fir_coeffs_H__


#include <systemc>
using namespace sc_core;
using namespace sc_dt;




#include <iostream>
#include <fstream>

struct fir_coeffs_ram : public sc_core::sc_module {

  static const unsigned DataWidth = 38;
  static const unsigned AddressRange = 31;
  static const unsigned AddressWidth = 5;

//latency = 1
//input_reg = 1
//output_reg = 0
sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in <sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


sc_lv<DataWidth> ram[AddressRange];


   SC_CTOR(fir_coeffs_ram) {
        ram[0] = "0b10010110011011000110110111100111011001";
        ram[1] = "0b00001100111111111010011111101011011010";
        ram[2] = "0b00001101100010101101101010111001111101";
        ram[3] = "0b00001101111011110100000101101011110110";
        ram[4] = "0b00001110010101100100011100110100011100";
        ram[5] = "0b00001110101101011011001011010100110101";
        ram[6] = "0b00001111000010100001101111100010101101";
        ram[7] = "0b00001111011000001110000011101011011001";
        ram[8] = "0b00001111101010000111000110100011101100";
        ram[9] = "0b00001111111100110110101011000110010001";
        ram[10] = "0b00010000001010111000010000010010010010";
        ram[11] = "0b00010000011001100011110001110100111110";
        ram[12] = "0b00010000100010100110100101111010111011";
        ram[13] = "0b00010000101011110101111111010100011110";
        ram[14] = "0b00010000101110111111010100001110001101";
        ram[15] = "0b00010000110010001000101001000111111011";
        ram[16] = "0b00010000101110111111010100001110001101";
        ram[17] = "0b00010000101011110101111111010100011110";
        ram[18] = "0b00010000100010100110100101111010111011";
        ram[19] = "0b00010000011001100011110001110100111110";
        ram[20] = "0b00010000001010111000010000010010010010";
        ram[21] = "0b00001111111100110110101011000110010001";
        ram[22] = "0b00001111101010000111000110100011101100";
        ram[23] = "0b00001111011000001110000011101011011001";
        ram[24] = "0b00001111000010100001101111100010101101";
        ram[25] = "0b00001110101101011011001011010100110101";
        ram[26] = "0b00001110010101100100011100110100011100";
        ram[27] = "0b00001101111011110100000101101011110110";
        ram[28] = "0b00001101100010101101101010111001111101";
        ram[29] = "0b00001100111111111010011111101011011010";
        ram[30] = "0b10010110011011000110110111100111011001";


SC_METHOD(prc_write_0);
  sensitive<<clk.pos();
   }


void prc_write_0()
{
    if (ce0.read() == sc_dt::Log_1) 
    {
            if(address0.read().is_01() && address0.read().to_uint()<AddressRange)
              q0 = ram[address0.read().to_uint()];
            else
              q0 = sc_lv<DataWidth>();
    }
}


}; //endmodule


SC_MODULE(fir_coeffs) {


static const unsigned DataWidth = 38;
static const unsigned AddressRange = 31;
static const unsigned AddressWidth = 5;

sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in<sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


fir_coeffs_ram* meminst;


SC_CTOR(fir_coeffs) {
meminst = new fir_coeffs_ram("fir_coeffs_ram");
meminst->address0(address0);
meminst->ce0(ce0);
meminst->q0(q0);

meminst->reset(reset);
meminst->clk(clk);
}
~fir_coeffs() {
    delete meminst;
}


};//endmodule
#endif
