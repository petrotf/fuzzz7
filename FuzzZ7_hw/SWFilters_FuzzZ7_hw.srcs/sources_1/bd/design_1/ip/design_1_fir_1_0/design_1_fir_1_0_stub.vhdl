-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Tue Jan 19 11:09:26 2021
-- Host        : DESKTOP-3T6RBG4 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               c:/Users/keeba/workspace/FuzzZ7_SWFilters_2.0/SWFilters_FuzzZ7_hw/SWFilters_FuzzZ7_hw.srcs/sources_1/bd/design_1/ip/design_1_fir_1_0/design_1_fir_1_0_stub.vhdl
-- Design      : design_1_fir_1_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_fir_1_0 is
  Port ( 
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    y_TVALID : out STD_LOGIC;
    y_TREADY : in STD_LOGIC;
    y_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    x_TVALID : in STD_LOGIC;
    x_TREADY : out STD_LOGIC;
    x_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end design_1_fir_1_0;

architecture stub of design_1_fir_1_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "ap_clk,ap_rst_n,y_TVALID,y_TREADY,y_TDATA[31:0],x_TVALID,x_TREADY,x_TDATA[31:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "fir,Vivado 2020.1";
begin
end;
