// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Tue Jan 19 11:09:26 2021
// Host        : DESKTOP-3T6RBG4 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/keeba/workspace/FuzzZ7_SWFilters_2.0/SWFilters_FuzzZ7_hw/SWFilters_FuzzZ7_hw.srcs/sources_1/bd/design_1/ip/design_1_fir_1_0/design_1_fir_1_0_stub.v
// Design      : design_1_fir_1_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "fir,Vivado 2020.1" *)
module design_1_fir_1_0(ap_clk, ap_rst_n, y_TVALID, y_TREADY, y_TDATA, 
  x_TVALID, x_TREADY, x_TDATA)
/* synthesis syn_black_box black_box_pad_pin="ap_clk,ap_rst_n,y_TVALID,y_TREADY,y_TDATA[31:0],x_TVALID,x_TREADY,x_TDATA[31:0]" */;
  input ap_clk;
  input ap_rst_n;
  output y_TVALID;
  input y_TREADY;
  output [31:0]y_TDATA;
  input x_TVALID;
  output x_TREADY;
  input [31:0]x_TDATA;
endmodule
