// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Tue Jan 19 11:09:24 2021
// Host        : DESKTOP-3T6RBG4 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_fir_1_0_sim_netlist.v
// Design      : design_1_fir_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_fir_1_0,fir,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "HLS" *) 
(* X_CORE_INFO = "fir,Vivado 2020.1" *) (* hls_module = "yes" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ap_clk,
    ap_rst_n,
    y_TVALID,
    y_TREADY,
    y_TDATA,
    x_TVALID,
    x_TREADY,
    x_TDATA);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF y:x, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 5e+07, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input ap_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input ap_rst_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 y TVALID" *) output y_TVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 y TREADY" *) input y_TREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 y TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME y, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 5e+07, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) output [31:0]y_TDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 x TVALID" *) input x_TVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 x TREADY" *) output x_TREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 x TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME x, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, LAYERED_METADATA undef, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 5e+07, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input [31:0]x_TDATA;

  wire ap_clk;
  wire ap_rst_n;
  wire [31:0]x_TDATA;
  wire x_TREADY;
  wire x_TVALID;
  wire [31:0]y_TDATA;
  wire y_TREADY;
  wire y_TVALID;

  (* ap_ST_fsm_state1 = "6'b000001" *) 
  (* ap_ST_fsm_state2 = "6'b000010" *) 
  (* ap_ST_fsm_state3 = "6'b000100" *) 
  (* ap_ST_fsm_state4 = "6'b001000" *) 
  (* ap_ST_fsm_state5 = "6'b010000" *) 
  (* ap_ST_fsm_state6 = "6'b100000" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir inst
       (.ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .x_TDATA(x_TDATA),
        .x_TREADY(x_TREADY),
        .x_TVALID(x_TVALID),
        .y_TDATA(y_TDATA),
        .y_TREADY(y_TREADY),
        .y_TVALID(y_TVALID));
endmodule

(* ap_ST_fsm_state1 = "6'b000001" *) (* ap_ST_fsm_state2 = "6'b000010" *) (* ap_ST_fsm_state3 = "6'b000100" *) 
(* ap_ST_fsm_state4 = "6'b001000" *) (* ap_ST_fsm_state5 = "6'b010000" *) (* ap_ST_fsm_state6 = "6'b100000" *) 
(* hls_module = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir
   (ap_clk,
    ap_rst_n,
    y_TDATA,
    y_TVALID,
    y_TREADY,
    x_TDATA,
    x_TVALID,
    x_TREADY);
  input ap_clk;
  input ap_rst_n;
  output [31:0]y_TDATA;
  output y_TVALID;
  input y_TREADY;
  input [31:0]x_TDATA;
  input x_TVALID;
  output x_TREADY;

  wire acc0_0_reg_130;
  wire \acc0_0_reg_130[11]_i_2_n_1 ;
  wire \acc0_0_reg_130[11]_i_3_n_1 ;
  wire \acc0_0_reg_130[11]_i_4_n_1 ;
  wire \acc0_0_reg_130[11]_i_5_n_1 ;
  wire \acc0_0_reg_130[15]_i_2_n_1 ;
  wire \acc0_0_reg_130[15]_i_3_n_1 ;
  wire \acc0_0_reg_130[15]_i_4_n_1 ;
  wire \acc0_0_reg_130[15]_i_5_n_1 ;
  wire \acc0_0_reg_130[19]_i_3_n_1 ;
  wire \acc0_0_reg_130[19]_i_4_n_1 ;
  wire \acc0_0_reg_130[19]_i_5_n_1 ;
  wire \acc0_0_reg_130[19]_i_6_n_1 ;
  wire \acc0_0_reg_130[19]_i_7_n_1 ;
  wire \acc0_0_reg_130[19]_i_8_n_1 ;
  wire \acc0_0_reg_130[19]_i_9_n_1 ;
  wire \acc0_0_reg_130[23]_i_10_n_1 ;
  wire \acc0_0_reg_130[23]_i_3_n_1 ;
  wire \acc0_0_reg_130[23]_i_4_n_1 ;
  wire \acc0_0_reg_130[23]_i_5_n_1 ;
  wire \acc0_0_reg_130[23]_i_6_n_1 ;
  wire \acc0_0_reg_130[23]_i_7_n_1 ;
  wire \acc0_0_reg_130[23]_i_8_n_1 ;
  wire \acc0_0_reg_130[23]_i_9_n_1 ;
  wire \acc0_0_reg_130[27]_i_10_n_1 ;
  wire \acc0_0_reg_130[27]_i_3_n_1 ;
  wire \acc0_0_reg_130[27]_i_4_n_1 ;
  wire \acc0_0_reg_130[27]_i_5_n_1 ;
  wire \acc0_0_reg_130[27]_i_6_n_1 ;
  wire \acc0_0_reg_130[27]_i_7_n_1 ;
  wire \acc0_0_reg_130[27]_i_8_n_1 ;
  wire \acc0_0_reg_130[27]_i_9_n_1 ;
  wire \acc0_0_reg_130[31]_i_10_n_1 ;
  wire \acc0_0_reg_130[31]_i_3_n_1 ;
  wire \acc0_0_reg_130[31]_i_4_n_1 ;
  wire \acc0_0_reg_130[31]_i_5_n_1 ;
  wire \acc0_0_reg_130[31]_i_6_n_1 ;
  wire \acc0_0_reg_130[31]_i_7_n_1 ;
  wire \acc0_0_reg_130[31]_i_8_n_1 ;
  wire \acc0_0_reg_130[31]_i_9_n_1 ;
  wire \acc0_0_reg_130[35]_i_10_n_1 ;
  wire \acc0_0_reg_130[35]_i_3_n_1 ;
  wire \acc0_0_reg_130[35]_i_4_n_1 ;
  wire \acc0_0_reg_130[35]_i_5_n_1 ;
  wire \acc0_0_reg_130[35]_i_6_n_1 ;
  wire \acc0_0_reg_130[35]_i_7_n_1 ;
  wire \acc0_0_reg_130[35]_i_8_n_1 ;
  wire \acc0_0_reg_130[35]_i_9_n_1 ;
  wire \acc0_0_reg_130[39]_i_10_n_1 ;
  wire \acc0_0_reg_130[39]_i_3_n_1 ;
  wire \acc0_0_reg_130[39]_i_4_n_1 ;
  wire \acc0_0_reg_130[39]_i_5_n_1 ;
  wire \acc0_0_reg_130[39]_i_6_n_1 ;
  wire \acc0_0_reg_130[39]_i_7_n_1 ;
  wire \acc0_0_reg_130[39]_i_8_n_1 ;
  wire \acc0_0_reg_130[39]_i_9_n_1 ;
  wire \acc0_0_reg_130[3]_i_2_n_1 ;
  wire \acc0_0_reg_130[3]_i_3_n_1 ;
  wire \acc0_0_reg_130[3]_i_4_n_1 ;
  wire \acc0_0_reg_130[3]_i_5_n_1 ;
  wire \acc0_0_reg_130[43]_i_10_n_1 ;
  wire \acc0_0_reg_130[43]_i_3_n_1 ;
  wire \acc0_0_reg_130[43]_i_4_n_1 ;
  wire \acc0_0_reg_130[43]_i_5_n_1 ;
  wire \acc0_0_reg_130[43]_i_6_n_1 ;
  wire \acc0_0_reg_130[43]_i_7_n_1 ;
  wire \acc0_0_reg_130[43]_i_8_n_1 ;
  wire \acc0_0_reg_130[43]_i_9_n_1 ;
  wire \acc0_0_reg_130[47]_i_10_n_1 ;
  wire \acc0_0_reg_130[47]_i_3_n_1 ;
  wire \acc0_0_reg_130[47]_i_4_n_1 ;
  wire \acc0_0_reg_130[47]_i_5_n_1 ;
  wire \acc0_0_reg_130[47]_i_6_n_1 ;
  wire \acc0_0_reg_130[47]_i_7_n_1 ;
  wire \acc0_0_reg_130[47]_i_8_n_1 ;
  wire \acc0_0_reg_130[47]_i_9_n_1 ;
  wire \acc0_0_reg_130[51]_i_10_n_1 ;
  wire \acc0_0_reg_130[51]_i_3_n_1 ;
  wire \acc0_0_reg_130[51]_i_4_n_1 ;
  wire \acc0_0_reg_130[51]_i_5_n_1 ;
  wire \acc0_0_reg_130[51]_i_6_n_1 ;
  wire \acc0_0_reg_130[51]_i_7_n_1 ;
  wire \acc0_0_reg_130[51]_i_8_n_1 ;
  wire \acc0_0_reg_130[51]_i_9_n_1 ;
  wire \acc0_0_reg_130[55]_i_10_n_1 ;
  wire \acc0_0_reg_130[55]_i_3_n_1 ;
  wire \acc0_0_reg_130[55]_i_4_n_1 ;
  wire \acc0_0_reg_130[55]_i_5_n_1 ;
  wire \acc0_0_reg_130[55]_i_6_n_1 ;
  wire \acc0_0_reg_130[55]_i_7_n_1 ;
  wire \acc0_0_reg_130[55]_i_8_n_1 ;
  wire \acc0_0_reg_130[55]_i_9_n_1 ;
  wire \acc0_0_reg_130[59]_i_10_n_1 ;
  wire \acc0_0_reg_130[59]_i_3_n_1 ;
  wire \acc0_0_reg_130[59]_i_4_n_1 ;
  wire \acc0_0_reg_130[59]_i_5_n_1 ;
  wire \acc0_0_reg_130[59]_i_6_n_1 ;
  wire \acc0_0_reg_130[59]_i_7_n_1 ;
  wire \acc0_0_reg_130[59]_i_8_n_1 ;
  wire \acc0_0_reg_130[59]_i_9_n_1 ;
  wire \acc0_0_reg_130[63]_i_10_n_1 ;
  wire \acc0_0_reg_130[63]_i_3_n_1 ;
  wire \acc0_0_reg_130[63]_i_4_n_1 ;
  wire \acc0_0_reg_130[63]_i_5_n_1 ;
  wire \acc0_0_reg_130[63]_i_6_n_1 ;
  wire \acc0_0_reg_130[63]_i_7_n_1 ;
  wire \acc0_0_reg_130[63]_i_8_n_1 ;
  wire \acc0_0_reg_130[63]_i_9_n_1 ;
  wire \acc0_0_reg_130[7]_i_2_n_1 ;
  wire \acc0_0_reg_130[7]_i_3_n_1 ;
  wire \acc0_0_reg_130[7]_i_4_n_1 ;
  wire \acc0_0_reg_130[7]_i_5_n_1 ;
  wire \acc0_0_reg_130_reg[11]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[11]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[11]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[11]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[15]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[15]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[15]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[15]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[19]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[19]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[19]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[19]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[19]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[19]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[19]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[19]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[23]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[23]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[23]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[23]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[23]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[23]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[23]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[23]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[27]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[27]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[27]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[27]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[27]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[27]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[27]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[27]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[31]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[31]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[31]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[31]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[31]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[31]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[31]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[31]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[35]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[35]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[35]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[35]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[35]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[35]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[35]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[35]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[39]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[39]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[39]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[39]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[39]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[39]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[39]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[39]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[3]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[3]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[3]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[3]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[43]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[43]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[43]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[43]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[43]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[43]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[43]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[43]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[47]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[47]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[47]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[47]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[47]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[47]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[47]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[47]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[51]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[51]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[51]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[51]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[51]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[51]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[51]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[51]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[55]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[55]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[55]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[55]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[55]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[55]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[55]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[55]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[59]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[59]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[59]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[59]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[59]_i_2_n_1 ;
  wire \acc0_0_reg_130_reg[59]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[59]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[59]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[63]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[63]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[63]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg[63]_i_2_n_2 ;
  wire \acc0_0_reg_130_reg[63]_i_2_n_3 ;
  wire \acc0_0_reg_130_reg[63]_i_2_n_4 ;
  wire \acc0_0_reg_130_reg[7]_i_1_n_1 ;
  wire \acc0_0_reg_130_reg[7]_i_1_n_2 ;
  wire \acc0_0_reg_130_reg[7]_i_1_n_3 ;
  wire \acc0_0_reg_130_reg[7]_i_1_n_4 ;
  wire \acc0_0_reg_130_reg_n_1_[0] ;
  wire \acc0_0_reg_130_reg_n_1_[10] ;
  wire \acc0_0_reg_130_reg_n_1_[11] ;
  wire \acc0_0_reg_130_reg_n_1_[12] ;
  wire \acc0_0_reg_130_reg_n_1_[13] ;
  wire \acc0_0_reg_130_reg_n_1_[14] ;
  wire \acc0_0_reg_130_reg_n_1_[15] ;
  wire \acc0_0_reg_130_reg_n_1_[16] ;
  wire \acc0_0_reg_130_reg_n_1_[17] ;
  wire \acc0_0_reg_130_reg_n_1_[18] ;
  wire \acc0_0_reg_130_reg_n_1_[19] ;
  wire \acc0_0_reg_130_reg_n_1_[1] ;
  wire \acc0_0_reg_130_reg_n_1_[20] ;
  wire \acc0_0_reg_130_reg_n_1_[21] ;
  wire \acc0_0_reg_130_reg_n_1_[22] ;
  wire \acc0_0_reg_130_reg_n_1_[23] ;
  wire \acc0_0_reg_130_reg_n_1_[24] ;
  wire \acc0_0_reg_130_reg_n_1_[25] ;
  wire \acc0_0_reg_130_reg_n_1_[26] ;
  wire \acc0_0_reg_130_reg_n_1_[27] ;
  wire \acc0_0_reg_130_reg_n_1_[28] ;
  wire \acc0_0_reg_130_reg_n_1_[29] ;
  wire \acc0_0_reg_130_reg_n_1_[2] ;
  wire \acc0_0_reg_130_reg_n_1_[30] ;
  wire \acc0_0_reg_130_reg_n_1_[31] ;
  wire \acc0_0_reg_130_reg_n_1_[32] ;
  wire \acc0_0_reg_130_reg_n_1_[33] ;
  wire \acc0_0_reg_130_reg_n_1_[34] ;
  wire \acc0_0_reg_130_reg_n_1_[35] ;
  wire \acc0_0_reg_130_reg_n_1_[36] ;
  wire \acc0_0_reg_130_reg_n_1_[37] ;
  wire \acc0_0_reg_130_reg_n_1_[38] ;
  wire \acc0_0_reg_130_reg_n_1_[39] ;
  wire \acc0_0_reg_130_reg_n_1_[3] ;
  wire \acc0_0_reg_130_reg_n_1_[40] ;
  wire \acc0_0_reg_130_reg_n_1_[41] ;
  wire \acc0_0_reg_130_reg_n_1_[42] ;
  wire \acc0_0_reg_130_reg_n_1_[43] ;
  wire \acc0_0_reg_130_reg_n_1_[44] ;
  wire \acc0_0_reg_130_reg_n_1_[45] ;
  wire \acc0_0_reg_130_reg_n_1_[46] ;
  wire \acc0_0_reg_130_reg_n_1_[47] ;
  wire \acc0_0_reg_130_reg_n_1_[48] ;
  wire \acc0_0_reg_130_reg_n_1_[49] ;
  wire \acc0_0_reg_130_reg_n_1_[4] ;
  wire \acc0_0_reg_130_reg_n_1_[50] ;
  wire \acc0_0_reg_130_reg_n_1_[51] ;
  wire \acc0_0_reg_130_reg_n_1_[52] ;
  wire \acc0_0_reg_130_reg_n_1_[53] ;
  wire \acc0_0_reg_130_reg_n_1_[54] ;
  wire \acc0_0_reg_130_reg_n_1_[55] ;
  wire \acc0_0_reg_130_reg_n_1_[56] ;
  wire \acc0_0_reg_130_reg_n_1_[57] ;
  wire \acc0_0_reg_130_reg_n_1_[58] ;
  wire \acc0_0_reg_130_reg_n_1_[59] ;
  wire \acc0_0_reg_130_reg_n_1_[5] ;
  wire \acc0_0_reg_130_reg_n_1_[60] ;
  wire \acc0_0_reg_130_reg_n_1_[61] ;
  wire \acc0_0_reg_130_reg_n_1_[62] ;
  wire \acc0_0_reg_130_reg_n_1_[63] ;
  wire \acc0_0_reg_130_reg_n_1_[6] ;
  wire \acc0_0_reg_130_reg_n_1_[7] ;
  wire \acc0_0_reg_130_reg_n_1_[8] ;
  wire \acc0_0_reg_130_reg_n_1_[9] ;
  wire [63:0]acc0_fu_257_p2;
  wire [63:0]acc1_0_reg_142;
  wire \acc1_0_reg_142[11]_i_2_n_1 ;
  wire \acc1_0_reg_142[11]_i_3_n_1 ;
  wire \acc1_0_reg_142[11]_i_4_n_1 ;
  wire \acc1_0_reg_142[11]_i_5_n_1 ;
  wire \acc1_0_reg_142[15]_i_2_n_1 ;
  wire \acc1_0_reg_142[15]_i_3_n_1 ;
  wire \acc1_0_reg_142[15]_i_4_n_1 ;
  wire \acc1_0_reg_142[15]_i_5_n_1 ;
  wire \acc1_0_reg_142[19]_i_3_n_1 ;
  wire \acc1_0_reg_142[19]_i_4_n_1 ;
  wire \acc1_0_reg_142[19]_i_5_n_1 ;
  wire \acc1_0_reg_142[19]_i_6_n_1 ;
  wire \acc1_0_reg_142[19]_i_7_n_1 ;
  wire \acc1_0_reg_142[19]_i_8_n_1 ;
  wire \acc1_0_reg_142[19]_i_9_n_1 ;
  wire \acc1_0_reg_142[23]_i_10_n_1 ;
  wire \acc1_0_reg_142[23]_i_3_n_1 ;
  wire \acc1_0_reg_142[23]_i_4_n_1 ;
  wire \acc1_0_reg_142[23]_i_5_n_1 ;
  wire \acc1_0_reg_142[23]_i_6_n_1 ;
  wire \acc1_0_reg_142[23]_i_7_n_1 ;
  wire \acc1_0_reg_142[23]_i_8_n_1 ;
  wire \acc1_0_reg_142[23]_i_9_n_1 ;
  wire \acc1_0_reg_142[27]_i_10_n_1 ;
  wire \acc1_0_reg_142[27]_i_3_n_1 ;
  wire \acc1_0_reg_142[27]_i_4_n_1 ;
  wire \acc1_0_reg_142[27]_i_5_n_1 ;
  wire \acc1_0_reg_142[27]_i_6_n_1 ;
  wire \acc1_0_reg_142[27]_i_7_n_1 ;
  wire \acc1_0_reg_142[27]_i_8_n_1 ;
  wire \acc1_0_reg_142[27]_i_9_n_1 ;
  wire \acc1_0_reg_142[31]_i_10_n_1 ;
  wire \acc1_0_reg_142[31]_i_3_n_1 ;
  wire \acc1_0_reg_142[31]_i_4_n_1 ;
  wire \acc1_0_reg_142[31]_i_5_n_1 ;
  wire \acc1_0_reg_142[31]_i_6_n_1 ;
  wire \acc1_0_reg_142[31]_i_7_n_1 ;
  wire \acc1_0_reg_142[31]_i_8_n_1 ;
  wire \acc1_0_reg_142[31]_i_9_n_1 ;
  wire \acc1_0_reg_142[35]_i_10_n_1 ;
  wire \acc1_0_reg_142[35]_i_3_n_1 ;
  wire \acc1_0_reg_142[35]_i_4_n_1 ;
  wire \acc1_0_reg_142[35]_i_5_n_1 ;
  wire \acc1_0_reg_142[35]_i_6_n_1 ;
  wire \acc1_0_reg_142[35]_i_7_n_1 ;
  wire \acc1_0_reg_142[35]_i_8_n_1 ;
  wire \acc1_0_reg_142[35]_i_9_n_1 ;
  wire \acc1_0_reg_142[39]_i_10_n_1 ;
  wire \acc1_0_reg_142[39]_i_3_n_1 ;
  wire \acc1_0_reg_142[39]_i_4_n_1 ;
  wire \acc1_0_reg_142[39]_i_5_n_1 ;
  wire \acc1_0_reg_142[39]_i_6_n_1 ;
  wire \acc1_0_reg_142[39]_i_7_n_1 ;
  wire \acc1_0_reg_142[39]_i_8_n_1 ;
  wire \acc1_0_reg_142[39]_i_9_n_1 ;
  wire \acc1_0_reg_142[3]_i_2_n_1 ;
  wire \acc1_0_reg_142[3]_i_3_n_1 ;
  wire \acc1_0_reg_142[3]_i_4_n_1 ;
  wire \acc1_0_reg_142[3]_i_5_n_1 ;
  wire \acc1_0_reg_142[43]_i_10_n_1 ;
  wire \acc1_0_reg_142[43]_i_3_n_1 ;
  wire \acc1_0_reg_142[43]_i_4_n_1 ;
  wire \acc1_0_reg_142[43]_i_5_n_1 ;
  wire \acc1_0_reg_142[43]_i_6_n_1 ;
  wire \acc1_0_reg_142[43]_i_7_n_1 ;
  wire \acc1_0_reg_142[43]_i_8_n_1 ;
  wire \acc1_0_reg_142[43]_i_9_n_1 ;
  wire \acc1_0_reg_142[47]_i_10_n_1 ;
  wire \acc1_0_reg_142[47]_i_3_n_1 ;
  wire \acc1_0_reg_142[47]_i_4_n_1 ;
  wire \acc1_0_reg_142[47]_i_5_n_1 ;
  wire \acc1_0_reg_142[47]_i_6_n_1 ;
  wire \acc1_0_reg_142[47]_i_7_n_1 ;
  wire \acc1_0_reg_142[47]_i_8_n_1 ;
  wire \acc1_0_reg_142[47]_i_9_n_1 ;
  wire \acc1_0_reg_142[51]_i_10_n_1 ;
  wire \acc1_0_reg_142[51]_i_3_n_1 ;
  wire \acc1_0_reg_142[51]_i_4_n_1 ;
  wire \acc1_0_reg_142[51]_i_5_n_1 ;
  wire \acc1_0_reg_142[51]_i_6_n_1 ;
  wire \acc1_0_reg_142[51]_i_7_n_1 ;
  wire \acc1_0_reg_142[51]_i_8_n_1 ;
  wire \acc1_0_reg_142[51]_i_9_n_1 ;
  wire \acc1_0_reg_142[55]_i_10_n_1 ;
  wire \acc1_0_reg_142[55]_i_3_n_1 ;
  wire \acc1_0_reg_142[55]_i_4_n_1 ;
  wire \acc1_0_reg_142[55]_i_5_n_1 ;
  wire \acc1_0_reg_142[55]_i_6_n_1 ;
  wire \acc1_0_reg_142[55]_i_7_n_1 ;
  wire \acc1_0_reg_142[55]_i_8_n_1 ;
  wire \acc1_0_reg_142[55]_i_9_n_1 ;
  wire \acc1_0_reg_142[59]_i_10_n_1 ;
  wire \acc1_0_reg_142[59]_i_3_n_1 ;
  wire \acc1_0_reg_142[59]_i_4_n_1 ;
  wire \acc1_0_reg_142[59]_i_5_n_1 ;
  wire \acc1_0_reg_142[59]_i_6_n_1 ;
  wire \acc1_0_reg_142[59]_i_7_n_1 ;
  wire \acc1_0_reg_142[59]_i_8_n_1 ;
  wire \acc1_0_reg_142[59]_i_9_n_1 ;
  wire \acc1_0_reg_142[63]_i_10_n_1 ;
  wire \acc1_0_reg_142[63]_i_3_n_1 ;
  wire \acc1_0_reg_142[63]_i_4_n_1 ;
  wire \acc1_0_reg_142[63]_i_5_n_1 ;
  wire \acc1_0_reg_142[63]_i_6_n_1 ;
  wire \acc1_0_reg_142[63]_i_7_n_1 ;
  wire \acc1_0_reg_142[63]_i_8_n_1 ;
  wire \acc1_0_reg_142[63]_i_9_n_1 ;
  wire \acc1_0_reg_142[7]_i_2_n_1 ;
  wire \acc1_0_reg_142[7]_i_3_n_1 ;
  wire \acc1_0_reg_142[7]_i_4_n_1 ;
  wire \acc1_0_reg_142[7]_i_5_n_1 ;
  wire \acc1_0_reg_142_reg[11]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[11]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[11]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[11]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[15]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[15]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[15]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[15]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[19]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[19]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[19]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[19]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[19]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[19]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[19]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[19]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[23]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[23]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[23]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[23]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[23]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[23]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[23]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[23]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[27]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[27]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[27]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[27]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[27]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[27]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[27]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[27]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[31]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[31]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[31]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[31]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[31]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[31]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[31]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[31]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[35]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[35]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[35]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[35]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[35]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[35]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[35]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[35]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[39]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[39]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[39]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[39]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[39]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[39]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[39]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[39]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[3]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[3]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[3]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[3]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[43]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[43]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[43]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[43]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[43]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[43]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[43]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[43]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[47]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[47]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[47]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[47]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[47]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[47]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[47]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[47]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[51]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[51]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[51]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[51]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[51]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[51]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[51]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[51]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[55]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[55]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[55]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[55]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[55]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[55]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[55]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[55]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[59]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[59]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[59]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[59]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[59]_i_2_n_1 ;
  wire \acc1_0_reg_142_reg[59]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[59]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[59]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[63]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[63]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[63]_i_1_n_4 ;
  wire \acc1_0_reg_142_reg[63]_i_2_n_2 ;
  wire \acc1_0_reg_142_reg[63]_i_2_n_3 ;
  wire \acc1_0_reg_142_reg[63]_i_2_n_4 ;
  wire \acc1_0_reg_142_reg[7]_i_1_n_1 ;
  wire \acc1_0_reg_142_reg[7]_i_1_n_2 ;
  wire \acc1_0_reg_142_reg[7]_i_1_n_3 ;
  wire \acc1_0_reg_142_reg[7]_i_1_n_4 ;
  wire [63:0]acc1_fu_273_p2;
  wire ack_out1;
  wire \ap_CS_fsm_reg_n_1_[0] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state5;
  wire ap_CS_fsm_state6;
  wire [5:0]ap_NS_fsm;
  wire ap_NS_fsm11_out;
  wire ap_NS_fsm13_out;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire coeffs_ce0;
  wire [36:0]\fir_coeffs_rom_U/q0_reg ;
  wire [4:0]i_0_reg_154;
  wire [4:0]i_fu_171_p2;
  wire [4:0]i_reg_298;
  wire \ibuf_inst/p_0_in ;
  wire mul_ln73_fu_251_p2__0_n_100;
  wire mul_ln73_fu_251_p2__0_n_101;
  wire mul_ln73_fu_251_p2__0_n_102;
  wire mul_ln73_fu_251_p2__0_n_103;
  wire mul_ln73_fu_251_p2__0_n_104;
  wire mul_ln73_fu_251_p2__0_n_105;
  wire mul_ln73_fu_251_p2__0_n_106;
  wire mul_ln73_fu_251_p2__0_n_59;
  wire mul_ln73_fu_251_p2__0_n_60;
  wire mul_ln73_fu_251_p2__0_n_61;
  wire mul_ln73_fu_251_p2__0_n_62;
  wire mul_ln73_fu_251_p2__0_n_63;
  wire mul_ln73_fu_251_p2__0_n_64;
  wire mul_ln73_fu_251_p2__0_n_65;
  wire mul_ln73_fu_251_p2__0_n_66;
  wire mul_ln73_fu_251_p2__0_n_67;
  wire mul_ln73_fu_251_p2__0_n_68;
  wire mul_ln73_fu_251_p2__0_n_69;
  wire mul_ln73_fu_251_p2__0_n_70;
  wire mul_ln73_fu_251_p2__0_n_71;
  wire mul_ln73_fu_251_p2__0_n_72;
  wire mul_ln73_fu_251_p2__0_n_73;
  wire mul_ln73_fu_251_p2__0_n_74;
  wire mul_ln73_fu_251_p2__0_n_75;
  wire mul_ln73_fu_251_p2__0_n_76;
  wire mul_ln73_fu_251_p2__0_n_77;
  wire mul_ln73_fu_251_p2__0_n_78;
  wire mul_ln73_fu_251_p2__0_n_79;
  wire mul_ln73_fu_251_p2__0_n_80;
  wire mul_ln73_fu_251_p2__0_n_81;
  wire mul_ln73_fu_251_p2__0_n_82;
  wire mul_ln73_fu_251_p2__0_n_83;
  wire mul_ln73_fu_251_p2__0_n_84;
  wire mul_ln73_fu_251_p2__0_n_85;
  wire mul_ln73_fu_251_p2__0_n_86;
  wire mul_ln73_fu_251_p2__0_n_87;
  wire mul_ln73_fu_251_p2__0_n_88;
  wire mul_ln73_fu_251_p2__0_n_89;
  wire mul_ln73_fu_251_p2__0_n_90;
  wire mul_ln73_fu_251_p2__0_n_91;
  wire mul_ln73_fu_251_p2__0_n_92;
  wire mul_ln73_fu_251_p2__0_n_93;
  wire mul_ln73_fu_251_p2__0_n_94;
  wire mul_ln73_fu_251_p2__0_n_95;
  wire mul_ln73_fu_251_p2__0_n_96;
  wire mul_ln73_fu_251_p2__0_n_97;
  wire mul_ln73_fu_251_p2__0_n_98;
  wire mul_ln73_fu_251_p2__0_n_99;
  wire mul_ln73_fu_251_p2__1_n_100;
  wire mul_ln73_fu_251_p2__1_n_101;
  wire mul_ln73_fu_251_p2__1_n_102;
  wire mul_ln73_fu_251_p2__1_n_103;
  wire mul_ln73_fu_251_p2__1_n_104;
  wire mul_ln73_fu_251_p2__1_n_105;
  wire mul_ln73_fu_251_p2__1_n_106;
  wire mul_ln73_fu_251_p2__1_n_107;
  wire mul_ln73_fu_251_p2__1_n_108;
  wire mul_ln73_fu_251_p2__1_n_109;
  wire mul_ln73_fu_251_p2__1_n_110;
  wire mul_ln73_fu_251_p2__1_n_111;
  wire mul_ln73_fu_251_p2__1_n_112;
  wire mul_ln73_fu_251_p2__1_n_113;
  wire mul_ln73_fu_251_p2__1_n_114;
  wire mul_ln73_fu_251_p2__1_n_115;
  wire mul_ln73_fu_251_p2__1_n_116;
  wire mul_ln73_fu_251_p2__1_n_117;
  wire mul_ln73_fu_251_p2__1_n_118;
  wire mul_ln73_fu_251_p2__1_n_119;
  wire mul_ln73_fu_251_p2__1_n_120;
  wire mul_ln73_fu_251_p2__1_n_121;
  wire mul_ln73_fu_251_p2__1_n_122;
  wire mul_ln73_fu_251_p2__1_n_123;
  wire mul_ln73_fu_251_p2__1_n_124;
  wire mul_ln73_fu_251_p2__1_n_125;
  wire mul_ln73_fu_251_p2__1_n_126;
  wire mul_ln73_fu_251_p2__1_n_127;
  wire mul_ln73_fu_251_p2__1_n_128;
  wire mul_ln73_fu_251_p2__1_n_129;
  wire mul_ln73_fu_251_p2__1_n_130;
  wire mul_ln73_fu_251_p2__1_n_131;
  wire mul_ln73_fu_251_p2__1_n_132;
  wire mul_ln73_fu_251_p2__1_n_133;
  wire mul_ln73_fu_251_p2__1_n_134;
  wire mul_ln73_fu_251_p2__1_n_135;
  wire mul_ln73_fu_251_p2__1_n_136;
  wire mul_ln73_fu_251_p2__1_n_137;
  wire mul_ln73_fu_251_p2__1_n_138;
  wire mul_ln73_fu_251_p2__1_n_139;
  wire mul_ln73_fu_251_p2__1_n_140;
  wire mul_ln73_fu_251_p2__1_n_141;
  wire mul_ln73_fu_251_p2__1_n_142;
  wire mul_ln73_fu_251_p2__1_n_143;
  wire mul_ln73_fu_251_p2__1_n_144;
  wire mul_ln73_fu_251_p2__1_n_145;
  wire mul_ln73_fu_251_p2__1_n_146;
  wire mul_ln73_fu_251_p2__1_n_147;
  wire mul_ln73_fu_251_p2__1_n_148;
  wire mul_ln73_fu_251_p2__1_n_149;
  wire mul_ln73_fu_251_p2__1_n_150;
  wire mul_ln73_fu_251_p2__1_n_151;
  wire mul_ln73_fu_251_p2__1_n_152;
  wire mul_ln73_fu_251_p2__1_n_153;
  wire mul_ln73_fu_251_p2__1_n_154;
  wire mul_ln73_fu_251_p2__1_n_59;
  wire mul_ln73_fu_251_p2__1_n_60;
  wire mul_ln73_fu_251_p2__1_n_61;
  wire mul_ln73_fu_251_p2__1_n_62;
  wire mul_ln73_fu_251_p2__1_n_63;
  wire mul_ln73_fu_251_p2__1_n_64;
  wire mul_ln73_fu_251_p2__1_n_65;
  wire mul_ln73_fu_251_p2__1_n_66;
  wire mul_ln73_fu_251_p2__1_n_67;
  wire mul_ln73_fu_251_p2__1_n_68;
  wire mul_ln73_fu_251_p2__1_n_69;
  wire mul_ln73_fu_251_p2__1_n_70;
  wire mul_ln73_fu_251_p2__1_n_71;
  wire mul_ln73_fu_251_p2__1_n_72;
  wire mul_ln73_fu_251_p2__1_n_73;
  wire mul_ln73_fu_251_p2__1_n_74;
  wire mul_ln73_fu_251_p2__1_n_75;
  wire mul_ln73_fu_251_p2__1_n_76;
  wire mul_ln73_fu_251_p2__1_n_77;
  wire mul_ln73_fu_251_p2__1_n_78;
  wire mul_ln73_fu_251_p2__1_n_79;
  wire mul_ln73_fu_251_p2__1_n_80;
  wire mul_ln73_fu_251_p2__1_n_81;
  wire mul_ln73_fu_251_p2__1_n_82;
  wire mul_ln73_fu_251_p2__1_n_83;
  wire mul_ln73_fu_251_p2__1_n_84;
  wire mul_ln73_fu_251_p2__1_n_85;
  wire mul_ln73_fu_251_p2__1_n_86;
  wire mul_ln73_fu_251_p2__1_n_87;
  wire mul_ln73_fu_251_p2__1_n_88;
  wire mul_ln73_fu_251_p2__1_n_89;
  wire mul_ln73_fu_251_p2__1_n_90;
  wire mul_ln73_fu_251_p2__1_n_91;
  wire mul_ln73_fu_251_p2__1_n_92;
  wire mul_ln73_fu_251_p2__1_n_93;
  wire mul_ln73_fu_251_p2__1_n_94;
  wire mul_ln73_fu_251_p2__1_n_95;
  wire mul_ln73_fu_251_p2__1_n_96;
  wire mul_ln73_fu_251_p2__1_n_97;
  wire mul_ln73_fu_251_p2__1_n_98;
  wire mul_ln73_fu_251_p2__1_n_99;
  wire mul_ln73_fu_251_p2__2_n_100;
  wire mul_ln73_fu_251_p2__2_n_101;
  wire mul_ln73_fu_251_p2__2_n_102;
  wire mul_ln73_fu_251_p2__2_n_103;
  wire mul_ln73_fu_251_p2__2_n_104;
  wire mul_ln73_fu_251_p2__2_n_105;
  wire mul_ln73_fu_251_p2__2_n_106;
  wire mul_ln73_fu_251_p2__2_n_59;
  wire mul_ln73_fu_251_p2__2_n_60;
  wire mul_ln73_fu_251_p2__2_n_61;
  wire mul_ln73_fu_251_p2__2_n_62;
  wire mul_ln73_fu_251_p2__2_n_63;
  wire mul_ln73_fu_251_p2__2_n_64;
  wire mul_ln73_fu_251_p2__2_n_65;
  wire mul_ln73_fu_251_p2__2_n_66;
  wire mul_ln73_fu_251_p2__2_n_67;
  wire mul_ln73_fu_251_p2__2_n_68;
  wire mul_ln73_fu_251_p2__2_n_69;
  wire mul_ln73_fu_251_p2__2_n_70;
  wire mul_ln73_fu_251_p2__2_n_71;
  wire mul_ln73_fu_251_p2__2_n_72;
  wire mul_ln73_fu_251_p2__2_n_73;
  wire mul_ln73_fu_251_p2__2_n_74;
  wire mul_ln73_fu_251_p2__2_n_75;
  wire mul_ln73_fu_251_p2__2_n_76;
  wire mul_ln73_fu_251_p2__2_n_77;
  wire mul_ln73_fu_251_p2__2_n_78;
  wire mul_ln73_fu_251_p2__2_n_79;
  wire mul_ln73_fu_251_p2__2_n_80;
  wire mul_ln73_fu_251_p2__2_n_81;
  wire mul_ln73_fu_251_p2__2_n_82;
  wire mul_ln73_fu_251_p2__2_n_83;
  wire mul_ln73_fu_251_p2__2_n_84;
  wire mul_ln73_fu_251_p2__2_n_85;
  wire mul_ln73_fu_251_p2__2_n_86;
  wire mul_ln73_fu_251_p2__2_n_87;
  wire mul_ln73_fu_251_p2__2_n_88;
  wire mul_ln73_fu_251_p2__2_n_89;
  wire mul_ln73_fu_251_p2__2_n_90;
  wire mul_ln73_fu_251_p2__2_n_91;
  wire mul_ln73_fu_251_p2__2_n_92;
  wire mul_ln73_fu_251_p2__2_n_93;
  wire mul_ln73_fu_251_p2__2_n_94;
  wire mul_ln73_fu_251_p2__2_n_95;
  wire mul_ln73_fu_251_p2__2_n_96;
  wire mul_ln73_fu_251_p2__2_n_97;
  wire mul_ln73_fu_251_p2__2_n_98;
  wire mul_ln73_fu_251_p2__2_n_99;
  wire [63:16]mul_ln73_fu_251_p2__3;
  wire mul_ln73_fu_251_p2_n_100;
  wire mul_ln73_fu_251_p2_n_101;
  wire mul_ln73_fu_251_p2_n_102;
  wire mul_ln73_fu_251_p2_n_103;
  wire mul_ln73_fu_251_p2_n_104;
  wire mul_ln73_fu_251_p2_n_105;
  wire mul_ln73_fu_251_p2_n_106;
  wire mul_ln73_fu_251_p2_n_107;
  wire mul_ln73_fu_251_p2_n_108;
  wire mul_ln73_fu_251_p2_n_109;
  wire mul_ln73_fu_251_p2_n_110;
  wire mul_ln73_fu_251_p2_n_111;
  wire mul_ln73_fu_251_p2_n_112;
  wire mul_ln73_fu_251_p2_n_113;
  wire mul_ln73_fu_251_p2_n_114;
  wire mul_ln73_fu_251_p2_n_115;
  wire mul_ln73_fu_251_p2_n_116;
  wire mul_ln73_fu_251_p2_n_117;
  wire mul_ln73_fu_251_p2_n_118;
  wire mul_ln73_fu_251_p2_n_119;
  wire mul_ln73_fu_251_p2_n_120;
  wire mul_ln73_fu_251_p2_n_121;
  wire mul_ln73_fu_251_p2_n_122;
  wire mul_ln73_fu_251_p2_n_123;
  wire mul_ln73_fu_251_p2_n_124;
  wire mul_ln73_fu_251_p2_n_125;
  wire mul_ln73_fu_251_p2_n_126;
  wire mul_ln73_fu_251_p2_n_127;
  wire mul_ln73_fu_251_p2_n_128;
  wire mul_ln73_fu_251_p2_n_129;
  wire mul_ln73_fu_251_p2_n_130;
  wire mul_ln73_fu_251_p2_n_131;
  wire mul_ln73_fu_251_p2_n_132;
  wire mul_ln73_fu_251_p2_n_133;
  wire mul_ln73_fu_251_p2_n_134;
  wire mul_ln73_fu_251_p2_n_135;
  wire mul_ln73_fu_251_p2_n_136;
  wire mul_ln73_fu_251_p2_n_137;
  wire mul_ln73_fu_251_p2_n_138;
  wire mul_ln73_fu_251_p2_n_139;
  wire mul_ln73_fu_251_p2_n_140;
  wire mul_ln73_fu_251_p2_n_141;
  wire mul_ln73_fu_251_p2_n_142;
  wire mul_ln73_fu_251_p2_n_143;
  wire mul_ln73_fu_251_p2_n_144;
  wire mul_ln73_fu_251_p2_n_145;
  wire mul_ln73_fu_251_p2_n_146;
  wire mul_ln73_fu_251_p2_n_147;
  wire mul_ln73_fu_251_p2_n_148;
  wire mul_ln73_fu_251_p2_n_149;
  wire mul_ln73_fu_251_p2_n_150;
  wire mul_ln73_fu_251_p2_n_151;
  wire mul_ln73_fu_251_p2_n_152;
  wire mul_ln73_fu_251_p2_n_153;
  wire mul_ln73_fu_251_p2_n_154;
  wire mul_ln73_fu_251_p2_n_59;
  wire mul_ln73_fu_251_p2_n_60;
  wire mul_ln73_fu_251_p2_n_61;
  wire mul_ln73_fu_251_p2_n_62;
  wire mul_ln73_fu_251_p2_n_63;
  wire mul_ln73_fu_251_p2_n_64;
  wire mul_ln73_fu_251_p2_n_65;
  wire mul_ln73_fu_251_p2_n_66;
  wire mul_ln73_fu_251_p2_n_67;
  wire mul_ln73_fu_251_p2_n_68;
  wire mul_ln73_fu_251_p2_n_69;
  wire mul_ln73_fu_251_p2_n_70;
  wire mul_ln73_fu_251_p2_n_71;
  wire mul_ln73_fu_251_p2_n_72;
  wire mul_ln73_fu_251_p2_n_73;
  wire mul_ln73_fu_251_p2_n_74;
  wire mul_ln73_fu_251_p2_n_75;
  wire mul_ln73_fu_251_p2_n_76;
  wire mul_ln73_fu_251_p2_n_77;
  wire mul_ln73_fu_251_p2_n_78;
  wire mul_ln73_fu_251_p2_n_79;
  wire mul_ln73_fu_251_p2_n_80;
  wire mul_ln73_fu_251_p2_n_81;
  wire mul_ln73_fu_251_p2_n_82;
  wire mul_ln73_fu_251_p2_n_83;
  wire mul_ln73_fu_251_p2_n_84;
  wire mul_ln73_fu_251_p2_n_85;
  wire mul_ln73_fu_251_p2_n_86;
  wire mul_ln73_fu_251_p2_n_87;
  wire mul_ln73_fu_251_p2_n_88;
  wire mul_ln73_fu_251_p2_n_89;
  wire mul_ln73_fu_251_p2_n_90;
  wire mul_ln73_fu_251_p2_n_91;
  wire mul_ln73_fu_251_p2_n_92;
  wire mul_ln73_fu_251_p2_n_93;
  wire mul_ln73_fu_251_p2_n_94;
  wire mul_ln73_fu_251_p2_n_95;
  wire mul_ln73_fu_251_p2_n_96;
  wire mul_ln73_fu_251_p2_n_97;
  wire mul_ln73_fu_251_p2_n_98;
  wire mul_ln73_fu_251_p2_n_99;
  wire mul_ln74_fu_267_p2__0_n_100;
  wire mul_ln74_fu_267_p2__0_n_101;
  wire mul_ln74_fu_267_p2__0_n_102;
  wire mul_ln74_fu_267_p2__0_n_103;
  wire mul_ln74_fu_267_p2__0_n_104;
  wire mul_ln74_fu_267_p2__0_n_105;
  wire mul_ln74_fu_267_p2__0_n_106;
  wire mul_ln74_fu_267_p2__0_n_59;
  wire mul_ln74_fu_267_p2__0_n_60;
  wire mul_ln74_fu_267_p2__0_n_61;
  wire mul_ln74_fu_267_p2__0_n_62;
  wire mul_ln74_fu_267_p2__0_n_63;
  wire mul_ln74_fu_267_p2__0_n_64;
  wire mul_ln74_fu_267_p2__0_n_65;
  wire mul_ln74_fu_267_p2__0_n_66;
  wire mul_ln74_fu_267_p2__0_n_67;
  wire mul_ln74_fu_267_p2__0_n_68;
  wire mul_ln74_fu_267_p2__0_n_69;
  wire mul_ln74_fu_267_p2__0_n_70;
  wire mul_ln74_fu_267_p2__0_n_71;
  wire mul_ln74_fu_267_p2__0_n_72;
  wire mul_ln74_fu_267_p2__0_n_73;
  wire mul_ln74_fu_267_p2__0_n_74;
  wire mul_ln74_fu_267_p2__0_n_75;
  wire mul_ln74_fu_267_p2__0_n_76;
  wire mul_ln74_fu_267_p2__0_n_77;
  wire mul_ln74_fu_267_p2__0_n_78;
  wire mul_ln74_fu_267_p2__0_n_79;
  wire mul_ln74_fu_267_p2__0_n_80;
  wire mul_ln74_fu_267_p2__0_n_81;
  wire mul_ln74_fu_267_p2__0_n_82;
  wire mul_ln74_fu_267_p2__0_n_83;
  wire mul_ln74_fu_267_p2__0_n_84;
  wire mul_ln74_fu_267_p2__0_n_85;
  wire mul_ln74_fu_267_p2__0_n_86;
  wire mul_ln74_fu_267_p2__0_n_87;
  wire mul_ln74_fu_267_p2__0_n_88;
  wire mul_ln74_fu_267_p2__0_n_89;
  wire mul_ln74_fu_267_p2__0_n_90;
  wire mul_ln74_fu_267_p2__0_n_91;
  wire mul_ln74_fu_267_p2__0_n_92;
  wire mul_ln74_fu_267_p2__0_n_93;
  wire mul_ln74_fu_267_p2__0_n_94;
  wire mul_ln74_fu_267_p2__0_n_95;
  wire mul_ln74_fu_267_p2__0_n_96;
  wire mul_ln74_fu_267_p2__0_n_97;
  wire mul_ln74_fu_267_p2__0_n_98;
  wire mul_ln74_fu_267_p2__0_n_99;
  wire mul_ln74_fu_267_p2__1_n_100;
  wire mul_ln74_fu_267_p2__1_n_101;
  wire mul_ln74_fu_267_p2__1_n_102;
  wire mul_ln74_fu_267_p2__1_n_103;
  wire mul_ln74_fu_267_p2__1_n_104;
  wire mul_ln74_fu_267_p2__1_n_105;
  wire mul_ln74_fu_267_p2__1_n_106;
  wire mul_ln74_fu_267_p2__1_n_107;
  wire mul_ln74_fu_267_p2__1_n_108;
  wire mul_ln74_fu_267_p2__1_n_109;
  wire mul_ln74_fu_267_p2__1_n_110;
  wire mul_ln74_fu_267_p2__1_n_111;
  wire mul_ln74_fu_267_p2__1_n_112;
  wire mul_ln74_fu_267_p2__1_n_113;
  wire mul_ln74_fu_267_p2__1_n_114;
  wire mul_ln74_fu_267_p2__1_n_115;
  wire mul_ln74_fu_267_p2__1_n_116;
  wire mul_ln74_fu_267_p2__1_n_117;
  wire mul_ln74_fu_267_p2__1_n_118;
  wire mul_ln74_fu_267_p2__1_n_119;
  wire mul_ln74_fu_267_p2__1_n_120;
  wire mul_ln74_fu_267_p2__1_n_121;
  wire mul_ln74_fu_267_p2__1_n_122;
  wire mul_ln74_fu_267_p2__1_n_123;
  wire mul_ln74_fu_267_p2__1_n_124;
  wire mul_ln74_fu_267_p2__1_n_125;
  wire mul_ln74_fu_267_p2__1_n_126;
  wire mul_ln74_fu_267_p2__1_n_127;
  wire mul_ln74_fu_267_p2__1_n_128;
  wire mul_ln74_fu_267_p2__1_n_129;
  wire mul_ln74_fu_267_p2__1_n_130;
  wire mul_ln74_fu_267_p2__1_n_131;
  wire mul_ln74_fu_267_p2__1_n_132;
  wire mul_ln74_fu_267_p2__1_n_133;
  wire mul_ln74_fu_267_p2__1_n_134;
  wire mul_ln74_fu_267_p2__1_n_135;
  wire mul_ln74_fu_267_p2__1_n_136;
  wire mul_ln74_fu_267_p2__1_n_137;
  wire mul_ln74_fu_267_p2__1_n_138;
  wire mul_ln74_fu_267_p2__1_n_139;
  wire mul_ln74_fu_267_p2__1_n_140;
  wire mul_ln74_fu_267_p2__1_n_141;
  wire mul_ln74_fu_267_p2__1_n_142;
  wire mul_ln74_fu_267_p2__1_n_143;
  wire mul_ln74_fu_267_p2__1_n_144;
  wire mul_ln74_fu_267_p2__1_n_145;
  wire mul_ln74_fu_267_p2__1_n_146;
  wire mul_ln74_fu_267_p2__1_n_147;
  wire mul_ln74_fu_267_p2__1_n_148;
  wire mul_ln74_fu_267_p2__1_n_149;
  wire mul_ln74_fu_267_p2__1_n_150;
  wire mul_ln74_fu_267_p2__1_n_151;
  wire mul_ln74_fu_267_p2__1_n_152;
  wire mul_ln74_fu_267_p2__1_n_153;
  wire mul_ln74_fu_267_p2__1_n_154;
  wire mul_ln74_fu_267_p2__1_n_59;
  wire mul_ln74_fu_267_p2__1_n_60;
  wire mul_ln74_fu_267_p2__1_n_61;
  wire mul_ln74_fu_267_p2__1_n_62;
  wire mul_ln74_fu_267_p2__1_n_63;
  wire mul_ln74_fu_267_p2__1_n_64;
  wire mul_ln74_fu_267_p2__1_n_65;
  wire mul_ln74_fu_267_p2__1_n_66;
  wire mul_ln74_fu_267_p2__1_n_67;
  wire mul_ln74_fu_267_p2__1_n_68;
  wire mul_ln74_fu_267_p2__1_n_69;
  wire mul_ln74_fu_267_p2__1_n_70;
  wire mul_ln74_fu_267_p2__1_n_71;
  wire mul_ln74_fu_267_p2__1_n_72;
  wire mul_ln74_fu_267_p2__1_n_73;
  wire mul_ln74_fu_267_p2__1_n_74;
  wire mul_ln74_fu_267_p2__1_n_75;
  wire mul_ln74_fu_267_p2__1_n_76;
  wire mul_ln74_fu_267_p2__1_n_77;
  wire mul_ln74_fu_267_p2__1_n_78;
  wire mul_ln74_fu_267_p2__1_n_79;
  wire mul_ln74_fu_267_p2__1_n_80;
  wire mul_ln74_fu_267_p2__1_n_81;
  wire mul_ln74_fu_267_p2__1_n_82;
  wire mul_ln74_fu_267_p2__1_n_83;
  wire mul_ln74_fu_267_p2__1_n_84;
  wire mul_ln74_fu_267_p2__1_n_85;
  wire mul_ln74_fu_267_p2__1_n_86;
  wire mul_ln74_fu_267_p2__1_n_87;
  wire mul_ln74_fu_267_p2__1_n_88;
  wire mul_ln74_fu_267_p2__1_n_89;
  wire mul_ln74_fu_267_p2__1_n_90;
  wire mul_ln74_fu_267_p2__1_n_91;
  wire mul_ln74_fu_267_p2__1_n_92;
  wire mul_ln74_fu_267_p2__1_n_93;
  wire mul_ln74_fu_267_p2__1_n_94;
  wire mul_ln74_fu_267_p2__1_n_95;
  wire mul_ln74_fu_267_p2__1_n_96;
  wire mul_ln74_fu_267_p2__1_n_97;
  wire mul_ln74_fu_267_p2__1_n_98;
  wire mul_ln74_fu_267_p2__1_n_99;
  wire mul_ln74_fu_267_p2__2_n_100;
  wire mul_ln74_fu_267_p2__2_n_101;
  wire mul_ln74_fu_267_p2__2_n_102;
  wire mul_ln74_fu_267_p2__2_n_103;
  wire mul_ln74_fu_267_p2__2_n_104;
  wire mul_ln74_fu_267_p2__2_n_105;
  wire mul_ln74_fu_267_p2__2_n_106;
  wire mul_ln74_fu_267_p2__2_n_59;
  wire mul_ln74_fu_267_p2__2_n_60;
  wire mul_ln74_fu_267_p2__2_n_61;
  wire mul_ln74_fu_267_p2__2_n_62;
  wire mul_ln74_fu_267_p2__2_n_63;
  wire mul_ln74_fu_267_p2__2_n_64;
  wire mul_ln74_fu_267_p2__2_n_65;
  wire mul_ln74_fu_267_p2__2_n_66;
  wire mul_ln74_fu_267_p2__2_n_67;
  wire mul_ln74_fu_267_p2__2_n_68;
  wire mul_ln74_fu_267_p2__2_n_69;
  wire mul_ln74_fu_267_p2__2_n_70;
  wire mul_ln74_fu_267_p2__2_n_71;
  wire mul_ln74_fu_267_p2__2_n_72;
  wire mul_ln74_fu_267_p2__2_n_73;
  wire mul_ln74_fu_267_p2__2_n_74;
  wire mul_ln74_fu_267_p2__2_n_75;
  wire mul_ln74_fu_267_p2__2_n_76;
  wire mul_ln74_fu_267_p2__2_n_77;
  wire mul_ln74_fu_267_p2__2_n_78;
  wire mul_ln74_fu_267_p2__2_n_79;
  wire mul_ln74_fu_267_p2__2_n_80;
  wire mul_ln74_fu_267_p2__2_n_81;
  wire mul_ln74_fu_267_p2__2_n_82;
  wire mul_ln74_fu_267_p2__2_n_83;
  wire mul_ln74_fu_267_p2__2_n_84;
  wire mul_ln74_fu_267_p2__2_n_85;
  wire mul_ln74_fu_267_p2__2_n_86;
  wire mul_ln74_fu_267_p2__2_n_87;
  wire mul_ln74_fu_267_p2__2_n_88;
  wire mul_ln74_fu_267_p2__2_n_89;
  wire mul_ln74_fu_267_p2__2_n_90;
  wire mul_ln74_fu_267_p2__2_n_91;
  wire mul_ln74_fu_267_p2__2_n_92;
  wire mul_ln74_fu_267_p2__2_n_93;
  wire mul_ln74_fu_267_p2__2_n_94;
  wire mul_ln74_fu_267_p2__2_n_95;
  wire mul_ln74_fu_267_p2__2_n_96;
  wire mul_ln74_fu_267_p2__2_n_97;
  wire mul_ln74_fu_267_p2__2_n_98;
  wire mul_ln74_fu_267_p2__2_n_99;
  wire [63:16]mul_ln74_fu_267_p2__3;
  wire mul_ln74_fu_267_p2_n_100;
  wire mul_ln74_fu_267_p2_n_101;
  wire mul_ln74_fu_267_p2_n_102;
  wire mul_ln74_fu_267_p2_n_103;
  wire mul_ln74_fu_267_p2_n_104;
  wire mul_ln74_fu_267_p2_n_105;
  wire mul_ln74_fu_267_p2_n_106;
  wire mul_ln74_fu_267_p2_n_107;
  wire mul_ln74_fu_267_p2_n_108;
  wire mul_ln74_fu_267_p2_n_109;
  wire mul_ln74_fu_267_p2_n_110;
  wire mul_ln74_fu_267_p2_n_111;
  wire mul_ln74_fu_267_p2_n_112;
  wire mul_ln74_fu_267_p2_n_113;
  wire mul_ln74_fu_267_p2_n_114;
  wire mul_ln74_fu_267_p2_n_115;
  wire mul_ln74_fu_267_p2_n_116;
  wire mul_ln74_fu_267_p2_n_117;
  wire mul_ln74_fu_267_p2_n_118;
  wire mul_ln74_fu_267_p2_n_119;
  wire mul_ln74_fu_267_p2_n_120;
  wire mul_ln74_fu_267_p2_n_121;
  wire mul_ln74_fu_267_p2_n_122;
  wire mul_ln74_fu_267_p2_n_123;
  wire mul_ln74_fu_267_p2_n_124;
  wire mul_ln74_fu_267_p2_n_125;
  wire mul_ln74_fu_267_p2_n_126;
  wire mul_ln74_fu_267_p2_n_127;
  wire mul_ln74_fu_267_p2_n_128;
  wire mul_ln74_fu_267_p2_n_129;
  wire mul_ln74_fu_267_p2_n_130;
  wire mul_ln74_fu_267_p2_n_131;
  wire mul_ln74_fu_267_p2_n_132;
  wire mul_ln74_fu_267_p2_n_133;
  wire mul_ln74_fu_267_p2_n_134;
  wire mul_ln74_fu_267_p2_n_135;
  wire mul_ln74_fu_267_p2_n_136;
  wire mul_ln74_fu_267_p2_n_137;
  wire mul_ln74_fu_267_p2_n_138;
  wire mul_ln74_fu_267_p2_n_139;
  wire mul_ln74_fu_267_p2_n_140;
  wire mul_ln74_fu_267_p2_n_141;
  wire mul_ln74_fu_267_p2_n_142;
  wire mul_ln74_fu_267_p2_n_143;
  wire mul_ln74_fu_267_p2_n_144;
  wire mul_ln74_fu_267_p2_n_145;
  wire mul_ln74_fu_267_p2_n_146;
  wire mul_ln74_fu_267_p2_n_147;
  wire mul_ln74_fu_267_p2_n_148;
  wire mul_ln74_fu_267_p2_n_149;
  wire mul_ln74_fu_267_p2_n_150;
  wire mul_ln74_fu_267_p2_n_151;
  wire mul_ln74_fu_267_p2_n_152;
  wire mul_ln74_fu_267_p2_n_153;
  wire mul_ln74_fu_267_p2_n_154;
  wire mul_ln74_fu_267_p2_n_59;
  wire mul_ln74_fu_267_p2_n_60;
  wire mul_ln74_fu_267_p2_n_61;
  wire mul_ln74_fu_267_p2_n_62;
  wire mul_ln74_fu_267_p2_n_63;
  wire mul_ln74_fu_267_p2_n_64;
  wire mul_ln74_fu_267_p2_n_65;
  wire mul_ln74_fu_267_p2_n_66;
  wire mul_ln74_fu_267_p2_n_67;
  wire mul_ln74_fu_267_p2_n_68;
  wire mul_ln74_fu_267_p2_n_69;
  wire mul_ln74_fu_267_p2_n_70;
  wire mul_ln74_fu_267_p2_n_71;
  wire mul_ln74_fu_267_p2_n_72;
  wire mul_ln74_fu_267_p2_n_73;
  wire mul_ln74_fu_267_p2_n_74;
  wire mul_ln74_fu_267_p2_n_75;
  wire mul_ln74_fu_267_p2_n_76;
  wire mul_ln74_fu_267_p2_n_77;
  wire mul_ln74_fu_267_p2_n_78;
  wire mul_ln74_fu_267_p2_n_79;
  wire mul_ln74_fu_267_p2_n_80;
  wire mul_ln74_fu_267_p2_n_81;
  wire mul_ln74_fu_267_p2_n_82;
  wire mul_ln74_fu_267_p2_n_83;
  wire mul_ln74_fu_267_p2_n_84;
  wire mul_ln74_fu_267_p2_n_85;
  wire mul_ln74_fu_267_p2_n_86;
  wire mul_ln74_fu_267_p2_n_87;
  wire mul_ln74_fu_267_p2_n_88;
  wire mul_ln74_fu_267_p2_n_89;
  wire mul_ln74_fu_267_p2_n_90;
  wire mul_ln74_fu_267_p2_n_91;
  wire mul_ln74_fu_267_p2_n_92;
  wire mul_ln74_fu_267_p2_n_93;
  wire mul_ln74_fu_267_p2_n_94;
  wire mul_ln74_fu_267_p2_n_95;
  wire mul_ln74_fu_267_p2_n_96;
  wire mul_ln74_fu_267_p2_n_97;
  wire mul_ln74_fu_267_p2_n_98;
  wire mul_ln74_fu_267_p2_n_99;
  wire mul_ln80_fu_191_p2__0__0_n_1;
  wire mul_ln80_fu_191_p2__0_n_100;
  wire mul_ln80_fu_191_p2__0_n_101;
  wire mul_ln80_fu_191_p2__0_n_102;
  wire mul_ln80_fu_191_p2__0_n_103;
  wire mul_ln80_fu_191_p2__0_n_104;
  wire mul_ln80_fu_191_p2__0_n_105;
  wire mul_ln80_fu_191_p2__0_n_106;
  wire mul_ln80_fu_191_p2__0_n_59;
  wire mul_ln80_fu_191_p2__0_n_60;
  wire mul_ln80_fu_191_p2__0_n_61;
  wire mul_ln80_fu_191_p2__0_n_62;
  wire mul_ln80_fu_191_p2__0_n_63;
  wire mul_ln80_fu_191_p2__0_n_64;
  wire mul_ln80_fu_191_p2__0_n_65;
  wire mul_ln80_fu_191_p2__0_n_66;
  wire mul_ln80_fu_191_p2__0_n_67;
  wire mul_ln80_fu_191_p2__0_n_68;
  wire mul_ln80_fu_191_p2__0_n_69;
  wire mul_ln80_fu_191_p2__0_n_70;
  wire mul_ln80_fu_191_p2__0_n_71;
  wire mul_ln80_fu_191_p2__0_n_72;
  wire mul_ln80_fu_191_p2__0_n_73;
  wire mul_ln80_fu_191_p2__0_n_74;
  wire mul_ln80_fu_191_p2__0_n_75;
  wire mul_ln80_fu_191_p2__0_n_76;
  wire mul_ln80_fu_191_p2__0_n_77;
  wire mul_ln80_fu_191_p2__0_n_78;
  wire mul_ln80_fu_191_p2__0_n_79;
  wire mul_ln80_fu_191_p2__0_n_80;
  wire mul_ln80_fu_191_p2__0_n_81;
  wire mul_ln80_fu_191_p2__0_n_82;
  wire mul_ln80_fu_191_p2__0_n_83;
  wire mul_ln80_fu_191_p2__0_n_84;
  wire mul_ln80_fu_191_p2__0_n_85;
  wire mul_ln80_fu_191_p2__0_n_86;
  wire mul_ln80_fu_191_p2__0_n_87;
  wire mul_ln80_fu_191_p2__0_n_88;
  wire mul_ln80_fu_191_p2__0_n_89;
  wire mul_ln80_fu_191_p2__0_n_90;
  wire mul_ln80_fu_191_p2__0_n_91;
  wire mul_ln80_fu_191_p2__0_n_92;
  wire mul_ln80_fu_191_p2__0_n_93;
  wire mul_ln80_fu_191_p2__0_n_94;
  wire mul_ln80_fu_191_p2__0_n_95;
  wire mul_ln80_fu_191_p2__0_n_96;
  wire mul_ln80_fu_191_p2__0_n_97;
  wire mul_ln80_fu_191_p2__0_n_98;
  wire mul_ln80_fu_191_p2__0_n_99;
  wire mul_ln80_fu_191_p2__10_n_1;
  wire mul_ln80_fu_191_p2__11_n_1;
  wire mul_ln80_fu_191_p2__12_n_1;
  wire mul_ln80_fu_191_p2__13_n_1;
  wire mul_ln80_fu_191_p2__14_n_1;
  wire mul_ln80_fu_191_p2__15_n_1;
  wire mul_ln80_fu_191_p2__16_n_1;
  wire mul_ln80_fu_191_p2__1__0_n_1;
  wire mul_ln80_fu_191_p2__1_n_100;
  wire mul_ln80_fu_191_p2__1_n_101;
  wire mul_ln80_fu_191_p2__1_n_102;
  wire mul_ln80_fu_191_p2__1_n_103;
  wire mul_ln80_fu_191_p2__1_n_104;
  wire mul_ln80_fu_191_p2__1_n_105;
  wire mul_ln80_fu_191_p2__1_n_106;
  wire mul_ln80_fu_191_p2__1_n_107;
  wire mul_ln80_fu_191_p2__1_n_108;
  wire mul_ln80_fu_191_p2__1_n_109;
  wire mul_ln80_fu_191_p2__1_n_110;
  wire mul_ln80_fu_191_p2__1_n_111;
  wire mul_ln80_fu_191_p2__1_n_112;
  wire mul_ln80_fu_191_p2__1_n_113;
  wire mul_ln80_fu_191_p2__1_n_114;
  wire mul_ln80_fu_191_p2__1_n_115;
  wire mul_ln80_fu_191_p2__1_n_116;
  wire mul_ln80_fu_191_p2__1_n_117;
  wire mul_ln80_fu_191_p2__1_n_118;
  wire mul_ln80_fu_191_p2__1_n_119;
  wire mul_ln80_fu_191_p2__1_n_120;
  wire mul_ln80_fu_191_p2__1_n_121;
  wire mul_ln80_fu_191_p2__1_n_122;
  wire mul_ln80_fu_191_p2__1_n_123;
  wire mul_ln80_fu_191_p2__1_n_124;
  wire mul_ln80_fu_191_p2__1_n_125;
  wire mul_ln80_fu_191_p2__1_n_126;
  wire mul_ln80_fu_191_p2__1_n_127;
  wire mul_ln80_fu_191_p2__1_n_128;
  wire mul_ln80_fu_191_p2__1_n_129;
  wire mul_ln80_fu_191_p2__1_n_130;
  wire mul_ln80_fu_191_p2__1_n_131;
  wire mul_ln80_fu_191_p2__1_n_132;
  wire mul_ln80_fu_191_p2__1_n_133;
  wire mul_ln80_fu_191_p2__1_n_134;
  wire mul_ln80_fu_191_p2__1_n_135;
  wire mul_ln80_fu_191_p2__1_n_136;
  wire mul_ln80_fu_191_p2__1_n_137;
  wire mul_ln80_fu_191_p2__1_n_138;
  wire mul_ln80_fu_191_p2__1_n_139;
  wire mul_ln80_fu_191_p2__1_n_140;
  wire mul_ln80_fu_191_p2__1_n_141;
  wire mul_ln80_fu_191_p2__1_n_142;
  wire mul_ln80_fu_191_p2__1_n_143;
  wire mul_ln80_fu_191_p2__1_n_144;
  wire mul_ln80_fu_191_p2__1_n_145;
  wire mul_ln80_fu_191_p2__1_n_146;
  wire mul_ln80_fu_191_p2__1_n_147;
  wire mul_ln80_fu_191_p2__1_n_148;
  wire mul_ln80_fu_191_p2__1_n_149;
  wire mul_ln80_fu_191_p2__1_n_150;
  wire mul_ln80_fu_191_p2__1_n_151;
  wire mul_ln80_fu_191_p2__1_n_152;
  wire mul_ln80_fu_191_p2__1_n_153;
  wire mul_ln80_fu_191_p2__1_n_154;
  wire mul_ln80_fu_191_p2__1_n_59;
  wire mul_ln80_fu_191_p2__1_n_60;
  wire mul_ln80_fu_191_p2__1_n_61;
  wire mul_ln80_fu_191_p2__1_n_62;
  wire mul_ln80_fu_191_p2__1_n_63;
  wire mul_ln80_fu_191_p2__1_n_64;
  wire mul_ln80_fu_191_p2__1_n_65;
  wire mul_ln80_fu_191_p2__1_n_66;
  wire mul_ln80_fu_191_p2__1_n_67;
  wire mul_ln80_fu_191_p2__1_n_68;
  wire mul_ln80_fu_191_p2__1_n_69;
  wire mul_ln80_fu_191_p2__1_n_70;
  wire mul_ln80_fu_191_p2__1_n_71;
  wire mul_ln80_fu_191_p2__1_n_72;
  wire mul_ln80_fu_191_p2__1_n_73;
  wire mul_ln80_fu_191_p2__1_n_74;
  wire mul_ln80_fu_191_p2__1_n_75;
  wire mul_ln80_fu_191_p2__1_n_76;
  wire mul_ln80_fu_191_p2__1_n_77;
  wire mul_ln80_fu_191_p2__1_n_78;
  wire mul_ln80_fu_191_p2__1_n_79;
  wire mul_ln80_fu_191_p2__1_n_80;
  wire mul_ln80_fu_191_p2__1_n_81;
  wire mul_ln80_fu_191_p2__1_n_82;
  wire mul_ln80_fu_191_p2__1_n_83;
  wire mul_ln80_fu_191_p2__1_n_84;
  wire mul_ln80_fu_191_p2__1_n_85;
  wire mul_ln80_fu_191_p2__1_n_86;
  wire mul_ln80_fu_191_p2__1_n_87;
  wire mul_ln80_fu_191_p2__1_n_88;
  wire mul_ln80_fu_191_p2__1_n_89;
  wire mul_ln80_fu_191_p2__1_n_90;
  wire mul_ln80_fu_191_p2__1_n_91;
  wire mul_ln80_fu_191_p2__1_n_92;
  wire mul_ln80_fu_191_p2__1_n_93;
  wire mul_ln80_fu_191_p2__1_n_94;
  wire mul_ln80_fu_191_p2__1_n_95;
  wire mul_ln80_fu_191_p2__1_n_96;
  wire mul_ln80_fu_191_p2__1_n_97;
  wire mul_ln80_fu_191_p2__1_n_98;
  wire mul_ln80_fu_191_p2__1_n_99;
  wire [63:16]mul_ln80_fu_191_p2__21;
  wire mul_ln80_fu_191_p2__2__0_n_1;
  wire mul_ln80_fu_191_p2__2_n_100;
  wire mul_ln80_fu_191_p2__2_n_101;
  wire mul_ln80_fu_191_p2__2_n_102;
  wire mul_ln80_fu_191_p2__2_n_103;
  wire mul_ln80_fu_191_p2__2_n_104;
  wire mul_ln80_fu_191_p2__2_n_105;
  wire mul_ln80_fu_191_p2__2_n_106;
  wire mul_ln80_fu_191_p2__2_n_59;
  wire mul_ln80_fu_191_p2__2_n_60;
  wire mul_ln80_fu_191_p2__2_n_61;
  wire mul_ln80_fu_191_p2__2_n_62;
  wire mul_ln80_fu_191_p2__2_n_63;
  wire mul_ln80_fu_191_p2__2_n_64;
  wire mul_ln80_fu_191_p2__2_n_65;
  wire mul_ln80_fu_191_p2__2_n_66;
  wire mul_ln80_fu_191_p2__2_n_67;
  wire mul_ln80_fu_191_p2__2_n_68;
  wire mul_ln80_fu_191_p2__2_n_69;
  wire mul_ln80_fu_191_p2__2_n_70;
  wire mul_ln80_fu_191_p2__2_n_71;
  wire mul_ln80_fu_191_p2__2_n_72;
  wire mul_ln80_fu_191_p2__2_n_73;
  wire mul_ln80_fu_191_p2__2_n_74;
  wire mul_ln80_fu_191_p2__2_n_75;
  wire mul_ln80_fu_191_p2__2_n_76;
  wire mul_ln80_fu_191_p2__2_n_77;
  wire mul_ln80_fu_191_p2__2_n_78;
  wire mul_ln80_fu_191_p2__2_n_79;
  wire mul_ln80_fu_191_p2__2_n_80;
  wire mul_ln80_fu_191_p2__2_n_81;
  wire mul_ln80_fu_191_p2__2_n_82;
  wire mul_ln80_fu_191_p2__2_n_83;
  wire mul_ln80_fu_191_p2__2_n_84;
  wire mul_ln80_fu_191_p2__2_n_85;
  wire mul_ln80_fu_191_p2__2_n_86;
  wire mul_ln80_fu_191_p2__2_n_87;
  wire mul_ln80_fu_191_p2__2_n_88;
  wire mul_ln80_fu_191_p2__2_n_89;
  wire mul_ln80_fu_191_p2__2_n_90;
  wire mul_ln80_fu_191_p2__2_n_91;
  wire mul_ln80_fu_191_p2__2_n_92;
  wire mul_ln80_fu_191_p2__2_n_93;
  wire mul_ln80_fu_191_p2__2_n_94;
  wire mul_ln80_fu_191_p2__2_n_95;
  wire mul_ln80_fu_191_p2__2_n_96;
  wire mul_ln80_fu_191_p2__2_n_97;
  wire mul_ln80_fu_191_p2__2_n_98;
  wire mul_ln80_fu_191_p2__2_n_99;
  wire mul_ln80_fu_191_p2__3_n_1;
  wire mul_ln80_fu_191_p2__4_n_1;
  wire mul_ln80_fu_191_p2__5_n_1;
  wire mul_ln80_fu_191_p2__6_n_1;
  wire mul_ln80_fu_191_p2__7_n_1;
  wire mul_ln80_fu_191_p2__8_n_1;
  wire mul_ln80_fu_191_p2__9_n_1;
  wire mul_ln80_fu_191_p2_n_100;
  wire mul_ln80_fu_191_p2_n_101;
  wire mul_ln80_fu_191_p2_n_102;
  wire mul_ln80_fu_191_p2_n_103;
  wire mul_ln80_fu_191_p2_n_104;
  wire mul_ln80_fu_191_p2_n_105;
  wire mul_ln80_fu_191_p2_n_106;
  wire mul_ln80_fu_191_p2_n_107;
  wire mul_ln80_fu_191_p2_n_108;
  wire mul_ln80_fu_191_p2_n_109;
  wire mul_ln80_fu_191_p2_n_110;
  wire mul_ln80_fu_191_p2_n_111;
  wire mul_ln80_fu_191_p2_n_112;
  wire mul_ln80_fu_191_p2_n_113;
  wire mul_ln80_fu_191_p2_n_114;
  wire mul_ln80_fu_191_p2_n_115;
  wire mul_ln80_fu_191_p2_n_116;
  wire mul_ln80_fu_191_p2_n_117;
  wire mul_ln80_fu_191_p2_n_118;
  wire mul_ln80_fu_191_p2_n_119;
  wire mul_ln80_fu_191_p2_n_120;
  wire mul_ln80_fu_191_p2_n_121;
  wire mul_ln80_fu_191_p2_n_122;
  wire mul_ln80_fu_191_p2_n_123;
  wire mul_ln80_fu_191_p2_n_124;
  wire mul_ln80_fu_191_p2_n_125;
  wire mul_ln80_fu_191_p2_n_126;
  wire mul_ln80_fu_191_p2_n_127;
  wire mul_ln80_fu_191_p2_n_128;
  wire mul_ln80_fu_191_p2_n_129;
  wire mul_ln80_fu_191_p2_n_130;
  wire mul_ln80_fu_191_p2_n_131;
  wire mul_ln80_fu_191_p2_n_132;
  wire mul_ln80_fu_191_p2_n_133;
  wire mul_ln80_fu_191_p2_n_134;
  wire mul_ln80_fu_191_p2_n_135;
  wire mul_ln80_fu_191_p2_n_136;
  wire mul_ln80_fu_191_p2_n_137;
  wire mul_ln80_fu_191_p2_n_138;
  wire mul_ln80_fu_191_p2_n_139;
  wire mul_ln80_fu_191_p2_n_140;
  wire mul_ln80_fu_191_p2_n_141;
  wire mul_ln80_fu_191_p2_n_142;
  wire mul_ln80_fu_191_p2_n_143;
  wire mul_ln80_fu_191_p2_n_144;
  wire mul_ln80_fu_191_p2_n_145;
  wire mul_ln80_fu_191_p2_n_146;
  wire mul_ln80_fu_191_p2_n_147;
  wire mul_ln80_fu_191_p2_n_148;
  wire mul_ln80_fu_191_p2_n_149;
  wire mul_ln80_fu_191_p2_n_150;
  wire mul_ln80_fu_191_p2_n_151;
  wire mul_ln80_fu_191_p2_n_152;
  wire mul_ln80_fu_191_p2_n_153;
  wire mul_ln80_fu_191_p2_n_154;
  wire mul_ln80_fu_191_p2_n_59;
  wire mul_ln80_fu_191_p2_n_60;
  wire mul_ln80_fu_191_p2_n_61;
  wire mul_ln80_fu_191_p2_n_62;
  wire mul_ln80_fu_191_p2_n_63;
  wire mul_ln80_fu_191_p2_n_64;
  wire mul_ln80_fu_191_p2_n_65;
  wire mul_ln80_fu_191_p2_n_66;
  wire mul_ln80_fu_191_p2_n_67;
  wire mul_ln80_fu_191_p2_n_68;
  wire mul_ln80_fu_191_p2_n_69;
  wire mul_ln80_fu_191_p2_n_70;
  wire mul_ln80_fu_191_p2_n_71;
  wire mul_ln80_fu_191_p2_n_72;
  wire mul_ln80_fu_191_p2_n_73;
  wire mul_ln80_fu_191_p2_n_74;
  wire mul_ln80_fu_191_p2_n_75;
  wire mul_ln80_fu_191_p2_n_76;
  wire mul_ln80_fu_191_p2_n_77;
  wire mul_ln80_fu_191_p2_n_78;
  wire mul_ln80_fu_191_p2_n_79;
  wire mul_ln80_fu_191_p2_n_80;
  wire mul_ln80_fu_191_p2_n_81;
  wire mul_ln80_fu_191_p2_n_82;
  wire mul_ln80_fu_191_p2_n_83;
  wire mul_ln80_fu_191_p2_n_84;
  wire mul_ln80_fu_191_p2_n_85;
  wire mul_ln80_fu_191_p2_n_86;
  wire mul_ln80_fu_191_p2_n_87;
  wire mul_ln80_fu_191_p2_n_88;
  wire mul_ln80_fu_191_p2_n_89;
  wire mul_ln80_fu_191_p2_n_90;
  wire mul_ln80_fu_191_p2_n_91;
  wire mul_ln80_fu_191_p2_n_92;
  wire mul_ln80_fu_191_p2_n_93;
  wire mul_ln80_fu_191_p2_n_94;
  wire mul_ln80_fu_191_p2_n_95;
  wire mul_ln80_fu_191_p2_n_96;
  wire mul_ln80_fu_191_p2_n_97;
  wire mul_ln80_fu_191_p2_n_98;
  wire mul_ln80_fu_191_p2_n_99;
  wire mul_ln81_fu_206_p2__0_n_100;
  wire mul_ln81_fu_206_p2__0_n_101;
  wire mul_ln81_fu_206_p2__0_n_102;
  wire mul_ln81_fu_206_p2__0_n_103;
  wire mul_ln81_fu_206_p2__0_n_104;
  wire mul_ln81_fu_206_p2__0_n_105;
  wire mul_ln81_fu_206_p2__0_n_106;
  wire mul_ln81_fu_206_p2__0_n_59;
  wire mul_ln81_fu_206_p2__0_n_60;
  wire mul_ln81_fu_206_p2__0_n_61;
  wire mul_ln81_fu_206_p2__0_n_62;
  wire mul_ln81_fu_206_p2__0_n_63;
  wire mul_ln81_fu_206_p2__0_n_64;
  wire mul_ln81_fu_206_p2__0_n_65;
  wire mul_ln81_fu_206_p2__0_n_66;
  wire mul_ln81_fu_206_p2__0_n_67;
  wire mul_ln81_fu_206_p2__0_n_68;
  wire mul_ln81_fu_206_p2__0_n_69;
  wire mul_ln81_fu_206_p2__0_n_70;
  wire mul_ln81_fu_206_p2__0_n_71;
  wire mul_ln81_fu_206_p2__0_n_72;
  wire mul_ln81_fu_206_p2__0_n_73;
  wire mul_ln81_fu_206_p2__0_n_74;
  wire mul_ln81_fu_206_p2__0_n_75;
  wire mul_ln81_fu_206_p2__0_n_76;
  wire mul_ln81_fu_206_p2__0_n_77;
  wire mul_ln81_fu_206_p2__0_n_78;
  wire mul_ln81_fu_206_p2__0_n_79;
  wire mul_ln81_fu_206_p2__0_n_80;
  wire mul_ln81_fu_206_p2__0_n_81;
  wire mul_ln81_fu_206_p2__0_n_82;
  wire mul_ln81_fu_206_p2__0_n_83;
  wire mul_ln81_fu_206_p2__0_n_84;
  wire mul_ln81_fu_206_p2__0_n_85;
  wire mul_ln81_fu_206_p2__0_n_86;
  wire mul_ln81_fu_206_p2__0_n_87;
  wire mul_ln81_fu_206_p2__0_n_88;
  wire mul_ln81_fu_206_p2__0_n_89;
  wire mul_ln81_fu_206_p2__0_n_90;
  wire mul_ln81_fu_206_p2__0_n_91;
  wire mul_ln81_fu_206_p2__0_n_92;
  wire mul_ln81_fu_206_p2__0_n_93;
  wire mul_ln81_fu_206_p2__0_n_94;
  wire mul_ln81_fu_206_p2__0_n_95;
  wire mul_ln81_fu_206_p2__0_n_96;
  wire mul_ln81_fu_206_p2__0_n_97;
  wire mul_ln81_fu_206_p2__0_n_98;
  wire mul_ln81_fu_206_p2__0_n_99;
  wire mul_ln81_fu_206_p2__1_n_100;
  wire mul_ln81_fu_206_p2__1_n_101;
  wire mul_ln81_fu_206_p2__1_n_102;
  wire mul_ln81_fu_206_p2__1_n_103;
  wire mul_ln81_fu_206_p2__1_n_104;
  wire mul_ln81_fu_206_p2__1_n_105;
  wire mul_ln81_fu_206_p2__1_n_106;
  wire mul_ln81_fu_206_p2__1_n_107;
  wire mul_ln81_fu_206_p2__1_n_108;
  wire mul_ln81_fu_206_p2__1_n_109;
  wire mul_ln81_fu_206_p2__1_n_110;
  wire mul_ln81_fu_206_p2__1_n_111;
  wire mul_ln81_fu_206_p2__1_n_112;
  wire mul_ln81_fu_206_p2__1_n_113;
  wire mul_ln81_fu_206_p2__1_n_114;
  wire mul_ln81_fu_206_p2__1_n_115;
  wire mul_ln81_fu_206_p2__1_n_116;
  wire mul_ln81_fu_206_p2__1_n_117;
  wire mul_ln81_fu_206_p2__1_n_118;
  wire mul_ln81_fu_206_p2__1_n_119;
  wire mul_ln81_fu_206_p2__1_n_120;
  wire mul_ln81_fu_206_p2__1_n_121;
  wire mul_ln81_fu_206_p2__1_n_122;
  wire mul_ln81_fu_206_p2__1_n_123;
  wire mul_ln81_fu_206_p2__1_n_124;
  wire mul_ln81_fu_206_p2__1_n_125;
  wire mul_ln81_fu_206_p2__1_n_126;
  wire mul_ln81_fu_206_p2__1_n_127;
  wire mul_ln81_fu_206_p2__1_n_128;
  wire mul_ln81_fu_206_p2__1_n_129;
  wire mul_ln81_fu_206_p2__1_n_130;
  wire mul_ln81_fu_206_p2__1_n_131;
  wire mul_ln81_fu_206_p2__1_n_132;
  wire mul_ln81_fu_206_p2__1_n_133;
  wire mul_ln81_fu_206_p2__1_n_134;
  wire mul_ln81_fu_206_p2__1_n_135;
  wire mul_ln81_fu_206_p2__1_n_136;
  wire mul_ln81_fu_206_p2__1_n_137;
  wire mul_ln81_fu_206_p2__1_n_138;
  wire mul_ln81_fu_206_p2__1_n_139;
  wire mul_ln81_fu_206_p2__1_n_140;
  wire mul_ln81_fu_206_p2__1_n_141;
  wire mul_ln81_fu_206_p2__1_n_142;
  wire mul_ln81_fu_206_p2__1_n_143;
  wire mul_ln81_fu_206_p2__1_n_144;
  wire mul_ln81_fu_206_p2__1_n_145;
  wire mul_ln81_fu_206_p2__1_n_146;
  wire mul_ln81_fu_206_p2__1_n_147;
  wire mul_ln81_fu_206_p2__1_n_148;
  wire mul_ln81_fu_206_p2__1_n_149;
  wire mul_ln81_fu_206_p2__1_n_150;
  wire mul_ln81_fu_206_p2__1_n_151;
  wire mul_ln81_fu_206_p2__1_n_152;
  wire mul_ln81_fu_206_p2__1_n_153;
  wire mul_ln81_fu_206_p2__1_n_154;
  wire mul_ln81_fu_206_p2__1_n_59;
  wire mul_ln81_fu_206_p2__1_n_60;
  wire mul_ln81_fu_206_p2__1_n_61;
  wire mul_ln81_fu_206_p2__1_n_62;
  wire mul_ln81_fu_206_p2__1_n_63;
  wire mul_ln81_fu_206_p2__1_n_64;
  wire mul_ln81_fu_206_p2__1_n_65;
  wire mul_ln81_fu_206_p2__1_n_66;
  wire mul_ln81_fu_206_p2__1_n_67;
  wire mul_ln81_fu_206_p2__1_n_68;
  wire mul_ln81_fu_206_p2__1_n_69;
  wire mul_ln81_fu_206_p2__1_n_70;
  wire mul_ln81_fu_206_p2__1_n_71;
  wire mul_ln81_fu_206_p2__1_n_72;
  wire mul_ln81_fu_206_p2__1_n_73;
  wire mul_ln81_fu_206_p2__1_n_74;
  wire mul_ln81_fu_206_p2__1_n_75;
  wire mul_ln81_fu_206_p2__1_n_76;
  wire mul_ln81_fu_206_p2__1_n_77;
  wire mul_ln81_fu_206_p2__1_n_78;
  wire mul_ln81_fu_206_p2__1_n_79;
  wire mul_ln81_fu_206_p2__1_n_80;
  wire mul_ln81_fu_206_p2__1_n_81;
  wire mul_ln81_fu_206_p2__1_n_82;
  wire mul_ln81_fu_206_p2__1_n_83;
  wire mul_ln81_fu_206_p2__1_n_84;
  wire mul_ln81_fu_206_p2__1_n_85;
  wire mul_ln81_fu_206_p2__1_n_86;
  wire mul_ln81_fu_206_p2__1_n_87;
  wire mul_ln81_fu_206_p2__1_n_88;
  wire mul_ln81_fu_206_p2__1_n_89;
  wire mul_ln81_fu_206_p2__1_n_90;
  wire mul_ln81_fu_206_p2__1_n_91;
  wire mul_ln81_fu_206_p2__1_n_92;
  wire mul_ln81_fu_206_p2__1_n_93;
  wire mul_ln81_fu_206_p2__1_n_94;
  wire mul_ln81_fu_206_p2__1_n_95;
  wire mul_ln81_fu_206_p2__1_n_96;
  wire mul_ln81_fu_206_p2__1_n_97;
  wire mul_ln81_fu_206_p2__1_n_98;
  wire mul_ln81_fu_206_p2__1_n_99;
  wire mul_ln81_fu_206_p2__2_n_100;
  wire mul_ln81_fu_206_p2__2_n_101;
  wire mul_ln81_fu_206_p2__2_n_102;
  wire mul_ln81_fu_206_p2__2_n_103;
  wire mul_ln81_fu_206_p2__2_n_104;
  wire mul_ln81_fu_206_p2__2_n_105;
  wire mul_ln81_fu_206_p2__2_n_106;
  wire mul_ln81_fu_206_p2__2_n_59;
  wire mul_ln81_fu_206_p2__2_n_60;
  wire mul_ln81_fu_206_p2__2_n_61;
  wire mul_ln81_fu_206_p2__2_n_62;
  wire mul_ln81_fu_206_p2__2_n_63;
  wire mul_ln81_fu_206_p2__2_n_64;
  wire mul_ln81_fu_206_p2__2_n_65;
  wire mul_ln81_fu_206_p2__2_n_66;
  wire mul_ln81_fu_206_p2__2_n_67;
  wire mul_ln81_fu_206_p2__2_n_68;
  wire mul_ln81_fu_206_p2__2_n_69;
  wire mul_ln81_fu_206_p2__2_n_70;
  wire mul_ln81_fu_206_p2__2_n_71;
  wire mul_ln81_fu_206_p2__2_n_72;
  wire mul_ln81_fu_206_p2__2_n_73;
  wire mul_ln81_fu_206_p2__2_n_74;
  wire mul_ln81_fu_206_p2__2_n_75;
  wire mul_ln81_fu_206_p2__2_n_76;
  wire mul_ln81_fu_206_p2__2_n_77;
  wire mul_ln81_fu_206_p2__2_n_78;
  wire mul_ln81_fu_206_p2__2_n_79;
  wire mul_ln81_fu_206_p2__2_n_80;
  wire mul_ln81_fu_206_p2__2_n_81;
  wire mul_ln81_fu_206_p2__2_n_82;
  wire mul_ln81_fu_206_p2__2_n_83;
  wire mul_ln81_fu_206_p2__2_n_84;
  wire mul_ln81_fu_206_p2__2_n_85;
  wire mul_ln81_fu_206_p2__2_n_86;
  wire mul_ln81_fu_206_p2__2_n_87;
  wire mul_ln81_fu_206_p2__2_n_88;
  wire mul_ln81_fu_206_p2__2_n_89;
  wire mul_ln81_fu_206_p2__2_n_90;
  wire mul_ln81_fu_206_p2__2_n_91;
  wire mul_ln81_fu_206_p2__2_n_92;
  wire mul_ln81_fu_206_p2__2_n_93;
  wire mul_ln81_fu_206_p2__2_n_94;
  wire mul_ln81_fu_206_p2__2_n_95;
  wire mul_ln81_fu_206_p2__2_n_96;
  wire mul_ln81_fu_206_p2__2_n_97;
  wire mul_ln81_fu_206_p2__2_n_98;
  wire mul_ln81_fu_206_p2__2_n_99;
  wire [63:16]mul_ln81_fu_206_p2__3;
  wire mul_ln81_fu_206_p2_n_100;
  wire mul_ln81_fu_206_p2_n_101;
  wire mul_ln81_fu_206_p2_n_102;
  wire mul_ln81_fu_206_p2_n_103;
  wire mul_ln81_fu_206_p2_n_104;
  wire mul_ln81_fu_206_p2_n_105;
  wire mul_ln81_fu_206_p2_n_106;
  wire mul_ln81_fu_206_p2_n_107;
  wire mul_ln81_fu_206_p2_n_108;
  wire mul_ln81_fu_206_p2_n_109;
  wire mul_ln81_fu_206_p2_n_110;
  wire mul_ln81_fu_206_p2_n_111;
  wire mul_ln81_fu_206_p2_n_112;
  wire mul_ln81_fu_206_p2_n_113;
  wire mul_ln81_fu_206_p2_n_114;
  wire mul_ln81_fu_206_p2_n_115;
  wire mul_ln81_fu_206_p2_n_116;
  wire mul_ln81_fu_206_p2_n_117;
  wire mul_ln81_fu_206_p2_n_118;
  wire mul_ln81_fu_206_p2_n_119;
  wire mul_ln81_fu_206_p2_n_120;
  wire mul_ln81_fu_206_p2_n_121;
  wire mul_ln81_fu_206_p2_n_122;
  wire mul_ln81_fu_206_p2_n_123;
  wire mul_ln81_fu_206_p2_n_124;
  wire mul_ln81_fu_206_p2_n_125;
  wire mul_ln81_fu_206_p2_n_126;
  wire mul_ln81_fu_206_p2_n_127;
  wire mul_ln81_fu_206_p2_n_128;
  wire mul_ln81_fu_206_p2_n_129;
  wire mul_ln81_fu_206_p2_n_130;
  wire mul_ln81_fu_206_p2_n_131;
  wire mul_ln81_fu_206_p2_n_132;
  wire mul_ln81_fu_206_p2_n_133;
  wire mul_ln81_fu_206_p2_n_134;
  wire mul_ln81_fu_206_p2_n_135;
  wire mul_ln81_fu_206_p2_n_136;
  wire mul_ln81_fu_206_p2_n_137;
  wire mul_ln81_fu_206_p2_n_138;
  wire mul_ln81_fu_206_p2_n_139;
  wire mul_ln81_fu_206_p2_n_140;
  wire mul_ln81_fu_206_p2_n_141;
  wire mul_ln81_fu_206_p2_n_142;
  wire mul_ln81_fu_206_p2_n_143;
  wire mul_ln81_fu_206_p2_n_144;
  wire mul_ln81_fu_206_p2_n_145;
  wire mul_ln81_fu_206_p2_n_146;
  wire mul_ln81_fu_206_p2_n_147;
  wire mul_ln81_fu_206_p2_n_148;
  wire mul_ln81_fu_206_p2_n_149;
  wire mul_ln81_fu_206_p2_n_150;
  wire mul_ln81_fu_206_p2_n_151;
  wire mul_ln81_fu_206_p2_n_152;
  wire mul_ln81_fu_206_p2_n_153;
  wire mul_ln81_fu_206_p2_n_154;
  wire mul_ln81_fu_206_p2_n_59;
  wire mul_ln81_fu_206_p2_n_60;
  wire mul_ln81_fu_206_p2_n_61;
  wire mul_ln81_fu_206_p2_n_62;
  wire mul_ln81_fu_206_p2_n_63;
  wire mul_ln81_fu_206_p2_n_64;
  wire mul_ln81_fu_206_p2_n_65;
  wire mul_ln81_fu_206_p2_n_66;
  wire mul_ln81_fu_206_p2_n_67;
  wire mul_ln81_fu_206_p2_n_68;
  wire mul_ln81_fu_206_p2_n_69;
  wire mul_ln81_fu_206_p2_n_70;
  wire mul_ln81_fu_206_p2_n_71;
  wire mul_ln81_fu_206_p2_n_72;
  wire mul_ln81_fu_206_p2_n_73;
  wire mul_ln81_fu_206_p2_n_74;
  wire mul_ln81_fu_206_p2_n_75;
  wire mul_ln81_fu_206_p2_n_76;
  wire mul_ln81_fu_206_p2_n_77;
  wire mul_ln81_fu_206_p2_n_78;
  wire mul_ln81_fu_206_p2_n_79;
  wire mul_ln81_fu_206_p2_n_80;
  wire mul_ln81_fu_206_p2_n_81;
  wire mul_ln81_fu_206_p2_n_82;
  wire mul_ln81_fu_206_p2_n_83;
  wire mul_ln81_fu_206_p2_n_84;
  wire mul_ln81_fu_206_p2_n_85;
  wire mul_ln81_fu_206_p2_n_86;
  wire mul_ln81_fu_206_p2_n_87;
  wire mul_ln81_fu_206_p2_n_88;
  wire mul_ln81_fu_206_p2_n_89;
  wire mul_ln81_fu_206_p2_n_90;
  wire mul_ln81_fu_206_p2_n_91;
  wire mul_ln81_fu_206_p2_n_92;
  wire mul_ln81_fu_206_p2_n_93;
  wire mul_ln81_fu_206_p2_n_94;
  wire mul_ln81_fu_206_p2_n_95;
  wire mul_ln81_fu_206_p2_n_96;
  wire mul_ln81_fu_206_p2_n_97;
  wire mul_ln81_fu_206_p2_n_98;
  wire mul_ln81_fu_206_p2_n_99;
  wire \odata[11]_i_10_n_1 ;
  wire \odata[11]_i_11_n_1 ;
  wire \odata[11]_i_8_n_1 ;
  wire \odata[11]_i_9_n_1 ;
  wire \odata[15]_i_10_n_1 ;
  wire \odata[15]_i_11_n_1 ;
  wire \odata[15]_i_8_n_1 ;
  wire \odata[15]_i_9_n_1 ;
  wire \odata[19]_i_10_n_1 ;
  wire \odata[19]_i_11_n_1 ;
  wire \odata[19]_i_8_n_1 ;
  wire \odata[19]_i_9_n_1 ;
  wire \odata[31]_i_10_n_1 ;
  wire \odata[31]_i_11_n_1 ;
  wire \odata[31]_i_12_n_1 ;
  wire \odata[31]_i_13_n_1 ;
  wire \odata[3]_i_15_n_1 ;
  wire \odata[3]_i_16_n_1 ;
  wire \odata[3]_i_17_n_1 ;
  wire \odata[3]_i_18_n_1 ;
  wire \odata[3]_i_25_n_1 ;
  wire \odata[3]_i_26_n_1 ;
  wire \odata[3]_i_27_n_1 ;
  wire \odata[3]_i_28_n_1 ;
  wire \odata[3]_i_35_n_1 ;
  wire \odata[3]_i_36_n_1 ;
  wire \odata[3]_i_37_n_1 ;
  wire \odata[3]_i_38_n_1 ;
  wire \odata[3]_i_45_n_1 ;
  wire \odata[3]_i_46_n_1 ;
  wire \odata[3]_i_47_n_1 ;
  wire \odata[3]_i_48_n_1 ;
  wire \odata[3]_i_55_n_1 ;
  wire \odata[3]_i_56_n_1 ;
  wire \odata[3]_i_57_n_1 ;
  wire \odata[3]_i_58_n_1 ;
  wire \odata[3]_i_65_n_1 ;
  wire \odata[3]_i_66_n_1 ;
  wire \odata[3]_i_67_n_1 ;
  wire \odata[3]_i_68_n_1 ;
  wire \odata[3]_i_74_n_1 ;
  wire \odata[3]_i_75_n_1 ;
  wire \odata[3]_i_76_n_1 ;
  wire \odata[7]_i_10_n_1 ;
  wire \odata[7]_i_11_n_1 ;
  wire \odata[7]_i_8_n_1 ;
  wire \odata[7]_i_9_n_1 ;
  wire \odata_reg[11]_i_3_n_1 ;
  wire \odata_reg[11]_i_3_n_2 ;
  wire \odata_reg[11]_i_3_n_3 ;
  wire \odata_reg[11]_i_3_n_4 ;
  wire \odata_reg[15]_i_3_n_1 ;
  wire \odata_reg[15]_i_3_n_2 ;
  wire \odata_reg[15]_i_3_n_3 ;
  wire \odata_reg[15]_i_3_n_4 ;
  wire \odata_reg[19]_i_3_n_1 ;
  wire \odata_reg[19]_i_3_n_2 ;
  wire \odata_reg[19]_i_3_n_3 ;
  wire \odata_reg[19]_i_3_n_4 ;
  wire \odata_reg[31]_i_5_n_2 ;
  wire \odata_reg[31]_i_5_n_3 ;
  wire \odata_reg[31]_i_5_n_4 ;
  wire \odata_reg[3]_i_10_n_1 ;
  wire \odata_reg[3]_i_10_n_2 ;
  wire \odata_reg[3]_i_10_n_3 ;
  wire \odata_reg[3]_i_10_n_4 ;
  wire \odata_reg[3]_i_20_n_1 ;
  wire \odata_reg[3]_i_20_n_2 ;
  wire \odata_reg[3]_i_20_n_3 ;
  wire \odata_reg[3]_i_20_n_4 ;
  wire \odata_reg[3]_i_30_n_1 ;
  wire \odata_reg[3]_i_30_n_2 ;
  wire \odata_reg[3]_i_30_n_3 ;
  wire \odata_reg[3]_i_30_n_4 ;
  wire \odata_reg[3]_i_40_n_1 ;
  wire \odata_reg[3]_i_40_n_2 ;
  wire \odata_reg[3]_i_40_n_3 ;
  wire \odata_reg[3]_i_40_n_4 ;
  wire \odata_reg[3]_i_4_n_1 ;
  wire \odata_reg[3]_i_4_n_2 ;
  wire \odata_reg[3]_i_4_n_3 ;
  wire \odata_reg[3]_i_4_n_4 ;
  wire \odata_reg[3]_i_50_n_1 ;
  wire \odata_reg[3]_i_50_n_2 ;
  wire \odata_reg[3]_i_50_n_3 ;
  wire \odata_reg[3]_i_50_n_4 ;
  wire \odata_reg[3]_i_60_n_1 ;
  wire \odata_reg[3]_i_60_n_2 ;
  wire \odata_reg[3]_i_60_n_3 ;
  wire \odata_reg[3]_i_60_n_4 ;
  wire \odata_reg[7]_i_3_n_1 ;
  wire \odata_reg[7]_i_3_n_2 ;
  wire \odata_reg[7]_i_3_n_3 ;
  wire \odata_reg[7]_i_3_n_4 ;
  wire [23:0]p_0_in;
  wire [31:0]q00;
  wire regslice_both_x_U_n_10;
  wire regslice_both_x_U_n_11;
  wire regslice_both_x_U_n_12;
  wire regslice_both_x_U_n_13;
  wire regslice_both_x_U_n_14;
  wire regslice_both_x_U_n_15;
  wire regslice_both_x_U_n_16;
  wire regslice_both_x_U_n_17;
  wire regslice_both_x_U_n_2;
  wire regslice_both_x_U_n_24;
  wire regslice_both_x_U_n_25;
  wire regslice_both_x_U_n_26;
  wire regslice_both_x_U_n_27;
  wire regslice_both_x_U_n_28;
  wire regslice_both_x_U_n_29;
  wire regslice_both_x_U_n_3;
  wire regslice_both_x_U_n_30;
  wire regslice_both_x_U_n_31;
  wire regslice_both_x_U_n_32;
  wire regslice_both_x_U_n_33;
  wire regslice_both_x_U_n_34;
  wire regslice_both_x_U_n_35;
  wire regslice_both_x_U_n_36;
  wire regslice_both_x_U_n_37;
  wire regslice_both_x_U_n_38;
  wire regslice_both_x_U_n_39;
  wire regslice_both_x_U_n_4;
  wire regslice_both_x_U_n_40;
  wire regslice_both_x_U_n_5;
  wire regslice_both_x_U_n_6;
  wire regslice_both_x_U_n_7;
  wire regslice_both_x_U_n_8;
  wire regslice_both_x_U_n_9;
  wire regslice_both_y_U_n_33;
  wire regslice_both_y_U_n_34;
  wire shift_reg0_U_n_5;
  wire shift_reg0_U_n_6;
  wire [4:0]shift_reg0_address0;
  wire shift_reg0_ce0;
  wire shift_reg1_U_n_10;
  wire shift_reg1_U_n_11;
  wire shift_reg1_U_n_12;
  wire shift_reg1_U_n_13;
  wire shift_reg1_U_n_14;
  wire shift_reg1_U_n_15;
  wire shift_reg1_U_n_16;
  wire shift_reg1_U_n_17;
  wire shift_reg1_U_n_18;
  wire shift_reg1_U_n_19;
  wire shift_reg1_U_n_2;
  wire shift_reg1_U_n_20;
  wire shift_reg1_U_n_21;
  wire shift_reg1_U_n_22;
  wire shift_reg1_U_n_23;
  wire shift_reg1_U_n_24;
  wire shift_reg1_U_n_25;
  wire shift_reg1_U_n_26;
  wire shift_reg1_U_n_27;
  wire shift_reg1_U_n_28;
  wire shift_reg1_U_n_29;
  wire shift_reg1_U_n_3;
  wire shift_reg1_U_n_30;
  wire shift_reg1_U_n_31;
  wire shift_reg1_U_n_32;
  wire shift_reg1_U_n_33;
  wire shift_reg1_U_n_34;
  wire shift_reg1_U_n_4;
  wire shift_reg1_U_n_5;
  wire shift_reg1_U_n_6;
  wire shift_reg1_U_n_7;
  wire shift_reg1_U_n_8;
  wire shift_reg1_U_n_9;
  wire [23:0]trunc_ln1_reg_329;
  wire \trunc_ln1_reg_329[11]_i_10_n_1 ;
  wire \trunc_ln1_reg_329[11]_i_3_n_1 ;
  wire \trunc_ln1_reg_329[11]_i_4_n_1 ;
  wire \trunc_ln1_reg_329[11]_i_5_n_1 ;
  wire \trunc_ln1_reg_329[11]_i_6_n_1 ;
  wire \trunc_ln1_reg_329[11]_i_7_n_1 ;
  wire \trunc_ln1_reg_329[11]_i_8_n_1 ;
  wire \trunc_ln1_reg_329[11]_i_9_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_10_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_3_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_4_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_5_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_6_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_7_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_8_n_1 ;
  wire \trunc_ln1_reg_329[15]_i_9_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_10_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_3_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_4_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_5_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_6_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_7_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_8_n_1 ;
  wire \trunc_ln1_reg_329[19]_i_9_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_10_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_11_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_4_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_5_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_6_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_7_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_8_n_1 ;
  wire \trunc_ln1_reg_329[23]_i_9_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_10_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_11_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_12_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_13_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_14_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_15_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_16_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_17_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_20_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_21_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_22_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_23_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_24_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_25_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_26_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_27_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_30_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_31_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_32_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_33_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_34_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_35_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_36_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_37_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_40_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_41_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_42_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_43_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_44_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_45_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_46_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_47_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_4_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_50_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_51_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_52_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_53_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_54_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_55_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_56_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_57_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_5_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_60_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_61_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_62_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_63_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_64_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_65_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_66_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_67_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_69_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_6_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_70_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_71_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_72_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_73_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_74_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_75_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_77_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_78_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_79_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_7_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_80_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_82_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_83_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_84_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_85_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_86_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_87_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_88_n_1 ;
  wire \trunc_ln1_reg_329[3]_i_89_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_10_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_3_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_4_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_5_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_6_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_7_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_8_n_1 ;
  wire \trunc_ln1_reg_329[7]_i_9_n_1 ;
  wire \trunc_ln1_reg_329_reg[11]_i_1_n_1 ;
  wire \trunc_ln1_reg_329_reg[11]_i_1_n_2 ;
  wire \trunc_ln1_reg_329_reg[11]_i_1_n_3 ;
  wire \trunc_ln1_reg_329_reg[11]_i_1_n_4 ;
  wire \trunc_ln1_reg_329_reg[11]_i_2_n_1 ;
  wire \trunc_ln1_reg_329_reg[11]_i_2_n_2 ;
  wire \trunc_ln1_reg_329_reg[11]_i_2_n_3 ;
  wire \trunc_ln1_reg_329_reg[11]_i_2_n_4 ;
  wire \trunc_ln1_reg_329_reg[15]_i_1_n_1 ;
  wire \trunc_ln1_reg_329_reg[15]_i_1_n_2 ;
  wire \trunc_ln1_reg_329_reg[15]_i_1_n_3 ;
  wire \trunc_ln1_reg_329_reg[15]_i_1_n_4 ;
  wire \trunc_ln1_reg_329_reg[15]_i_2_n_1 ;
  wire \trunc_ln1_reg_329_reg[15]_i_2_n_2 ;
  wire \trunc_ln1_reg_329_reg[15]_i_2_n_3 ;
  wire \trunc_ln1_reg_329_reg[15]_i_2_n_4 ;
  wire \trunc_ln1_reg_329_reg[19]_i_1_n_1 ;
  wire \trunc_ln1_reg_329_reg[19]_i_1_n_2 ;
  wire \trunc_ln1_reg_329_reg[19]_i_1_n_3 ;
  wire \trunc_ln1_reg_329_reg[19]_i_1_n_4 ;
  wire \trunc_ln1_reg_329_reg[19]_i_2_n_1 ;
  wire \trunc_ln1_reg_329_reg[19]_i_2_n_2 ;
  wire \trunc_ln1_reg_329_reg[19]_i_2_n_3 ;
  wire \trunc_ln1_reg_329_reg[19]_i_2_n_4 ;
  wire \trunc_ln1_reg_329_reg[23]_i_2_n_2 ;
  wire \trunc_ln1_reg_329_reg[23]_i_2_n_3 ;
  wire \trunc_ln1_reg_329_reg[23]_i_2_n_4 ;
  wire \trunc_ln1_reg_329_reg[23]_i_3_n_2 ;
  wire \trunc_ln1_reg_329_reg[23]_i_3_n_3 ;
  wire \trunc_ln1_reg_329_reg[23]_i_3_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_18_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_18_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_18_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_18_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_19_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_19_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_19_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_19_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_1_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_1_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_1_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_1_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_28_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_28_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_28_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_28_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_29_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_29_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_29_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_29_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_2_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_2_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_2_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_2_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_38_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_38_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_38_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_38_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_39_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_39_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_39_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_39_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_3_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_3_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_3_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_3_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_48_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_48_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_48_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_48_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_49_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_49_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_49_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_49_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_58_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_58_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_58_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_58_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_59_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_59_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_59_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_59_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_68_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_68_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_68_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_68_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_76_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_76_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_76_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_76_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_81_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_81_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_81_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_81_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_8_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_8_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_8_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_8_n_4 ;
  wire \trunc_ln1_reg_329_reg[3]_i_9_n_1 ;
  wire \trunc_ln1_reg_329_reg[3]_i_9_n_2 ;
  wire \trunc_ln1_reg_329_reg[3]_i_9_n_3 ;
  wire \trunc_ln1_reg_329_reg[3]_i_9_n_4 ;
  wire \trunc_ln1_reg_329_reg[7]_i_1_n_1 ;
  wire \trunc_ln1_reg_329_reg[7]_i_1_n_2 ;
  wire \trunc_ln1_reg_329_reg[7]_i_1_n_3 ;
  wire \trunc_ln1_reg_329_reg[7]_i_1_n_4 ;
  wire \trunc_ln1_reg_329_reg[7]_i_2_n_1 ;
  wire \trunc_ln1_reg_329_reg[7]_i_2_n_2 ;
  wire \trunc_ln1_reg_329_reg[7]_i_2_n_3 ;
  wire \trunc_ln1_reg_329_reg[7]_i_2_n_4 ;
  wire [31:0]x0_reg_283;
  wire [31:0]x1_reg_289;
  wire [31:0]x_TDATA;
  wire x_TREADY;
  wire x_TVALID;
  wire x_TVALID_int;
  wire [30:0]\^y_TDATA ;
  wire y_TREADY;
  wire y_TVALID;
  wire \zext_ln70_reg_313_reg_n_1_[0] ;
  wire \zext_ln70_reg_313_reg_n_1_[1] ;
  wire \zext_ln70_reg_313_reg_n_1_[2] ;
  wire \zext_ln70_reg_313_reg_n_1_[3] ;
  wire \zext_ln70_reg_313_reg_n_1_[4] ;
  wire [3:3]\NLW_acc0_0_reg_130_reg[63]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_acc0_0_reg_130_reg[63]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_acc1_0_reg_142_reg[63]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_acc1_0_reg_142_reg[63]_i_2_CO_UNCONNECTED ;
  wire NLW_mul_ln73_fu_251_p2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln73_fu_251_p2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln73_fu_251_p2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln73_fu_251_p2_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln73_fu_251_p2__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln73_fu_251_p2__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln73_fu_251_p2__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln73_fu_251_p2__0_PCOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__1_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__1_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__1_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__1_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__1_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__1_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln73_fu_251_p2__1_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln73_fu_251_p2__1_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln73_fu_251_p2__1_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln73_fu_251_p2__2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln73_fu_251_p2__2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln73_fu_251_p2__2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln73_fu_251_p2__2_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln73_fu_251_p2__2_PCOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln74_fu_267_p2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln74_fu_267_p2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln74_fu_267_p2_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln74_fu_267_p2__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln74_fu_267_p2__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln74_fu_267_p2__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln74_fu_267_p2__0_PCOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__1_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__1_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__1_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__1_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__1_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__1_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln74_fu_267_p2__1_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln74_fu_267_p2__1_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln74_fu_267_p2__1_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln74_fu_267_p2__2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln74_fu_267_p2__2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln74_fu_267_p2__2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln74_fu_267_p2__2_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln74_fu_267_p2__2_PCOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln80_fu_191_p2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln80_fu_191_p2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln80_fu_191_p2_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln80_fu_191_p2__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln80_fu_191_p2__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln80_fu_191_p2__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln80_fu_191_p2__0_PCOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__1_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__1_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__1_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__1_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__1_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__1_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln80_fu_191_p2__1_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln80_fu_191_p2__1_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln80_fu_191_p2__1_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln80_fu_191_p2__2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln80_fu_191_p2__2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln80_fu_191_p2__2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln80_fu_191_p2__2_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln80_fu_191_p2__2_PCOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln81_fu_206_p2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln81_fu_206_p2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln81_fu_206_p2_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln81_fu_206_p2__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln81_fu_206_p2__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln81_fu_206_p2__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln81_fu_206_p2__0_PCOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__1_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__1_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__1_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__1_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__1_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__1_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln81_fu_206_p2__1_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln81_fu_206_p2__1_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln81_fu_206_p2__1_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln81_fu_206_p2__2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln81_fu_206_p2__2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln81_fu_206_p2__2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln81_fu_206_p2__2_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln81_fu_206_p2__2_PCOUT_UNCONNECTED;
  wire [3:3]\NLW_odata_reg[31]_i_5_CO_UNCONNECTED ;
  wire [3:3]\NLW_trunc_ln1_reg_329_reg[23]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_trunc_ln1_reg_329_reg[23]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_18_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_28_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_38_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_48_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_58_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_68_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_76_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_8_O_UNCONNECTED ;
  wire [3:0]\NLW_trunc_ln1_reg_329_reg[3]_i_81_O_UNCONNECTED ;

  assign y_TDATA[31] = \^y_TDATA [30];
  assign y_TDATA[30] = \^y_TDATA [30];
  assign y_TDATA[29] = \^y_TDATA [30];
  assign y_TDATA[28] = \^y_TDATA [30];
  assign y_TDATA[27] = \^y_TDATA [30];
  assign y_TDATA[26] = \^y_TDATA [30];
  assign y_TDATA[25] = \^y_TDATA [30];
  assign y_TDATA[24] = \^y_TDATA [30];
  assign y_TDATA[23] = \^y_TDATA [30];
  assign y_TDATA[22:0] = \^y_TDATA [22:0];
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[11]_i_2 
       (.I0(mul_ln73_fu_251_p2__1_n_95),
        .I1(\acc0_0_reg_130_reg_n_1_[11] ),
        .O(\acc0_0_reg_130[11]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[11]_i_3 
       (.I0(mul_ln73_fu_251_p2__1_n_96),
        .I1(\acc0_0_reg_130_reg_n_1_[10] ),
        .O(\acc0_0_reg_130[11]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[11]_i_4 
       (.I0(mul_ln73_fu_251_p2__1_n_97),
        .I1(\acc0_0_reg_130_reg_n_1_[9] ),
        .O(\acc0_0_reg_130[11]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[11]_i_5 
       (.I0(mul_ln73_fu_251_p2__1_n_98),
        .I1(\acc0_0_reg_130_reg_n_1_[8] ),
        .O(\acc0_0_reg_130[11]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[15]_i_2 
       (.I0(mul_ln73_fu_251_p2__1_n_91),
        .I1(\acc0_0_reg_130_reg_n_1_[15] ),
        .O(\acc0_0_reg_130[15]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[15]_i_3 
       (.I0(mul_ln73_fu_251_p2__1_n_92),
        .I1(\acc0_0_reg_130_reg_n_1_[14] ),
        .O(\acc0_0_reg_130[15]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[15]_i_4 
       (.I0(mul_ln73_fu_251_p2__1_n_93),
        .I1(\acc0_0_reg_130_reg_n_1_[13] ),
        .O(\acc0_0_reg_130[15]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[15]_i_5 
       (.I0(mul_ln73_fu_251_p2__1_n_94),
        .I1(\acc0_0_reg_130_reg_n_1_[12] ),
        .O(\acc0_0_reg_130[15]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[19]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[19]),
        .I1(\acc0_0_reg_130_reg_n_1_[19] ),
        .O(\acc0_0_reg_130[19]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[19]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[18]),
        .I1(\acc0_0_reg_130_reg_n_1_[18] ),
        .O(\acc0_0_reg_130[19]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[19]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[17]),
        .I1(\acc0_0_reg_130_reg_n_1_[17] ),
        .O(\acc0_0_reg_130[19]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[19]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[16]),
        .I1(\acc0_0_reg_130_reg_n_1_[16] ),
        .O(\acc0_0_reg_130[19]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[19]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_104),
        .I1(mul_ln73_fu_251_p2_n_104),
        .O(\acc0_0_reg_130[19]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[19]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_105),
        .I1(mul_ln73_fu_251_p2_n_105),
        .O(\acc0_0_reg_130[19]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[19]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_106),
        .I1(mul_ln73_fu_251_p2_n_106),
        .O(\acc0_0_reg_130[19]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_103),
        .I1(mul_ln73_fu_251_p2_n_103),
        .O(\acc0_0_reg_130[23]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[23]),
        .I1(\acc0_0_reg_130_reg_n_1_[23] ),
        .O(\acc0_0_reg_130[23]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[22]),
        .I1(\acc0_0_reg_130_reg_n_1_[22] ),
        .O(\acc0_0_reg_130[23]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[21]),
        .I1(\acc0_0_reg_130_reg_n_1_[21] ),
        .O(\acc0_0_reg_130[23]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[20]),
        .I1(\acc0_0_reg_130_reg_n_1_[20] ),
        .O(\acc0_0_reg_130[23]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_100),
        .I1(mul_ln73_fu_251_p2_n_100),
        .O(\acc0_0_reg_130[23]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_101),
        .I1(mul_ln73_fu_251_p2_n_101),
        .O(\acc0_0_reg_130[23]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[23]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_102),
        .I1(mul_ln73_fu_251_p2_n_102),
        .O(\acc0_0_reg_130[23]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_99),
        .I1(mul_ln73_fu_251_p2_n_99),
        .O(\acc0_0_reg_130[27]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[27]),
        .I1(\acc0_0_reg_130_reg_n_1_[27] ),
        .O(\acc0_0_reg_130[27]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[26]),
        .I1(\acc0_0_reg_130_reg_n_1_[26] ),
        .O(\acc0_0_reg_130[27]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[25]),
        .I1(\acc0_0_reg_130_reg_n_1_[25] ),
        .O(\acc0_0_reg_130[27]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[24]),
        .I1(\acc0_0_reg_130_reg_n_1_[24] ),
        .O(\acc0_0_reg_130[27]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_96),
        .I1(mul_ln73_fu_251_p2_n_96),
        .O(\acc0_0_reg_130[27]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_97),
        .I1(mul_ln73_fu_251_p2_n_97),
        .O(\acc0_0_reg_130[27]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[27]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_98),
        .I1(mul_ln73_fu_251_p2_n_98),
        .O(\acc0_0_reg_130[27]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_95),
        .I1(mul_ln73_fu_251_p2_n_95),
        .O(\acc0_0_reg_130[31]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[31]),
        .I1(\acc0_0_reg_130_reg_n_1_[31] ),
        .O(\acc0_0_reg_130[31]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[30]),
        .I1(\acc0_0_reg_130_reg_n_1_[30] ),
        .O(\acc0_0_reg_130[31]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[29]),
        .I1(\acc0_0_reg_130_reg_n_1_[29] ),
        .O(\acc0_0_reg_130[31]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[28]),
        .I1(\acc0_0_reg_130_reg_n_1_[28] ),
        .O(\acc0_0_reg_130[31]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_92),
        .I1(mul_ln73_fu_251_p2_n_92),
        .O(\acc0_0_reg_130[31]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_93),
        .I1(mul_ln73_fu_251_p2_n_93),
        .O(\acc0_0_reg_130[31]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[31]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_94),
        .I1(mul_ln73_fu_251_p2_n_94),
        .O(\acc0_0_reg_130[31]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_91),
        .I1(mul_ln73_fu_251_p2_n_91),
        .O(\acc0_0_reg_130[35]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[35]),
        .I1(\acc0_0_reg_130_reg_n_1_[35] ),
        .O(\acc0_0_reg_130[35]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[34]),
        .I1(\acc0_0_reg_130_reg_n_1_[34] ),
        .O(\acc0_0_reg_130[35]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[33]),
        .I1(\acc0_0_reg_130_reg_n_1_[33] ),
        .O(\acc0_0_reg_130[35]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[32]),
        .I1(\acc0_0_reg_130_reg_n_1_[32] ),
        .O(\acc0_0_reg_130[35]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_88),
        .I1(mul_ln73_fu_251_p2__0_n_105),
        .O(\acc0_0_reg_130[35]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_89),
        .I1(mul_ln73_fu_251_p2__0_n_106),
        .O(\acc0_0_reg_130[35]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[35]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_90),
        .I1(mul_ln73_fu_251_p2_n_90),
        .O(\acc0_0_reg_130[35]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_87),
        .I1(mul_ln73_fu_251_p2__0_n_104),
        .O(\acc0_0_reg_130[39]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[39]),
        .I1(\acc0_0_reg_130_reg_n_1_[39] ),
        .O(\acc0_0_reg_130[39]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[38]),
        .I1(\acc0_0_reg_130_reg_n_1_[38] ),
        .O(\acc0_0_reg_130[39]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[37]),
        .I1(\acc0_0_reg_130_reg_n_1_[37] ),
        .O(\acc0_0_reg_130[39]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[36]),
        .I1(\acc0_0_reg_130_reg_n_1_[36] ),
        .O(\acc0_0_reg_130[39]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_84),
        .I1(mul_ln73_fu_251_p2__0_n_101),
        .O(\acc0_0_reg_130[39]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_85),
        .I1(mul_ln73_fu_251_p2__0_n_102),
        .O(\acc0_0_reg_130[39]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[39]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_86),
        .I1(mul_ln73_fu_251_p2__0_n_103),
        .O(\acc0_0_reg_130[39]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[3]_i_2 
       (.I0(mul_ln73_fu_251_p2__1_n_103),
        .I1(\acc0_0_reg_130_reg_n_1_[3] ),
        .O(\acc0_0_reg_130[3]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[3]_i_3 
       (.I0(mul_ln73_fu_251_p2__1_n_104),
        .I1(\acc0_0_reg_130_reg_n_1_[2] ),
        .O(\acc0_0_reg_130[3]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[3]_i_4 
       (.I0(mul_ln73_fu_251_p2__1_n_105),
        .I1(\acc0_0_reg_130_reg_n_1_[1] ),
        .O(\acc0_0_reg_130[3]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[3]_i_5 
       (.I0(mul_ln73_fu_251_p2__1_n_106),
        .I1(\acc0_0_reg_130_reg_n_1_[0] ),
        .O(\acc0_0_reg_130[3]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_83),
        .I1(mul_ln73_fu_251_p2__0_n_100),
        .O(\acc0_0_reg_130[43]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[43]),
        .I1(\acc0_0_reg_130_reg_n_1_[43] ),
        .O(\acc0_0_reg_130[43]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[42]),
        .I1(\acc0_0_reg_130_reg_n_1_[42] ),
        .O(\acc0_0_reg_130[43]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[41]),
        .I1(\acc0_0_reg_130_reg_n_1_[41] ),
        .O(\acc0_0_reg_130[43]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[40]),
        .I1(\acc0_0_reg_130_reg_n_1_[40] ),
        .O(\acc0_0_reg_130[43]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_80),
        .I1(mul_ln73_fu_251_p2__0_n_97),
        .O(\acc0_0_reg_130[43]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_81),
        .I1(mul_ln73_fu_251_p2__0_n_98),
        .O(\acc0_0_reg_130[43]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[43]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_82),
        .I1(mul_ln73_fu_251_p2__0_n_99),
        .O(\acc0_0_reg_130[43]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_79),
        .I1(mul_ln73_fu_251_p2__0_n_96),
        .O(\acc0_0_reg_130[47]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[47]),
        .I1(\acc0_0_reg_130_reg_n_1_[47] ),
        .O(\acc0_0_reg_130[47]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[46]),
        .I1(\acc0_0_reg_130_reg_n_1_[46] ),
        .O(\acc0_0_reg_130[47]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[45]),
        .I1(\acc0_0_reg_130_reg_n_1_[45] ),
        .O(\acc0_0_reg_130[47]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[44]),
        .I1(\acc0_0_reg_130_reg_n_1_[44] ),
        .O(\acc0_0_reg_130[47]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_76),
        .I1(mul_ln73_fu_251_p2__0_n_93),
        .O(\acc0_0_reg_130[47]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_77),
        .I1(mul_ln73_fu_251_p2__0_n_94),
        .O(\acc0_0_reg_130[47]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[47]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_78),
        .I1(mul_ln73_fu_251_p2__0_n_95),
        .O(\acc0_0_reg_130[47]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_75),
        .I1(mul_ln73_fu_251_p2__0_n_92),
        .O(\acc0_0_reg_130[51]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[51]),
        .I1(\acc0_0_reg_130_reg_n_1_[51] ),
        .O(\acc0_0_reg_130[51]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[50]),
        .I1(\acc0_0_reg_130_reg_n_1_[50] ),
        .O(\acc0_0_reg_130[51]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[49]),
        .I1(\acc0_0_reg_130_reg_n_1_[49] ),
        .O(\acc0_0_reg_130[51]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[48]),
        .I1(\acc0_0_reg_130_reg_n_1_[48] ),
        .O(\acc0_0_reg_130[51]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_72),
        .I1(mul_ln73_fu_251_p2__0_n_89),
        .O(\acc0_0_reg_130[51]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_73),
        .I1(mul_ln73_fu_251_p2__0_n_90),
        .O(\acc0_0_reg_130[51]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[51]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_74),
        .I1(mul_ln73_fu_251_p2__0_n_91),
        .O(\acc0_0_reg_130[51]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_71),
        .I1(mul_ln73_fu_251_p2__0_n_88),
        .O(\acc0_0_reg_130[55]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[55]),
        .I1(\acc0_0_reg_130_reg_n_1_[55] ),
        .O(\acc0_0_reg_130[55]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[54]),
        .I1(\acc0_0_reg_130_reg_n_1_[54] ),
        .O(\acc0_0_reg_130[55]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[53]),
        .I1(\acc0_0_reg_130_reg_n_1_[53] ),
        .O(\acc0_0_reg_130[55]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[52]),
        .I1(\acc0_0_reg_130_reg_n_1_[52] ),
        .O(\acc0_0_reg_130[55]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_68),
        .I1(mul_ln73_fu_251_p2__0_n_85),
        .O(\acc0_0_reg_130[55]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_69),
        .I1(mul_ln73_fu_251_p2__0_n_86),
        .O(\acc0_0_reg_130[55]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[55]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_70),
        .I1(mul_ln73_fu_251_p2__0_n_87),
        .O(\acc0_0_reg_130[55]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_67),
        .I1(mul_ln73_fu_251_p2__0_n_84),
        .O(\acc0_0_reg_130[59]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_3 
       (.I0(mul_ln73_fu_251_p2__3[59]),
        .I1(\acc0_0_reg_130_reg_n_1_[59] ),
        .O(\acc0_0_reg_130[59]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[58]),
        .I1(\acc0_0_reg_130_reg_n_1_[58] ),
        .O(\acc0_0_reg_130[59]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[57]),
        .I1(\acc0_0_reg_130_reg_n_1_[57] ),
        .O(\acc0_0_reg_130[59]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[56]),
        .I1(\acc0_0_reg_130_reg_n_1_[56] ),
        .O(\acc0_0_reg_130[59]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_7 
       (.I0(mul_ln73_fu_251_p2__2_n_64),
        .I1(mul_ln73_fu_251_p2__0_n_81),
        .O(\acc0_0_reg_130[59]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_65),
        .I1(mul_ln73_fu_251_p2__0_n_82),
        .O(\acc0_0_reg_130[59]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[59]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_66),
        .I1(mul_ln73_fu_251_p2__0_n_83),
        .O(\acc0_0_reg_130[59]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_10 
       (.I0(mul_ln73_fu_251_p2__2_n_63),
        .I1(mul_ln73_fu_251_p2__0_n_80),
        .O(\acc0_0_reg_130[63]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_3 
       (.I0(\acc0_0_reg_130_reg_n_1_[63] ),
        .I1(mul_ln73_fu_251_p2__3[63]),
        .O(\acc0_0_reg_130[63]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_4 
       (.I0(mul_ln73_fu_251_p2__3[62]),
        .I1(\acc0_0_reg_130_reg_n_1_[62] ),
        .O(\acc0_0_reg_130[63]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_5 
       (.I0(mul_ln73_fu_251_p2__3[61]),
        .I1(\acc0_0_reg_130_reg_n_1_[61] ),
        .O(\acc0_0_reg_130[63]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_6 
       (.I0(mul_ln73_fu_251_p2__3[60]),
        .I1(\acc0_0_reg_130_reg_n_1_[60] ),
        .O(\acc0_0_reg_130[63]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_7 
       (.I0(mul_ln73_fu_251_p2__0_n_77),
        .I1(mul_ln73_fu_251_p2__2_n_60),
        .O(\acc0_0_reg_130[63]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_8 
       (.I0(mul_ln73_fu_251_p2__2_n_61),
        .I1(mul_ln73_fu_251_p2__0_n_78),
        .O(\acc0_0_reg_130[63]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[63]_i_9 
       (.I0(mul_ln73_fu_251_p2__2_n_62),
        .I1(mul_ln73_fu_251_p2__0_n_79),
        .O(\acc0_0_reg_130[63]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[7]_i_2 
       (.I0(mul_ln73_fu_251_p2__1_n_99),
        .I1(\acc0_0_reg_130_reg_n_1_[7] ),
        .O(\acc0_0_reg_130[7]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[7]_i_3 
       (.I0(mul_ln73_fu_251_p2__1_n_100),
        .I1(\acc0_0_reg_130_reg_n_1_[6] ),
        .O(\acc0_0_reg_130[7]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[7]_i_4 
       (.I0(mul_ln73_fu_251_p2__1_n_101),
        .I1(\acc0_0_reg_130_reg_n_1_[5] ),
        .O(\acc0_0_reg_130[7]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc0_0_reg_130[7]_i_5 
       (.I0(mul_ln73_fu_251_p2__1_n_102),
        .I1(\acc0_0_reg_130_reg_n_1_[4] ),
        .O(\acc0_0_reg_130[7]_i_5_n_1 ));
  FDRE \acc0_0_reg_130_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[0]),
        .Q(\acc0_0_reg_130_reg_n_1_[0] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[10]),
        .Q(\acc0_0_reg_130_reg_n_1_[10] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[11] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[11]),
        .Q(\acc0_0_reg_130_reg_n_1_[11] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[11]_i_1 
       (.CI(\acc0_0_reg_130_reg[7]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[11]_i_1_n_1 ,\acc0_0_reg_130_reg[11]_i_1_n_2 ,\acc0_0_reg_130_reg[11]_i_1_n_3 ,\acc0_0_reg_130_reg[11]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__1_n_95,mul_ln73_fu_251_p2__1_n_96,mul_ln73_fu_251_p2__1_n_97,mul_ln73_fu_251_p2__1_n_98}),
        .O(acc0_fu_257_p2[11:8]),
        .S({\acc0_0_reg_130[11]_i_2_n_1 ,\acc0_0_reg_130[11]_i_3_n_1 ,\acc0_0_reg_130[11]_i_4_n_1 ,\acc0_0_reg_130[11]_i_5_n_1 }));
  FDRE \acc0_0_reg_130_reg[12] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[12]),
        .Q(\acc0_0_reg_130_reg_n_1_[12] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[13] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[13]),
        .Q(\acc0_0_reg_130_reg_n_1_[13] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[14] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[14]),
        .Q(\acc0_0_reg_130_reg_n_1_[14] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[15] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[15]),
        .Q(\acc0_0_reg_130_reg_n_1_[15] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[15]_i_1 
       (.CI(\acc0_0_reg_130_reg[11]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[15]_i_1_n_1 ,\acc0_0_reg_130_reg[15]_i_1_n_2 ,\acc0_0_reg_130_reg[15]_i_1_n_3 ,\acc0_0_reg_130_reg[15]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__1_n_91,mul_ln73_fu_251_p2__1_n_92,mul_ln73_fu_251_p2__1_n_93,mul_ln73_fu_251_p2__1_n_94}),
        .O(acc0_fu_257_p2[15:12]),
        .S({\acc0_0_reg_130[15]_i_2_n_1 ,\acc0_0_reg_130[15]_i_3_n_1 ,\acc0_0_reg_130[15]_i_4_n_1 ,\acc0_0_reg_130[15]_i_5_n_1 }));
  FDRE \acc0_0_reg_130_reg[16] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[16]),
        .Q(\acc0_0_reg_130_reg_n_1_[16] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[17] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[17]),
        .Q(\acc0_0_reg_130_reg_n_1_[17] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[18] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[18]),
        .Q(\acc0_0_reg_130_reg_n_1_[18] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[19] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[19]),
        .Q(\acc0_0_reg_130_reg_n_1_[19] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[19]_i_1 
       (.CI(\acc0_0_reg_130_reg[15]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[19]_i_1_n_1 ,\acc0_0_reg_130_reg[19]_i_1_n_2 ,\acc0_0_reg_130_reg[19]_i_1_n_3 ,\acc0_0_reg_130_reg[19]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[19:16]),
        .O(acc0_fu_257_p2[19:16]),
        .S({\acc0_0_reg_130[19]_i_3_n_1 ,\acc0_0_reg_130[19]_i_4_n_1 ,\acc0_0_reg_130[19]_i_5_n_1 ,\acc0_0_reg_130[19]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[19]_i_2 
       (.CI(1'b0),
        .CO({\acc0_0_reg_130_reg[19]_i_2_n_1 ,\acc0_0_reg_130_reg[19]_i_2_n_2 ,\acc0_0_reg_130_reg[19]_i_2_n_3 ,\acc0_0_reg_130_reg[19]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_104,mul_ln73_fu_251_p2__2_n_105,mul_ln73_fu_251_p2__2_n_106,1'b0}),
        .O(mul_ln73_fu_251_p2__3[19:16]),
        .S({\acc0_0_reg_130[19]_i_7_n_1 ,\acc0_0_reg_130[19]_i_8_n_1 ,\acc0_0_reg_130[19]_i_9_n_1 ,mul_ln73_fu_251_p2__1_n_90}));
  FDRE \acc0_0_reg_130_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[1]),
        .Q(\acc0_0_reg_130_reg_n_1_[1] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[20] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[20]),
        .Q(\acc0_0_reg_130_reg_n_1_[20] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[21] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[21]),
        .Q(\acc0_0_reg_130_reg_n_1_[21] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[22] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[22]),
        .Q(\acc0_0_reg_130_reg_n_1_[22] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[23] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[23]),
        .Q(\acc0_0_reg_130_reg_n_1_[23] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[23]_i_1 
       (.CI(\acc0_0_reg_130_reg[19]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[23]_i_1_n_1 ,\acc0_0_reg_130_reg[23]_i_1_n_2 ,\acc0_0_reg_130_reg[23]_i_1_n_3 ,\acc0_0_reg_130_reg[23]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[23:20]),
        .O(acc0_fu_257_p2[23:20]),
        .S({\acc0_0_reg_130[23]_i_3_n_1 ,\acc0_0_reg_130[23]_i_4_n_1 ,\acc0_0_reg_130[23]_i_5_n_1 ,\acc0_0_reg_130[23]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[23]_i_2 
       (.CI(\acc0_0_reg_130_reg[19]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[23]_i_2_n_1 ,\acc0_0_reg_130_reg[23]_i_2_n_2 ,\acc0_0_reg_130_reg[23]_i_2_n_3 ,\acc0_0_reg_130_reg[23]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_100,mul_ln73_fu_251_p2__2_n_101,mul_ln73_fu_251_p2__2_n_102,mul_ln73_fu_251_p2__2_n_103}),
        .O(mul_ln73_fu_251_p2__3[23:20]),
        .S({\acc0_0_reg_130[23]_i_7_n_1 ,\acc0_0_reg_130[23]_i_8_n_1 ,\acc0_0_reg_130[23]_i_9_n_1 ,\acc0_0_reg_130[23]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[24] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[24]),
        .Q(\acc0_0_reg_130_reg_n_1_[24] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[25] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[25]),
        .Q(\acc0_0_reg_130_reg_n_1_[25] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[26] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[26]),
        .Q(\acc0_0_reg_130_reg_n_1_[26] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[27] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[27]),
        .Q(\acc0_0_reg_130_reg_n_1_[27] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[27]_i_1 
       (.CI(\acc0_0_reg_130_reg[23]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[27]_i_1_n_1 ,\acc0_0_reg_130_reg[27]_i_1_n_2 ,\acc0_0_reg_130_reg[27]_i_1_n_3 ,\acc0_0_reg_130_reg[27]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[27:24]),
        .O(acc0_fu_257_p2[27:24]),
        .S({\acc0_0_reg_130[27]_i_3_n_1 ,\acc0_0_reg_130[27]_i_4_n_1 ,\acc0_0_reg_130[27]_i_5_n_1 ,\acc0_0_reg_130[27]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[27]_i_2 
       (.CI(\acc0_0_reg_130_reg[23]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[27]_i_2_n_1 ,\acc0_0_reg_130_reg[27]_i_2_n_2 ,\acc0_0_reg_130_reg[27]_i_2_n_3 ,\acc0_0_reg_130_reg[27]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_96,mul_ln73_fu_251_p2__2_n_97,mul_ln73_fu_251_p2__2_n_98,mul_ln73_fu_251_p2__2_n_99}),
        .O(mul_ln73_fu_251_p2__3[27:24]),
        .S({\acc0_0_reg_130[27]_i_7_n_1 ,\acc0_0_reg_130[27]_i_8_n_1 ,\acc0_0_reg_130[27]_i_9_n_1 ,\acc0_0_reg_130[27]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[28] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[28]),
        .Q(\acc0_0_reg_130_reg_n_1_[28] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[29] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[29]),
        .Q(\acc0_0_reg_130_reg_n_1_[29] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[2]),
        .Q(\acc0_0_reg_130_reg_n_1_[2] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[30] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[30]),
        .Q(\acc0_0_reg_130_reg_n_1_[30] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[31] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[31]),
        .Q(\acc0_0_reg_130_reg_n_1_[31] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[31]_i_1 
       (.CI(\acc0_0_reg_130_reg[27]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[31]_i_1_n_1 ,\acc0_0_reg_130_reg[31]_i_1_n_2 ,\acc0_0_reg_130_reg[31]_i_1_n_3 ,\acc0_0_reg_130_reg[31]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[31:28]),
        .O(acc0_fu_257_p2[31:28]),
        .S({\acc0_0_reg_130[31]_i_3_n_1 ,\acc0_0_reg_130[31]_i_4_n_1 ,\acc0_0_reg_130[31]_i_5_n_1 ,\acc0_0_reg_130[31]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[31]_i_2 
       (.CI(\acc0_0_reg_130_reg[27]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[31]_i_2_n_1 ,\acc0_0_reg_130_reg[31]_i_2_n_2 ,\acc0_0_reg_130_reg[31]_i_2_n_3 ,\acc0_0_reg_130_reg[31]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_92,mul_ln73_fu_251_p2__2_n_93,mul_ln73_fu_251_p2__2_n_94,mul_ln73_fu_251_p2__2_n_95}),
        .O(mul_ln73_fu_251_p2__3[31:28]),
        .S({\acc0_0_reg_130[31]_i_7_n_1 ,\acc0_0_reg_130[31]_i_8_n_1 ,\acc0_0_reg_130[31]_i_9_n_1 ,\acc0_0_reg_130[31]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[32] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[32]),
        .Q(\acc0_0_reg_130_reg_n_1_[32] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[33] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[33]),
        .Q(\acc0_0_reg_130_reg_n_1_[33] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[34] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[34]),
        .Q(\acc0_0_reg_130_reg_n_1_[34] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[35] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[35]),
        .Q(\acc0_0_reg_130_reg_n_1_[35] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[35]_i_1 
       (.CI(\acc0_0_reg_130_reg[31]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[35]_i_1_n_1 ,\acc0_0_reg_130_reg[35]_i_1_n_2 ,\acc0_0_reg_130_reg[35]_i_1_n_3 ,\acc0_0_reg_130_reg[35]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[35:32]),
        .O(acc0_fu_257_p2[35:32]),
        .S({\acc0_0_reg_130[35]_i_3_n_1 ,\acc0_0_reg_130[35]_i_4_n_1 ,\acc0_0_reg_130[35]_i_5_n_1 ,\acc0_0_reg_130[35]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[35]_i_2 
       (.CI(\acc0_0_reg_130_reg[31]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[35]_i_2_n_1 ,\acc0_0_reg_130_reg[35]_i_2_n_2 ,\acc0_0_reg_130_reg[35]_i_2_n_3 ,\acc0_0_reg_130_reg[35]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_88,mul_ln73_fu_251_p2__2_n_89,mul_ln73_fu_251_p2__2_n_90,mul_ln73_fu_251_p2__2_n_91}),
        .O(mul_ln73_fu_251_p2__3[35:32]),
        .S({\acc0_0_reg_130[35]_i_7_n_1 ,\acc0_0_reg_130[35]_i_8_n_1 ,\acc0_0_reg_130[35]_i_9_n_1 ,\acc0_0_reg_130[35]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[36] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[36]),
        .Q(\acc0_0_reg_130_reg_n_1_[36] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[37] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[37]),
        .Q(\acc0_0_reg_130_reg_n_1_[37] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[38] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[38]),
        .Q(\acc0_0_reg_130_reg_n_1_[38] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[39] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[39]),
        .Q(\acc0_0_reg_130_reg_n_1_[39] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[39]_i_1 
       (.CI(\acc0_0_reg_130_reg[35]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[39]_i_1_n_1 ,\acc0_0_reg_130_reg[39]_i_1_n_2 ,\acc0_0_reg_130_reg[39]_i_1_n_3 ,\acc0_0_reg_130_reg[39]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[39:36]),
        .O(acc0_fu_257_p2[39:36]),
        .S({\acc0_0_reg_130[39]_i_3_n_1 ,\acc0_0_reg_130[39]_i_4_n_1 ,\acc0_0_reg_130[39]_i_5_n_1 ,\acc0_0_reg_130[39]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[39]_i_2 
       (.CI(\acc0_0_reg_130_reg[35]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[39]_i_2_n_1 ,\acc0_0_reg_130_reg[39]_i_2_n_2 ,\acc0_0_reg_130_reg[39]_i_2_n_3 ,\acc0_0_reg_130_reg[39]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_84,mul_ln73_fu_251_p2__2_n_85,mul_ln73_fu_251_p2__2_n_86,mul_ln73_fu_251_p2__2_n_87}),
        .O(mul_ln73_fu_251_p2__3[39:36]),
        .S({\acc0_0_reg_130[39]_i_7_n_1 ,\acc0_0_reg_130[39]_i_8_n_1 ,\acc0_0_reg_130[39]_i_9_n_1 ,\acc0_0_reg_130[39]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[3]),
        .Q(\acc0_0_reg_130_reg_n_1_[3] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\acc0_0_reg_130_reg[3]_i_1_n_1 ,\acc0_0_reg_130_reg[3]_i_1_n_2 ,\acc0_0_reg_130_reg[3]_i_1_n_3 ,\acc0_0_reg_130_reg[3]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__1_n_103,mul_ln73_fu_251_p2__1_n_104,mul_ln73_fu_251_p2__1_n_105,mul_ln73_fu_251_p2__1_n_106}),
        .O(acc0_fu_257_p2[3:0]),
        .S({\acc0_0_reg_130[3]_i_2_n_1 ,\acc0_0_reg_130[3]_i_3_n_1 ,\acc0_0_reg_130[3]_i_4_n_1 ,\acc0_0_reg_130[3]_i_5_n_1 }));
  FDRE \acc0_0_reg_130_reg[40] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[40]),
        .Q(\acc0_0_reg_130_reg_n_1_[40] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[41] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[41]),
        .Q(\acc0_0_reg_130_reg_n_1_[41] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[42] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[42]),
        .Q(\acc0_0_reg_130_reg_n_1_[42] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[43] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[43]),
        .Q(\acc0_0_reg_130_reg_n_1_[43] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[43]_i_1 
       (.CI(\acc0_0_reg_130_reg[39]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[43]_i_1_n_1 ,\acc0_0_reg_130_reg[43]_i_1_n_2 ,\acc0_0_reg_130_reg[43]_i_1_n_3 ,\acc0_0_reg_130_reg[43]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[43:40]),
        .O(acc0_fu_257_p2[43:40]),
        .S({\acc0_0_reg_130[43]_i_3_n_1 ,\acc0_0_reg_130[43]_i_4_n_1 ,\acc0_0_reg_130[43]_i_5_n_1 ,\acc0_0_reg_130[43]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[43]_i_2 
       (.CI(\acc0_0_reg_130_reg[39]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[43]_i_2_n_1 ,\acc0_0_reg_130_reg[43]_i_2_n_2 ,\acc0_0_reg_130_reg[43]_i_2_n_3 ,\acc0_0_reg_130_reg[43]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_80,mul_ln73_fu_251_p2__2_n_81,mul_ln73_fu_251_p2__2_n_82,mul_ln73_fu_251_p2__2_n_83}),
        .O(mul_ln73_fu_251_p2__3[43:40]),
        .S({\acc0_0_reg_130[43]_i_7_n_1 ,\acc0_0_reg_130[43]_i_8_n_1 ,\acc0_0_reg_130[43]_i_9_n_1 ,\acc0_0_reg_130[43]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[44] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[44]),
        .Q(\acc0_0_reg_130_reg_n_1_[44] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[45] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[45]),
        .Q(\acc0_0_reg_130_reg_n_1_[45] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[46] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[46]),
        .Q(\acc0_0_reg_130_reg_n_1_[46] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[47] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[47]),
        .Q(\acc0_0_reg_130_reg_n_1_[47] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[47]_i_1 
       (.CI(\acc0_0_reg_130_reg[43]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[47]_i_1_n_1 ,\acc0_0_reg_130_reg[47]_i_1_n_2 ,\acc0_0_reg_130_reg[47]_i_1_n_3 ,\acc0_0_reg_130_reg[47]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[47:44]),
        .O(acc0_fu_257_p2[47:44]),
        .S({\acc0_0_reg_130[47]_i_3_n_1 ,\acc0_0_reg_130[47]_i_4_n_1 ,\acc0_0_reg_130[47]_i_5_n_1 ,\acc0_0_reg_130[47]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[47]_i_2 
       (.CI(\acc0_0_reg_130_reg[43]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[47]_i_2_n_1 ,\acc0_0_reg_130_reg[47]_i_2_n_2 ,\acc0_0_reg_130_reg[47]_i_2_n_3 ,\acc0_0_reg_130_reg[47]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_76,mul_ln73_fu_251_p2__2_n_77,mul_ln73_fu_251_p2__2_n_78,mul_ln73_fu_251_p2__2_n_79}),
        .O(mul_ln73_fu_251_p2__3[47:44]),
        .S({\acc0_0_reg_130[47]_i_7_n_1 ,\acc0_0_reg_130[47]_i_8_n_1 ,\acc0_0_reg_130[47]_i_9_n_1 ,\acc0_0_reg_130[47]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[48] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[48]),
        .Q(\acc0_0_reg_130_reg_n_1_[48] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[49] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[49]),
        .Q(\acc0_0_reg_130_reg_n_1_[49] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[4]),
        .Q(\acc0_0_reg_130_reg_n_1_[4] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[50] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[50]),
        .Q(\acc0_0_reg_130_reg_n_1_[50] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[51] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[51]),
        .Q(\acc0_0_reg_130_reg_n_1_[51] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[51]_i_1 
       (.CI(\acc0_0_reg_130_reg[47]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[51]_i_1_n_1 ,\acc0_0_reg_130_reg[51]_i_1_n_2 ,\acc0_0_reg_130_reg[51]_i_1_n_3 ,\acc0_0_reg_130_reg[51]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[51:48]),
        .O(acc0_fu_257_p2[51:48]),
        .S({\acc0_0_reg_130[51]_i_3_n_1 ,\acc0_0_reg_130[51]_i_4_n_1 ,\acc0_0_reg_130[51]_i_5_n_1 ,\acc0_0_reg_130[51]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[51]_i_2 
       (.CI(\acc0_0_reg_130_reg[47]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[51]_i_2_n_1 ,\acc0_0_reg_130_reg[51]_i_2_n_2 ,\acc0_0_reg_130_reg[51]_i_2_n_3 ,\acc0_0_reg_130_reg[51]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_72,mul_ln73_fu_251_p2__2_n_73,mul_ln73_fu_251_p2__2_n_74,mul_ln73_fu_251_p2__2_n_75}),
        .O(mul_ln73_fu_251_p2__3[51:48]),
        .S({\acc0_0_reg_130[51]_i_7_n_1 ,\acc0_0_reg_130[51]_i_8_n_1 ,\acc0_0_reg_130[51]_i_9_n_1 ,\acc0_0_reg_130[51]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[52] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[52]),
        .Q(\acc0_0_reg_130_reg_n_1_[52] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[53] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[53]),
        .Q(\acc0_0_reg_130_reg_n_1_[53] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[54] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[54]),
        .Q(\acc0_0_reg_130_reg_n_1_[54] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[55] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[55]),
        .Q(\acc0_0_reg_130_reg_n_1_[55] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[55]_i_1 
       (.CI(\acc0_0_reg_130_reg[51]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[55]_i_1_n_1 ,\acc0_0_reg_130_reg[55]_i_1_n_2 ,\acc0_0_reg_130_reg[55]_i_1_n_3 ,\acc0_0_reg_130_reg[55]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[55:52]),
        .O(acc0_fu_257_p2[55:52]),
        .S({\acc0_0_reg_130[55]_i_3_n_1 ,\acc0_0_reg_130[55]_i_4_n_1 ,\acc0_0_reg_130[55]_i_5_n_1 ,\acc0_0_reg_130[55]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[55]_i_2 
       (.CI(\acc0_0_reg_130_reg[51]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[55]_i_2_n_1 ,\acc0_0_reg_130_reg[55]_i_2_n_2 ,\acc0_0_reg_130_reg[55]_i_2_n_3 ,\acc0_0_reg_130_reg[55]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_68,mul_ln73_fu_251_p2__2_n_69,mul_ln73_fu_251_p2__2_n_70,mul_ln73_fu_251_p2__2_n_71}),
        .O(mul_ln73_fu_251_p2__3[55:52]),
        .S({\acc0_0_reg_130[55]_i_7_n_1 ,\acc0_0_reg_130[55]_i_8_n_1 ,\acc0_0_reg_130[55]_i_9_n_1 ,\acc0_0_reg_130[55]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[56] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[56]),
        .Q(\acc0_0_reg_130_reg_n_1_[56] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[57] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[57]),
        .Q(\acc0_0_reg_130_reg_n_1_[57] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[58] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[58]),
        .Q(\acc0_0_reg_130_reg_n_1_[58] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[59] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[59]),
        .Q(\acc0_0_reg_130_reg_n_1_[59] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[59]_i_1 
       (.CI(\acc0_0_reg_130_reg[55]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[59]_i_1_n_1 ,\acc0_0_reg_130_reg[59]_i_1_n_2 ,\acc0_0_reg_130_reg[59]_i_1_n_3 ,\acc0_0_reg_130_reg[59]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln73_fu_251_p2__3[59:56]),
        .O(acc0_fu_257_p2[59:56]),
        .S({\acc0_0_reg_130[59]_i_3_n_1 ,\acc0_0_reg_130[59]_i_4_n_1 ,\acc0_0_reg_130[59]_i_5_n_1 ,\acc0_0_reg_130[59]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[59]_i_2 
       (.CI(\acc0_0_reg_130_reg[55]_i_2_n_1 ),
        .CO({\acc0_0_reg_130_reg[59]_i_2_n_1 ,\acc0_0_reg_130_reg[59]_i_2_n_2 ,\acc0_0_reg_130_reg[59]_i_2_n_3 ,\acc0_0_reg_130_reg[59]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__2_n_64,mul_ln73_fu_251_p2__2_n_65,mul_ln73_fu_251_p2__2_n_66,mul_ln73_fu_251_p2__2_n_67}),
        .O(mul_ln73_fu_251_p2__3[59:56]),
        .S({\acc0_0_reg_130[59]_i_7_n_1 ,\acc0_0_reg_130[59]_i_8_n_1 ,\acc0_0_reg_130[59]_i_9_n_1 ,\acc0_0_reg_130[59]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[5]),
        .Q(\acc0_0_reg_130_reg_n_1_[5] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[60] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[60]),
        .Q(\acc0_0_reg_130_reg_n_1_[60] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[61] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[61]),
        .Q(\acc0_0_reg_130_reg_n_1_[61] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[62] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[62]),
        .Q(\acc0_0_reg_130_reg_n_1_[62] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[63] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[63]),
        .Q(\acc0_0_reg_130_reg_n_1_[63] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[63]_i_1 
       (.CI(\acc0_0_reg_130_reg[59]_i_1_n_1 ),
        .CO({\NLW_acc0_0_reg_130_reg[63]_i_1_CO_UNCONNECTED [3],\acc0_0_reg_130_reg[63]_i_1_n_2 ,\acc0_0_reg_130_reg[63]_i_1_n_3 ,\acc0_0_reg_130_reg[63]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln73_fu_251_p2__3[62:60]}),
        .O(acc0_fu_257_p2[63:60]),
        .S({\acc0_0_reg_130[63]_i_3_n_1 ,\acc0_0_reg_130[63]_i_4_n_1 ,\acc0_0_reg_130[63]_i_5_n_1 ,\acc0_0_reg_130[63]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[63]_i_2 
       (.CI(\acc0_0_reg_130_reg[59]_i_2_n_1 ),
        .CO({\NLW_acc0_0_reg_130_reg[63]_i_2_CO_UNCONNECTED [3],\acc0_0_reg_130_reg[63]_i_2_n_2 ,\acc0_0_reg_130_reg[63]_i_2_n_3 ,\acc0_0_reg_130_reg[63]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln73_fu_251_p2__2_n_61,mul_ln73_fu_251_p2__2_n_62,mul_ln73_fu_251_p2__2_n_63}),
        .O(mul_ln73_fu_251_p2__3[63:60]),
        .S({\acc0_0_reg_130[63]_i_7_n_1 ,\acc0_0_reg_130[63]_i_8_n_1 ,\acc0_0_reg_130[63]_i_9_n_1 ,\acc0_0_reg_130[63]_i_10_n_1 }));
  FDRE \acc0_0_reg_130_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[6]),
        .Q(\acc0_0_reg_130_reg_n_1_[6] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[7]),
        .Q(\acc0_0_reg_130_reg_n_1_[7] ),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc0_0_reg_130_reg[7]_i_1 
       (.CI(\acc0_0_reg_130_reg[3]_i_1_n_1 ),
        .CO({\acc0_0_reg_130_reg[7]_i_1_n_1 ,\acc0_0_reg_130_reg[7]_i_1_n_2 ,\acc0_0_reg_130_reg[7]_i_1_n_3 ,\acc0_0_reg_130_reg[7]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln73_fu_251_p2__1_n_99,mul_ln73_fu_251_p2__1_n_100,mul_ln73_fu_251_p2__1_n_101,mul_ln73_fu_251_p2__1_n_102}),
        .O(acc0_fu_257_p2[7:4]),
        .S({\acc0_0_reg_130[7]_i_2_n_1 ,\acc0_0_reg_130[7]_i_3_n_1 ,\acc0_0_reg_130[7]_i_4_n_1 ,\acc0_0_reg_130[7]_i_5_n_1 }));
  FDRE \acc0_0_reg_130_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[8]),
        .Q(\acc0_0_reg_130_reg_n_1_[8] ),
        .R(acc0_0_reg_130));
  FDRE \acc0_0_reg_130_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc0_fu_257_p2[9]),
        .Q(\acc0_0_reg_130_reg_n_1_[9] ),
        .R(acc0_0_reg_130));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[11]_i_2 
       (.I0(mul_ln74_fu_267_p2__1_n_95),
        .I1(acc1_0_reg_142[11]),
        .O(\acc1_0_reg_142[11]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[11]_i_3 
       (.I0(mul_ln74_fu_267_p2__1_n_96),
        .I1(acc1_0_reg_142[10]),
        .O(\acc1_0_reg_142[11]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[11]_i_4 
       (.I0(mul_ln74_fu_267_p2__1_n_97),
        .I1(acc1_0_reg_142[9]),
        .O(\acc1_0_reg_142[11]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[11]_i_5 
       (.I0(mul_ln74_fu_267_p2__1_n_98),
        .I1(acc1_0_reg_142[8]),
        .O(\acc1_0_reg_142[11]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[15]_i_2 
       (.I0(mul_ln74_fu_267_p2__1_n_91),
        .I1(acc1_0_reg_142[15]),
        .O(\acc1_0_reg_142[15]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[15]_i_3 
       (.I0(mul_ln74_fu_267_p2__1_n_92),
        .I1(acc1_0_reg_142[14]),
        .O(\acc1_0_reg_142[15]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[15]_i_4 
       (.I0(mul_ln74_fu_267_p2__1_n_93),
        .I1(acc1_0_reg_142[13]),
        .O(\acc1_0_reg_142[15]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[15]_i_5 
       (.I0(mul_ln74_fu_267_p2__1_n_94),
        .I1(acc1_0_reg_142[12]),
        .O(\acc1_0_reg_142[15]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[19]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[19]),
        .I1(acc1_0_reg_142[19]),
        .O(\acc1_0_reg_142[19]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[19]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[18]),
        .I1(acc1_0_reg_142[18]),
        .O(\acc1_0_reg_142[19]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[19]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[17]),
        .I1(acc1_0_reg_142[17]),
        .O(\acc1_0_reg_142[19]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[19]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[16]),
        .I1(acc1_0_reg_142[16]),
        .O(\acc1_0_reg_142[19]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[19]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_104),
        .I1(mul_ln74_fu_267_p2_n_104),
        .O(\acc1_0_reg_142[19]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[19]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_105),
        .I1(mul_ln74_fu_267_p2_n_105),
        .O(\acc1_0_reg_142[19]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[19]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_106),
        .I1(mul_ln74_fu_267_p2_n_106),
        .O(\acc1_0_reg_142[19]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_103),
        .I1(mul_ln74_fu_267_p2_n_103),
        .O(\acc1_0_reg_142[23]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[23]),
        .I1(acc1_0_reg_142[23]),
        .O(\acc1_0_reg_142[23]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[22]),
        .I1(acc1_0_reg_142[22]),
        .O(\acc1_0_reg_142[23]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[21]),
        .I1(acc1_0_reg_142[21]),
        .O(\acc1_0_reg_142[23]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[20]),
        .I1(acc1_0_reg_142[20]),
        .O(\acc1_0_reg_142[23]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_100),
        .I1(mul_ln74_fu_267_p2_n_100),
        .O(\acc1_0_reg_142[23]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_101),
        .I1(mul_ln74_fu_267_p2_n_101),
        .O(\acc1_0_reg_142[23]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[23]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_102),
        .I1(mul_ln74_fu_267_p2_n_102),
        .O(\acc1_0_reg_142[23]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_99),
        .I1(mul_ln74_fu_267_p2_n_99),
        .O(\acc1_0_reg_142[27]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[27]),
        .I1(acc1_0_reg_142[27]),
        .O(\acc1_0_reg_142[27]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[26]),
        .I1(acc1_0_reg_142[26]),
        .O(\acc1_0_reg_142[27]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[25]),
        .I1(acc1_0_reg_142[25]),
        .O(\acc1_0_reg_142[27]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[24]),
        .I1(acc1_0_reg_142[24]),
        .O(\acc1_0_reg_142[27]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_96),
        .I1(mul_ln74_fu_267_p2_n_96),
        .O(\acc1_0_reg_142[27]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_97),
        .I1(mul_ln74_fu_267_p2_n_97),
        .O(\acc1_0_reg_142[27]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[27]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_98),
        .I1(mul_ln74_fu_267_p2_n_98),
        .O(\acc1_0_reg_142[27]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_95),
        .I1(mul_ln74_fu_267_p2_n_95),
        .O(\acc1_0_reg_142[31]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[31]),
        .I1(acc1_0_reg_142[31]),
        .O(\acc1_0_reg_142[31]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[30]),
        .I1(acc1_0_reg_142[30]),
        .O(\acc1_0_reg_142[31]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[29]),
        .I1(acc1_0_reg_142[29]),
        .O(\acc1_0_reg_142[31]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[28]),
        .I1(acc1_0_reg_142[28]),
        .O(\acc1_0_reg_142[31]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_92),
        .I1(mul_ln74_fu_267_p2_n_92),
        .O(\acc1_0_reg_142[31]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_93),
        .I1(mul_ln74_fu_267_p2_n_93),
        .O(\acc1_0_reg_142[31]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[31]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_94),
        .I1(mul_ln74_fu_267_p2_n_94),
        .O(\acc1_0_reg_142[31]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_91),
        .I1(mul_ln74_fu_267_p2_n_91),
        .O(\acc1_0_reg_142[35]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[35]),
        .I1(acc1_0_reg_142[35]),
        .O(\acc1_0_reg_142[35]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[34]),
        .I1(acc1_0_reg_142[34]),
        .O(\acc1_0_reg_142[35]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[33]),
        .I1(acc1_0_reg_142[33]),
        .O(\acc1_0_reg_142[35]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[32]),
        .I1(acc1_0_reg_142[32]),
        .O(\acc1_0_reg_142[35]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_88),
        .I1(mul_ln74_fu_267_p2__0_n_105),
        .O(\acc1_0_reg_142[35]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_89),
        .I1(mul_ln74_fu_267_p2__0_n_106),
        .O(\acc1_0_reg_142[35]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[35]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_90),
        .I1(mul_ln74_fu_267_p2_n_90),
        .O(\acc1_0_reg_142[35]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_87),
        .I1(mul_ln74_fu_267_p2__0_n_104),
        .O(\acc1_0_reg_142[39]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[39]),
        .I1(acc1_0_reg_142[39]),
        .O(\acc1_0_reg_142[39]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[38]),
        .I1(acc1_0_reg_142[38]),
        .O(\acc1_0_reg_142[39]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[37]),
        .I1(acc1_0_reg_142[37]),
        .O(\acc1_0_reg_142[39]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[36]),
        .I1(acc1_0_reg_142[36]),
        .O(\acc1_0_reg_142[39]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_84),
        .I1(mul_ln74_fu_267_p2__0_n_101),
        .O(\acc1_0_reg_142[39]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_85),
        .I1(mul_ln74_fu_267_p2__0_n_102),
        .O(\acc1_0_reg_142[39]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[39]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_86),
        .I1(mul_ln74_fu_267_p2__0_n_103),
        .O(\acc1_0_reg_142[39]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[3]_i_2 
       (.I0(mul_ln74_fu_267_p2__1_n_103),
        .I1(acc1_0_reg_142[3]),
        .O(\acc1_0_reg_142[3]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[3]_i_3 
       (.I0(mul_ln74_fu_267_p2__1_n_104),
        .I1(acc1_0_reg_142[2]),
        .O(\acc1_0_reg_142[3]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[3]_i_4 
       (.I0(mul_ln74_fu_267_p2__1_n_105),
        .I1(acc1_0_reg_142[1]),
        .O(\acc1_0_reg_142[3]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[3]_i_5 
       (.I0(mul_ln74_fu_267_p2__1_n_106),
        .I1(acc1_0_reg_142[0]),
        .O(\acc1_0_reg_142[3]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_83),
        .I1(mul_ln74_fu_267_p2__0_n_100),
        .O(\acc1_0_reg_142[43]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[43]),
        .I1(acc1_0_reg_142[43]),
        .O(\acc1_0_reg_142[43]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[42]),
        .I1(acc1_0_reg_142[42]),
        .O(\acc1_0_reg_142[43]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[41]),
        .I1(acc1_0_reg_142[41]),
        .O(\acc1_0_reg_142[43]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[40]),
        .I1(acc1_0_reg_142[40]),
        .O(\acc1_0_reg_142[43]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_80),
        .I1(mul_ln74_fu_267_p2__0_n_97),
        .O(\acc1_0_reg_142[43]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_81),
        .I1(mul_ln74_fu_267_p2__0_n_98),
        .O(\acc1_0_reg_142[43]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[43]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_82),
        .I1(mul_ln74_fu_267_p2__0_n_99),
        .O(\acc1_0_reg_142[43]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_79),
        .I1(mul_ln74_fu_267_p2__0_n_96),
        .O(\acc1_0_reg_142[47]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[47]),
        .I1(acc1_0_reg_142[47]),
        .O(\acc1_0_reg_142[47]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[46]),
        .I1(acc1_0_reg_142[46]),
        .O(\acc1_0_reg_142[47]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[45]),
        .I1(acc1_0_reg_142[45]),
        .O(\acc1_0_reg_142[47]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[44]),
        .I1(acc1_0_reg_142[44]),
        .O(\acc1_0_reg_142[47]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_76),
        .I1(mul_ln74_fu_267_p2__0_n_93),
        .O(\acc1_0_reg_142[47]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_77),
        .I1(mul_ln74_fu_267_p2__0_n_94),
        .O(\acc1_0_reg_142[47]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[47]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_78),
        .I1(mul_ln74_fu_267_p2__0_n_95),
        .O(\acc1_0_reg_142[47]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_75),
        .I1(mul_ln74_fu_267_p2__0_n_92),
        .O(\acc1_0_reg_142[51]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[51]),
        .I1(acc1_0_reg_142[51]),
        .O(\acc1_0_reg_142[51]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[50]),
        .I1(acc1_0_reg_142[50]),
        .O(\acc1_0_reg_142[51]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[49]),
        .I1(acc1_0_reg_142[49]),
        .O(\acc1_0_reg_142[51]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[48]),
        .I1(acc1_0_reg_142[48]),
        .O(\acc1_0_reg_142[51]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_72),
        .I1(mul_ln74_fu_267_p2__0_n_89),
        .O(\acc1_0_reg_142[51]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_73),
        .I1(mul_ln74_fu_267_p2__0_n_90),
        .O(\acc1_0_reg_142[51]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[51]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_74),
        .I1(mul_ln74_fu_267_p2__0_n_91),
        .O(\acc1_0_reg_142[51]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_71),
        .I1(mul_ln74_fu_267_p2__0_n_88),
        .O(\acc1_0_reg_142[55]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[55]),
        .I1(acc1_0_reg_142[55]),
        .O(\acc1_0_reg_142[55]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[54]),
        .I1(acc1_0_reg_142[54]),
        .O(\acc1_0_reg_142[55]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[53]),
        .I1(acc1_0_reg_142[53]),
        .O(\acc1_0_reg_142[55]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[52]),
        .I1(acc1_0_reg_142[52]),
        .O(\acc1_0_reg_142[55]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_68),
        .I1(mul_ln74_fu_267_p2__0_n_85),
        .O(\acc1_0_reg_142[55]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_69),
        .I1(mul_ln74_fu_267_p2__0_n_86),
        .O(\acc1_0_reg_142[55]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[55]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_70),
        .I1(mul_ln74_fu_267_p2__0_n_87),
        .O(\acc1_0_reg_142[55]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_67),
        .I1(mul_ln74_fu_267_p2__0_n_84),
        .O(\acc1_0_reg_142[59]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[59]),
        .I1(acc1_0_reg_142[59]),
        .O(\acc1_0_reg_142[59]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[58]),
        .I1(acc1_0_reg_142[58]),
        .O(\acc1_0_reg_142[59]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[57]),
        .I1(acc1_0_reg_142[57]),
        .O(\acc1_0_reg_142[59]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[56]),
        .I1(acc1_0_reg_142[56]),
        .O(\acc1_0_reg_142[59]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_64),
        .I1(mul_ln74_fu_267_p2__0_n_81),
        .O(\acc1_0_reg_142[59]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_65),
        .I1(mul_ln74_fu_267_p2__0_n_82),
        .O(\acc1_0_reg_142[59]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[59]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_66),
        .I1(mul_ln74_fu_267_p2__0_n_83),
        .O(\acc1_0_reg_142[59]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_10 
       (.I0(mul_ln74_fu_267_p2__2_n_63),
        .I1(mul_ln74_fu_267_p2__0_n_80),
        .O(\acc1_0_reg_142[63]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_3 
       (.I0(mul_ln74_fu_267_p2__3[63]),
        .I1(acc1_0_reg_142[63]),
        .O(\acc1_0_reg_142[63]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_4 
       (.I0(mul_ln74_fu_267_p2__3[62]),
        .I1(acc1_0_reg_142[62]),
        .O(\acc1_0_reg_142[63]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_5 
       (.I0(mul_ln74_fu_267_p2__3[61]),
        .I1(acc1_0_reg_142[61]),
        .O(\acc1_0_reg_142[63]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_6 
       (.I0(mul_ln74_fu_267_p2__3[60]),
        .I1(acc1_0_reg_142[60]),
        .O(\acc1_0_reg_142[63]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_7 
       (.I0(mul_ln74_fu_267_p2__2_n_60),
        .I1(mul_ln74_fu_267_p2__0_n_77),
        .O(\acc1_0_reg_142[63]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_8 
       (.I0(mul_ln74_fu_267_p2__2_n_61),
        .I1(mul_ln74_fu_267_p2__0_n_78),
        .O(\acc1_0_reg_142[63]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[63]_i_9 
       (.I0(mul_ln74_fu_267_p2__2_n_62),
        .I1(mul_ln74_fu_267_p2__0_n_79),
        .O(\acc1_0_reg_142[63]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[7]_i_2 
       (.I0(mul_ln74_fu_267_p2__1_n_99),
        .I1(acc1_0_reg_142[7]),
        .O(\acc1_0_reg_142[7]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[7]_i_3 
       (.I0(mul_ln74_fu_267_p2__1_n_100),
        .I1(acc1_0_reg_142[6]),
        .O(\acc1_0_reg_142[7]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[7]_i_4 
       (.I0(mul_ln74_fu_267_p2__1_n_101),
        .I1(acc1_0_reg_142[5]),
        .O(\acc1_0_reg_142[7]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc1_0_reg_142[7]_i_5 
       (.I0(mul_ln74_fu_267_p2__1_n_102),
        .I1(acc1_0_reg_142[4]),
        .O(\acc1_0_reg_142[7]_i_5_n_1 ));
  FDRE \acc1_0_reg_142_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[0]),
        .Q(acc1_0_reg_142[0]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[10]),
        .Q(acc1_0_reg_142[10]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[11] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[11]),
        .Q(acc1_0_reg_142[11]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[11]_i_1 
       (.CI(\acc1_0_reg_142_reg[7]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[11]_i_1_n_1 ,\acc1_0_reg_142_reg[11]_i_1_n_2 ,\acc1_0_reg_142_reg[11]_i_1_n_3 ,\acc1_0_reg_142_reg[11]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__1_n_95,mul_ln74_fu_267_p2__1_n_96,mul_ln74_fu_267_p2__1_n_97,mul_ln74_fu_267_p2__1_n_98}),
        .O(acc1_fu_273_p2[11:8]),
        .S({\acc1_0_reg_142[11]_i_2_n_1 ,\acc1_0_reg_142[11]_i_3_n_1 ,\acc1_0_reg_142[11]_i_4_n_1 ,\acc1_0_reg_142[11]_i_5_n_1 }));
  FDRE \acc1_0_reg_142_reg[12] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[12]),
        .Q(acc1_0_reg_142[12]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[13] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[13]),
        .Q(acc1_0_reg_142[13]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[14] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[14]),
        .Q(acc1_0_reg_142[14]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[15] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[15]),
        .Q(acc1_0_reg_142[15]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[15]_i_1 
       (.CI(\acc1_0_reg_142_reg[11]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[15]_i_1_n_1 ,\acc1_0_reg_142_reg[15]_i_1_n_2 ,\acc1_0_reg_142_reg[15]_i_1_n_3 ,\acc1_0_reg_142_reg[15]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__1_n_91,mul_ln74_fu_267_p2__1_n_92,mul_ln74_fu_267_p2__1_n_93,mul_ln74_fu_267_p2__1_n_94}),
        .O(acc1_fu_273_p2[15:12]),
        .S({\acc1_0_reg_142[15]_i_2_n_1 ,\acc1_0_reg_142[15]_i_3_n_1 ,\acc1_0_reg_142[15]_i_4_n_1 ,\acc1_0_reg_142[15]_i_5_n_1 }));
  FDRE \acc1_0_reg_142_reg[16] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[16]),
        .Q(acc1_0_reg_142[16]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[17] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[17]),
        .Q(acc1_0_reg_142[17]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[18] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[18]),
        .Q(acc1_0_reg_142[18]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[19] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[19]),
        .Q(acc1_0_reg_142[19]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[19]_i_1 
       (.CI(\acc1_0_reg_142_reg[15]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[19]_i_1_n_1 ,\acc1_0_reg_142_reg[19]_i_1_n_2 ,\acc1_0_reg_142_reg[19]_i_1_n_3 ,\acc1_0_reg_142_reg[19]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[19:16]),
        .O(acc1_fu_273_p2[19:16]),
        .S({\acc1_0_reg_142[19]_i_3_n_1 ,\acc1_0_reg_142[19]_i_4_n_1 ,\acc1_0_reg_142[19]_i_5_n_1 ,\acc1_0_reg_142[19]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[19]_i_2 
       (.CI(1'b0),
        .CO({\acc1_0_reg_142_reg[19]_i_2_n_1 ,\acc1_0_reg_142_reg[19]_i_2_n_2 ,\acc1_0_reg_142_reg[19]_i_2_n_3 ,\acc1_0_reg_142_reg[19]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_104,mul_ln74_fu_267_p2__2_n_105,mul_ln74_fu_267_p2__2_n_106,1'b0}),
        .O(mul_ln74_fu_267_p2__3[19:16]),
        .S({\acc1_0_reg_142[19]_i_7_n_1 ,\acc1_0_reg_142[19]_i_8_n_1 ,\acc1_0_reg_142[19]_i_9_n_1 ,mul_ln74_fu_267_p2__1_n_90}));
  FDRE \acc1_0_reg_142_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[1]),
        .Q(acc1_0_reg_142[1]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[20] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[20]),
        .Q(acc1_0_reg_142[20]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[21] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[21]),
        .Q(acc1_0_reg_142[21]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[22] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[22]),
        .Q(acc1_0_reg_142[22]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[23] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[23]),
        .Q(acc1_0_reg_142[23]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[23]_i_1 
       (.CI(\acc1_0_reg_142_reg[19]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[23]_i_1_n_1 ,\acc1_0_reg_142_reg[23]_i_1_n_2 ,\acc1_0_reg_142_reg[23]_i_1_n_3 ,\acc1_0_reg_142_reg[23]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[23:20]),
        .O(acc1_fu_273_p2[23:20]),
        .S({\acc1_0_reg_142[23]_i_3_n_1 ,\acc1_0_reg_142[23]_i_4_n_1 ,\acc1_0_reg_142[23]_i_5_n_1 ,\acc1_0_reg_142[23]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[23]_i_2 
       (.CI(\acc1_0_reg_142_reg[19]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[23]_i_2_n_1 ,\acc1_0_reg_142_reg[23]_i_2_n_2 ,\acc1_0_reg_142_reg[23]_i_2_n_3 ,\acc1_0_reg_142_reg[23]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_100,mul_ln74_fu_267_p2__2_n_101,mul_ln74_fu_267_p2__2_n_102,mul_ln74_fu_267_p2__2_n_103}),
        .O(mul_ln74_fu_267_p2__3[23:20]),
        .S({\acc1_0_reg_142[23]_i_7_n_1 ,\acc1_0_reg_142[23]_i_8_n_1 ,\acc1_0_reg_142[23]_i_9_n_1 ,\acc1_0_reg_142[23]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[24] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[24]),
        .Q(acc1_0_reg_142[24]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[25] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[25]),
        .Q(acc1_0_reg_142[25]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[26] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[26]),
        .Q(acc1_0_reg_142[26]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[27] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[27]),
        .Q(acc1_0_reg_142[27]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[27]_i_1 
       (.CI(\acc1_0_reg_142_reg[23]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[27]_i_1_n_1 ,\acc1_0_reg_142_reg[27]_i_1_n_2 ,\acc1_0_reg_142_reg[27]_i_1_n_3 ,\acc1_0_reg_142_reg[27]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[27:24]),
        .O(acc1_fu_273_p2[27:24]),
        .S({\acc1_0_reg_142[27]_i_3_n_1 ,\acc1_0_reg_142[27]_i_4_n_1 ,\acc1_0_reg_142[27]_i_5_n_1 ,\acc1_0_reg_142[27]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[27]_i_2 
       (.CI(\acc1_0_reg_142_reg[23]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[27]_i_2_n_1 ,\acc1_0_reg_142_reg[27]_i_2_n_2 ,\acc1_0_reg_142_reg[27]_i_2_n_3 ,\acc1_0_reg_142_reg[27]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_96,mul_ln74_fu_267_p2__2_n_97,mul_ln74_fu_267_p2__2_n_98,mul_ln74_fu_267_p2__2_n_99}),
        .O(mul_ln74_fu_267_p2__3[27:24]),
        .S({\acc1_0_reg_142[27]_i_7_n_1 ,\acc1_0_reg_142[27]_i_8_n_1 ,\acc1_0_reg_142[27]_i_9_n_1 ,\acc1_0_reg_142[27]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[28] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[28]),
        .Q(acc1_0_reg_142[28]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[29] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[29]),
        .Q(acc1_0_reg_142[29]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[2]),
        .Q(acc1_0_reg_142[2]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[30] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[30]),
        .Q(acc1_0_reg_142[30]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[31] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[31]),
        .Q(acc1_0_reg_142[31]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[31]_i_1 
       (.CI(\acc1_0_reg_142_reg[27]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[31]_i_1_n_1 ,\acc1_0_reg_142_reg[31]_i_1_n_2 ,\acc1_0_reg_142_reg[31]_i_1_n_3 ,\acc1_0_reg_142_reg[31]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[31:28]),
        .O(acc1_fu_273_p2[31:28]),
        .S({\acc1_0_reg_142[31]_i_3_n_1 ,\acc1_0_reg_142[31]_i_4_n_1 ,\acc1_0_reg_142[31]_i_5_n_1 ,\acc1_0_reg_142[31]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[31]_i_2 
       (.CI(\acc1_0_reg_142_reg[27]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[31]_i_2_n_1 ,\acc1_0_reg_142_reg[31]_i_2_n_2 ,\acc1_0_reg_142_reg[31]_i_2_n_3 ,\acc1_0_reg_142_reg[31]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_92,mul_ln74_fu_267_p2__2_n_93,mul_ln74_fu_267_p2__2_n_94,mul_ln74_fu_267_p2__2_n_95}),
        .O(mul_ln74_fu_267_p2__3[31:28]),
        .S({\acc1_0_reg_142[31]_i_7_n_1 ,\acc1_0_reg_142[31]_i_8_n_1 ,\acc1_0_reg_142[31]_i_9_n_1 ,\acc1_0_reg_142[31]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[32] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[32]),
        .Q(acc1_0_reg_142[32]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[33] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[33]),
        .Q(acc1_0_reg_142[33]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[34] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[34]),
        .Q(acc1_0_reg_142[34]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[35] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[35]),
        .Q(acc1_0_reg_142[35]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[35]_i_1 
       (.CI(\acc1_0_reg_142_reg[31]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[35]_i_1_n_1 ,\acc1_0_reg_142_reg[35]_i_1_n_2 ,\acc1_0_reg_142_reg[35]_i_1_n_3 ,\acc1_0_reg_142_reg[35]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[35:32]),
        .O(acc1_fu_273_p2[35:32]),
        .S({\acc1_0_reg_142[35]_i_3_n_1 ,\acc1_0_reg_142[35]_i_4_n_1 ,\acc1_0_reg_142[35]_i_5_n_1 ,\acc1_0_reg_142[35]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[35]_i_2 
       (.CI(\acc1_0_reg_142_reg[31]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[35]_i_2_n_1 ,\acc1_0_reg_142_reg[35]_i_2_n_2 ,\acc1_0_reg_142_reg[35]_i_2_n_3 ,\acc1_0_reg_142_reg[35]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_88,mul_ln74_fu_267_p2__2_n_89,mul_ln74_fu_267_p2__2_n_90,mul_ln74_fu_267_p2__2_n_91}),
        .O(mul_ln74_fu_267_p2__3[35:32]),
        .S({\acc1_0_reg_142[35]_i_7_n_1 ,\acc1_0_reg_142[35]_i_8_n_1 ,\acc1_0_reg_142[35]_i_9_n_1 ,\acc1_0_reg_142[35]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[36] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[36]),
        .Q(acc1_0_reg_142[36]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[37] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[37]),
        .Q(acc1_0_reg_142[37]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[38] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[38]),
        .Q(acc1_0_reg_142[38]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[39] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[39]),
        .Q(acc1_0_reg_142[39]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[39]_i_1 
       (.CI(\acc1_0_reg_142_reg[35]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[39]_i_1_n_1 ,\acc1_0_reg_142_reg[39]_i_1_n_2 ,\acc1_0_reg_142_reg[39]_i_1_n_3 ,\acc1_0_reg_142_reg[39]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[39:36]),
        .O(acc1_fu_273_p2[39:36]),
        .S({\acc1_0_reg_142[39]_i_3_n_1 ,\acc1_0_reg_142[39]_i_4_n_1 ,\acc1_0_reg_142[39]_i_5_n_1 ,\acc1_0_reg_142[39]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[39]_i_2 
       (.CI(\acc1_0_reg_142_reg[35]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[39]_i_2_n_1 ,\acc1_0_reg_142_reg[39]_i_2_n_2 ,\acc1_0_reg_142_reg[39]_i_2_n_3 ,\acc1_0_reg_142_reg[39]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_84,mul_ln74_fu_267_p2__2_n_85,mul_ln74_fu_267_p2__2_n_86,mul_ln74_fu_267_p2__2_n_87}),
        .O(mul_ln74_fu_267_p2__3[39:36]),
        .S({\acc1_0_reg_142[39]_i_7_n_1 ,\acc1_0_reg_142[39]_i_8_n_1 ,\acc1_0_reg_142[39]_i_9_n_1 ,\acc1_0_reg_142[39]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[3]),
        .Q(acc1_0_reg_142[3]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\acc1_0_reg_142_reg[3]_i_1_n_1 ,\acc1_0_reg_142_reg[3]_i_1_n_2 ,\acc1_0_reg_142_reg[3]_i_1_n_3 ,\acc1_0_reg_142_reg[3]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__1_n_103,mul_ln74_fu_267_p2__1_n_104,mul_ln74_fu_267_p2__1_n_105,mul_ln74_fu_267_p2__1_n_106}),
        .O(acc1_fu_273_p2[3:0]),
        .S({\acc1_0_reg_142[3]_i_2_n_1 ,\acc1_0_reg_142[3]_i_3_n_1 ,\acc1_0_reg_142[3]_i_4_n_1 ,\acc1_0_reg_142[3]_i_5_n_1 }));
  FDRE \acc1_0_reg_142_reg[40] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[40]),
        .Q(acc1_0_reg_142[40]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[41] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[41]),
        .Q(acc1_0_reg_142[41]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[42] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[42]),
        .Q(acc1_0_reg_142[42]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[43] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[43]),
        .Q(acc1_0_reg_142[43]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[43]_i_1 
       (.CI(\acc1_0_reg_142_reg[39]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[43]_i_1_n_1 ,\acc1_0_reg_142_reg[43]_i_1_n_2 ,\acc1_0_reg_142_reg[43]_i_1_n_3 ,\acc1_0_reg_142_reg[43]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[43:40]),
        .O(acc1_fu_273_p2[43:40]),
        .S({\acc1_0_reg_142[43]_i_3_n_1 ,\acc1_0_reg_142[43]_i_4_n_1 ,\acc1_0_reg_142[43]_i_5_n_1 ,\acc1_0_reg_142[43]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[43]_i_2 
       (.CI(\acc1_0_reg_142_reg[39]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[43]_i_2_n_1 ,\acc1_0_reg_142_reg[43]_i_2_n_2 ,\acc1_0_reg_142_reg[43]_i_2_n_3 ,\acc1_0_reg_142_reg[43]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_80,mul_ln74_fu_267_p2__2_n_81,mul_ln74_fu_267_p2__2_n_82,mul_ln74_fu_267_p2__2_n_83}),
        .O(mul_ln74_fu_267_p2__3[43:40]),
        .S({\acc1_0_reg_142[43]_i_7_n_1 ,\acc1_0_reg_142[43]_i_8_n_1 ,\acc1_0_reg_142[43]_i_9_n_1 ,\acc1_0_reg_142[43]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[44] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[44]),
        .Q(acc1_0_reg_142[44]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[45] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[45]),
        .Q(acc1_0_reg_142[45]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[46] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[46]),
        .Q(acc1_0_reg_142[46]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[47] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[47]),
        .Q(acc1_0_reg_142[47]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[47]_i_1 
       (.CI(\acc1_0_reg_142_reg[43]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[47]_i_1_n_1 ,\acc1_0_reg_142_reg[47]_i_1_n_2 ,\acc1_0_reg_142_reg[47]_i_1_n_3 ,\acc1_0_reg_142_reg[47]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[47:44]),
        .O(acc1_fu_273_p2[47:44]),
        .S({\acc1_0_reg_142[47]_i_3_n_1 ,\acc1_0_reg_142[47]_i_4_n_1 ,\acc1_0_reg_142[47]_i_5_n_1 ,\acc1_0_reg_142[47]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[47]_i_2 
       (.CI(\acc1_0_reg_142_reg[43]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[47]_i_2_n_1 ,\acc1_0_reg_142_reg[47]_i_2_n_2 ,\acc1_0_reg_142_reg[47]_i_2_n_3 ,\acc1_0_reg_142_reg[47]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_76,mul_ln74_fu_267_p2__2_n_77,mul_ln74_fu_267_p2__2_n_78,mul_ln74_fu_267_p2__2_n_79}),
        .O(mul_ln74_fu_267_p2__3[47:44]),
        .S({\acc1_0_reg_142[47]_i_7_n_1 ,\acc1_0_reg_142[47]_i_8_n_1 ,\acc1_0_reg_142[47]_i_9_n_1 ,\acc1_0_reg_142[47]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[48] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[48]),
        .Q(acc1_0_reg_142[48]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[49] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[49]),
        .Q(acc1_0_reg_142[49]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[4]),
        .Q(acc1_0_reg_142[4]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[50] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[50]),
        .Q(acc1_0_reg_142[50]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[51] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[51]),
        .Q(acc1_0_reg_142[51]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[51]_i_1 
       (.CI(\acc1_0_reg_142_reg[47]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[51]_i_1_n_1 ,\acc1_0_reg_142_reg[51]_i_1_n_2 ,\acc1_0_reg_142_reg[51]_i_1_n_3 ,\acc1_0_reg_142_reg[51]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[51:48]),
        .O(acc1_fu_273_p2[51:48]),
        .S({\acc1_0_reg_142[51]_i_3_n_1 ,\acc1_0_reg_142[51]_i_4_n_1 ,\acc1_0_reg_142[51]_i_5_n_1 ,\acc1_0_reg_142[51]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[51]_i_2 
       (.CI(\acc1_0_reg_142_reg[47]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[51]_i_2_n_1 ,\acc1_0_reg_142_reg[51]_i_2_n_2 ,\acc1_0_reg_142_reg[51]_i_2_n_3 ,\acc1_0_reg_142_reg[51]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_72,mul_ln74_fu_267_p2__2_n_73,mul_ln74_fu_267_p2__2_n_74,mul_ln74_fu_267_p2__2_n_75}),
        .O(mul_ln74_fu_267_p2__3[51:48]),
        .S({\acc1_0_reg_142[51]_i_7_n_1 ,\acc1_0_reg_142[51]_i_8_n_1 ,\acc1_0_reg_142[51]_i_9_n_1 ,\acc1_0_reg_142[51]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[52] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[52]),
        .Q(acc1_0_reg_142[52]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[53] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[53]),
        .Q(acc1_0_reg_142[53]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[54] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[54]),
        .Q(acc1_0_reg_142[54]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[55] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[55]),
        .Q(acc1_0_reg_142[55]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[55]_i_1 
       (.CI(\acc1_0_reg_142_reg[51]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[55]_i_1_n_1 ,\acc1_0_reg_142_reg[55]_i_1_n_2 ,\acc1_0_reg_142_reg[55]_i_1_n_3 ,\acc1_0_reg_142_reg[55]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[55:52]),
        .O(acc1_fu_273_p2[55:52]),
        .S({\acc1_0_reg_142[55]_i_3_n_1 ,\acc1_0_reg_142[55]_i_4_n_1 ,\acc1_0_reg_142[55]_i_5_n_1 ,\acc1_0_reg_142[55]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[55]_i_2 
       (.CI(\acc1_0_reg_142_reg[51]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[55]_i_2_n_1 ,\acc1_0_reg_142_reg[55]_i_2_n_2 ,\acc1_0_reg_142_reg[55]_i_2_n_3 ,\acc1_0_reg_142_reg[55]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_68,mul_ln74_fu_267_p2__2_n_69,mul_ln74_fu_267_p2__2_n_70,mul_ln74_fu_267_p2__2_n_71}),
        .O(mul_ln74_fu_267_p2__3[55:52]),
        .S({\acc1_0_reg_142[55]_i_7_n_1 ,\acc1_0_reg_142[55]_i_8_n_1 ,\acc1_0_reg_142[55]_i_9_n_1 ,\acc1_0_reg_142[55]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[56] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[56]),
        .Q(acc1_0_reg_142[56]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[57] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[57]),
        .Q(acc1_0_reg_142[57]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[58] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[58]),
        .Q(acc1_0_reg_142[58]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[59] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[59]),
        .Q(acc1_0_reg_142[59]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[59]_i_1 
       (.CI(\acc1_0_reg_142_reg[55]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[59]_i_1_n_1 ,\acc1_0_reg_142_reg[59]_i_1_n_2 ,\acc1_0_reg_142_reg[59]_i_1_n_3 ,\acc1_0_reg_142_reg[59]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln74_fu_267_p2__3[59:56]),
        .O(acc1_fu_273_p2[59:56]),
        .S({\acc1_0_reg_142[59]_i_3_n_1 ,\acc1_0_reg_142[59]_i_4_n_1 ,\acc1_0_reg_142[59]_i_5_n_1 ,\acc1_0_reg_142[59]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[59]_i_2 
       (.CI(\acc1_0_reg_142_reg[55]_i_2_n_1 ),
        .CO({\acc1_0_reg_142_reg[59]_i_2_n_1 ,\acc1_0_reg_142_reg[59]_i_2_n_2 ,\acc1_0_reg_142_reg[59]_i_2_n_3 ,\acc1_0_reg_142_reg[59]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__2_n_64,mul_ln74_fu_267_p2__2_n_65,mul_ln74_fu_267_p2__2_n_66,mul_ln74_fu_267_p2__2_n_67}),
        .O(mul_ln74_fu_267_p2__3[59:56]),
        .S({\acc1_0_reg_142[59]_i_7_n_1 ,\acc1_0_reg_142[59]_i_8_n_1 ,\acc1_0_reg_142[59]_i_9_n_1 ,\acc1_0_reg_142[59]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[5]),
        .Q(acc1_0_reg_142[5]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[60] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[60]),
        .Q(acc1_0_reg_142[60]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[61] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[61]),
        .Q(acc1_0_reg_142[61]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[62] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[62]),
        .Q(acc1_0_reg_142[62]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[63] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[63]),
        .Q(acc1_0_reg_142[63]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[63]_i_1 
       (.CI(\acc1_0_reg_142_reg[59]_i_1_n_1 ),
        .CO({\NLW_acc1_0_reg_142_reg[63]_i_1_CO_UNCONNECTED [3],\acc1_0_reg_142_reg[63]_i_1_n_2 ,\acc1_0_reg_142_reg[63]_i_1_n_3 ,\acc1_0_reg_142_reg[63]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln74_fu_267_p2__3[62:60]}),
        .O(acc1_fu_273_p2[63:60]),
        .S({\acc1_0_reg_142[63]_i_3_n_1 ,\acc1_0_reg_142[63]_i_4_n_1 ,\acc1_0_reg_142[63]_i_5_n_1 ,\acc1_0_reg_142[63]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[63]_i_2 
       (.CI(\acc1_0_reg_142_reg[59]_i_2_n_1 ),
        .CO({\NLW_acc1_0_reg_142_reg[63]_i_2_CO_UNCONNECTED [3],\acc1_0_reg_142_reg[63]_i_2_n_2 ,\acc1_0_reg_142_reg[63]_i_2_n_3 ,\acc1_0_reg_142_reg[63]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln74_fu_267_p2__2_n_61,mul_ln74_fu_267_p2__2_n_62,mul_ln74_fu_267_p2__2_n_63}),
        .O(mul_ln74_fu_267_p2__3[63:60]),
        .S({\acc1_0_reg_142[63]_i_7_n_1 ,\acc1_0_reg_142[63]_i_8_n_1 ,\acc1_0_reg_142[63]_i_9_n_1 ,\acc1_0_reg_142[63]_i_10_n_1 }));
  FDRE \acc1_0_reg_142_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[6]),
        .Q(acc1_0_reg_142[6]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[7]),
        .Q(acc1_0_reg_142[7]),
        .R(acc0_0_reg_130));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \acc1_0_reg_142_reg[7]_i_1 
       (.CI(\acc1_0_reg_142_reg[3]_i_1_n_1 ),
        .CO({\acc1_0_reg_142_reg[7]_i_1_n_1 ,\acc1_0_reg_142_reg[7]_i_1_n_2 ,\acc1_0_reg_142_reg[7]_i_1_n_3 ,\acc1_0_reg_142_reg[7]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln74_fu_267_p2__1_n_99,mul_ln74_fu_267_p2__1_n_100,mul_ln74_fu_267_p2__1_n_101,mul_ln74_fu_267_p2__1_n_102}),
        .O(acc1_fu_273_p2[7:4]),
        .S({\acc1_0_reg_142[7]_i_2_n_1 ,\acc1_0_reg_142[7]_i_3_n_1 ,\acc1_0_reg_142[7]_i_4_n_1 ,\acc1_0_reg_142[7]_i_5_n_1 }));
  FDRE \acc1_0_reg_142_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[8]),
        .Q(acc1_0_reg_142[8]),
        .R(acc0_0_reg_130));
  FDRE \acc1_0_reg_142_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(acc1_fu_273_p2[9]),
        .Q(acc1_0_reg_142[9]),
        .R(acc0_0_reg_130));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_CS_fsm_state3),
        .I1(i_0_reg_154[3]),
        .I2(i_0_reg_154[4]),
        .I3(i_0_reg_154[2]),
        .I4(i_0_reg_154[1]),
        .I5(i_0_reg_154[0]),
        .O(ap_NS_fsm[3]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_1_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state5),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state6),
        .R(ap_rst_n_inv));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_coeffs coeffs_U
       (.Q(i_0_reg_154),
        .ap_clk(ap_clk),
        .coeffs_ce0(coeffs_ce0),
        .out(\fir_coeffs_rom_U/q0_reg ));
  FDRE \i_0_reg_154_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_reg_298[0]),
        .Q(i_0_reg_154[0]),
        .R(acc0_0_reg_130));
  FDSE \i_0_reg_154_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_reg_298[1]),
        .Q(i_0_reg_154[1]),
        .S(acc0_0_reg_130));
  FDSE \i_0_reg_154_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_reg_298[2]),
        .Q(i_0_reg_154[2]),
        .S(acc0_0_reg_130));
  FDSE \i_0_reg_154_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_reg_298[3]),
        .Q(i_0_reg_154[3]),
        .S(acc0_0_reg_130));
  FDSE \i_0_reg_154_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_reg_298[4]),
        .Q(i_0_reg_154[4]),
        .S(acc0_0_reg_130));
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_298[0]_i_1 
       (.I0(i_0_reg_154[0]),
        .O(i_fu_171_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \i_reg_298[1]_i_1 
       (.I0(i_0_reg_154[1]),
        .I1(i_0_reg_154[0]),
        .O(i_fu_171_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hE1)) 
    \i_reg_298[2]_i_1 
       (.I0(i_0_reg_154[0]),
        .I1(i_0_reg_154[1]),
        .I2(i_0_reg_154[2]),
        .O(i_fu_171_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \i_reg_298[3]_i_1 
       (.I0(i_0_reg_154[2]),
        .I1(i_0_reg_154[1]),
        .I2(i_0_reg_154[0]),
        .I3(i_0_reg_154[3]),
        .O(i_fu_171_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \i_reg_298[4]_i_1 
       (.I0(i_0_reg_154[4]),
        .I1(i_0_reg_154[3]),
        .I2(i_0_reg_154[2]),
        .I3(i_0_reg_154[1]),
        .I4(i_0_reg_154[0]),
        .O(i_fu_171_p2[4]));
  FDRE \i_reg_298_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_fu_171_p2[0]),
        .Q(i_reg_298[0]),
        .R(1'b0));
  FDRE \i_reg_298_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_fu_171_p2[1]),
        .Q(i_reg_298[1]),
        .R(1'b0));
  FDRE \i_reg_298_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_fu_171_p2[2]),
        .Q(i_reg_298[2]),
        .R(1'b0));
  FDRE \i_reg_298_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_fu_171_p2[3]),
        .Q(i_reg_298[3]),
        .R(1'b0));
  FDRE \i_reg_298_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_fu_171_p2[4]),
        .Q(i_reg_298[4]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 22x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln73_fu_251_p2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [36:35],\fir_coeffs_rom_U/q0_reg [35:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln73_fu_251_p2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,q00[16:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln73_fu_251_p2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln73_fu_251_p2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln73_fu_251_p2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln73_fu_251_p2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln73_fu_251_p2_OVERFLOW_UNCONNECTED),
        .P({mul_ln73_fu_251_p2_n_59,mul_ln73_fu_251_p2_n_60,mul_ln73_fu_251_p2_n_61,mul_ln73_fu_251_p2_n_62,mul_ln73_fu_251_p2_n_63,mul_ln73_fu_251_p2_n_64,mul_ln73_fu_251_p2_n_65,mul_ln73_fu_251_p2_n_66,mul_ln73_fu_251_p2_n_67,mul_ln73_fu_251_p2_n_68,mul_ln73_fu_251_p2_n_69,mul_ln73_fu_251_p2_n_70,mul_ln73_fu_251_p2_n_71,mul_ln73_fu_251_p2_n_72,mul_ln73_fu_251_p2_n_73,mul_ln73_fu_251_p2_n_74,mul_ln73_fu_251_p2_n_75,mul_ln73_fu_251_p2_n_76,mul_ln73_fu_251_p2_n_77,mul_ln73_fu_251_p2_n_78,mul_ln73_fu_251_p2_n_79,mul_ln73_fu_251_p2_n_80,mul_ln73_fu_251_p2_n_81,mul_ln73_fu_251_p2_n_82,mul_ln73_fu_251_p2_n_83,mul_ln73_fu_251_p2_n_84,mul_ln73_fu_251_p2_n_85,mul_ln73_fu_251_p2_n_86,mul_ln73_fu_251_p2_n_87,mul_ln73_fu_251_p2_n_88,mul_ln73_fu_251_p2_n_89,mul_ln73_fu_251_p2_n_90,mul_ln73_fu_251_p2_n_91,mul_ln73_fu_251_p2_n_92,mul_ln73_fu_251_p2_n_93,mul_ln73_fu_251_p2_n_94,mul_ln73_fu_251_p2_n_95,mul_ln73_fu_251_p2_n_96,mul_ln73_fu_251_p2_n_97,mul_ln73_fu_251_p2_n_98,mul_ln73_fu_251_p2_n_99,mul_ln73_fu_251_p2_n_100,mul_ln73_fu_251_p2_n_101,mul_ln73_fu_251_p2_n_102,mul_ln73_fu_251_p2_n_103,mul_ln73_fu_251_p2_n_104,mul_ln73_fu_251_p2_n_105,mul_ln73_fu_251_p2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln73_fu_251_p2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln73_fu_251_p2_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln73_fu_251_p2_n_107,mul_ln73_fu_251_p2_n_108,mul_ln73_fu_251_p2_n_109,mul_ln73_fu_251_p2_n_110,mul_ln73_fu_251_p2_n_111,mul_ln73_fu_251_p2_n_112,mul_ln73_fu_251_p2_n_113,mul_ln73_fu_251_p2_n_114,mul_ln73_fu_251_p2_n_115,mul_ln73_fu_251_p2_n_116,mul_ln73_fu_251_p2_n_117,mul_ln73_fu_251_p2_n_118,mul_ln73_fu_251_p2_n_119,mul_ln73_fu_251_p2_n_120,mul_ln73_fu_251_p2_n_121,mul_ln73_fu_251_p2_n_122,mul_ln73_fu_251_p2_n_123,mul_ln73_fu_251_p2_n_124,mul_ln73_fu_251_p2_n_125,mul_ln73_fu_251_p2_n_126,mul_ln73_fu_251_p2_n_127,mul_ln73_fu_251_p2_n_128,mul_ln73_fu_251_p2_n_129,mul_ln73_fu_251_p2_n_130,mul_ln73_fu_251_p2_n_131,mul_ln73_fu_251_p2_n_132,mul_ln73_fu_251_p2_n_133,mul_ln73_fu_251_p2_n_134,mul_ln73_fu_251_p2_n_135,mul_ln73_fu_251_p2_n_136,mul_ln73_fu_251_p2_n_137,mul_ln73_fu_251_p2_n_138,mul_ln73_fu_251_p2_n_139,mul_ln73_fu_251_p2_n_140,mul_ln73_fu_251_p2_n_141,mul_ln73_fu_251_p2_n_142,mul_ln73_fu_251_p2_n_143,mul_ln73_fu_251_p2_n_144,mul_ln73_fu_251_p2_n_145,mul_ln73_fu_251_p2_n_146,mul_ln73_fu_251_p2_n_147,mul_ln73_fu_251_p2_n_148,mul_ln73_fu_251_p2_n_149,mul_ln73_fu_251_p2_n_150,mul_ln73_fu_251_p2_n_151,mul_ln73_fu_251_p2_n_152,mul_ln73_fu_251_p2_n_153,mul_ln73_fu_251_p2_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln73_fu_251_p2_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 22x15 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln73_fu_251_p2__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [36:35],\fir_coeffs_rom_U/q0_reg [35:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln73_fu_251_p2__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({q00[31],q00[31],q00[31],q00[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln73_fu_251_p2__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln73_fu_251_p2__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln73_fu_251_p2__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln73_fu_251_p2__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln73_fu_251_p2__0_OVERFLOW_UNCONNECTED),
        .P({mul_ln73_fu_251_p2__0_n_59,mul_ln73_fu_251_p2__0_n_60,mul_ln73_fu_251_p2__0_n_61,mul_ln73_fu_251_p2__0_n_62,mul_ln73_fu_251_p2__0_n_63,mul_ln73_fu_251_p2__0_n_64,mul_ln73_fu_251_p2__0_n_65,mul_ln73_fu_251_p2__0_n_66,mul_ln73_fu_251_p2__0_n_67,mul_ln73_fu_251_p2__0_n_68,mul_ln73_fu_251_p2__0_n_69,mul_ln73_fu_251_p2__0_n_70,mul_ln73_fu_251_p2__0_n_71,mul_ln73_fu_251_p2__0_n_72,mul_ln73_fu_251_p2__0_n_73,mul_ln73_fu_251_p2__0_n_74,mul_ln73_fu_251_p2__0_n_75,mul_ln73_fu_251_p2__0_n_76,mul_ln73_fu_251_p2__0_n_77,mul_ln73_fu_251_p2__0_n_78,mul_ln73_fu_251_p2__0_n_79,mul_ln73_fu_251_p2__0_n_80,mul_ln73_fu_251_p2__0_n_81,mul_ln73_fu_251_p2__0_n_82,mul_ln73_fu_251_p2__0_n_83,mul_ln73_fu_251_p2__0_n_84,mul_ln73_fu_251_p2__0_n_85,mul_ln73_fu_251_p2__0_n_86,mul_ln73_fu_251_p2__0_n_87,mul_ln73_fu_251_p2__0_n_88,mul_ln73_fu_251_p2__0_n_89,mul_ln73_fu_251_p2__0_n_90,mul_ln73_fu_251_p2__0_n_91,mul_ln73_fu_251_p2__0_n_92,mul_ln73_fu_251_p2__0_n_93,mul_ln73_fu_251_p2__0_n_94,mul_ln73_fu_251_p2__0_n_95,mul_ln73_fu_251_p2__0_n_96,mul_ln73_fu_251_p2__0_n_97,mul_ln73_fu_251_p2__0_n_98,mul_ln73_fu_251_p2__0_n_99,mul_ln73_fu_251_p2__0_n_100,mul_ln73_fu_251_p2__0_n_101,mul_ln73_fu_251_p2__0_n_102,mul_ln73_fu_251_p2__0_n_103,mul_ln73_fu_251_p2__0_n_104,mul_ln73_fu_251_p2__0_n_105,mul_ln73_fu_251_p2__0_n_106}),
        .PATTERNBDETECT(NLW_mul_ln73_fu_251_p2__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln73_fu_251_p2__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln73_fu_251_p2_n_107,mul_ln73_fu_251_p2_n_108,mul_ln73_fu_251_p2_n_109,mul_ln73_fu_251_p2_n_110,mul_ln73_fu_251_p2_n_111,mul_ln73_fu_251_p2_n_112,mul_ln73_fu_251_p2_n_113,mul_ln73_fu_251_p2_n_114,mul_ln73_fu_251_p2_n_115,mul_ln73_fu_251_p2_n_116,mul_ln73_fu_251_p2_n_117,mul_ln73_fu_251_p2_n_118,mul_ln73_fu_251_p2_n_119,mul_ln73_fu_251_p2_n_120,mul_ln73_fu_251_p2_n_121,mul_ln73_fu_251_p2_n_122,mul_ln73_fu_251_p2_n_123,mul_ln73_fu_251_p2_n_124,mul_ln73_fu_251_p2_n_125,mul_ln73_fu_251_p2_n_126,mul_ln73_fu_251_p2_n_127,mul_ln73_fu_251_p2_n_128,mul_ln73_fu_251_p2_n_129,mul_ln73_fu_251_p2_n_130,mul_ln73_fu_251_p2_n_131,mul_ln73_fu_251_p2_n_132,mul_ln73_fu_251_p2_n_133,mul_ln73_fu_251_p2_n_134,mul_ln73_fu_251_p2_n_135,mul_ln73_fu_251_p2_n_136,mul_ln73_fu_251_p2_n_137,mul_ln73_fu_251_p2_n_138,mul_ln73_fu_251_p2_n_139,mul_ln73_fu_251_p2_n_140,mul_ln73_fu_251_p2_n_141,mul_ln73_fu_251_p2_n_142,mul_ln73_fu_251_p2_n_143,mul_ln73_fu_251_p2_n_144,mul_ln73_fu_251_p2_n_145,mul_ln73_fu_251_p2_n_146,mul_ln73_fu_251_p2_n_147,mul_ln73_fu_251_p2_n_148,mul_ln73_fu_251_p2_n_149,mul_ln73_fu_251_p2_n_150,mul_ln73_fu_251_p2_n_151,mul_ln73_fu_251_p2_n_152,mul_ln73_fu_251_p2_n_153,mul_ln73_fu_251_p2_n_154}),
        .PCOUT(NLW_mul_ln73_fu_251_p2__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln73_fu_251_p2__0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln73_fu_251_p2__1
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln73_fu_251_p2__1_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,q00[16:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln73_fu_251_p2__1_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln73_fu_251_p2__1_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln73_fu_251_p2__1_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln73_fu_251_p2__1_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln73_fu_251_p2__1_OVERFLOW_UNCONNECTED),
        .P({mul_ln73_fu_251_p2__1_n_59,mul_ln73_fu_251_p2__1_n_60,mul_ln73_fu_251_p2__1_n_61,mul_ln73_fu_251_p2__1_n_62,mul_ln73_fu_251_p2__1_n_63,mul_ln73_fu_251_p2__1_n_64,mul_ln73_fu_251_p2__1_n_65,mul_ln73_fu_251_p2__1_n_66,mul_ln73_fu_251_p2__1_n_67,mul_ln73_fu_251_p2__1_n_68,mul_ln73_fu_251_p2__1_n_69,mul_ln73_fu_251_p2__1_n_70,mul_ln73_fu_251_p2__1_n_71,mul_ln73_fu_251_p2__1_n_72,mul_ln73_fu_251_p2__1_n_73,mul_ln73_fu_251_p2__1_n_74,mul_ln73_fu_251_p2__1_n_75,mul_ln73_fu_251_p2__1_n_76,mul_ln73_fu_251_p2__1_n_77,mul_ln73_fu_251_p2__1_n_78,mul_ln73_fu_251_p2__1_n_79,mul_ln73_fu_251_p2__1_n_80,mul_ln73_fu_251_p2__1_n_81,mul_ln73_fu_251_p2__1_n_82,mul_ln73_fu_251_p2__1_n_83,mul_ln73_fu_251_p2__1_n_84,mul_ln73_fu_251_p2__1_n_85,mul_ln73_fu_251_p2__1_n_86,mul_ln73_fu_251_p2__1_n_87,mul_ln73_fu_251_p2__1_n_88,mul_ln73_fu_251_p2__1_n_89,mul_ln73_fu_251_p2__1_n_90,mul_ln73_fu_251_p2__1_n_91,mul_ln73_fu_251_p2__1_n_92,mul_ln73_fu_251_p2__1_n_93,mul_ln73_fu_251_p2__1_n_94,mul_ln73_fu_251_p2__1_n_95,mul_ln73_fu_251_p2__1_n_96,mul_ln73_fu_251_p2__1_n_97,mul_ln73_fu_251_p2__1_n_98,mul_ln73_fu_251_p2__1_n_99,mul_ln73_fu_251_p2__1_n_100,mul_ln73_fu_251_p2__1_n_101,mul_ln73_fu_251_p2__1_n_102,mul_ln73_fu_251_p2__1_n_103,mul_ln73_fu_251_p2__1_n_104,mul_ln73_fu_251_p2__1_n_105,mul_ln73_fu_251_p2__1_n_106}),
        .PATTERNBDETECT(NLW_mul_ln73_fu_251_p2__1_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln73_fu_251_p2__1_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln73_fu_251_p2__1_n_107,mul_ln73_fu_251_p2__1_n_108,mul_ln73_fu_251_p2__1_n_109,mul_ln73_fu_251_p2__1_n_110,mul_ln73_fu_251_p2__1_n_111,mul_ln73_fu_251_p2__1_n_112,mul_ln73_fu_251_p2__1_n_113,mul_ln73_fu_251_p2__1_n_114,mul_ln73_fu_251_p2__1_n_115,mul_ln73_fu_251_p2__1_n_116,mul_ln73_fu_251_p2__1_n_117,mul_ln73_fu_251_p2__1_n_118,mul_ln73_fu_251_p2__1_n_119,mul_ln73_fu_251_p2__1_n_120,mul_ln73_fu_251_p2__1_n_121,mul_ln73_fu_251_p2__1_n_122,mul_ln73_fu_251_p2__1_n_123,mul_ln73_fu_251_p2__1_n_124,mul_ln73_fu_251_p2__1_n_125,mul_ln73_fu_251_p2__1_n_126,mul_ln73_fu_251_p2__1_n_127,mul_ln73_fu_251_p2__1_n_128,mul_ln73_fu_251_p2__1_n_129,mul_ln73_fu_251_p2__1_n_130,mul_ln73_fu_251_p2__1_n_131,mul_ln73_fu_251_p2__1_n_132,mul_ln73_fu_251_p2__1_n_133,mul_ln73_fu_251_p2__1_n_134,mul_ln73_fu_251_p2__1_n_135,mul_ln73_fu_251_p2__1_n_136,mul_ln73_fu_251_p2__1_n_137,mul_ln73_fu_251_p2__1_n_138,mul_ln73_fu_251_p2__1_n_139,mul_ln73_fu_251_p2__1_n_140,mul_ln73_fu_251_p2__1_n_141,mul_ln73_fu_251_p2__1_n_142,mul_ln73_fu_251_p2__1_n_143,mul_ln73_fu_251_p2__1_n_144,mul_ln73_fu_251_p2__1_n_145,mul_ln73_fu_251_p2__1_n_146,mul_ln73_fu_251_p2__1_n_147,mul_ln73_fu_251_p2__1_n_148,mul_ln73_fu_251_p2__1_n_149,mul_ln73_fu_251_p2__1_n_150,mul_ln73_fu_251_p2__1_n_151,mul_ln73_fu_251_p2__1_n_152,mul_ln73_fu_251_p2__1_n_153,mul_ln73_fu_251_p2__1_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln73_fu_251_p2__1_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x15 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln73_fu_251_p2__2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln73_fu_251_p2__2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({q00[31],q00[31],q00[31],q00[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln73_fu_251_p2__2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln73_fu_251_p2__2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln73_fu_251_p2__2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln73_fu_251_p2__2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln73_fu_251_p2__2_OVERFLOW_UNCONNECTED),
        .P({mul_ln73_fu_251_p2__2_n_59,mul_ln73_fu_251_p2__2_n_60,mul_ln73_fu_251_p2__2_n_61,mul_ln73_fu_251_p2__2_n_62,mul_ln73_fu_251_p2__2_n_63,mul_ln73_fu_251_p2__2_n_64,mul_ln73_fu_251_p2__2_n_65,mul_ln73_fu_251_p2__2_n_66,mul_ln73_fu_251_p2__2_n_67,mul_ln73_fu_251_p2__2_n_68,mul_ln73_fu_251_p2__2_n_69,mul_ln73_fu_251_p2__2_n_70,mul_ln73_fu_251_p2__2_n_71,mul_ln73_fu_251_p2__2_n_72,mul_ln73_fu_251_p2__2_n_73,mul_ln73_fu_251_p2__2_n_74,mul_ln73_fu_251_p2__2_n_75,mul_ln73_fu_251_p2__2_n_76,mul_ln73_fu_251_p2__2_n_77,mul_ln73_fu_251_p2__2_n_78,mul_ln73_fu_251_p2__2_n_79,mul_ln73_fu_251_p2__2_n_80,mul_ln73_fu_251_p2__2_n_81,mul_ln73_fu_251_p2__2_n_82,mul_ln73_fu_251_p2__2_n_83,mul_ln73_fu_251_p2__2_n_84,mul_ln73_fu_251_p2__2_n_85,mul_ln73_fu_251_p2__2_n_86,mul_ln73_fu_251_p2__2_n_87,mul_ln73_fu_251_p2__2_n_88,mul_ln73_fu_251_p2__2_n_89,mul_ln73_fu_251_p2__2_n_90,mul_ln73_fu_251_p2__2_n_91,mul_ln73_fu_251_p2__2_n_92,mul_ln73_fu_251_p2__2_n_93,mul_ln73_fu_251_p2__2_n_94,mul_ln73_fu_251_p2__2_n_95,mul_ln73_fu_251_p2__2_n_96,mul_ln73_fu_251_p2__2_n_97,mul_ln73_fu_251_p2__2_n_98,mul_ln73_fu_251_p2__2_n_99,mul_ln73_fu_251_p2__2_n_100,mul_ln73_fu_251_p2__2_n_101,mul_ln73_fu_251_p2__2_n_102,mul_ln73_fu_251_p2__2_n_103,mul_ln73_fu_251_p2__2_n_104,mul_ln73_fu_251_p2__2_n_105,mul_ln73_fu_251_p2__2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln73_fu_251_p2__2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln73_fu_251_p2__2_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln73_fu_251_p2__1_n_107,mul_ln73_fu_251_p2__1_n_108,mul_ln73_fu_251_p2__1_n_109,mul_ln73_fu_251_p2__1_n_110,mul_ln73_fu_251_p2__1_n_111,mul_ln73_fu_251_p2__1_n_112,mul_ln73_fu_251_p2__1_n_113,mul_ln73_fu_251_p2__1_n_114,mul_ln73_fu_251_p2__1_n_115,mul_ln73_fu_251_p2__1_n_116,mul_ln73_fu_251_p2__1_n_117,mul_ln73_fu_251_p2__1_n_118,mul_ln73_fu_251_p2__1_n_119,mul_ln73_fu_251_p2__1_n_120,mul_ln73_fu_251_p2__1_n_121,mul_ln73_fu_251_p2__1_n_122,mul_ln73_fu_251_p2__1_n_123,mul_ln73_fu_251_p2__1_n_124,mul_ln73_fu_251_p2__1_n_125,mul_ln73_fu_251_p2__1_n_126,mul_ln73_fu_251_p2__1_n_127,mul_ln73_fu_251_p2__1_n_128,mul_ln73_fu_251_p2__1_n_129,mul_ln73_fu_251_p2__1_n_130,mul_ln73_fu_251_p2__1_n_131,mul_ln73_fu_251_p2__1_n_132,mul_ln73_fu_251_p2__1_n_133,mul_ln73_fu_251_p2__1_n_134,mul_ln73_fu_251_p2__1_n_135,mul_ln73_fu_251_p2__1_n_136,mul_ln73_fu_251_p2__1_n_137,mul_ln73_fu_251_p2__1_n_138,mul_ln73_fu_251_p2__1_n_139,mul_ln73_fu_251_p2__1_n_140,mul_ln73_fu_251_p2__1_n_141,mul_ln73_fu_251_p2__1_n_142,mul_ln73_fu_251_p2__1_n_143,mul_ln73_fu_251_p2__1_n_144,mul_ln73_fu_251_p2__1_n_145,mul_ln73_fu_251_p2__1_n_146,mul_ln73_fu_251_p2__1_n_147,mul_ln73_fu_251_p2__1_n_148,mul_ln73_fu_251_p2__1_n_149,mul_ln73_fu_251_p2__1_n_150,mul_ln73_fu_251_p2__1_n_151,mul_ln73_fu_251_p2__1_n_152,mul_ln73_fu_251_p2__1_n_153,mul_ln73_fu_251_p2__1_n_154}),
        .PCOUT(NLW_mul_ln73_fu_251_p2__2_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln73_fu_251_p2__2_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 22x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln74_fu_267_p2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [36:35],\fir_coeffs_rom_U/q0_reg [35:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln74_fu_267_p2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,shift_reg1_U_n_18,shift_reg1_U_n_19,shift_reg1_U_n_20,shift_reg1_U_n_21,shift_reg1_U_n_22,shift_reg1_U_n_23,shift_reg1_U_n_24,shift_reg1_U_n_25,shift_reg1_U_n_26,shift_reg1_U_n_27,shift_reg1_U_n_28,shift_reg1_U_n_29,shift_reg1_U_n_30,shift_reg1_U_n_31,shift_reg1_U_n_32,shift_reg1_U_n_33,shift_reg1_U_n_34}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln74_fu_267_p2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln74_fu_267_p2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln74_fu_267_p2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln74_fu_267_p2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln74_fu_267_p2_OVERFLOW_UNCONNECTED),
        .P({mul_ln74_fu_267_p2_n_59,mul_ln74_fu_267_p2_n_60,mul_ln74_fu_267_p2_n_61,mul_ln74_fu_267_p2_n_62,mul_ln74_fu_267_p2_n_63,mul_ln74_fu_267_p2_n_64,mul_ln74_fu_267_p2_n_65,mul_ln74_fu_267_p2_n_66,mul_ln74_fu_267_p2_n_67,mul_ln74_fu_267_p2_n_68,mul_ln74_fu_267_p2_n_69,mul_ln74_fu_267_p2_n_70,mul_ln74_fu_267_p2_n_71,mul_ln74_fu_267_p2_n_72,mul_ln74_fu_267_p2_n_73,mul_ln74_fu_267_p2_n_74,mul_ln74_fu_267_p2_n_75,mul_ln74_fu_267_p2_n_76,mul_ln74_fu_267_p2_n_77,mul_ln74_fu_267_p2_n_78,mul_ln74_fu_267_p2_n_79,mul_ln74_fu_267_p2_n_80,mul_ln74_fu_267_p2_n_81,mul_ln74_fu_267_p2_n_82,mul_ln74_fu_267_p2_n_83,mul_ln74_fu_267_p2_n_84,mul_ln74_fu_267_p2_n_85,mul_ln74_fu_267_p2_n_86,mul_ln74_fu_267_p2_n_87,mul_ln74_fu_267_p2_n_88,mul_ln74_fu_267_p2_n_89,mul_ln74_fu_267_p2_n_90,mul_ln74_fu_267_p2_n_91,mul_ln74_fu_267_p2_n_92,mul_ln74_fu_267_p2_n_93,mul_ln74_fu_267_p2_n_94,mul_ln74_fu_267_p2_n_95,mul_ln74_fu_267_p2_n_96,mul_ln74_fu_267_p2_n_97,mul_ln74_fu_267_p2_n_98,mul_ln74_fu_267_p2_n_99,mul_ln74_fu_267_p2_n_100,mul_ln74_fu_267_p2_n_101,mul_ln74_fu_267_p2_n_102,mul_ln74_fu_267_p2_n_103,mul_ln74_fu_267_p2_n_104,mul_ln74_fu_267_p2_n_105,mul_ln74_fu_267_p2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln74_fu_267_p2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln74_fu_267_p2_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln74_fu_267_p2_n_107,mul_ln74_fu_267_p2_n_108,mul_ln74_fu_267_p2_n_109,mul_ln74_fu_267_p2_n_110,mul_ln74_fu_267_p2_n_111,mul_ln74_fu_267_p2_n_112,mul_ln74_fu_267_p2_n_113,mul_ln74_fu_267_p2_n_114,mul_ln74_fu_267_p2_n_115,mul_ln74_fu_267_p2_n_116,mul_ln74_fu_267_p2_n_117,mul_ln74_fu_267_p2_n_118,mul_ln74_fu_267_p2_n_119,mul_ln74_fu_267_p2_n_120,mul_ln74_fu_267_p2_n_121,mul_ln74_fu_267_p2_n_122,mul_ln74_fu_267_p2_n_123,mul_ln74_fu_267_p2_n_124,mul_ln74_fu_267_p2_n_125,mul_ln74_fu_267_p2_n_126,mul_ln74_fu_267_p2_n_127,mul_ln74_fu_267_p2_n_128,mul_ln74_fu_267_p2_n_129,mul_ln74_fu_267_p2_n_130,mul_ln74_fu_267_p2_n_131,mul_ln74_fu_267_p2_n_132,mul_ln74_fu_267_p2_n_133,mul_ln74_fu_267_p2_n_134,mul_ln74_fu_267_p2_n_135,mul_ln74_fu_267_p2_n_136,mul_ln74_fu_267_p2_n_137,mul_ln74_fu_267_p2_n_138,mul_ln74_fu_267_p2_n_139,mul_ln74_fu_267_p2_n_140,mul_ln74_fu_267_p2_n_141,mul_ln74_fu_267_p2_n_142,mul_ln74_fu_267_p2_n_143,mul_ln74_fu_267_p2_n_144,mul_ln74_fu_267_p2_n_145,mul_ln74_fu_267_p2_n_146,mul_ln74_fu_267_p2_n_147,mul_ln74_fu_267_p2_n_148,mul_ln74_fu_267_p2_n_149,mul_ln74_fu_267_p2_n_150,mul_ln74_fu_267_p2_n_151,mul_ln74_fu_267_p2_n_152,mul_ln74_fu_267_p2_n_153,mul_ln74_fu_267_p2_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln74_fu_267_p2_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 22x15 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln74_fu_267_p2__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [36:35],\fir_coeffs_rom_U/q0_reg [35:17]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln74_fu_267_p2__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({shift_reg1_U_n_3,shift_reg1_U_n_3,shift_reg1_U_n_3,shift_reg1_U_n_3,shift_reg1_U_n_4,shift_reg1_U_n_5,shift_reg1_U_n_6,shift_reg1_U_n_7,shift_reg1_U_n_8,shift_reg1_U_n_9,shift_reg1_U_n_10,shift_reg1_U_n_11,shift_reg1_U_n_12,shift_reg1_U_n_13,shift_reg1_U_n_14,shift_reg1_U_n_15,shift_reg1_U_n_16,shift_reg1_U_n_17}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln74_fu_267_p2__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln74_fu_267_p2__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln74_fu_267_p2__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln74_fu_267_p2__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln74_fu_267_p2__0_OVERFLOW_UNCONNECTED),
        .P({mul_ln74_fu_267_p2__0_n_59,mul_ln74_fu_267_p2__0_n_60,mul_ln74_fu_267_p2__0_n_61,mul_ln74_fu_267_p2__0_n_62,mul_ln74_fu_267_p2__0_n_63,mul_ln74_fu_267_p2__0_n_64,mul_ln74_fu_267_p2__0_n_65,mul_ln74_fu_267_p2__0_n_66,mul_ln74_fu_267_p2__0_n_67,mul_ln74_fu_267_p2__0_n_68,mul_ln74_fu_267_p2__0_n_69,mul_ln74_fu_267_p2__0_n_70,mul_ln74_fu_267_p2__0_n_71,mul_ln74_fu_267_p2__0_n_72,mul_ln74_fu_267_p2__0_n_73,mul_ln74_fu_267_p2__0_n_74,mul_ln74_fu_267_p2__0_n_75,mul_ln74_fu_267_p2__0_n_76,mul_ln74_fu_267_p2__0_n_77,mul_ln74_fu_267_p2__0_n_78,mul_ln74_fu_267_p2__0_n_79,mul_ln74_fu_267_p2__0_n_80,mul_ln74_fu_267_p2__0_n_81,mul_ln74_fu_267_p2__0_n_82,mul_ln74_fu_267_p2__0_n_83,mul_ln74_fu_267_p2__0_n_84,mul_ln74_fu_267_p2__0_n_85,mul_ln74_fu_267_p2__0_n_86,mul_ln74_fu_267_p2__0_n_87,mul_ln74_fu_267_p2__0_n_88,mul_ln74_fu_267_p2__0_n_89,mul_ln74_fu_267_p2__0_n_90,mul_ln74_fu_267_p2__0_n_91,mul_ln74_fu_267_p2__0_n_92,mul_ln74_fu_267_p2__0_n_93,mul_ln74_fu_267_p2__0_n_94,mul_ln74_fu_267_p2__0_n_95,mul_ln74_fu_267_p2__0_n_96,mul_ln74_fu_267_p2__0_n_97,mul_ln74_fu_267_p2__0_n_98,mul_ln74_fu_267_p2__0_n_99,mul_ln74_fu_267_p2__0_n_100,mul_ln74_fu_267_p2__0_n_101,mul_ln74_fu_267_p2__0_n_102,mul_ln74_fu_267_p2__0_n_103,mul_ln74_fu_267_p2__0_n_104,mul_ln74_fu_267_p2__0_n_105,mul_ln74_fu_267_p2__0_n_106}),
        .PATTERNBDETECT(NLW_mul_ln74_fu_267_p2__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln74_fu_267_p2__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln74_fu_267_p2_n_107,mul_ln74_fu_267_p2_n_108,mul_ln74_fu_267_p2_n_109,mul_ln74_fu_267_p2_n_110,mul_ln74_fu_267_p2_n_111,mul_ln74_fu_267_p2_n_112,mul_ln74_fu_267_p2_n_113,mul_ln74_fu_267_p2_n_114,mul_ln74_fu_267_p2_n_115,mul_ln74_fu_267_p2_n_116,mul_ln74_fu_267_p2_n_117,mul_ln74_fu_267_p2_n_118,mul_ln74_fu_267_p2_n_119,mul_ln74_fu_267_p2_n_120,mul_ln74_fu_267_p2_n_121,mul_ln74_fu_267_p2_n_122,mul_ln74_fu_267_p2_n_123,mul_ln74_fu_267_p2_n_124,mul_ln74_fu_267_p2_n_125,mul_ln74_fu_267_p2_n_126,mul_ln74_fu_267_p2_n_127,mul_ln74_fu_267_p2_n_128,mul_ln74_fu_267_p2_n_129,mul_ln74_fu_267_p2_n_130,mul_ln74_fu_267_p2_n_131,mul_ln74_fu_267_p2_n_132,mul_ln74_fu_267_p2_n_133,mul_ln74_fu_267_p2_n_134,mul_ln74_fu_267_p2_n_135,mul_ln74_fu_267_p2_n_136,mul_ln74_fu_267_p2_n_137,mul_ln74_fu_267_p2_n_138,mul_ln74_fu_267_p2_n_139,mul_ln74_fu_267_p2_n_140,mul_ln74_fu_267_p2_n_141,mul_ln74_fu_267_p2_n_142,mul_ln74_fu_267_p2_n_143,mul_ln74_fu_267_p2_n_144,mul_ln74_fu_267_p2_n_145,mul_ln74_fu_267_p2_n_146,mul_ln74_fu_267_p2_n_147,mul_ln74_fu_267_p2_n_148,mul_ln74_fu_267_p2_n_149,mul_ln74_fu_267_p2_n_150,mul_ln74_fu_267_p2_n_151,mul_ln74_fu_267_p2_n_152,mul_ln74_fu_267_p2_n_153,mul_ln74_fu_267_p2_n_154}),
        .PCOUT(NLW_mul_ln74_fu_267_p2__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln74_fu_267_p2__0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln74_fu_267_p2__1
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln74_fu_267_p2__1_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,shift_reg1_U_n_18,shift_reg1_U_n_19,shift_reg1_U_n_20,shift_reg1_U_n_21,shift_reg1_U_n_22,shift_reg1_U_n_23,shift_reg1_U_n_24,shift_reg1_U_n_25,shift_reg1_U_n_26,shift_reg1_U_n_27,shift_reg1_U_n_28,shift_reg1_U_n_29,shift_reg1_U_n_30,shift_reg1_U_n_31,shift_reg1_U_n_32,shift_reg1_U_n_33,shift_reg1_U_n_34}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln74_fu_267_p2__1_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln74_fu_267_p2__1_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln74_fu_267_p2__1_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln74_fu_267_p2__1_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln74_fu_267_p2__1_OVERFLOW_UNCONNECTED),
        .P({mul_ln74_fu_267_p2__1_n_59,mul_ln74_fu_267_p2__1_n_60,mul_ln74_fu_267_p2__1_n_61,mul_ln74_fu_267_p2__1_n_62,mul_ln74_fu_267_p2__1_n_63,mul_ln74_fu_267_p2__1_n_64,mul_ln74_fu_267_p2__1_n_65,mul_ln74_fu_267_p2__1_n_66,mul_ln74_fu_267_p2__1_n_67,mul_ln74_fu_267_p2__1_n_68,mul_ln74_fu_267_p2__1_n_69,mul_ln74_fu_267_p2__1_n_70,mul_ln74_fu_267_p2__1_n_71,mul_ln74_fu_267_p2__1_n_72,mul_ln74_fu_267_p2__1_n_73,mul_ln74_fu_267_p2__1_n_74,mul_ln74_fu_267_p2__1_n_75,mul_ln74_fu_267_p2__1_n_76,mul_ln74_fu_267_p2__1_n_77,mul_ln74_fu_267_p2__1_n_78,mul_ln74_fu_267_p2__1_n_79,mul_ln74_fu_267_p2__1_n_80,mul_ln74_fu_267_p2__1_n_81,mul_ln74_fu_267_p2__1_n_82,mul_ln74_fu_267_p2__1_n_83,mul_ln74_fu_267_p2__1_n_84,mul_ln74_fu_267_p2__1_n_85,mul_ln74_fu_267_p2__1_n_86,mul_ln74_fu_267_p2__1_n_87,mul_ln74_fu_267_p2__1_n_88,mul_ln74_fu_267_p2__1_n_89,mul_ln74_fu_267_p2__1_n_90,mul_ln74_fu_267_p2__1_n_91,mul_ln74_fu_267_p2__1_n_92,mul_ln74_fu_267_p2__1_n_93,mul_ln74_fu_267_p2__1_n_94,mul_ln74_fu_267_p2__1_n_95,mul_ln74_fu_267_p2__1_n_96,mul_ln74_fu_267_p2__1_n_97,mul_ln74_fu_267_p2__1_n_98,mul_ln74_fu_267_p2__1_n_99,mul_ln74_fu_267_p2__1_n_100,mul_ln74_fu_267_p2__1_n_101,mul_ln74_fu_267_p2__1_n_102,mul_ln74_fu_267_p2__1_n_103,mul_ln74_fu_267_p2__1_n_104,mul_ln74_fu_267_p2__1_n_105,mul_ln74_fu_267_p2__1_n_106}),
        .PATTERNBDETECT(NLW_mul_ln74_fu_267_p2__1_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln74_fu_267_p2__1_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln74_fu_267_p2__1_n_107,mul_ln74_fu_267_p2__1_n_108,mul_ln74_fu_267_p2__1_n_109,mul_ln74_fu_267_p2__1_n_110,mul_ln74_fu_267_p2__1_n_111,mul_ln74_fu_267_p2__1_n_112,mul_ln74_fu_267_p2__1_n_113,mul_ln74_fu_267_p2__1_n_114,mul_ln74_fu_267_p2__1_n_115,mul_ln74_fu_267_p2__1_n_116,mul_ln74_fu_267_p2__1_n_117,mul_ln74_fu_267_p2__1_n_118,mul_ln74_fu_267_p2__1_n_119,mul_ln74_fu_267_p2__1_n_120,mul_ln74_fu_267_p2__1_n_121,mul_ln74_fu_267_p2__1_n_122,mul_ln74_fu_267_p2__1_n_123,mul_ln74_fu_267_p2__1_n_124,mul_ln74_fu_267_p2__1_n_125,mul_ln74_fu_267_p2__1_n_126,mul_ln74_fu_267_p2__1_n_127,mul_ln74_fu_267_p2__1_n_128,mul_ln74_fu_267_p2__1_n_129,mul_ln74_fu_267_p2__1_n_130,mul_ln74_fu_267_p2__1_n_131,mul_ln74_fu_267_p2__1_n_132,mul_ln74_fu_267_p2__1_n_133,mul_ln74_fu_267_p2__1_n_134,mul_ln74_fu_267_p2__1_n_135,mul_ln74_fu_267_p2__1_n_136,mul_ln74_fu_267_p2__1_n_137,mul_ln74_fu_267_p2__1_n_138,mul_ln74_fu_267_p2__1_n_139,mul_ln74_fu_267_p2__1_n_140,mul_ln74_fu_267_p2__1_n_141,mul_ln74_fu_267_p2__1_n_142,mul_ln74_fu_267_p2__1_n_143,mul_ln74_fu_267_p2__1_n_144,mul_ln74_fu_267_p2__1_n_145,mul_ln74_fu_267_p2__1_n_146,mul_ln74_fu_267_p2__1_n_147,mul_ln74_fu_267_p2__1_n_148,mul_ln74_fu_267_p2__1_n_149,mul_ln74_fu_267_p2__1_n_150,mul_ln74_fu_267_p2__1_n_151,mul_ln74_fu_267_p2__1_n_152,mul_ln74_fu_267_p2__1_n_153,mul_ln74_fu_267_p2__1_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln74_fu_267_p2__1_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x15 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln74_fu_267_p2__2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\fir_coeffs_rom_U/q0_reg [16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln74_fu_267_p2__2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({shift_reg1_U_n_3,shift_reg1_U_n_3,shift_reg1_U_n_3,shift_reg1_U_n_3,shift_reg1_U_n_4,shift_reg1_U_n_5,shift_reg1_U_n_6,shift_reg1_U_n_7,shift_reg1_U_n_8,shift_reg1_U_n_9,shift_reg1_U_n_10,shift_reg1_U_n_11,shift_reg1_U_n_12,shift_reg1_U_n_13,shift_reg1_U_n_14,shift_reg1_U_n_15,shift_reg1_U_n_16,shift_reg1_U_n_17}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln74_fu_267_p2__2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln74_fu_267_p2__2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln74_fu_267_p2__2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(shift_reg0_ce0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln74_fu_267_p2__2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln74_fu_267_p2__2_OVERFLOW_UNCONNECTED),
        .P({mul_ln74_fu_267_p2__2_n_59,mul_ln74_fu_267_p2__2_n_60,mul_ln74_fu_267_p2__2_n_61,mul_ln74_fu_267_p2__2_n_62,mul_ln74_fu_267_p2__2_n_63,mul_ln74_fu_267_p2__2_n_64,mul_ln74_fu_267_p2__2_n_65,mul_ln74_fu_267_p2__2_n_66,mul_ln74_fu_267_p2__2_n_67,mul_ln74_fu_267_p2__2_n_68,mul_ln74_fu_267_p2__2_n_69,mul_ln74_fu_267_p2__2_n_70,mul_ln74_fu_267_p2__2_n_71,mul_ln74_fu_267_p2__2_n_72,mul_ln74_fu_267_p2__2_n_73,mul_ln74_fu_267_p2__2_n_74,mul_ln74_fu_267_p2__2_n_75,mul_ln74_fu_267_p2__2_n_76,mul_ln74_fu_267_p2__2_n_77,mul_ln74_fu_267_p2__2_n_78,mul_ln74_fu_267_p2__2_n_79,mul_ln74_fu_267_p2__2_n_80,mul_ln74_fu_267_p2__2_n_81,mul_ln74_fu_267_p2__2_n_82,mul_ln74_fu_267_p2__2_n_83,mul_ln74_fu_267_p2__2_n_84,mul_ln74_fu_267_p2__2_n_85,mul_ln74_fu_267_p2__2_n_86,mul_ln74_fu_267_p2__2_n_87,mul_ln74_fu_267_p2__2_n_88,mul_ln74_fu_267_p2__2_n_89,mul_ln74_fu_267_p2__2_n_90,mul_ln74_fu_267_p2__2_n_91,mul_ln74_fu_267_p2__2_n_92,mul_ln74_fu_267_p2__2_n_93,mul_ln74_fu_267_p2__2_n_94,mul_ln74_fu_267_p2__2_n_95,mul_ln74_fu_267_p2__2_n_96,mul_ln74_fu_267_p2__2_n_97,mul_ln74_fu_267_p2__2_n_98,mul_ln74_fu_267_p2__2_n_99,mul_ln74_fu_267_p2__2_n_100,mul_ln74_fu_267_p2__2_n_101,mul_ln74_fu_267_p2__2_n_102,mul_ln74_fu_267_p2__2_n_103,mul_ln74_fu_267_p2__2_n_104,mul_ln74_fu_267_p2__2_n_105,mul_ln74_fu_267_p2__2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln74_fu_267_p2__2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln74_fu_267_p2__2_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln74_fu_267_p2__1_n_107,mul_ln74_fu_267_p2__1_n_108,mul_ln74_fu_267_p2__1_n_109,mul_ln74_fu_267_p2__1_n_110,mul_ln74_fu_267_p2__1_n_111,mul_ln74_fu_267_p2__1_n_112,mul_ln74_fu_267_p2__1_n_113,mul_ln74_fu_267_p2__1_n_114,mul_ln74_fu_267_p2__1_n_115,mul_ln74_fu_267_p2__1_n_116,mul_ln74_fu_267_p2__1_n_117,mul_ln74_fu_267_p2__1_n_118,mul_ln74_fu_267_p2__1_n_119,mul_ln74_fu_267_p2__1_n_120,mul_ln74_fu_267_p2__1_n_121,mul_ln74_fu_267_p2__1_n_122,mul_ln74_fu_267_p2__1_n_123,mul_ln74_fu_267_p2__1_n_124,mul_ln74_fu_267_p2__1_n_125,mul_ln74_fu_267_p2__1_n_126,mul_ln74_fu_267_p2__1_n_127,mul_ln74_fu_267_p2__1_n_128,mul_ln74_fu_267_p2__1_n_129,mul_ln74_fu_267_p2__1_n_130,mul_ln74_fu_267_p2__1_n_131,mul_ln74_fu_267_p2__1_n_132,mul_ln74_fu_267_p2__1_n_133,mul_ln74_fu_267_p2__1_n_134,mul_ln74_fu_267_p2__1_n_135,mul_ln74_fu_267_p2__1_n_136,mul_ln74_fu_267_p2__1_n_137,mul_ln74_fu_267_p2__1_n_138,mul_ln74_fu_267_p2__1_n_139,mul_ln74_fu_267_p2__1_n_140,mul_ln74_fu_267_p2__1_n_141,mul_ln74_fu_267_p2__1_n_142,mul_ln74_fu_267_p2__1_n_143,mul_ln74_fu_267_p2__1_n_144,mul_ln74_fu_267_p2__1_n_145,mul_ln74_fu_267_p2__1_n_146,mul_ln74_fu_267_p2__1_n_147,mul_ln74_fu_267_p2__1_n_148,mul_ln74_fu_267_p2__1_n_149,mul_ln74_fu_267_p2__1_n_150,mul_ln74_fu_267_p2__1_n_151,mul_ln74_fu_267_p2__1_n_152,mul_ln74_fu_267_p2__1_n_153,mul_ln74_fu_267_p2__1_n_154}),
        .PCOUT(NLW_mul_ln74_fu_267_p2__2_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln74_fu_267_p2__2_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 15x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln80_fu_191_p2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln80_fu_191_p2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_4,regslice_both_x_U_n_5,regslice_both_x_U_n_6,regslice_both_x_U_n_7,regslice_both_x_U_n_8,regslice_both_x_U_n_9,regslice_both_x_U_n_10,regslice_both_x_U_n_11,regslice_both_x_U_n_12,regslice_both_x_U_n_13,regslice_both_x_U_n_14,regslice_both_x_U_n_15,regslice_both_x_U_n_16,regslice_both_x_U_n_17}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln80_fu_191_p2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln80_fu_191_p2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln80_fu_191_p2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(ap_NS_fsm11_out),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln80_fu_191_p2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln80_fu_191_p2_OVERFLOW_UNCONNECTED),
        .P({mul_ln80_fu_191_p2_n_59,mul_ln80_fu_191_p2_n_60,mul_ln80_fu_191_p2_n_61,mul_ln80_fu_191_p2_n_62,mul_ln80_fu_191_p2_n_63,mul_ln80_fu_191_p2_n_64,mul_ln80_fu_191_p2_n_65,mul_ln80_fu_191_p2_n_66,mul_ln80_fu_191_p2_n_67,mul_ln80_fu_191_p2_n_68,mul_ln80_fu_191_p2_n_69,mul_ln80_fu_191_p2_n_70,mul_ln80_fu_191_p2_n_71,mul_ln80_fu_191_p2_n_72,mul_ln80_fu_191_p2_n_73,mul_ln80_fu_191_p2_n_74,mul_ln80_fu_191_p2_n_75,mul_ln80_fu_191_p2_n_76,mul_ln80_fu_191_p2_n_77,mul_ln80_fu_191_p2_n_78,mul_ln80_fu_191_p2_n_79,mul_ln80_fu_191_p2_n_80,mul_ln80_fu_191_p2_n_81,mul_ln80_fu_191_p2_n_82,mul_ln80_fu_191_p2_n_83,mul_ln80_fu_191_p2_n_84,mul_ln80_fu_191_p2_n_85,mul_ln80_fu_191_p2_n_86,mul_ln80_fu_191_p2_n_87,mul_ln80_fu_191_p2_n_88,mul_ln80_fu_191_p2_n_89,mul_ln80_fu_191_p2_n_90,mul_ln80_fu_191_p2_n_91,mul_ln80_fu_191_p2_n_92,mul_ln80_fu_191_p2_n_93,mul_ln80_fu_191_p2_n_94,mul_ln80_fu_191_p2_n_95,mul_ln80_fu_191_p2_n_96,mul_ln80_fu_191_p2_n_97,mul_ln80_fu_191_p2_n_98,mul_ln80_fu_191_p2_n_99,mul_ln80_fu_191_p2_n_100,mul_ln80_fu_191_p2_n_101,mul_ln80_fu_191_p2_n_102,mul_ln80_fu_191_p2_n_103,mul_ln80_fu_191_p2_n_104,mul_ln80_fu_191_p2_n_105,mul_ln80_fu_191_p2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln80_fu_191_p2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln80_fu_191_p2_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln80_fu_191_p2_n_107,mul_ln80_fu_191_p2_n_108,mul_ln80_fu_191_p2_n_109,mul_ln80_fu_191_p2_n_110,mul_ln80_fu_191_p2_n_111,mul_ln80_fu_191_p2_n_112,mul_ln80_fu_191_p2_n_113,mul_ln80_fu_191_p2_n_114,mul_ln80_fu_191_p2_n_115,mul_ln80_fu_191_p2_n_116,mul_ln80_fu_191_p2_n_117,mul_ln80_fu_191_p2_n_118,mul_ln80_fu_191_p2_n_119,mul_ln80_fu_191_p2_n_120,mul_ln80_fu_191_p2_n_121,mul_ln80_fu_191_p2_n_122,mul_ln80_fu_191_p2_n_123,mul_ln80_fu_191_p2_n_124,mul_ln80_fu_191_p2_n_125,mul_ln80_fu_191_p2_n_126,mul_ln80_fu_191_p2_n_127,mul_ln80_fu_191_p2_n_128,mul_ln80_fu_191_p2_n_129,mul_ln80_fu_191_p2_n_130,mul_ln80_fu_191_p2_n_131,mul_ln80_fu_191_p2_n_132,mul_ln80_fu_191_p2_n_133,mul_ln80_fu_191_p2_n_134,mul_ln80_fu_191_p2_n_135,mul_ln80_fu_191_p2_n_136,mul_ln80_fu_191_p2_n_137,mul_ln80_fu_191_p2_n_138,mul_ln80_fu_191_p2_n_139,mul_ln80_fu_191_p2_n_140,mul_ln80_fu_191_p2_n_141,mul_ln80_fu_191_p2_n_142,mul_ln80_fu_191_p2_n_143,mul_ln80_fu_191_p2_n_144,mul_ln80_fu_191_p2_n_145,mul_ln80_fu_191_p2_n_146,mul_ln80_fu_191_p2_n_147,mul_ln80_fu_191_p2_n_148,mul_ln80_fu_191_p2_n_149,mul_ln80_fu_191_p2_n_150,mul_ln80_fu_191_p2_n_151,mul_ln80_fu_191_p2_n_152,mul_ln80_fu_191_p2_n_153,mul_ln80_fu_191_p2_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln80_fu_191_p2_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 15x22 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln80_fu_191_p2__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln80_fu_191_p2__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_4,regslice_both_x_U_n_5,regslice_both_x_U_n_6,regslice_both_x_U_n_7,regslice_both_x_U_n_8,regslice_both_x_U_n_9,regslice_both_x_U_n_10,regslice_both_x_U_n_11,regslice_both_x_U_n_12,regslice_both_x_U_n_13,regslice_both_x_U_n_14,regslice_both_x_U_n_15,regslice_both_x_U_n_16,regslice_both_x_U_n_17}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln80_fu_191_p2__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln80_fu_191_p2__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln80_fu_191_p2__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(ap_NS_fsm11_out),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln80_fu_191_p2__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln80_fu_191_p2__0_OVERFLOW_UNCONNECTED),
        .P({mul_ln80_fu_191_p2__0_n_59,mul_ln80_fu_191_p2__0_n_60,mul_ln80_fu_191_p2__0_n_61,mul_ln80_fu_191_p2__0_n_62,mul_ln80_fu_191_p2__0_n_63,mul_ln80_fu_191_p2__0_n_64,mul_ln80_fu_191_p2__0_n_65,mul_ln80_fu_191_p2__0_n_66,mul_ln80_fu_191_p2__0_n_67,mul_ln80_fu_191_p2__0_n_68,mul_ln80_fu_191_p2__0_n_69,mul_ln80_fu_191_p2__0_n_70,mul_ln80_fu_191_p2__0_n_71,mul_ln80_fu_191_p2__0_n_72,mul_ln80_fu_191_p2__0_n_73,mul_ln80_fu_191_p2__0_n_74,mul_ln80_fu_191_p2__0_n_75,mul_ln80_fu_191_p2__0_n_76,mul_ln80_fu_191_p2__0_n_77,mul_ln80_fu_191_p2__0_n_78,mul_ln80_fu_191_p2__0_n_79,mul_ln80_fu_191_p2__0_n_80,mul_ln80_fu_191_p2__0_n_81,mul_ln80_fu_191_p2__0_n_82,mul_ln80_fu_191_p2__0_n_83,mul_ln80_fu_191_p2__0_n_84,mul_ln80_fu_191_p2__0_n_85,mul_ln80_fu_191_p2__0_n_86,mul_ln80_fu_191_p2__0_n_87,mul_ln80_fu_191_p2__0_n_88,mul_ln80_fu_191_p2__0_n_89,mul_ln80_fu_191_p2__0_n_90,mul_ln80_fu_191_p2__0_n_91,mul_ln80_fu_191_p2__0_n_92,mul_ln80_fu_191_p2__0_n_93,mul_ln80_fu_191_p2__0_n_94,mul_ln80_fu_191_p2__0_n_95,mul_ln80_fu_191_p2__0_n_96,mul_ln80_fu_191_p2__0_n_97,mul_ln80_fu_191_p2__0_n_98,mul_ln80_fu_191_p2__0_n_99,mul_ln80_fu_191_p2__0_n_100,mul_ln80_fu_191_p2__0_n_101,mul_ln80_fu_191_p2__0_n_102,mul_ln80_fu_191_p2__0_n_103,mul_ln80_fu_191_p2__0_n_104,mul_ln80_fu_191_p2__0_n_105,mul_ln80_fu_191_p2__0_n_106}),
        .PATTERNBDETECT(NLW_mul_ln80_fu_191_p2__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln80_fu_191_p2__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln80_fu_191_p2_n_107,mul_ln80_fu_191_p2_n_108,mul_ln80_fu_191_p2_n_109,mul_ln80_fu_191_p2_n_110,mul_ln80_fu_191_p2_n_111,mul_ln80_fu_191_p2_n_112,mul_ln80_fu_191_p2_n_113,mul_ln80_fu_191_p2_n_114,mul_ln80_fu_191_p2_n_115,mul_ln80_fu_191_p2_n_116,mul_ln80_fu_191_p2_n_117,mul_ln80_fu_191_p2_n_118,mul_ln80_fu_191_p2_n_119,mul_ln80_fu_191_p2_n_120,mul_ln80_fu_191_p2_n_121,mul_ln80_fu_191_p2_n_122,mul_ln80_fu_191_p2_n_123,mul_ln80_fu_191_p2_n_124,mul_ln80_fu_191_p2_n_125,mul_ln80_fu_191_p2_n_126,mul_ln80_fu_191_p2_n_127,mul_ln80_fu_191_p2_n_128,mul_ln80_fu_191_p2_n_129,mul_ln80_fu_191_p2_n_130,mul_ln80_fu_191_p2_n_131,mul_ln80_fu_191_p2_n_132,mul_ln80_fu_191_p2_n_133,mul_ln80_fu_191_p2_n_134,mul_ln80_fu_191_p2_n_135,mul_ln80_fu_191_p2_n_136,mul_ln80_fu_191_p2_n_137,mul_ln80_fu_191_p2_n_138,mul_ln80_fu_191_p2_n_139,mul_ln80_fu_191_p2_n_140,mul_ln80_fu_191_p2_n_141,mul_ln80_fu_191_p2_n_142,mul_ln80_fu_191_p2_n_143,mul_ln80_fu_191_p2_n_144,mul_ln80_fu_191_p2_n_145,mul_ln80_fu_191_p2_n_146,mul_ln80_fu_191_p2_n_147,mul_ln80_fu_191_p2_n_148,mul_ln80_fu_191_p2_n_149,mul_ln80_fu_191_p2_n_150,mul_ln80_fu_191_p2_n_151,mul_ln80_fu_191_p2_n_152,mul_ln80_fu_191_p2_n_153,mul_ln80_fu_191_p2_n_154}),
        .PCOUT(NLW_mul_ln80_fu_191_p2__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln80_fu_191_p2__0_UNDERFLOW_UNCONNECTED));
  FDRE mul_ln80_fu_191_p2__0__0
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_40),
        .Q(mul_ln80_fu_191_p2__0__0_n_1),
        .R(ap_rst_n_inv));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln80_fu_191_p2__1
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,mul_ln80_fu_191_p2__0__0_n_1,mul_ln80_fu_191_p2__1__0_n_1,mul_ln80_fu_191_p2__2__0_n_1,mul_ln80_fu_191_p2__3_n_1,mul_ln80_fu_191_p2__4_n_1,mul_ln80_fu_191_p2__5_n_1,mul_ln80_fu_191_p2__6_n_1,mul_ln80_fu_191_p2__7_n_1,mul_ln80_fu_191_p2__8_n_1,mul_ln80_fu_191_p2__9_n_1,mul_ln80_fu_191_p2__10_n_1,mul_ln80_fu_191_p2__11_n_1,mul_ln80_fu_191_p2__12_n_1,mul_ln80_fu_191_p2__13_n_1,mul_ln80_fu_191_p2__14_n_1,mul_ln80_fu_191_p2__15_n_1,mul_ln80_fu_191_p2__16_n_1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln80_fu_191_p2__1_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln80_fu_191_p2__1_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln80_fu_191_p2__1_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln80_fu_191_p2__1_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(ap_NS_fsm11_out),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln80_fu_191_p2__1_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln80_fu_191_p2__1_OVERFLOW_UNCONNECTED),
        .P({mul_ln80_fu_191_p2__1_n_59,mul_ln80_fu_191_p2__1_n_60,mul_ln80_fu_191_p2__1_n_61,mul_ln80_fu_191_p2__1_n_62,mul_ln80_fu_191_p2__1_n_63,mul_ln80_fu_191_p2__1_n_64,mul_ln80_fu_191_p2__1_n_65,mul_ln80_fu_191_p2__1_n_66,mul_ln80_fu_191_p2__1_n_67,mul_ln80_fu_191_p2__1_n_68,mul_ln80_fu_191_p2__1_n_69,mul_ln80_fu_191_p2__1_n_70,mul_ln80_fu_191_p2__1_n_71,mul_ln80_fu_191_p2__1_n_72,mul_ln80_fu_191_p2__1_n_73,mul_ln80_fu_191_p2__1_n_74,mul_ln80_fu_191_p2__1_n_75,mul_ln80_fu_191_p2__1_n_76,mul_ln80_fu_191_p2__1_n_77,mul_ln80_fu_191_p2__1_n_78,mul_ln80_fu_191_p2__1_n_79,mul_ln80_fu_191_p2__1_n_80,mul_ln80_fu_191_p2__1_n_81,mul_ln80_fu_191_p2__1_n_82,mul_ln80_fu_191_p2__1_n_83,mul_ln80_fu_191_p2__1_n_84,mul_ln80_fu_191_p2__1_n_85,mul_ln80_fu_191_p2__1_n_86,mul_ln80_fu_191_p2__1_n_87,mul_ln80_fu_191_p2__1_n_88,mul_ln80_fu_191_p2__1_n_89,mul_ln80_fu_191_p2__1_n_90,mul_ln80_fu_191_p2__1_n_91,mul_ln80_fu_191_p2__1_n_92,mul_ln80_fu_191_p2__1_n_93,mul_ln80_fu_191_p2__1_n_94,mul_ln80_fu_191_p2__1_n_95,mul_ln80_fu_191_p2__1_n_96,mul_ln80_fu_191_p2__1_n_97,mul_ln80_fu_191_p2__1_n_98,mul_ln80_fu_191_p2__1_n_99,mul_ln80_fu_191_p2__1_n_100,mul_ln80_fu_191_p2__1_n_101,mul_ln80_fu_191_p2__1_n_102,mul_ln80_fu_191_p2__1_n_103,mul_ln80_fu_191_p2__1_n_104,mul_ln80_fu_191_p2__1_n_105,mul_ln80_fu_191_p2__1_n_106}),
        .PATTERNBDETECT(NLW_mul_ln80_fu_191_p2__1_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln80_fu_191_p2__1_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln80_fu_191_p2__1_n_107,mul_ln80_fu_191_p2__1_n_108,mul_ln80_fu_191_p2__1_n_109,mul_ln80_fu_191_p2__1_n_110,mul_ln80_fu_191_p2__1_n_111,mul_ln80_fu_191_p2__1_n_112,mul_ln80_fu_191_p2__1_n_113,mul_ln80_fu_191_p2__1_n_114,mul_ln80_fu_191_p2__1_n_115,mul_ln80_fu_191_p2__1_n_116,mul_ln80_fu_191_p2__1_n_117,mul_ln80_fu_191_p2__1_n_118,mul_ln80_fu_191_p2__1_n_119,mul_ln80_fu_191_p2__1_n_120,mul_ln80_fu_191_p2__1_n_121,mul_ln80_fu_191_p2__1_n_122,mul_ln80_fu_191_p2__1_n_123,mul_ln80_fu_191_p2__1_n_124,mul_ln80_fu_191_p2__1_n_125,mul_ln80_fu_191_p2__1_n_126,mul_ln80_fu_191_p2__1_n_127,mul_ln80_fu_191_p2__1_n_128,mul_ln80_fu_191_p2__1_n_129,mul_ln80_fu_191_p2__1_n_130,mul_ln80_fu_191_p2__1_n_131,mul_ln80_fu_191_p2__1_n_132,mul_ln80_fu_191_p2__1_n_133,mul_ln80_fu_191_p2__1_n_134,mul_ln80_fu_191_p2__1_n_135,mul_ln80_fu_191_p2__1_n_136,mul_ln80_fu_191_p2__1_n_137,mul_ln80_fu_191_p2__1_n_138,mul_ln80_fu_191_p2__1_n_139,mul_ln80_fu_191_p2__1_n_140,mul_ln80_fu_191_p2__1_n_141,mul_ln80_fu_191_p2__1_n_142,mul_ln80_fu_191_p2__1_n_143,mul_ln80_fu_191_p2__1_n_144,mul_ln80_fu_191_p2__1_n_145,mul_ln80_fu_191_p2__1_n_146,mul_ln80_fu_191_p2__1_n_147,mul_ln80_fu_191_p2__1_n_148,mul_ln80_fu_191_p2__1_n_149,mul_ln80_fu_191_p2__1_n_150,mul_ln80_fu_191_p2__1_n_151,mul_ln80_fu_191_p2__1_n_152,mul_ln80_fu_191_p2__1_n_153,mul_ln80_fu_191_p2__1_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln80_fu_191_p2__1_UNDERFLOW_UNCONNECTED));
  FDRE mul_ln80_fu_191_p2__10
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_30),
        .Q(mul_ln80_fu_191_p2__10_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__11
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_29),
        .Q(mul_ln80_fu_191_p2__11_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__12
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_28),
        .Q(mul_ln80_fu_191_p2__12_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__13
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_27),
        .Q(mul_ln80_fu_191_p2__13_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__14
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_26),
        .Q(mul_ln80_fu_191_p2__14_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__15
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_25),
        .Q(mul_ln80_fu_191_p2__15_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__16
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_24),
        .Q(mul_ln80_fu_191_p2__16_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__1__0
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_39),
        .Q(mul_ln80_fu_191_p2__1__0_n_1),
        .R(ap_rst_n_inv));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x22 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln80_fu_191_p2__2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln80_fu_191_p2__2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,mul_ln80_fu_191_p2__0__0_n_1,mul_ln80_fu_191_p2__1__0_n_1,mul_ln80_fu_191_p2__2__0_n_1,mul_ln80_fu_191_p2__3_n_1,mul_ln80_fu_191_p2__4_n_1,mul_ln80_fu_191_p2__5_n_1,mul_ln80_fu_191_p2__6_n_1,mul_ln80_fu_191_p2__7_n_1,mul_ln80_fu_191_p2__8_n_1,mul_ln80_fu_191_p2__9_n_1,mul_ln80_fu_191_p2__10_n_1,mul_ln80_fu_191_p2__11_n_1,mul_ln80_fu_191_p2__12_n_1,mul_ln80_fu_191_p2__13_n_1,mul_ln80_fu_191_p2__14_n_1,mul_ln80_fu_191_p2__15_n_1,mul_ln80_fu_191_p2__16_n_1}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln80_fu_191_p2__2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln80_fu_191_p2__2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln80_fu_191_p2__2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(ap_NS_fsm11_out),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln80_fu_191_p2__2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln80_fu_191_p2__2_OVERFLOW_UNCONNECTED),
        .P({mul_ln80_fu_191_p2__2_n_59,mul_ln80_fu_191_p2__2_n_60,mul_ln80_fu_191_p2__2_n_61,mul_ln80_fu_191_p2__2_n_62,mul_ln80_fu_191_p2__2_n_63,mul_ln80_fu_191_p2__2_n_64,mul_ln80_fu_191_p2__2_n_65,mul_ln80_fu_191_p2__2_n_66,mul_ln80_fu_191_p2__2_n_67,mul_ln80_fu_191_p2__2_n_68,mul_ln80_fu_191_p2__2_n_69,mul_ln80_fu_191_p2__2_n_70,mul_ln80_fu_191_p2__2_n_71,mul_ln80_fu_191_p2__2_n_72,mul_ln80_fu_191_p2__2_n_73,mul_ln80_fu_191_p2__2_n_74,mul_ln80_fu_191_p2__2_n_75,mul_ln80_fu_191_p2__2_n_76,mul_ln80_fu_191_p2__2_n_77,mul_ln80_fu_191_p2__2_n_78,mul_ln80_fu_191_p2__2_n_79,mul_ln80_fu_191_p2__2_n_80,mul_ln80_fu_191_p2__2_n_81,mul_ln80_fu_191_p2__2_n_82,mul_ln80_fu_191_p2__2_n_83,mul_ln80_fu_191_p2__2_n_84,mul_ln80_fu_191_p2__2_n_85,mul_ln80_fu_191_p2__2_n_86,mul_ln80_fu_191_p2__2_n_87,mul_ln80_fu_191_p2__2_n_88,mul_ln80_fu_191_p2__2_n_89,mul_ln80_fu_191_p2__2_n_90,mul_ln80_fu_191_p2__2_n_91,mul_ln80_fu_191_p2__2_n_92,mul_ln80_fu_191_p2__2_n_93,mul_ln80_fu_191_p2__2_n_94,mul_ln80_fu_191_p2__2_n_95,mul_ln80_fu_191_p2__2_n_96,mul_ln80_fu_191_p2__2_n_97,mul_ln80_fu_191_p2__2_n_98,mul_ln80_fu_191_p2__2_n_99,mul_ln80_fu_191_p2__2_n_100,mul_ln80_fu_191_p2__2_n_101,mul_ln80_fu_191_p2__2_n_102,mul_ln80_fu_191_p2__2_n_103,mul_ln80_fu_191_p2__2_n_104,mul_ln80_fu_191_p2__2_n_105,mul_ln80_fu_191_p2__2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln80_fu_191_p2__2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln80_fu_191_p2__2_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln80_fu_191_p2__1_n_107,mul_ln80_fu_191_p2__1_n_108,mul_ln80_fu_191_p2__1_n_109,mul_ln80_fu_191_p2__1_n_110,mul_ln80_fu_191_p2__1_n_111,mul_ln80_fu_191_p2__1_n_112,mul_ln80_fu_191_p2__1_n_113,mul_ln80_fu_191_p2__1_n_114,mul_ln80_fu_191_p2__1_n_115,mul_ln80_fu_191_p2__1_n_116,mul_ln80_fu_191_p2__1_n_117,mul_ln80_fu_191_p2__1_n_118,mul_ln80_fu_191_p2__1_n_119,mul_ln80_fu_191_p2__1_n_120,mul_ln80_fu_191_p2__1_n_121,mul_ln80_fu_191_p2__1_n_122,mul_ln80_fu_191_p2__1_n_123,mul_ln80_fu_191_p2__1_n_124,mul_ln80_fu_191_p2__1_n_125,mul_ln80_fu_191_p2__1_n_126,mul_ln80_fu_191_p2__1_n_127,mul_ln80_fu_191_p2__1_n_128,mul_ln80_fu_191_p2__1_n_129,mul_ln80_fu_191_p2__1_n_130,mul_ln80_fu_191_p2__1_n_131,mul_ln80_fu_191_p2__1_n_132,mul_ln80_fu_191_p2__1_n_133,mul_ln80_fu_191_p2__1_n_134,mul_ln80_fu_191_p2__1_n_135,mul_ln80_fu_191_p2__1_n_136,mul_ln80_fu_191_p2__1_n_137,mul_ln80_fu_191_p2__1_n_138,mul_ln80_fu_191_p2__1_n_139,mul_ln80_fu_191_p2__1_n_140,mul_ln80_fu_191_p2__1_n_141,mul_ln80_fu_191_p2__1_n_142,mul_ln80_fu_191_p2__1_n_143,mul_ln80_fu_191_p2__1_n_144,mul_ln80_fu_191_p2__1_n_145,mul_ln80_fu_191_p2__1_n_146,mul_ln80_fu_191_p2__1_n_147,mul_ln80_fu_191_p2__1_n_148,mul_ln80_fu_191_p2__1_n_149,mul_ln80_fu_191_p2__1_n_150,mul_ln80_fu_191_p2__1_n_151,mul_ln80_fu_191_p2__1_n_152,mul_ln80_fu_191_p2__1_n_153,mul_ln80_fu_191_p2__1_n_154}),
        .PCOUT(NLW_mul_ln80_fu_191_p2__2_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln80_fu_191_p2__2_UNDERFLOW_UNCONNECTED));
  FDRE mul_ln80_fu_191_p2__2__0
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_38),
        .Q(mul_ln80_fu_191_p2__2__0_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__3
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_37),
        .Q(mul_ln80_fu_191_p2__3_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__4
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_36),
        .Q(mul_ln80_fu_191_p2__4_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__5
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_35),
        .Q(mul_ln80_fu_191_p2__5_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__6
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_34),
        .Q(mul_ln80_fu_191_p2__6_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__7
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_33),
        .Q(mul_ln80_fu_191_p2__7_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__8
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_32),
        .Q(mul_ln80_fu_191_p2__8_n_1),
        .R(ap_rst_n_inv));
  FDRE mul_ln80_fu_191_p2__9
       (.C(ap_clk),
        .CE(regslice_both_x_U_n_2),
        .D(regslice_both_x_U_n_31),
        .Q(mul_ln80_fu_191_p2__9_n_1),
        .R(ap_rst_n_inv));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 15x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln81_fu_206_p2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln81_fu_206_p2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_4,regslice_both_x_U_n_5,regslice_both_x_U_n_6,regslice_both_x_U_n_7,regslice_both_x_U_n_8,regslice_both_x_U_n_9,regslice_both_x_U_n_10,regslice_both_x_U_n_11,regslice_both_x_U_n_12,regslice_both_x_U_n_13,regslice_both_x_U_n_14,regslice_both_x_U_n_15,regslice_both_x_U_n_16,regslice_both_x_U_n_17}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln81_fu_206_p2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln81_fu_206_p2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln81_fu_206_p2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(ack_out1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln81_fu_206_p2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln81_fu_206_p2_OVERFLOW_UNCONNECTED),
        .P({mul_ln81_fu_206_p2_n_59,mul_ln81_fu_206_p2_n_60,mul_ln81_fu_206_p2_n_61,mul_ln81_fu_206_p2_n_62,mul_ln81_fu_206_p2_n_63,mul_ln81_fu_206_p2_n_64,mul_ln81_fu_206_p2_n_65,mul_ln81_fu_206_p2_n_66,mul_ln81_fu_206_p2_n_67,mul_ln81_fu_206_p2_n_68,mul_ln81_fu_206_p2_n_69,mul_ln81_fu_206_p2_n_70,mul_ln81_fu_206_p2_n_71,mul_ln81_fu_206_p2_n_72,mul_ln81_fu_206_p2_n_73,mul_ln81_fu_206_p2_n_74,mul_ln81_fu_206_p2_n_75,mul_ln81_fu_206_p2_n_76,mul_ln81_fu_206_p2_n_77,mul_ln81_fu_206_p2_n_78,mul_ln81_fu_206_p2_n_79,mul_ln81_fu_206_p2_n_80,mul_ln81_fu_206_p2_n_81,mul_ln81_fu_206_p2_n_82,mul_ln81_fu_206_p2_n_83,mul_ln81_fu_206_p2_n_84,mul_ln81_fu_206_p2_n_85,mul_ln81_fu_206_p2_n_86,mul_ln81_fu_206_p2_n_87,mul_ln81_fu_206_p2_n_88,mul_ln81_fu_206_p2_n_89,mul_ln81_fu_206_p2_n_90,mul_ln81_fu_206_p2_n_91,mul_ln81_fu_206_p2_n_92,mul_ln81_fu_206_p2_n_93,mul_ln81_fu_206_p2_n_94,mul_ln81_fu_206_p2_n_95,mul_ln81_fu_206_p2_n_96,mul_ln81_fu_206_p2_n_97,mul_ln81_fu_206_p2_n_98,mul_ln81_fu_206_p2_n_99,mul_ln81_fu_206_p2_n_100,mul_ln81_fu_206_p2_n_101,mul_ln81_fu_206_p2_n_102,mul_ln81_fu_206_p2_n_103,mul_ln81_fu_206_p2_n_104,mul_ln81_fu_206_p2_n_105,mul_ln81_fu_206_p2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln81_fu_206_p2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln81_fu_206_p2_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln81_fu_206_p2_n_107,mul_ln81_fu_206_p2_n_108,mul_ln81_fu_206_p2_n_109,mul_ln81_fu_206_p2_n_110,mul_ln81_fu_206_p2_n_111,mul_ln81_fu_206_p2_n_112,mul_ln81_fu_206_p2_n_113,mul_ln81_fu_206_p2_n_114,mul_ln81_fu_206_p2_n_115,mul_ln81_fu_206_p2_n_116,mul_ln81_fu_206_p2_n_117,mul_ln81_fu_206_p2_n_118,mul_ln81_fu_206_p2_n_119,mul_ln81_fu_206_p2_n_120,mul_ln81_fu_206_p2_n_121,mul_ln81_fu_206_p2_n_122,mul_ln81_fu_206_p2_n_123,mul_ln81_fu_206_p2_n_124,mul_ln81_fu_206_p2_n_125,mul_ln81_fu_206_p2_n_126,mul_ln81_fu_206_p2_n_127,mul_ln81_fu_206_p2_n_128,mul_ln81_fu_206_p2_n_129,mul_ln81_fu_206_p2_n_130,mul_ln81_fu_206_p2_n_131,mul_ln81_fu_206_p2_n_132,mul_ln81_fu_206_p2_n_133,mul_ln81_fu_206_p2_n_134,mul_ln81_fu_206_p2_n_135,mul_ln81_fu_206_p2_n_136,mul_ln81_fu_206_p2_n_137,mul_ln81_fu_206_p2_n_138,mul_ln81_fu_206_p2_n_139,mul_ln81_fu_206_p2_n_140,mul_ln81_fu_206_p2_n_141,mul_ln81_fu_206_p2_n_142,mul_ln81_fu_206_p2_n_143,mul_ln81_fu_206_p2_n_144,mul_ln81_fu_206_p2_n_145,mul_ln81_fu_206_p2_n_146,mul_ln81_fu_206_p2_n_147,mul_ln81_fu_206_p2_n_148,mul_ln81_fu_206_p2_n_149,mul_ln81_fu_206_p2_n_150,mul_ln81_fu_206_p2_n_151,mul_ln81_fu_206_p2_n_152,mul_ln81_fu_206_p2_n_153,mul_ln81_fu_206_p2_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln81_fu_206_p2_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 15x22 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln81_fu_206_p2__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln81_fu_206_p2__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_3,regslice_both_x_U_n_4,regslice_both_x_U_n_5,regslice_both_x_U_n_6,regslice_both_x_U_n_7,regslice_both_x_U_n_8,regslice_both_x_U_n_9,regslice_both_x_U_n_10,regslice_both_x_U_n_11,regslice_both_x_U_n_12,regslice_both_x_U_n_13,regslice_both_x_U_n_14,regslice_both_x_U_n_15,regslice_both_x_U_n_16,regslice_both_x_U_n_17}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln81_fu_206_p2__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln81_fu_206_p2__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln81_fu_206_p2__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(ack_out1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln81_fu_206_p2__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln81_fu_206_p2__0_OVERFLOW_UNCONNECTED),
        .P({mul_ln81_fu_206_p2__0_n_59,mul_ln81_fu_206_p2__0_n_60,mul_ln81_fu_206_p2__0_n_61,mul_ln81_fu_206_p2__0_n_62,mul_ln81_fu_206_p2__0_n_63,mul_ln81_fu_206_p2__0_n_64,mul_ln81_fu_206_p2__0_n_65,mul_ln81_fu_206_p2__0_n_66,mul_ln81_fu_206_p2__0_n_67,mul_ln81_fu_206_p2__0_n_68,mul_ln81_fu_206_p2__0_n_69,mul_ln81_fu_206_p2__0_n_70,mul_ln81_fu_206_p2__0_n_71,mul_ln81_fu_206_p2__0_n_72,mul_ln81_fu_206_p2__0_n_73,mul_ln81_fu_206_p2__0_n_74,mul_ln81_fu_206_p2__0_n_75,mul_ln81_fu_206_p2__0_n_76,mul_ln81_fu_206_p2__0_n_77,mul_ln81_fu_206_p2__0_n_78,mul_ln81_fu_206_p2__0_n_79,mul_ln81_fu_206_p2__0_n_80,mul_ln81_fu_206_p2__0_n_81,mul_ln81_fu_206_p2__0_n_82,mul_ln81_fu_206_p2__0_n_83,mul_ln81_fu_206_p2__0_n_84,mul_ln81_fu_206_p2__0_n_85,mul_ln81_fu_206_p2__0_n_86,mul_ln81_fu_206_p2__0_n_87,mul_ln81_fu_206_p2__0_n_88,mul_ln81_fu_206_p2__0_n_89,mul_ln81_fu_206_p2__0_n_90,mul_ln81_fu_206_p2__0_n_91,mul_ln81_fu_206_p2__0_n_92,mul_ln81_fu_206_p2__0_n_93,mul_ln81_fu_206_p2__0_n_94,mul_ln81_fu_206_p2__0_n_95,mul_ln81_fu_206_p2__0_n_96,mul_ln81_fu_206_p2__0_n_97,mul_ln81_fu_206_p2__0_n_98,mul_ln81_fu_206_p2__0_n_99,mul_ln81_fu_206_p2__0_n_100,mul_ln81_fu_206_p2__0_n_101,mul_ln81_fu_206_p2__0_n_102,mul_ln81_fu_206_p2__0_n_103,mul_ln81_fu_206_p2__0_n_104,mul_ln81_fu_206_p2__0_n_105,mul_ln81_fu_206_p2__0_n_106}),
        .PATTERNBDETECT(NLW_mul_ln81_fu_206_p2__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln81_fu_206_p2__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln81_fu_206_p2_n_107,mul_ln81_fu_206_p2_n_108,mul_ln81_fu_206_p2_n_109,mul_ln81_fu_206_p2_n_110,mul_ln81_fu_206_p2_n_111,mul_ln81_fu_206_p2_n_112,mul_ln81_fu_206_p2_n_113,mul_ln81_fu_206_p2_n_114,mul_ln81_fu_206_p2_n_115,mul_ln81_fu_206_p2_n_116,mul_ln81_fu_206_p2_n_117,mul_ln81_fu_206_p2_n_118,mul_ln81_fu_206_p2_n_119,mul_ln81_fu_206_p2_n_120,mul_ln81_fu_206_p2_n_121,mul_ln81_fu_206_p2_n_122,mul_ln81_fu_206_p2_n_123,mul_ln81_fu_206_p2_n_124,mul_ln81_fu_206_p2_n_125,mul_ln81_fu_206_p2_n_126,mul_ln81_fu_206_p2_n_127,mul_ln81_fu_206_p2_n_128,mul_ln81_fu_206_p2_n_129,mul_ln81_fu_206_p2_n_130,mul_ln81_fu_206_p2_n_131,mul_ln81_fu_206_p2_n_132,mul_ln81_fu_206_p2_n_133,mul_ln81_fu_206_p2_n_134,mul_ln81_fu_206_p2_n_135,mul_ln81_fu_206_p2_n_136,mul_ln81_fu_206_p2_n_137,mul_ln81_fu_206_p2_n_138,mul_ln81_fu_206_p2_n_139,mul_ln81_fu_206_p2_n_140,mul_ln81_fu_206_p2_n_141,mul_ln81_fu_206_p2_n_142,mul_ln81_fu_206_p2_n_143,mul_ln81_fu_206_p2_n_144,mul_ln81_fu_206_p2_n_145,mul_ln81_fu_206_p2_n_146,mul_ln81_fu_206_p2_n_147,mul_ln81_fu_206_p2_n_148,mul_ln81_fu_206_p2_n_149,mul_ln81_fu_206_p2_n_150,mul_ln81_fu_206_p2_n_151,mul_ln81_fu_206_p2_n_152,mul_ln81_fu_206_p2_n_153,mul_ln81_fu_206_p2_n_154}),
        .PCOUT(NLW_mul_ln81_fu_206_p2__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln81_fu_206_p2__0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln81_fu_206_p2__1
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,mul_ln80_fu_191_p2__0__0_n_1,mul_ln80_fu_191_p2__1__0_n_1,mul_ln80_fu_191_p2__2__0_n_1,mul_ln80_fu_191_p2__3_n_1,mul_ln80_fu_191_p2__4_n_1,mul_ln80_fu_191_p2__5_n_1,mul_ln80_fu_191_p2__6_n_1,mul_ln80_fu_191_p2__7_n_1,mul_ln80_fu_191_p2__8_n_1,mul_ln80_fu_191_p2__9_n_1,mul_ln80_fu_191_p2__10_n_1,mul_ln80_fu_191_p2__11_n_1,mul_ln80_fu_191_p2__12_n_1,mul_ln80_fu_191_p2__13_n_1,mul_ln80_fu_191_p2__14_n_1,mul_ln80_fu_191_p2__15_n_1,mul_ln80_fu_191_p2__16_n_1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln81_fu_206_p2__1_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln81_fu_206_p2__1_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln81_fu_206_p2__1_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln81_fu_206_p2__1_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(ack_out1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln81_fu_206_p2__1_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln81_fu_206_p2__1_OVERFLOW_UNCONNECTED),
        .P({mul_ln81_fu_206_p2__1_n_59,mul_ln81_fu_206_p2__1_n_60,mul_ln81_fu_206_p2__1_n_61,mul_ln81_fu_206_p2__1_n_62,mul_ln81_fu_206_p2__1_n_63,mul_ln81_fu_206_p2__1_n_64,mul_ln81_fu_206_p2__1_n_65,mul_ln81_fu_206_p2__1_n_66,mul_ln81_fu_206_p2__1_n_67,mul_ln81_fu_206_p2__1_n_68,mul_ln81_fu_206_p2__1_n_69,mul_ln81_fu_206_p2__1_n_70,mul_ln81_fu_206_p2__1_n_71,mul_ln81_fu_206_p2__1_n_72,mul_ln81_fu_206_p2__1_n_73,mul_ln81_fu_206_p2__1_n_74,mul_ln81_fu_206_p2__1_n_75,mul_ln81_fu_206_p2__1_n_76,mul_ln81_fu_206_p2__1_n_77,mul_ln81_fu_206_p2__1_n_78,mul_ln81_fu_206_p2__1_n_79,mul_ln81_fu_206_p2__1_n_80,mul_ln81_fu_206_p2__1_n_81,mul_ln81_fu_206_p2__1_n_82,mul_ln81_fu_206_p2__1_n_83,mul_ln81_fu_206_p2__1_n_84,mul_ln81_fu_206_p2__1_n_85,mul_ln81_fu_206_p2__1_n_86,mul_ln81_fu_206_p2__1_n_87,mul_ln81_fu_206_p2__1_n_88,mul_ln81_fu_206_p2__1_n_89,mul_ln81_fu_206_p2__1_n_90,mul_ln81_fu_206_p2__1_n_91,mul_ln81_fu_206_p2__1_n_92,mul_ln81_fu_206_p2__1_n_93,mul_ln81_fu_206_p2__1_n_94,mul_ln81_fu_206_p2__1_n_95,mul_ln81_fu_206_p2__1_n_96,mul_ln81_fu_206_p2__1_n_97,mul_ln81_fu_206_p2__1_n_98,mul_ln81_fu_206_p2__1_n_99,mul_ln81_fu_206_p2__1_n_100,mul_ln81_fu_206_p2__1_n_101,mul_ln81_fu_206_p2__1_n_102,mul_ln81_fu_206_p2__1_n_103,mul_ln81_fu_206_p2__1_n_104,mul_ln81_fu_206_p2__1_n_105,mul_ln81_fu_206_p2__1_n_106}),
        .PATTERNBDETECT(NLW_mul_ln81_fu_206_p2__1_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln81_fu_206_p2__1_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln81_fu_206_p2__1_n_107,mul_ln81_fu_206_p2__1_n_108,mul_ln81_fu_206_p2__1_n_109,mul_ln81_fu_206_p2__1_n_110,mul_ln81_fu_206_p2__1_n_111,mul_ln81_fu_206_p2__1_n_112,mul_ln81_fu_206_p2__1_n_113,mul_ln81_fu_206_p2__1_n_114,mul_ln81_fu_206_p2__1_n_115,mul_ln81_fu_206_p2__1_n_116,mul_ln81_fu_206_p2__1_n_117,mul_ln81_fu_206_p2__1_n_118,mul_ln81_fu_206_p2__1_n_119,mul_ln81_fu_206_p2__1_n_120,mul_ln81_fu_206_p2__1_n_121,mul_ln81_fu_206_p2__1_n_122,mul_ln81_fu_206_p2__1_n_123,mul_ln81_fu_206_p2__1_n_124,mul_ln81_fu_206_p2__1_n_125,mul_ln81_fu_206_p2__1_n_126,mul_ln81_fu_206_p2__1_n_127,mul_ln81_fu_206_p2__1_n_128,mul_ln81_fu_206_p2__1_n_129,mul_ln81_fu_206_p2__1_n_130,mul_ln81_fu_206_p2__1_n_131,mul_ln81_fu_206_p2__1_n_132,mul_ln81_fu_206_p2__1_n_133,mul_ln81_fu_206_p2__1_n_134,mul_ln81_fu_206_p2__1_n_135,mul_ln81_fu_206_p2__1_n_136,mul_ln81_fu_206_p2__1_n_137,mul_ln81_fu_206_p2__1_n_138,mul_ln81_fu_206_p2__1_n_139,mul_ln81_fu_206_p2__1_n_140,mul_ln81_fu_206_p2__1_n_141,mul_ln81_fu_206_p2__1_n_142,mul_ln81_fu_206_p2__1_n_143,mul_ln81_fu_206_p2__1_n_144,mul_ln81_fu_206_p2__1_n_145,mul_ln81_fu_206_p2__1_n_146,mul_ln81_fu_206_p2__1_n_147,mul_ln81_fu_206_p2__1_n_148,mul_ln81_fu_206_p2__1_n_149,mul_ln81_fu_206_p2__1_n_150,mul_ln81_fu_206_p2__1_n_151,mul_ln81_fu_206_p2__1_n_152,mul_ln81_fu_206_p2__1_n_153,mul_ln81_fu_206_p2__1_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln81_fu_206_p2__1_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x22 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln81_fu_206_p2__2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln81_fu_206_p2__2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,mul_ln80_fu_191_p2__0__0_n_1,mul_ln80_fu_191_p2__1__0_n_1,mul_ln80_fu_191_p2__2__0_n_1,mul_ln80_fu_191_p2__3_n_1,mul_ln80_fu_191_p2__4_n_1,mul_ln80_fu_191_p2__5_n_1,mul_ln80_fu_191_p2__6_n_1,mul_ln80_fu_191_p2__7_n_1,mul_ln80_fu_191_p2__8_n_1,mul_ln80_fu_191_p2__9_n_1,mul_ln80_fu_191_p2__10_n_1,mul_ln80_fu_191_p2__11_n_1,mul_ln80_fu_191_p2__12_n_1,mul_ln80_fu_191_p2__13_n_1,mul_ln80_fu_191_p2__14_n_1,mul_ln80_fu_191_p2__15_n_1,mul_ln80_fu_191_p2__16_n_1}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln81_fu_206_p2__2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln81_fu_206_p2__2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln81_fu_206_p2__2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(ack_out1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln81_fu_206_p2__2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln81_fu_206_p2__2_OVERFLOW_UNCONNECTED),
        .P({mul_ln81_fu_206_p2__2_n_59,mul_ln81_fu_206_p2__2_n_60,mul_ln81_fu_206_p2__2_n_61,mul_ln81_fu_206_p2__2_n_62,mul_ln81_fu_206_p2__2_n_63,mul_ln81_fu_206_p2__2_n_64,mul_ln81_fu_206_p2__2_n_65,mul_ln81_fu_206_p2__2_n_66,mul_ln81_fu_206_p2__2_n_67,mul_ln81_fu_206_p2__2_n_68,mul_ln81_fu_206_p2__2_n_69,mul_ln81_fu_206_p2__2_n_70,mul_ln81_fu_206_p2__2_n_71,mul_ln81_fu_206_p2__2_n_72,mul_ln81_fu_206_p2__2_n_73,mul_ln81_fu_206_p2__2_n_74,mul_ln81_fu_206_p2__2_n_75,mul_ln81_fu_206_p2__2_n_76,mul_ln81_fu_206_p2__2_n_77,mul_ln81_fu_206_p2__2_n_78,mul_ln81_fu_206_p2__2_n_79,mul_ln81_fu_206_p2__2_n_80,mul_ln81_fu_206_p2__2_n_81,mul_ln81_fu_206_p2__2_n_82,mul_ln81_fu_206_p2__2_n_83,mul_ln81_fu_206_p2__2_n_84,mul_ln81_fu_206_p2__2_n_85,mul_ln81_fu_206_p2__2_n_86,mul_ln81_fu_206_p2__2_n_87,mul_ln81_fu_206_p2__2_n_88,mul_ln81_fu_206_p2__2_n_89,mul_ln81_fu_206_p2__2_n_90,mul_ln81_fu_206_p2__2_n_91,mul_ln81_fu_206_p2__2_n_92,mul_ln81_fu_206_p2__2_n_93,mul_ln81_fu_206_p2__2_n_94,mul_ln81_fu_206_p2__2_n_95,mul_ln81_fu_206_p2__2_n_96,mul_ln81_fu_206_p2__2_n_97,mul_ln81_fu_206_p2__2_n_98,mul_ln81_fu_206_p2__2_n_99,mul_ln81_fu_206_p2__2_n_100,mul_ln81_fu_206_p2__2_n_101,mul_ln81_fu_206_p2__2_n_102,mul_ln81_fu_206_p2__2_n_103,mul_ln81_fu_206_p2__2_n_104,mul_ln81_fu_206_p2__2_n_105,mul_ln81_fu_206_p2__2_n_106}),
        .PATTERNBDETECT(NLW_mul_ln81_fu_206_p2__2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln81_fu_206_p2__2_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln81_fu_206_p2__1_n_107,mul_ln81_fu_206_p2__1_n_108,mul_ln81_fu_206_p2__1_n_109,mul_ln81_fu_206_p2__1_n_110,mul_ln81_fu_206_p2__1_n_111,mul_ln81_fu_206_p2__1_n_112,mul_ln81_fu_206_p2__1_n_113,mul_ln81_fu_206_p2__1_n_114,mul_ln81_fu_206_p2__1_n_115,mul_ln81_fu_206_p2__1_n_116,mul_ln81_fu_206_p2__1_n_117,mul_ln81_fu_206_p2__1_n_118,mul_ln81_fu_206_p2__1_n_119,mul_ln81_fu_206_p2__1_n_120,mul_ln81_fu_206_p2__1_n_121,mul_ln81_fu_206_p2__1_n_122,mul_ln81_fu_206_p2__1_n_123,mul_ln81_fu_206_p2__1_n_124,mul_ln81_fu_206_p2__1_n_125,mul_ln81_fu_206_p2__1_n_126,mul_ln81_fu_206_p2__1_n_127,mul_ln81_fu_206_p2__1_n_128,mul_ln81_fu_206_p2__1_n_129,mul_ln81_fu_206_p2__1_n_130,mul_ln81_fu_206_p2__1_n_131,mul_ln81_fu_206_p2__1_n_132,mul_ln81_fu_206_p2__1_n_133,mul_ln81_fu_206_p2__1_n_134,mul_ln81_fu_206_p2__1_n_135,mul_ln81_fu_206_p2__1_n_136,mul_ln81_fu_206_p2__1_n_137,mul_ln81_fu_206_p2__1_n_138,mul_ln81_fu_206_p2__1_n_139,mul_ln81_fu_206_p2__1_n_140,mul_ln81_fu_206_p2__1_n_141,mul_ln81_fu_206_p2__1_n_142,mul_ln81_fu_206_p2__1_n_143,mul_ln81_fu_206_p2__1_n_144,mul_ln81_fu_206_p2__1_n_145,mul_ln81_fu_206_p2__1_n_146,mul_ln81_fu_206_p2__1_n_147,mul_ln81_fu_206_p2__1_n_148,mul_ln81_fu_206_p2__1_n_149,mul_ln81_fu_206_p2__1_n_150,mul_ln81_fu_206_p2__1_n_151,mul_ln81_fu_206_p2__1_n_152,mul_ln81_fu_206_p2__1_n_153,mul_ln81_fu_206_p2__1_n_154}),
        .PCOUT(NLW_mul_ln81_fu_206_p2__2_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln81_fu_206_p2__2_UNDERFLOW_UNCONNECTED));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_10 
       (.I0(mul_ln80_fu_191_p2__2_n_74),
        .I1(mul_ln80_fu_191_p2__0_n_91),
        .O(\odata[11]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_11 
       (.I0(mul_ln80_fu_191_p2__2_n_75),
        .I1(mul_ln80_fu_191_p2__0_n_92),
        .O(\odata[11]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_8 
       (.I0(mul_ln80_fu_191_p2__2_n_72),
        .I1(mul_ln80_fu_191_p2__0_n_89),
        .O(\odata[11]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_9 
       (.I0(mul_ln80_fu_191_p2__2_n_73),
        .I1(mul_ln80_fu_191_p2__0_n_90),
        .O(\odata[11]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_10 
       (.I0(mul_ln80_fu_191_p2__2_n_70),
        .I1(mul_ln80_fu_191_p2__0_n_87),
        .O(\odata[15]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_11 
       (.I0(mul_ln80_fu_191_p2__2_n_71),
        .I1(mul_ln80_fu_191_p2__0_n_88),
        .O(\odata[15]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_8 
       (.I0(mul_ln80_fu_191_p2__2_n_68),
        .I1(mul_ln80_fu_191_p2__0_n_85),
        .O(\odata[15]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_9 
       (.I0(mul_ln80_fu_191_p2__2_n_69),
        .I1(mul_ln80_fu_191_p2__0_n_86),
        .O(\odata[15]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_10 
       (.I0(mul_ln80_fu_191_p2__2_n_66),
        .I1(mul_ln80_fu_191_p2__0_n_83),
        .O(\odata[19]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_11 
       (.I0(mul_ln80_fu_191_p2__2_n_67),
        .I1(mul_ln80_fu_191_p2__0_n_84),
        .O(\odata[19]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_8 
       (.I0(mul_ln80_fu_191_p2__2_n_64),
        .I1(mul_ln80_fu_191_p2__0_n_81),
        .O(\odata[19]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_9 
       (.I0(mul_ln80_fu_191_p2__2_n_65),
        .I1(mul_ln80_fu_191_p2__0_n_82),
        .O(\odata[19]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_10 
       (.I0(mul_ln80_fu_191_p2__0_n_77),
        .I1(mul_ln80_fu_191_p2__2_n_60),
        .O(\odata[31]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_11 
       (.I0(mul_ln80_fu_191_p2__2_n_61),
        .I1(mul_ln80_fu_191_p2__0_n_78),
        .O(\odata[31]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_12 
       (.I0(mul_ln80_fu_191_p2__2_n_62),
        .I1(mul_ln80_fu_191_p2__0_n_79),
        .O(\odata[31]_i_12_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_13 
       (.I0(mul_ln80_fu_191_p2__2_n_63),
        .I1(mul_ln80_fu_191_p2__0_n_80),
        .O(\odata[31]_i_13_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_15 
       (.I0(mul_ln80_fu_191_p2__2_n_80),
        .I1(mul_ln80_fu_191_p2__0_n_97),
        .O(\odata[3]_i_15_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_16 
       (.I0(mul_ln80_fu_191_p2__2_n_81),
        .I1(mul_ln80_fu_191_p2__0_n_98),
        .O(\odata[3]_i_16_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_17 
       (.I0(mul_ln80_fu_191_p2__2_n_82),
        .I1(mul_ln80_fu_191_p2__0_n_99),
        .O(\odata[3]_i_17_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_18 
       (.I0(mul_ln80_fu_191_p2__2_n_83),
        .I1(mul_ln80_fu_191_p2__0_n_100),
        .O(\odata[3]_i_18_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_25 
       (.I0(mul_ln80_fu_191_p2__2_n_84),
        .I1(mul_ln80_fu_191_p2__0_n_101),
        .O(\odata[3]_i_25_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_26 
       (.I0(mul_ln80_fu_191_p2__2_n_85),
        .I1(mul_ln80_fu_191_p2__0_n_102),
        .O(\odata[3]_i_26_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_27 
       (.I0(mul_ln80_fu_191_p2__2_n_86),
        .I1(mul_ln80_fu_191_p2__0_n_103),
        .O(\odata[3]_i_27_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_28 
       (.I0(mul_ln80_fu_191_p2__2_n_87),
        .I1(mul_ln80_fu_191_p2__0_n_104),
        .O(\odata[3]_i_28_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_35 
       (.I0(mul_ln80_fu_191_p2__2_n_88),
        .I1(mul_ln80_fu_191_p2__0_n_105),
        .O(\odata[3]_i_35_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_36 
       (.I0(mul_ln80_fu_191_p2__2_n_89),
        .I1(mul_ln80_fu_191_p2__0_n_106),
        .O(\odata[3]_i_36_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_37 
       (.I0(mul_ln80_fu_191_p2__2_n_90),
        .I1(mul_ln80_fu_191_p2_n_90),
        .O(\odata[3]_i_37_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_38 
       (.I0(mul_ln80_fu_191_p2__2_n_91),
        .I1(mul_ln80_fu_191_p2_n_91),
        .O(\odata[3]_i_38_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_45 
       (.I0(mul_ln80_fu_191_p2__2_n_92),
        .I1(mul_ln80_fu_191_p2_n_92),
        .O(\odata[3]_i_45_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_46 
       (.I0(mul_ln80_fu_191_p2__2_n_93),
        .I1(mul_ln80_fu_191_p2_n_93),
        .O(\odata[3]_i_46_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_47 
       (.I0(mul_ln80_fu_191_p2__2_n_94),
        .I1(mul_ln80_fu_191_p2_n_94),
        .O(\odata[3]_i_47_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_48 
       (.I0(mul_ln80_fu_191_p2__2_n_95),
        .I1(mul_ln80_fu_191_p2_n_95),
        .O(\odata[3]_i_48_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_55 
       (.I0(mul_ln80_fu_191_p2__2_n_96),
        .I1(mul_ln80_fu_191_p2_n_96),
        .O(\odata[3]_i_55_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_56 
       (.I0(mul_ln80_fu_191_p2__2_n_97),
        .I1(mul_ln80_fu_191_p2_n_97),
        .O(\odata[3]_i_56_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_57 
       (.I0(mul_ln80_fu_191_p2__2_n_98),
        .I1(mul_ln80_fu_191_p2_n_98),
        .O(\odata[3]_i_57_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_58 
       (.I0(mul_ln80_fu_191_p2__2_n_99),
        .I1(mul_ln80_fu_191_p2_n_99),
        .O(\odata[3]_i_58_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_65 
       (.I0(mul_ln80_fu_191_p2__2_n_100),
        .I1(mul_ln80_fu_191_p2_n_100),
        .O(\odata[3]_i_65_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_66 
       (.I0(mul_ln80_fu_191_p2__2_n_101),
        .I1(mul_ln80_fu_191_p2_n_101),
        .O(\odata[3]_i_66_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_67 
       (.I0(mul_ln80_fu_191_p2__2_n_102),
        .I1(mul_ln80_fu_191_p2_n_102),
        .O(\odata[3]_i_67_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_68 
       (.I0(mul_ln80_fu_191_p2__2_n_103),
        .I1(mul_ln80_fu_191_p2_n_103),
        .O(\odata[3]_i_68_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_74 
       (.I0(mul_ln80_fu_191_p2__2_n_104),
        .I1(mul_ln80_fu_191_p2_n_104),
        .O(\odata[3]_i_74_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_75 
       (.I0(mul_ln80_fu_191_p2__2_n_105),
        .I1(mul_ln80_fu_191_p2_n_105),
        .O(\odata[3]_i_75_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_76 
       (.I0(mul_ln80_fu_191_p2__2_n_106),
        .I1(mul_ln80_fu_191_p2_n_106),
        .O(\odata[3]_i_76_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_10 
       (.I0(mul_ln80_fu_191_p2__2_n_78),
        .I1(mul_ln80_fu_191_p2__0_n_95),
        .O(\odata[7]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_11 
       (.I0(mul_ln80_fu_191_p2__2_n_79),
        .I1(mul_ln80_fu_191_p2__0_n_96),
        .O(\odata[7]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_8 
       (.I0(mul_ln80_fu_191_p2__2_n_76),
        .I1(mul_ln80_fu_191_p2__0_n_93),
        .O(\odata[7]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_9 
       (.I0(mul_ln80_fu_191_p2__2_n_77),
        .I1(mul_ln80_fu_191_p2__0_n_94),
        .O(\odata[7]_i_9_n_1 ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[11]_i_3 
       (.CI(\odata_reg[7]_i_3_n_1 ),
        .CO({\odata_reg[11]_i_3_n_1 ,\odata_reg[11]_i_3_n_2 ,\odata_reg[11]_i_3_n_3 ,\odata_reg[11]_i_3_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_72,mul_ln80_fu_191_p2__2_n_73,mul_ln80_fu_191_p2__2_n_74,mul_ln80_fu_191_p2__2_n_75}),
        .O(mul_ln80_fu_191_p2__21[51:48]),
        .S({\odata[11]_i_8_n_1 ,\odata[11]_i_9_n_1 ,\odata[11]_i_10_n_1 ,\odata[11]_i_11_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[15]_i_3 
       (.CI(\odata_reg[11]_i_3_n_1 ),
        .CO({\odata_reg[15]_i_3_n_1 ,\odata_reg[15]_i_3_n_2 ,\odata_reg[15]_i_3_n_3 ,\odata_reg[15]_i_3_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_68,mul_ln80_fu_191_p2__2_n_69,mul_ln80_fu_191_p2__2_n_70,mul_ln80_fu_191_p2__2_n_71}),
        .O(mul_ln80_fu_191_p2__21[55:52]),
        .S({\odata[15]_i_8_n_1 ,\odata[15]_i_9_n_1 ,\odata[15]_i_10_n_1 ,\odata[15]_i_11_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[19]_i_3 
       (.CI(\odata_reg[15]_i_3_n_1 ),
        .CO({\odata_reg[19]_i_3_n_1 ,\odata_reg[19]_i_3_n_2 ,\odata_reg[19]_i_3_n_3 ,\odata_reg[19]_i_3_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_64,mul_ln80_fu_191_p2__2_n_65,mul_ln80_fu_191_p2__2_n_66,mul_ln80_fu_191_p2__2_n_67}),
        .O(mul_ln80_fu_191_p2__21[59:56]),
        .S({\odata[19]_i_8_n_1 ,\odata[19]_i_9_n_1 ,\odata[19]_i_10_n_1 ,\odata[19]_i_11_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[31]_i_5 
       (.CI(\odata_reg[19]_i_3_n_1 ),
        .CO({\NLW_odata_reg[31]_i_5_CO_UNCONNECTED [3],\odata_reg[31]_i_5_n_2 ,\odata_reg[31]_i_5_n_3 ,\odata_reg[31]_i_5_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln80_fu_191_p2__2_n_61,mul_ln80_fu_191_p2__2_n_62,mul_ln80_fu_191_p2__2_n_63}),
        .O(mul_ln80_fu_191_p2__21[63:60]),
        .S({\odata[31]_i_10_n_1 ,\odata[31]_i_11_n_1 ,\odata[31]_i_12_n_1 ,\odata[31]_i_13_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_10 
       (.CI(\odata_reg[3]_i_20_n_1 ),
        .CO({\odata_reg[3]_i_10_n_1 ,\odata_reg[3]_i_10_n_2 ,\odata_reg[3]_i_10_n_3 ,\odata_reg[3]_i_10_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_84,mul_ln80_fu_191_p2__2_n_85,mul_ln80_fu_191_p2__2_n_86,mul_ln80_fu_191_p2__2_n_87}),
        .O(mul_ln80_fu_191_p2__21[39:36]),
        .S({\odata[3]_i_25_n_1 ,\odata[3]_i_26_n_1 ,\odata[3]_i_27_n_1 ,\odata[3]_i_28_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_20 
       (.CI(\odata_reg[3]_i_30_n_1 ),
        .CO({\odata_reg[3]_i_20_n_1 ,\odata_reg[3]_i_20_n_2 ,\odata_reg[3]_i_20_n_3 ,\odata_reg[3]_i_20_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_88,mul_ln80_fu_191_p2__2_n_89,mul_ln80_fu_191_p2__2_n_90,mul_ln80_fu_191_p2__2_n_91}),
        .O(mul_ln80_fu_191_p2__21[35:32]),
        .S({\odata[3]_i_35_n_1 ,\odata[3]_i_36_n_1 ,\odata[3]_i_37_n_1 ,\odata[3]_i_38_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_30 
       (.CI(\odata_reg[3]_i_40_n_1 ),
        .CO({\odata_reg[3]_i_30_n_1 ,\odata_reg[3]_i_30_n_2 ,\odata_reg[3]_i_30_n_3 ,\odata_reg[3]_i_30_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_92,mul_ln80_fu_191_p2__2_n_93,mul_ln80_fu_191_p2__2_n_94,mul_ln80_fu_191_p2__2_n_95}),
        .O(mul_ln80_fu_191_p2__21[31:28]),
        .S({\odata[3]_i_45_n_1 ,\odata[3]_i_46_n_1 ,\odata[3]_i_47_n_1 ,\odata[3]_i_48_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_4 
       (.CI(\odata_reg[3]_i_10_n_1 ),
        .CO({\odata_reg[3]_i_4_n_1 ,\odata_reg[3]_i_4_n_2 ,\odata_reg[3]_i_4_n_3 ,\odata_reg[3]_i_4_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_80,mul_ln80_fu_191_p2__2_n_81,mul_ln80_fu_191_p2__2_n_82,mul_ln80_fu_191_p2__2_n_83}),
        .O(mul_ln80_fu_191_p2__21[43:40]),
        .S({\odata[3]_i_15_n_1 ,\odata[3]_i_16_n_1 ,\odata[3]_i_17_n_1 ,\odata[3]_i_18_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_40 
       (.CI(\odata_reg[3]_i_50_n_1 ),
        .CO({\odata_reg[3]_i_40_n_1 ,\odata_reg[3]_i_40_n_2 ,\odata_reg[3]_i_40_n_3 ,\odata_reg[3]_i_40_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_96,mul_ln80_fu_191_p2__2_n_97,mul_ln80_fu_191_p2__2_n_98,mul_ln80_fu_191_p2__2_n_99}),
        .O(mul_ln80_fu_191_p2__21[27:24]),
        .S({\odata[3]_i_55_n_1 ,\odata[3]_i_56_n_1 ,\odata[3]_i_57_n_1 ,\odata[3]_i_58_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_50 
       (.CI(\odata_reg[3]_i_60_n_1 ),
        .CO({\odata_reg[3]_i_50_n_1 ,\odata_reg[3]_i_50_n_2 ,\odata_reg[3]_i_50_n_3 ,\odata_reg[3]_i_50_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_100,mul_ln80_fu_191_p2__2_n_101,mul_ln80_fu_191_p2__2_n_102,mul_ln80_fu_191_p2__2_n_103}),
        .O(mul_ln80_fu_191_p2__21[23:20]),
        .S({\odata[3]_i_65_n_1 ,\odata[3]_i_66_n_1 ,\odata[3]_i_67_n_1 ,\odata[3]_i_68_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_60 
       (.CI(1'b0),
        .CO({\odata_reg[3]_i_60_n_1 ,\odata_reg[3]_i_60_n_2 ,\odata_reg[3]_i_60_n_3 ,\odata_reg[3]_i_60_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_104,mul_ln80_fu_191_p2__2_n_105,mul_ln80_fu_191_p2__2_n_106,1'b0}),
        .O(mul_ln80_fu_191_p2__21[19:16]),
        .S({\odata[3]_i_74_n_1 ,\odata[3]_i_75_n_1 ,\odata[3]_i_76_n_1 ,mul_ln80_fu_191_p2__1_n_90}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[7]_i_3 
       (.CI(\odata_reg[3]_i_4_n_1 ),
        .CO({\odata_reg[7]_i_3_n_1 ,\odata_reg[7]_i_3_n_2 ,\odata_reg[7]_i_3_n_3 ,\odata_reg[7]_i_3_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln80_fu_191_p2__2_n_76,mul_ln80_fu_191_p2__2_n_77,mul_ln80_fu_191_p2__2_n_78,mul_ln80_fu_191_p2__2_n_79}),
        .O(mul_ln80_fu_191_p2__21[47:44]),
        .S({\odata[7]_i_8_n_1 ,\odata[7]_i_9_n_1 ,\odata[7]_i_10_n_1 ,\odata[7]_i_11_n_1 }));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_regslice_both regslice_both_x_U
       (.D({regslice_both_x_U_n_3,regslice_both_x_U_n_4,regslice_both_x_U_n_5,regslice_both_x_U_n_6,regslice_both_x_U_n_7,regslice_both_x_U_n_8,regslice_both_x_U_n_9,regslice_both_x_U_n_10,regslice_both_x_U_n_11,regslice_both_x_U_n_12,regslice_both_x_U_n_13,regslice_both_x_U_n_14,regslice_both_x_U_n_15,regslice_both_x_U_n_16,regslice_both_x_U_n_17}),
        .Q({ap_CS_fsm_state4,ap_CS_fsm_state3,ap_CS_fsm_state2,\ap_CS_fsm_reg_n_1_[0] }),
        .SR(ap_rst_n_inv),
        .ack_out1(ack_out1),
        .\ap_CS_fsm_reg[1] (regslice_both_x_U_n_2),
        .\ap_CS_fsm_reg[1]_0 (acc0_0_reg_130),
        .\ap_CS_fsm_reg[2] (shift_reg0_U_n_6),
        .\ap_CS_fsm_reg[2]_0 (\ibuf_inst/p_0_in ),
        .ap_NS_fsm11_out(ap_NS_fsm11_out),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .\ireg_reg[32] ({x_TVALID,x_TDATA}),
        .\odata_reg[32] (ap_NS_fsm[2:1]),
        .\x_TDATA[0] (regslice_both_x_U_n_24),
        .\x_TDATA[10] (regslice_both_x_U_n_34),
        .\x_TDATA[11] (regslice_both_x_U_n_35),
        .\x_TDATA[12] (regslice_both_x_U_n_36),
        .\x_TDATA[13] (regslice_both_x_U_n_37),
        .\x_TDATA[14] (regslice_both_x_U_n_38),
        .\x_TDATA[15] (regslice_both_x_U_n_39),
        .\x_TDATA[16] (regslice_both_x_U_n_40),
        .\x_TDATA[1] (regslice_both_x_U_n_25),
        .\x_TDATA[2] (regslice_both_x_U_n_26),
        .\x_TDATA[3] (regslice_both_x_U_n_27),
        .\x_TDATA[4] (regslice_both_x_U_n_28),
        .\x_TDATA[5] (regslice_both_x_U_n_29),
        .\x_TDATA[6] (regslice_both_x_U_n_30),
        .\x_TDATA[7] (regslice_both_x_U_n_31),
        .\x_TDATA[8] (regslice_both_x_U_n_32),
        .\x_TDATA[9] (regslice_both_x_U_n_33),
        .x_TREADY(x_TREADY),
        .x_TVALID_int(x_TVALID_int));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_regslice_both_0 regslice_both_y_U
       (.D({ap_NS_fsm[5:4],ap_NS_fsm[0]}),
        .P({mul_ln80_fu_191_p2__1_n_91,mul_ln80_fu_191_p2__1_n_92,mul_ln80_fu_191_p2__1_n_93,mul_ln80_fu_191_p2__1_n_94,mul_ln80_fu_191_p2__1_n_95,mul_ln80_fu_191_p2__1_n_96,mul_ln80_fu_191_p2__1_n_97,mul_ln80_fu_191_p2__1_n_98,mul_ln80_fu_191_p2__1_n_99,mul_ln80_fu_191_p2__1_n_100,mul_ln80_fu_191_p2__1_n_101,mul_ln80_fu_191_p2__1_n_102,mul_ln80_fu_191_p2__1_n_103,mul_ln80_fu_191_p2__1_n_104,mul_ln80_fu_191_p2__1_n_105,mul_ln80_fu_191_p2__1_n_106}),
        .Q({ap_CS_fsm_state6,ap_CS_fsm_state5,ap_CS_fsm_state4,ap_CS_fsm_state3,\ap_CS_fsm_reg_n_1_[0] }),
        .SR(ap_rst_n_inv),
        .\ap_CS_fsm_reg[3] (regslice_both_y_U_n_33),
        .\ap_CS_fsm_reg[3]_0 (regslice_both_y_U_n_34),
        .ap_NS_fsm13_out(ap_NS_fsm13_out),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .coeffs_ce0(coeffs_ce0),
        .\ireg_reg[32] (\ibuf_inst/p_0_in ),
        .mul_ln80_fu_191_p2__21(mul_ln80_fu_191_p2__21),
        .\odata_reg[31] (trunc_ln1_reg_329),
        .\odata_reg[31]_i_4 ({\acc0_0_reg_130_reg_n_1_[63] ,\acc0_0_reg_130_reg_n_1_[62] ,\acc0_0_reg_130_reg_n_1_[61] ,\acc0_0_reg_130_reg_n_1_[60] ,\acc0_0_reg_130_reg_n_1_[59] ,\acc0_0_reg_130_reg_n_1_[58] ,\acc0_0_reg_130_reg_n_1_[57] ,\acc0_0_reg_130_reg_n_1_[56] ,\acc0_0_reg_130_reg_n_1_[55] ,\acc0_0_reg_130_reg_n_1_[54] ,\acc0_0_reg_130_reg_n_1_[53] ,\acc0_0_reg_130_reg_n_1_[52] ,\acc0_0_reg_130_reg_n_1_[51] ,\acc0_0_reg_130_reg_n_1_[50] ,\acc0_0_reg_130_reg_n_1_[49] ,\acc0_0_reg_130_reg_n_1_[48] ,\acc0_0_reg_130_reg_n_1_[47] ,\acc0_0_reg_130_reg_n_1_[46] ,\acc0_0_reg_130_reg_n_1_[45] ,\acc0_0_reg_130_reg_n_1_[44] ,\acc0_0_reg_130_reg_n_1_[43] ,\acc0_0_reg_130_reg_n_1_[42] ,\acc0_0_reg_130_reg_n_1_[41] ,\acc0_0_reg_130_reg_n_1_[40] ,\acc0_0_reg_130_reg_n_1_[39] ,\acc0_0_reg_130_reg_n_1_[38] ,\acc0_0_reg_130_reg_n_1_[37] ,\acc0_0_reg_130_reg_n_1_[36] ,\acc0_0_reg_130_reg_n_1_[35] ,\acc0_0_reg_130_reg_n_1_[34] ,\acc0_0_reg_130_reg_n_1_[33] ,\acc0_0_reg_130_reg_n_1_[32] ,\acc0_0_reg_130_reg_n_1_[31] ,\acc0_0_reg_130_reg_n_1_[30] ,\acc0_0_reg_130_reg_n_1_[29] ,\acc0_0_reg_130_reg_n_1_[28] ,\acc0_0_reg_130_reg_n_1_[27] ,\acc0_0_reg_130_reg_n_1_[26] ,\acc0_0_reg_130_reg_n_1_[25] ,\acc0_0_reg_130_reg_n_1_[24] ,\acc0_0_reg_130_reg_n_1_[23] ,\acc0_0_reg_130_reg_n_1_[22] ,\acc0_0_reg_130_reg_n_1_[21] ,\acc0_0_reg_130_reg_n_1_[20] ,\acc0_0_reg_130_reg_n_1_[19] ,\acc0_0_reg_130_reg_n_1_[18] ,\acc0_0_reg_130_reg_n_1_[17] ,\acc0_0_reg_130_reg_n_1_[16] ,\acc0_0_reg_130_reg_n_1_[15] ,\acc0_0_reg_130_reg_n_1_[14] ,\acc0_0_reg_130_reg_n_1_[13] ,\acc0_0_reg_130_reg_n_1_[12] ,\acc0_0_reg_130_reg_n_1_[11] ,\acc0_0_reg_130_reg_n_1_[10] ,\acc0_0_reg_130_reg_n_1_[9] ,\acc0_0_reg_130_reg_n_1_[8] ,\acc0_0_reg_130_reg_n_1_[7] ,\acc0_0_reg_130_reg_n_1_[6] ,\acc0_0_reg_130_reg_n_1_[5] ,\acc0_0_reg_130_reg_n_1_[4] ,\acc0_0_reg_130_reg_n_1_[3] ,\acc0_0_reg_130_reg_n_1_[2] ,\acc0_0_reg_130_reg_n_1_[1] ,\acc0_0_reg_130_reg_n_1_[0] }),
        .\odata_reg[32] ({y_TVALID,\^y_TDATA [30],\^y_TDATA [22:0]}),
        .\q0_reg[0] (shift_reg0_U_n_6),
        .shift_reg0_address0(shift_reg0_address0[4]),
        .shift_reg0_ce0(shift_reg0_ce0),
        .\trunc_ln1_reg_329_reg[0] (shift_reg0_U_n_5),
        .x_TVALID_int(x_TVALID_int),
        .y_TREADY(y_TREADY));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0 shift_reg0_U
       (.Q({\zext_ln70_reg_313_reg_n_1_[3] ,\zext_ln70_reg_313_reg_n_1_[2] ,\zext_ln70_reg_313_reg_n_1_[1] ,\zext_ln70_reg_313_reg_n_1_[0] }),
        .\ap_CS_fsm_reg[2] (shift_reg0_U_n_5),
        .ap_clk(ap_clk),
        .\i_0_reg_154_reg[0] (shift_reg0_U_n_6),
        .q00(q00),
        .\q0_reg[0] (i_0_reg_154),
        .\q0_reg[0]_0 (regslice_both_y_U_n_33),
        .\q0_reg[31] (regslice_both_y_U_n_34),
        .\q0_reg[31]_0 (shift_reg0_address0[4]),
        .ram_reg_0_15_0_0__62({ap_CS_fsm_state4,ap_CS_fsm_state3}),
        .ram_reg_0_15_0_0__62_0(shift_reg1_U_n_2),
        .ram_reg_0_15_0_0__62_1(x0_reg_283),
        .shift_reg0_address0(shift_reg0_address0[3:0]),
        .shift_reg0_ce0(shift_reg0_ce0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0_1 shift_reg1_U
       (.B({shift_reg1_U_n_3,shift_reg1_U_n_4,shift_reg1_U_n_5,shift_reg1_U_n_6,shift_reg1_U_n_7,shift_reg1_U_n_8,shift_reg1_U_n_9,shift_reg1_U_n_10,shift_reg1_U_n_11,shift_reg1_U_n_12,shift_reg1_U_n_13,shift_reg1_U_n_14,shift_reg1_U_n_15,shift_reg1_U_n_16,shift_reg1_U_n_17}),
        .D({shift_reg1_U_n_18,shift_reg1_U_n_19,shift_reg1_U_n_20,shift_reg1_U_n_21,shift_reg1_U_n_22,shift_reg1_U_n_23,shift_reg1_U_n_24,shift_reg1_U_n_25,shift_reg1_U_n_26,shift_reg1_U_n_27,shift_reg1_U_n_28,shift_reg1_U_n_29,shift_reg1_U_n_30,shift_reg1_U_n_31,shift_reg1_U_n_32,shift_reg1_U_n_33,shift_reg1_U_n_34}),
        .Q(\zext_ln70_reg_313_reg_n_1_[4] ),
        .ap_clk(ap_clk),
        .\i_0_reg_154_reg[2] (shift_reg1_U_n_2),
        .\q0_reg[0] (regslice_both_y_U_n_34),
        .\q0_reg[31] ({ap_CS_fsm_state4,ap_CS_fsm_state3}),
        .\q0_reg[31]_0 (i_0_reg_154),
        .\q0_reg[31]_1 (regslice_both_y_U_n_33),
        .ram_reg_0_15_0_0__62(x1_reg_289),
        .shift_reg0_address0(shift_reg0_address0[3:0]),
        .shift_reg0_ce0(shift_reg0_ce0),
        .\zext_ln70_reg_313_reg[4] (shift_reg0_address0[4]));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_10 
       (.I0(mul_ln81_fu_206_p2__2_n_75),
        .I1(mul_ln81_fu_206_p2__0_n_92),
        .O(\trunc_ln1_reg_329[11]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_3 
       (.I0(mul_ln81_fu_206_p2__3[51]),
        .I1(acc1_0_reg_142[51]),
        .O(\trunc_ln1_reg_329[11]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_4 
       (.I0(mul_ln81_fu_206_p2__3[50]),
        .I1(acc1_0_reg_142[50]),
        .O(\trunc_ln1_reg_329[11]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_5 
       (.I0(mul_ln81_fu_206_p2__3[49]),
        .I1(acc1_0_reg_142[49]),
        .O(\trunc_ln1_reg_329[11]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_6 
       (.I0(mul_ln81_fu_206_p2__3[48]),
        .I1(acc1_0_reg_142[48]),
        .O(\trunc_ln1_reg_329[11]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_7 
       (.I0(mul_ln81_fu_206_p2__2_n_72),
        .I1(mul_ln81_fu_206_p2__0_n_89),
        .O(\trunc_ln1_reg_329[11]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_8 
       (.I0(mul_ln81_fu_206_p2__2_n_73),
        .I1(mul_ln81_fu_206_p2__0_n_90),
        .O(\trunc_ln1_reg_329[11]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[11]_i_9 
       (.I0(mul_ln81_fu_206_p2__2_n_74),
        .I1(mul_ln81_fu_206_p2__0_n_91),
        .O(\trunc_ln1_reg_329[11]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_10 
       (.I0(mul_ln81_fu_206_p2__2_n_71),
        .I1(mul_ln81_fu_206_p2__0_n_88),
        .O(\trunc_ln1_reg_329[15]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_3 
       (.I0(mul_ln81_fu_206_p2__3[55]),
        .I1(acc1_0_reg_142[55]),
        .O(\trunc_ln1_reg_329[15]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_4 
       (.I0(mul_ln81_fu_206_p2__3[54]),
        .I1(acc1_0_reg_142[54]),
        .O(\trunc_ln1_reg_329[15]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_5 
       (.I0(mul_ln81_fu_206_p2__3[53]),
        .I1(acc1_0_reg_142[53]),
        .O(\trunc_ln1_reg_329[15]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_6 
       (.I0(mul_ln81_fu_206_p2__3[52]),
        .I1(acc1_0_reg_142[52]),
        .O(\trunc_ln1_reg_329[15]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_7 
       (.I0(mul_ln81_fu_206_p2__2_n_68),
        .I1(mul_ln81_fu_206_p2__0_n_85),
        .O(\trunc_ln1_reg_329[15]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_8 
       (.I0(mul_ln81_fu_206_p2__2_n_69),
        .I1(mul_ln81_fu_206_p2__0_n_86),
        .O(\trunc_ln1_reg_329[15]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[15]_i_9 
       (.I0(mul_ln81_fu_206_p2__2_n_70),
        .I1(mul_ln81_fu_206_p2__0_n_87),
        .O(\trunc_ln1_reg_329[15]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_10 
       (.I0(mul_ln81_fu_206_p2__2_n_67),
        .I1(mul_ln81_fu_206_p2__0_n_84),
        .O(\trunc_ln1_reg_329[19]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_3 
       (.I0(mul_ln81_fu_206_p2__3[59]),
        .I1(acc1_0_reg_142[59]),
        .O(\trunc_ln1_reg_329[19]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_4 
       (.I0(mul_ln81_fu_206_p2__3[58]),
        .I1(acc1_0_reg_142[58]),
        .O(\trunc_ln1_reg_329[19]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_5 
       (.I0(mul_ln81_fu_206_p2__3[57]),
        .I1(acc1_0_reg_142[57]),
        .O(\trunc_ln1_reg_329[19]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_6 
       (.I0(mul_ln81_fu_206_p2__3[56]),
        .I1(acc1_0_reg_142[56]),
        .O(\trunc_ln1_reg_329[19]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_7 
       (.I0(mul_ln81_fu_206_p2__2_n_64),
        .I1(mul_ln81_fu_206_p2__0_n_81),
        .O(\trunc_ln1_reg_329[19]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_8 
       (.I0(mul_ln81_fu_206_p2__2_n_65),
        .I1(mul_ln81_fu_206_p2__0_n_82),
        .O(\trunc_ln1_reg_329[19]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[19]_i_9 
       (.I0(mul_ln81_fu_206_p2__2_n_66),
        .I1(mul_ln81_fu_206_p2__0_n_83),
        .O(\trunc_ln1_reg_329[19]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_10 
       (.I0(mul_ln81_fu_206_p2__2_n_62),
        .I1(mul_ln81_fu_206_p2__0_n_79),
        .O(\trunc_ln1_reg_329[23]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_11 
       (.I0(mul_ln81_fu_206_p2__2_n_63),
        .I1(mul_ln81_fu_206_p2__0_n_80),
        .O(\trunc_ln1_reg_329[23]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_4 
       (.I0(mul_ln81_fu_206_p2__3[63]),
        .I1(acc1_0_reg_142[63]),
        .O(\trunc_ln1_reg_329[23]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_5 
       (.I0(mul_ln81_fu_206_p2__3[62]),
        .I1(acc1_0_reg_142[62]),
        .O(\trunc_ln1_reg_329[23]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_6 
       (.I0(mul_ln81_fu_206_p2__3[61]),
        .I1(acc1_0_reg_142[61]),
        .O(\trunc_ln1_reg_329[23]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_7 
       (.I0(mul_ln81_fu_206_p2__3[60]),
        .I1(acc1_0_reg_142[60]),
        .O(\trunc_ln1_reg_329[23]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_8 
       (.I0(mul_ln81_fu_206_p2__2_n_60),
        .I1(mul_ln81_fu_206_p2__0_n_77),
        .O(\trunc_ln1_reg_329[23]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[23]_i_9 
       (.I0(mul_ln81_fu_206_p2__2_n_61),
        .I1(mul_ln81_fu_206_p2__0_n_78),
        .O(\trunc_ln1_reg_329[23]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_10 
       (.I0(mul_ln81_fu_206_p2__3[39]),
        .I1(acc1_0_reg_142[39]),
        .O(\trunc_ln1_reg_329[3]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_11 
       (.I0(mul_ln81_fu_206_p2__3[38]),
        .I1(acc1_0_reg_142[38]),
        .O(\trunc_ln1_reg_329[3]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_12 
       (.I0(mul_ln81_fu_206_p2__3[37]),
        .I1(acc1_0_reg_142[37]),
        .O(\trunc_ln1_reg_329[3]_i_12_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_13 
       (.I0(mul_ln81_fu_206_p2__3[36]),
        .I1(acc1_0_reg_142[36]),
        .O(\trunc_ln1_reg_329[3]_i_13_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_14 
       (.I0(mul_ln81_fu_206_p2__2_n_80),
        .I1(mul_ln81_fu_206_p2__0_n_97),
        .O(\trunc_ln1_reg_329[3]_i_14_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_15 
       (.I0(mul_ln81_fu_206_p2__2_n_81),
        .I1(mul_ln81_fu_206_p2__0_n_98),
        .O(\trunc_ln1_reg_329[3]_i_15_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_16 
       (.I0(mul_ln81_fu_206_p2__2_n_82),
        .I1(mul_ln81_fu_206_p2__0_n_99),
        .O(\trunc_ln1_reg_329[3]_i_16_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_17 
       (.I0(mul_ln81_fu_206_p2__2_n_83),
        .I1(mul_ln81_fu_206_p2__0_n_100),
        .O(\trunc_ln1_reg_329[3]_i_17_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_20 
       (.I0(mul_ln81_fu_206_p2__3[35]),
        .I1(acc1_0_reg_142[35]),
        .O(\trunc_ln1_reg_329[3]_i_20_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_21 
       (.I0(mul_ln81_fu_206_p2__3[34]),
        .I1(acc1_0_reg_142[34]),
        .O(\trunc_ln1_reg_329[3]_i_21_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_22 
       (.I0(mul_ln81_fu_206_p2__3[33]),
        .I1(acc1_0_reg_142[33]),
        .O(\trunc_ln1_reg_329[3]_i_22_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_23 
       (.I0(mul_ln81_fu_206_p2__3[32]),
        .I1(acc1_0_reg_142[32]),
        .O(\trunc_ln1_reg_329[3]_i_23_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_24 
       (.I0(mul_ln81_fu_206_p2__2_n_84),
        .I1(mul_ln81_fu_206_p2__0_n_101),
        .O(\trunc_ln1_reg_329[3]_i_24_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_25 
       (.I0(mul_ln81_fu_206_p2__2_n_85),
        .I1(mul_ln81_fu_206_p2__0_n_102),
        .O(\trunc_ln1_reg_329[3]_i_25_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_26 
       (.I0(mul_ln81_fu_206_p2__2_n_86),
        .I1(mul_ln81_fu_206_p2__0_n_103),
        .O(\trunc_ln1_reg_329[3]_i_26_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_27 
       (.I0(mul_ln81_fu_206_p2__2_n_87),
        .I1(mul_ln81_fu_206_p2__0_n_104),
        .O(\trunc_ln1_reg_329[3]_i_27_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_30 
       (.I0(mul_ln81_fu_206_p2__3[31]),
        .I1(acc1_0_reg_142[31]),
        .O(\trunc_ln1_reg_329[3]_i_30_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_31 
       (.I0(mul_ln81_fu_206_p2__3[30]),
        .I1(acc1_0_reg_142[30]),
        .O(\trunc_ln1_reg_329[3]_i_31_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_32 
       (.I0(mul_ln81_fu_206_p2__3[29]),
        .I1(acc1_0_reg_142[29]),
        .O(\trunc_ln1_reg_329[3]_i_32_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_33 
       (.I0(mul_ln81_fu_206_p2__3[28]),
        .I1(acc1_0_reg_142[28]),
        .O(\trunc_ln1_reg_329[3]_i_33_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_34 
       (.I0(mul_ln81_fu_206_p2__2_n_88),
        .I1(mul_ln81_fu_206_p2__0_n_105),
        .O(\trunc_ln1_reg_329[3]_i_34_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_35 
       (.I0(mul_ln81_fu_206_p2__2_n_89),
        .I1(mul_ln81_fu_206_p2__0_n_106),
        .O(\trunc_ln1_reg_329[3]_i_35_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_36 
       (.I0(mul_ln81_fu_206_p2__2_n_90),
        .I1(mul_ln81_fu_206_p2_n_90),
        .O(\trunc_ln1_reg_329[3]_i_36_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_37 
       (.I0(mul_ln81_fu_206_p2__2_n_91),
        .I1(mul_ln81_fu_206_p2_n_91),
        .O(\trunc_ln1_reg_329[3]_i_37_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_4 
       (.I0(mul_ln81_fu_206_p2__3[43]),
        .I1(acc1_0_reg_142[43]),
        .O(\trunc_ln1_reg_329[3]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_40 
       (.I0(mul_ln81_fu_206_p2__3[27]),
        .I1(acc1_0_reg_142[27]),
        .O(\trunc_ln1_reg_329[3]_i_40_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_41 
       (.I0(mul_ln81_fu_206_p2__3[26]),
        .I1(acc1_0_reg_142[26]),
        .O(\trunc_ln1_reg_329[3]_i_41_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_42 
       (.I0(mul_ln81_fu_206_p2__3[25]),
        .I1(acc1_0_reg_142[25]),
        .O(\trunc_ln1_reg_329[3]_i_42_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_43 
       (.I0(mul_ln81_fu_206_p2__3[24]),
        .I1(acc1_0_reg_142[24]),
        .O(\trunc_ln1_reg_329[3]_i_43_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_44 
       (.I0(mul_ln81_fu_206_p2__2_n_92),
        .I1(mul_ln81_fu_206_p2_n_92),
        .O(\trunc_ln1_reg_329[3]_i_44_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_45 
       (.I0(mul_ln81_fu_206_p2__2_n_93),
        .I1(mul_ln81_fu_206_p2_n_93),
        .O(\trunc_ln1_reg_329[3]_i_45_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_46 
       (.I0(mul_ln81_fu_206_p2__2_n_94),
        .I1(mul_ln81_fu_206_p2_n_94),
        .O(\trunc_ln1_reg_329[3]_i_46_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_47 
       (.I0(mul_ln81_fu_206_p2__2_n_95),
        .I1(mul_ln81_fu_206_p2_n_95),
        .O(\trunc_ln1_reg_329[3]_i_47_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_5 
       (.I0(mul_ln81_fu_206_p2__3[42]),
        .I1(acc1_0_reg_142[42]),
        .O(\trunc_ln1_reg_329[3]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_50 
       (.I0(mul_ln81_fu_206_p2__3[23]),
        .I1(acc1_0_reg_142[23]),
        .O(\trunc_ln1_reg_329[3]_i_50_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_51 
       (.I0(mul_ln81_fu_206_p2__3[22]),
        .I1(acc1_0_reg_142[22]),
        .O(\trunc_ln1_reg_329[3]_i_51_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_52 
       (.I0(mul_ln81_fu_206_p2__3[21]),
        .I1(acc1_0_reg_142[21]),
        .O(\trunc_ln1_reg_329[3]_i_52_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_53 
       (.I0(mul_ln81_fu_206_p2__3[20]),
        .I1(acc1_0_reg_142[20]),
        .O(\trunc_ln1_reg_329[3]_i_53_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_54 
       (.I0(mul_ln81_fu_206_p2__2_n_96),
        .I1(mul_ln81_fu_206_p2_n_96),
        .O(\trunc_ln1_reg_329[3]_i_54_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_55 
       (.I0(mul_ln81_fu_206_p2__2_n_97),
        .I1(mul_ln81_fu_206_p2_n_97),
        .O(\trunc_ln1_reg_329[3]_i_55_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_56 
       (.I0(mul_ln81_fu_206_p2__2_n_98),
        .I1(mul_ln81_fu_206_p2_n_98),
        .O(\trunc_ln1_reg_329[3]_i_56_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_57 
       (.I0(mul_ln81_fu_206_p2__2_n_99),
        .I1(mul_ln81_fu_206_p2_n_99),
        .O(\trunc_ln1_reg_329[3]_i_57_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_6 
       (.I0(mul_ln81_fu_206_p2__3[41]),
        .I1(acc1_0_reg_142[41]),
        .O(\trunc_ln1_reg_329[3]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_60 
       (.I0(mul_ln81_fu_206_p2__3[19]),
        .I1(acc1_0_reg_142[19]),
        .O(\trunc_ln1_reg_329[3]_i_60_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_61 
       (.I0(mul_ln81_fu_206_p2__3[18]),
        .I1(acc1_0_reg_142[18]),
        .O(\trunc_ln1_reg_329[3]_i_61_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_62 
       (.I0(mul_ln81_fu_206_p2__3[17]),
        .I1(acc1_0_reg_142[17]),
        .O(\trunc_ln1_reg_329[3]_i_62_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_63 
       (.I0(mul_ln81_fu_206_p2__3[16]),
        .I1(acc1_0_reg_142[16]),
        .O(\trunc_ln1_reg_329[3]_i_63_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_64 
       (.I0(mul_ln81_fu_206_p2__2_n_100),
        .I1(mul_ln81_fu_206_p2_n_100),
        .O(\trunc_ln1_reg_329[3]_i_64_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_65 
       (.I0(mul_ln81_fu_206_p2__2_n_101),
        .I1(mul_ln81_fu_206_p2_n_101),
        .O(\trunc_ln1_reg_329[3]_i_65_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_66 
       (.I0(mul_ln81_fu_206_p2__2_n_102),
        .I1(mul_ln81_fu_206_p2_n_102),
        .O(\trunc_ln1_reg_329[3]_i_66_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_67 
       (.I0(mul_ln81_fu_206_p2__2_n_103),
        .I1(mul_ln81_fu_206_p2_n_103),
        .O(\trunc_ln1_reg_329[3]_i_67_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_69 
       (.I0(mul_ln81_fu_206_p2__1_n_91),
        .I1(acc1_0_reg_142[15]),
        .O(\trunc_ln1_reg_329[3]_i_69_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_7 
       (.I0(mul_ln81_fu_206_p2__3[40]),
        .I1(acc1_0_reg_142[40]),
        .O(\trunc_ln1_reg_329[3]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_70 
       (.I0(mul_ln81_fu_206_p2__1_n_92),
        .I1(acc1_0_reg_142[14]),
        .O(\trunc_ln1_reg_329[3]_i_70_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_71 
       (.I0(mul_ln81_fu_206_p2__1_n_93),
        .I1(acc1_0_reg_142[13]),
        .O(\trunc_ln1_reg_329[3]_i_71_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_72 
       (.I0(mul_ln81_fu_206_p2__1_n_94),
        .I1(acc1_0_reg_142[12]),
        .O(\trunc_ln1_reg_329[3]_i_72_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_73 
       (.I0(mul_ln81_fu_206_p2__2_n_104),
        .I1(mul_ln81_fu_206_p2_n_104),
        .O(\trunc_ln1_reg_329[3]_i_73_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_74 
       (.I0(mul_ln81_fu_206_p2__2_n_105),
        .I1(mul_ln81_fu_206_p2_n_105),
        .O(\trunc_ln1_reg_329[3]_i_74_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_75 
       (.I0(mul_ln81_fu_206_p2__2_n_106),
        .I1(mul_ln81_fu_206_p2_n_106),
        .O(\trunc_ln1_reg_329[3]_i_75_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_77 
       (.I0(mul_ln81_fu_206_p2__1_n_95),
        .I1(acc1_0_reg_142[11]),
        .O(\trunc_ln1_reg_329[3]_i_77_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_78 
       (.I0(mul_ln81_fu_206_p2__1_n_96),
        .I1(acc1_0_reg_142[10]),
        .O(\trunc_ln1_reg_329[3]_i_78_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_79 
       (.I0(mul_ln81_fu_206_p2__1_n_97),
        .I1(acc1_0_reg_142[9]),
        .O(\trunc_ln1_reg_329[3]_i_79_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_80 
       (.I0(mul_ln81_fu_206_p2__1_n_98),
        .I1(acc1_0_reg_142[8]),
        .O(\trunc_ln1_reg_329[3]_i_80_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_82 
       (.I0(mul_ln81_fu_206_p2__1_n_99),
        .I1(acc1_0_reg_142[7]),
        .O(\trunc_ln1_reg_329[3]_i_82_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_83 
       (.I0(mul_ln81_fu_206_p2__1_n_100),
        .I1(acc1_0_reg_142[6]),
        .O(\trunc_ln1_reg_329[3]_i_83_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_84 
       (.I0(mul_ln81_fu_206_p2__1_n_101),
        .I1(acc1_0_reg_142[5]),
        .O(\trunc_ln1_reg_329[3]_i_84_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_85 
       (.I0(mul_ln81_fu_206_p2__1_n_102),
        .I1(acc1_0_reg_142[4]),
        .O(\trunc_ln1_reg_329[3]_i_85_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_86 
       (.I0(mul_ln81_fu_206_p2__1_n_103),
        .I1(acc1_0_reg_142[3]),
        .O(\trunc_ln1_reg_329[3]_i_86_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_87 
       (.I0(mul_ln81_fu_206_p2__1_n_104),
        .I1(acc1_0_reg_142[2]),
        .O(\trunc_ln1_reg_329[3]_i_87_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_88 
       (.I0(mul_ln81_fu_206_p2__1_n_105),
        .I1(acc1_0_reg_142[1]),
        .O(\trunc_ln1_reg_329[3]_i_88_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[3]_i_89 
       (.I0(mul_ln81_fu_206_p2__1_n_106),
        .I1(acc1_0_reg_142[0]),
        .O(\trunc_ln1_reg_329[3]_i_89_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_10 
       (.I0(mul_ln81_fu_206_p2__2_n_79),
        .I1(mul_ln81_fu_206_p2__0_n_96),
        .O(\trunc_ln1_reg_329[7]_i_10_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_3 
       (.I0(mul_ln81_fu_206_p2__3[47]),
        .I1(acc1_0_reg_142[47]),
        .O(\trunc_ln1_reg_329[7]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_4 
       (.I0(mul_ln81_fu_206_p2__3[46]),
        .I1(acc1_0_reg_142[46]),
        .O(\trunc_ln1_reg_329[7]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_5 
       (.I0(mul_ln81_fu_206_p2__3[45]),
        .I1(acc1_0_reg_142[45]),
        .O(\trunc_ln1_reg_329[7]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_6 
       (.I0(mul_ln81_fu_206_p2__3[44]),
        .I1(acc1_0_reg_142[44]),
        .O(\trunc_ln1_reg_329[7]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_7 
       (.I0(mul_ln81_fu_206_p2__2_n_76),
        .I1(mul_ln81_fu_206_p2__0_n_93),
        .O(\trunc_ln1_reg_329[7]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_8 
       (.I0(mul_ln81_fu_206_p2__2_n_77),
        .I1(mul_ln81_fu_206_p2__0_n_94),
        .O(\trunc_ln1_reg_329[7]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \trunc_ln1_reg_329[7]_i_9 
       (.I0(mul_ln81_fu_206_p2__2_n_78),
        .I1(mul_ln81_fu_206_p2__0_n_95),
        .O(\trunc_ln1_reg_329[7]_i_9_n_1 ));
  FDRE \trunc_ln1_reg_329_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[0]),
        .Q(trunc_ln1_reg_329[0]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[10]),
        .Q(trunc_ln1_reg_329[10]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[11]),
        .Q(trunc_ln1_reg_329[11]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[11]_i_1 
       (.CI(\trunc_ln1_reg_329_reg[7]_i_1_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[11]_i_1_n_1 ,\trunc_ln1_reg_329_reg[11]_i_1_n_2 ,\trunc_ln1_reg_329_reg[11]_i_1_n_3 ,\trunc_ln1_reg_329_reg[11]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[51:48]),
        .O(p_0_in[11:8]),
        .S({\trunc_ln1_reg_329[11]_i_3_n_1 ,\trunc_ln1_reg_329[11]_i_4_n_1 ,\trunc_ln1_reg_329[11]_i_5_n_1 ,\trunc_ln1_reg_329[11]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[11]_i_2 
       (.CI(\trunc_ln1_reg_329_reg[7]_i_2_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[11]_i_2_n_1 ,\trunc_ln1_reg_329_reg[11]_i_2_n_2 ,\trunc_ln1_reg_329_reg[11]_i_2_n_3 ,\trunc_ln1_reg_329_reg[11]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_72,mul_ln81_fu_206_p2__2_n_73,mul_ln81_fu_206_p2__2_n_74,mul_ln81_fu_206_p2__2_n_75}),
        .O(mul_ln81_fu_206_p2__3[51:48]),
        .S({\trunc_ln1_reg_329[11]_i_7_n_1 ,\trunc_ln1_reg_329[11]_i_8_n_1 ,\trunc_ln1_reg_329[11]_i_9_n_1 ,\trunc_ln1_reg_329[11]_i_10_n_1 }));
  FDRE \trunc_ln1_reg_329_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[12]),
        .Q(trunc_ln1_reg_329[12]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[13]),
        .Q(trunc_ln1_reg_329[13]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[14]),
        .Q(trunc_ln1_reg_329[14]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[15]),
        .Q(trunc_ln1_reg_329[15]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[15]_i_1 
       (.CI(\trunc_ln1_reg_329_reg[11]_i_1_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[15]_i_1_n_1 ,\trunc_ln1_reg_329_reg[15]_i_1_n_2 ,\trunc_ln1_reg_329_reg[15]_i_1_n_3 ,\trunc_ln1_reg_329_reg[15]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[55:52]),
        .O(p_0_in[15:12]),
        .S({\trunc_ln1_reg_329[15]_i_3_n_1 ,\trunc_ln1_reg_329[15]_i_4_n_1 ,\trunc_ln1_reg_329[15]_i_5_n_1 ,\trunc_ln1_reg_329[15]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[15]_i_2 
       (.CI(\trunc_ln1_reg_329_reg[11]_i_2_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[15]_i_2_n_1 ,\trunc_ln1_reg_329_reg[15]_i_2_n_2 ,\trunc_ln1_reg_329_reg[15]_i_2_n_3 ,\trunc_ln1_reg_329_reg[15]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_68,mul_ln81_fu_206_p2__2_n_69,mul_ln81_fu_206_p2__2_n_70,mul_ln81_fu_206_p2__2_n_71}),
        .O(mul_ln81_fu_206_p2__3[55:52]),
        .S({\trunc_ln1_reg_329[15]_i_7_n_1 ,\trunc_ln1_reg_329[15]_i_8_n_1 ,\trunc_ln1_reg_329[15]_i_9_n_1 ,\trunc_ln1_reg_329[15]_i_10_n_1 }));
  FDRE \trunc_ln1_reg_329_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[16]),
        .Q(trunc_ln1_reg_329[16]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[17]),
        .Q(trunc_ln1_reg_329[17]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[18]),
        .Q(trunc_ln1_reg_329[18]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[19]),
        .Q(trunc_ln1_reg_329[19]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[19]_i_1 
       (.CI(\trunc_ln1_reg_329_reg[15]_i_1_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[19]_i_1_n_1 ,\trunc_ln1_reg_329_reg[19]_i_1_n_2 ,\trunc_ln1_reg_329_reg[19]_i_1_n_3 ,\trunc_ln1_reg_329_reg[19]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[59:56]),
        .O(p_0_in[19:16]),
        .S({\trunc_ln1_reg_329[19]_i_3_n_1 ,\trunc_ln1_reg_329[19]_i_4_n_1 ,\trunc_ln1_reg_329[19]_i_5_n_1 ,\trunc_ln1_reg_329[19]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[19]_i_2 
       (.CI(\trunc_ln1_reg_329_reg[15]_i_2_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[19]_i_2_n_1 ,\trunc_ln1_reg_329_reg[19]_i_2_n_2 ,\trunc_ln1_reg_329_reg[19]_i_2_n_3 ,\trunc_ln1_reg_329_reg[19]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_64,mul_ln81_fu_206_p2__2_n_65,mul_ln81_fu_206_p2__2_n_66,mul_ln81_fu_206_p2__2_n_67}),
        .O(mul_ln81_fu_206_p2__3[59:56]),
        .S({\trunc_ln1_reg_329[19]_i_7_n_1 ,\trunc_ln1_reg_329[19]_i_8_n_1 ,\trunc_ln1_reg_329[19]_i_9_n_1 ,\trunc_ln1_reg_329[19]_i_10_n_1 }));
  FDRE \trunc_ln1_reg_329_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[1]),
        .Q(trunc_ln1_reg_329[1]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[20]),
        .Q(trunc_ln1_reg_329[20]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[21]),
        .Q(trunc_ln1_reg_329[21]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[22]),
        .Q(trunc_ln1_reg_329[22]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[23]),
        .Q(trunc_ln1_reg_329[23]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[23]_i_2 
       (.CI(\trunc_ln1_reg_329_reg[19]_i_1_n_1 ),
        .CO({\NLW_trunc_ln1_reg_329_reg[23]_i_2_CO_UNCONNECTED [3],\trunc_ln1_reg_329_reg[23]_i_2_n_2 ,\trunc_ln1_reg_329_reg[23]_i_2_n_3 ,\trunc_ln1_reg_329_reg[23]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln81_fu_206_p2__3[62:60]}),
        .O(p_0_in[23:20]),
        .S({\trunc_ln1_reg_329[23]_i_4_n_1 ,\trunc_ln1_reg_329[23]_i_5_n_1 ,\trunc_ln1_reg_329[23]_i_6_n_1 ,\trunc_ln1_reg_329[23]_i_7_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[23]_i_3 
       (.CI(\trunc_ln1_reg_329_reg[19]_i_2_n_1 ),
        .CO({\NLW_trunc_ln1_reg_329_reg[23]_i_3_CO_UNCONNECTED [3],\trunc_ln1_reg_329_reg[23]_i_3_n_2 ,\trunc_ln1_reg_329_reg[23]_i_3_n_3 ,\trunc_ln1_reg_329_reg[23]_i_3_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln81_fu_206_p2__2_n_61,mul_ln81_fu_206_p2__2_n_62,mul_ln81_fu_206_p2__2_n_63}),
        .O(mul_ln81_fu_206_p2__3[63:60]),
        .S({\trunc_ln1_reg_329[23]_i_8_n_1 ,\trunc_ln1_reg_329[23]_i_9_n_1 ,\trunc_ln1_reg_329[23]_i_10_n_1 ,\trunc_ln1_reg_329[23]_i_11_n_1 }));
  FDRE \trunc_ln1_reg_329_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[2]),
        .Q(trunc_ln1_reg_329[2]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[3]),
        .Q(trunc_ln1_reg_329[3]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_1 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_2_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_1_n_1 ,\trunc_ln1_reg_329_reg[3]_i_1_n_2 ,\trunc_ln1_reg_329_reg[3]_i_1_n_3 ,\trunc_ln1_reg_329_reg[3]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[43:40]),
        .O(p_0_in[3:0]),
        .S({\trunc_ln1_reg_329[3]_i_4_n_1 ,\trunc_ln1_reg_329[3]_i_5_n_1 ,\trunc_ln1_reg_329[3]_i_6_n_1 ,\trunc_ln1_reg_329[3]_i_7_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_18 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_28_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_18_n_1 ,\trunc_ln1_reg_329_reg[3]_i_18_n_2 ,\trunc_ln1_reg_329_reg[3]_i_18_n_3 ,\trunc_ln1_reg_329_reg[3]_i_18_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[31:28]),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_18_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_30_n_1 ,\trunc_ln1_reg_329[3]_i_31_n_1 ,\trunc_ln1_reg_329[3]_i_32_n_1 ,\trunc_ln1_reg_329[3]_i_33_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_19 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_29_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_19_n_1 ,\trunc_ln1_reg_329_reg[3]_i_19_n_2 ,\trunc_ln1_reg_329_reg[3]_i_19_n_3 ,\trunc_ln1_reg_329_reg[3]_i_19_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_88,mul_ln81_fu_206_p2__2_n_89,mul_ln81_fu_206_p2__2_n_90,mul_ln81_fu_206_p2__2_n_91}),
        .O(mul_ln81_fu_206_p2__3[35:32]),
        .S({\trunc_ln1_reg_329[3]_i_34_n_1 ,\trunc_ln1_reg_329[3]_i_35_n_1 ,\trunc_ln1_reg_329[3]_i_36_n_1 ,\trunc_ln1_reg_329[3]_i_37_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_2 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_8_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_2_n_1 ,\trunc_ln1_reg_329_reg[3]_i_2_n_2 ,\trunc_ln1_reg_329_reg[3]_i_2_n_3 ,\trunc_ln1_reg_329_reg[3]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[39:36]),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_2_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_10_n_1 ,\trunc_ln1_reg_329[3]_i_11_n_1 ,\trunc_ln1_reg_329[3]_i_12_n_1 ,\trunc_ln1_reg_329[3]_i_13_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_28 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_38_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_28_n_1 ,\trunc_ln1_reg_329_reg[3]_i_28_n_2 ,\trunc_ln1_reg_329_reg[3]_i_28_n_3 ,\trunc_ln1_reg_329_reg[3]_i_28_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[27:24]),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_28_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_40_n_1 ,\trunc_ln1_reg_329[3]_i_41_n_1 ,\trunc_ln1_reg_329[3]_i_42_n_1 ,\trunc_ln1_reg_329[3]_i_43_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_29 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_39_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_29_n_1 ,\trunc_ln1_reg_329_reg[3]_i_29_n_2 ,\trunc_ln1_reg_329_reg[3]_i_29_n_3 ,\trunc_ln1_reg_329_reg[3]_i_29_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_92,mul_ln81_fu_206_p2__2_n_93,mul_ln81_fu_206_p2__2_n_94,mul_ln81_fu_206_p2__2_n_95}),
        .O(mul_ln81_fu_206_p2__3[31:28]),
        .S({\trunc_ln1_reg_329[3]_i_44_n_1 ,\trunc_ln1_reg_329[3]_i_45_n_1 ,\trunc_ln1_reg_329[3]_i_46_n_1 ,\trunc_ln1_reg_329[3]_i_47_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_3 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_9_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_3_n_1 ,\trunc_ln1_reg_329_reg[3]_i_3_n_2 ,\trunc_ln1_reg_329_reg[3]_i_3_n_3 ,\trunc_ln1_reg_329_reg[3]_i_3_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_80,mul_ln81_fu_206_p2__2_n_81,mul_ln81_fu_206_p2__2_n_82,mul_ln81_fu_206_p2__2_n_83}),
        .O(mul_ln81_fu_206_p2__3[43:40]),
        .S({\trunc_ln1_reg_329[3]_i_14_n_1 ,\trunc_ln1_reg_329[3]_i_15_n_1 ,\trunc_ln1_reg_329[3]_i_16_n_1 ,\trunc_ln1_reg_329[3]_i_17_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_38 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_48_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_38_n_1 ,\trunc_ln1_reg_329_reg[3]_i_38_n_2 ,\trunc_ln1_reg_329_reg[3]_i_38_n_3 ,\trunc_ln1_reg_329_reg[3]_i_38_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[23:20]),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_38_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_50_n_1 ,\trunc_ln1_reg_329[3]_i_51_n_1 ,\trunc_ln1_reg_329[3]_i_52_n_1 ,\trunc_ln1_reg_329[3]_i_53_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_39 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_49_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_39_n_1 ,\trunc_ln1_reg_329_reg[3]_i_39_n_2 ,\trunc_ln1_reg_329_reg[3]_i_39_n_3 ,\trunc_ln1_reg_329_reg[3]_i_39_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_96,mul_ln81_fu_206_p2__2_n_97,mul_ln81_fu_206_p2__2_n_98,mul_ln81_fu_206_p2__2_n_99}),
        .O(mul_ln81_fu_206_p2__3[27:24]),
        .S({\trunc_ln1_reg_329[3]_i_54_n_1 ,\trunc_ln1_reg_329[3]_i_55_n_1 ,\trunc_ln1_reg_329[3]_i_56_n_1 ,\trunc_ln1_reg_329[3]_i_57_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_48 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_58_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_48_n_1 ,\trunc_ln1_reg_329_reg[3]_i_48_n_2 ,\trunc_ln1_reg_329_reg[3]_i_48_n_3 ,\trunc_ln1_reg_329_reg[3]_i_48_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[19:16]),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_48_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_60_n_1 ,\trunc_ln1_reg_329[3]_i_61_n_1 ,\trunc_ln1_reg_329[3]_i_62_n_1 ,\trunc_ln1_reg_329[3]_i_63_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_49 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_59_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_49_n_1 ,\trunc_ln1_reg_329_reg[3]_i_49_n_2 ,\trunc_ln1_reg_329_reg[3]_i_49_n_3 ,\trunc_ln1_reg_329_reg[3]_i_49_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_100,mul_ln81_fu_206_p2__2_n_101,mul_ln81_fu_206_p2__2_n_102,mul_ln81_fu_206_p2__2_n_103}),
        .O(mul_ln81_fu_206_p2__3[23:20]),
        .S({\trunc_ln1_reg_329[3]_i_64_n_1 ,\trunc_ln1_reg_329[3]_i_65_n_1 ,\trunc_ln1_reg_329[3]_i_66_n_1 ,\trunc_ln1_reg_329[3]_i_67_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_58 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_68_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_58_n_1 ,\trunc_ln1_reg_329_reg[3]_i_58_n_2 ,\trunc_ln1_reg_329_reg[3]_i_58_n_3 ,\trunc_ln1_reg_329_reg[3]_i_58_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__1_n_91,mul_ln81_fu_206_p2__1_n_92,mul_ln81_fu_206_p2__1_n_93,mul_ln81_fu_206_p2__1_n_94}),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_58_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_69_n_1 ,\trunc_ln1_reg_329[3]_i_70_n_1 ,\trunc_ln1_reg_329[3]_i_71_n_1 ,\trunc_ln1_reg_329[3]_i_72_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_59 
       (.CI(1'b0),
        .CO({\trunc_ln1_reg_329_reg[3]_i_59_n_1 ,\trunc_ln1_reg_329_reg[3]_i_59_n_2 ,\trunc_ln1_reg_329_reg[3]_i_59_n_3 ,\trunc_ln1_reg_329_reg[3]_i_59_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_104,mul_ln81_fu_206_p2__2_n_105,mul_ln81_fu_206_p2__2_n_106,1'b0}),
        .O(mul_ln81_fu_206_p2__3[19:16]),
        .S({\trunc_ln1_reg_329[3]_i_73_n_1 ,\trunc_ln1_reg_329[3]_i_74_n_1 ,\trunc_ln1_reg_329[3]_i_75_n_1 ,mul_ln81_fu_206_p2__1_n_90}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_68 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_76_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_68_n_1 ,\trunc_ln1_reg_329_reg[3]_i_68_n_2 ,\trunc_ln1_reg_329_reg[3]_i_68_n_3 ,\trunc_ln1_reg_329_reg[3]_i_68_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__1_n_95,mul_ln81_fu_206_p2__1_n_96,mul_ln81_fu_206_p2__1_n_97,mul_ln81_fu_206_p2__1_n_98}),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_68_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_77_n_1 ,\trunc_ln1_reg_329[3]_i_78_n_1 ,\trunc_ln1_reg_329[3]_i_79_n_1 ,\trunc_ln1_reg_329[3]_i_80_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_76 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_81_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_76_n_1 ,\trunc_ln1_reg_329_reg[3]_i_76_n_2 ,\trunc_ln1_reg_329_reg[3]_i_76_n_3 ,\trunc_ln1_reg_329_reg[3]_i_76_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__1_n_99,mul_ln81_fu_206_p2__1_n_100,mul_ln81_fu_206_p2__1_n_101,mul_ln81_fu_206_p2__1_n_102}),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_76_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_82_n_1 ,\trunc_ln1_reg_329[3]_i_83_n_1 ,\trunc_ln1_reg_329[3]_i_84_n_1 ,\trunc_ln1_reg_329[3]_i_85_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_8 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_18_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_8_n_1 ,\trunc_ln1_reg_329_reg[3]_i_8_n_2 ,\trunc_ln1_reg_329_reg[3]_i_8_n_3 ,\trunc_ln1_reg_329_reg[3]_i_8_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[35:32]),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_8_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_20_n_1 ,\trunc_ln1_reg_329[3]_i_21_n_1 ,\trunc_ln1_reg_329[3]_i_22_n_1 ,\trunc_ln1_reg_329[3]_i_23_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_81 
       (.CI(1'b0),
        .CO({\trunc_ln1_reg_329_reg[3]_i_81_n_1 ,\trunc_ln1_reg_329_reg[3]_i_81_n_2 ,\trunc_ln1_reg_329_reg[3]_i_81_n_3 ,\trunc_ln1_reg_329_reg[3]_i_81_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__1_n_103,mul_ln81_fu_206_p2__1_n_104,mul_ln81_fu_206_p2__1_n_105,mul_ln81_fu_206_p2__1_n_106}),
        .O(\NLW_trunc_ln1_reg_329_reg[3]_i_81_O_UNCONNECTED [3:0]),
        .S({\trunc_ln1_reg_329[3]_i_86_n_1 ,\trunc_ln1_reg_329[3]_i_87_n_1 ,\trunc_ln1_reg_329[3]_i_88_n_1 ,\trunc_ln1_reg_329[3]_i_89_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[3]_i_9 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_19_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[3]_i_9_n_1 ,\trunc_ln1_reg_329_reg[3]_i_9_n_2 ,\trunc_ln1_reg_329_reg[3]_i_9_n_3 ,\trunc_ln1_reg_329_reg[3]_i_9_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_84,mul_ln81_fu_206_p2__2_n_85,mul_ln81_fu_206_p2__2_n_86,mul_ln81_fu_206_p2__2_n_87}),
        .O(mul_ln81_fu_206_p2__3[39:36]),
        .S({\trunc_ln1_reg_329[3]_i_24_n_1 ,\trunc_ln1_reg_329[3]_i_25_n_1 ,\trunc_ln1_reg_329[3]_i_26_n_1 ,\trunc_ln1_reg_329[3]_i_27_n_1 }));
  FDRE \trunc_ln1_reg_329_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[4]),
        .Q(trunc_ln1_reg_329[4]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[5]),
        .Q(trunc_ln1_reg_329[5]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[6]),
        .Q(trunc_ln1_reg_329[6]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[7]),
        .Q(trunc_ln1_reg_329[7]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[7]_i_1 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_1_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[7]_i_1_n_1 ,\trunc_ln1_reg_329_reg[7]_i_1_n_2 ,\trunc_ln1_reg_329_reg[7]_i_1_n_3 ,\trunc_ln1_reg_329_reg[7]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln81_fu_206_p2__3[47:44]),
        .O(p_0_in[7:4]),
        .S({\trunc_ln1_reg_329[7]_i_3_n_1 ,\trunc_ln1_reg_329[7]_i_4_n_1 ,\trunc_ln1_reg_329[7]_i_5_n_1 ,\trunc_ln1_reg_329[7]_i_6_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \trunc_ln1_reg_329_reg[7]_i_2 
       (.CI(\trunc_ln1_reg_329_reg[3]_i_3_n_1 ),
        .CO({\trunc_ln1_reg_329_reg[7]_i_2_n_1 ,\trunc_ln1_reg_329_reg[7]_i_2_n_2 ,\trunc_ln1_reg_329_reg[7]_i_2_n_3 ,\trunc_ln1_reg_329_reg[7]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({mul_ln81_fu_206_p2__2_n_76,mul_ln81_fu_206_p2__2_n_77,mul_ln81_fu_206_p2__2_n_78,mul_ln81_fu_206_p2__2_n_79}),
        .O(mul_ln81_fu_206_p2__3[47:44]),
        .S({\trunc_ln1_reg_329[7]_i_7_n_1 ,\trunc_ln1_reg_329[7]_i_8_n_1 ,\trunc_ln1_reg_329[7]_i_9_n_1 ,\trunc_ln1_reg_329[7]_i_10_n_1 }));
  FDRE \trunc_ln1_reg_329_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[8]),
        .Q(trunc_ln1_reg_329[8]),
        .R(1'b0));
  FDRE \trunc_ln1_reg_329_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm13_out),
        .D(p_0_in[9]),
        .Q(trunc_ln1_reg_329[9]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__16_n_1),
        .Q(x0_reg_283[0]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__6_n_1),
        .Q(x0_reg_283[10]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__5_n_1),
        .Q(x0_reg_283[11]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__4_n_1),
        .Q(x0_reg_283[12]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__3_n_1),
        .Q(x0_reg_283[13]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__2__0_n_1),
        .Q(x0_reg_283[14]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__1__0_n_1),
        .Q(x0_reg_283[15]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__0__0_n_1),
        .Q(x0_reg_283[16]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_17),
        .Q(x0_reg_283[17]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_16),
        .Q(x0_reg_283[18]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_15),
        .Q(x0_reg_283[19]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__15_n_1),
        .Q(x0_reg_283[1]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_14),
        .Q(x0_reg_283[20]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_13),
        .Q(x0_reg_283[21]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_12),
        .Q(x0_reg_283[22]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_11),
        .Q(x0_reg_283[23]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[24] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_10),
        .Q(x0_reg_283[24]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[25] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_9),
        .Q(x0_reg_283[25]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[26] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_8),
        .Q(x0_reg_283[26]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[27] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_7),
        .Q(x0_reg_283[27]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[28] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_6),
        .Q(x0_reg_283[28]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[29] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_5),
        .Q(x0_reg_283[29]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__14_n_1),
        .Q(x0_reg_283[2]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[30] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_4),
        .Q(x0_reg_283[30]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[31] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(regslice_both_x_U_n_3),
        .Q(x0_reg_283[31]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__13_n_1),
        .Q(x0_reg_283[3]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__12_n_1),
        .Q(x0_reg_283[4]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__11_n_1),
        .Q(x0_reg_283[5]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__10_n_1),
        .Q(x0_reg_283[6]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__9_n_1),
        .Q(x0_reg_283[7]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__8_n_1),
        .Q(x0_reg_283[8]),
        .R(1'b0));
  FDRE \x0_reg_283_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(mul_ln80_fu_191_p2__7_n_1),
        .Q(x0_reg_283[9]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[0] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__16_n_1),
        .Q(x1_reg_289[0]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[10] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__6_n_1),
        .Q(x1_reg_289[10]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[11] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__5_n_1),
        .Q(x1_reg_289[11]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[12] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__4_n_1),
        .Q(x1_reg_289[12]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[13] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__3_n_1),
        .Q(x1_reg_289[13]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[14] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__2__0_n_1),
        .Q(x1_reg_289[14]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[15] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__1__0_n_1),
        .Q(x1_reg_289[15]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[16] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__0__0_n_1),
        .Q(x1_reg_289[16]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[17] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_17),
        .Q(x1_reg_289[17]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[18] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_16),
        .Q(x1_reg_289[18]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[19] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_15),
        .Q(x1_reg_289[19]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[1] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__15_n_1),
        .Q(x1_reg_289[1]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[20] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_14),
        .Q(x1_reg_289[20]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[21] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_13),
        .Q(x1_reg_289[21]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[22] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_12),
        .Q(x1_reg_289[22]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[23] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_11),
        .Q(x1_reg_289[23]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[24] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_10),
        .Q(x1_reg_289[24]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[25] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_9),
        .Q(x1_reg_289[25]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[26] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_8),
        .Q(x1_reg_289[26]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[27] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_7),
        .Q(x1_reg_289[27]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[28] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_6),
        .Q(x1_reg_289[28]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[29] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_5),
        .Q(x1_reg_289[29]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[2] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__14_n_1),
        .Q(x1_reg_289[2]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[30] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_4),
        .Q(x1_reg_289[30]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[31] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(regslice_both_x_U_n_3),
        .Q(x1_reg_289[31]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[3] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__13_n_1),
        .Q(x1_reg_289[3]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[4] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__12_n_1),
        .Q(x1_reg_289[4]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[5] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__11_n_1),
        .Q(x1_reg_289[5]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[6] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__10_n_1),
        .Q(x1_reg_289[6]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[7] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__9_n_1),
        .Q(x1_reg_289[7]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[8] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__8_n_1),
        .Q(x1_reg_289[8]),
        .R(1'b0));
  FDRE \x1_reg_289_reg[9] 
       (.C(ap_clk),
        .CE(ack_out1),
        .D(mul_ln80_fu_191_p2__7_n_1),
        .Q(x1_reg_289[9]),
        .R(1'b0));
  FDRE \zext_ln70_reg_313_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_0_reg_154[0]),
        .Q(\zext_ln70_reg_313_reg_n_1_[0] ),
        .R(1'b0));
  FDRE \zext_ln70_reg_313_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_0_reg_154[1]),
        .Q(\zext_ln70_reg_313_reg_n_1_[1] ),
        .R(1'b0));
  FDRE \zext_ln70_reg_313_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_0_reg_154[2]),
        .Q(\zext_ln70_reg_313_reg_n_1_[2] ),
        .R(1'b0));
  FDRE \zext_ln70_reg_313_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_0_reg_154[3]),
        .Q(\zext_ln70_reg_313_reg_n_1_[3] ),
        .R(1'b0));
  FDRE \zext_ln70_reg_313_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_0_reg_154[4]),
        .Q(\zext_ln70_reg_313_reg_n_1_[4] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_coeffs
   (out,
    ap_clk,
    coeffs_ce0,
    Q);
  output [36:0]out;
  input ap_clk;
  input coeffs_ce0;
  input [4:0]Q;

  wire [4:0]Q;
  wire ap_clk;
  wire coeffs_ce0;
  wire [36:0]out;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_coeffs_rom fir_coeffs_rom_U
       (.Q(Q),
        .ap_clk(ap_clk),
        .coeffs_ce0(coeffs_ce0),
        .out(out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_coeffs_rom
   (out,
    ap_clk,
    coeffs_ce0,
    Q);
  output [36:0]out;
  input ap_clk;
  input coeffs_ce0;
  input [4:0]Q;

  wire [4:0]Q;
  wire ap_clk;
  wire coeffs_ce0;
  wire [36:0]out;
  wire [15:1]NLW_q0_reg_1_DOADO_UNCONNECTED;
  wire [15:0]NLW_q0_reg_1_DOBDO_UNCONNECTED;
  wire [1:0]NLW_q0_reg_1_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_q0_reg_1_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1184" *) 
  (* RTL_RAM_NAME = "coeffs_U/fir_coeffs_rom_U/q0" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_ext_slice_begin = "18" *) 
  (* ram_ext_slice_end = "35" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h000000000000000000000000000000000000000000000000361209ED9ED82127),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000001000015555500001),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h91FB438DF51E5EBB1D3E0492B19168EC3AD9F8ADB535CD1C5AF6AE7DFADA79D9),
    .INIT_01(256'h000079D9FADAAE7D5AF6CD1CB535F8AD3AD968ECB19104921D3E5EBBF51E438D),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0C880BBF0AF508A6066302B8FF36FA87F60EF0A1EB5BE564DEF4D8ADCFFA66C6),
    .INIT_21(256'h000066C6CFFAD8ADDEF4E564EB5BF0A1F60EFA87FF3602B8066308A60AF50BBF),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    q0_reg_0
       (.ADDRARDADDR({1'b0,1'b0,1'b0,1'b0,1'b0,Q,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,1'b0,1'b0,1'b0,1'b0,Q,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(out[15:0]),
        .DOBDO(out[33:18]),
        .DOPADOP(out[17:16]),
        .DOPBDOP(out[35:34]),
        .ENARDEN(coeffs_ce0),
        .ENBWREN(coeffs_ce0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d0" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1184" *) 
  (* RTL_RAM_NAME = "coeffs_U/fir_coeffs_rom_U/q0" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "31" *) 
  (* bram_slice_begin = "36" *) 
  (* bram_slice_end = "36" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_ext_slice_begin = "37" *) 
  (* ram_ext_slice_end = "36" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "36" *) 
  (* ram_slice_end = "36" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000001),
    .INIT_01(256'h0000000100000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    q0_reg_1
       (.ADDRARDADDR({1'b0,1'b0,1'b0,1'b0,1'b0,Q,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,1'b0,1'b0,1'b0,1'b0,Q,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO({NLW_q0_reg_1_DOADO_UNCONNECTED[15:1],out[36]}),
        .DOBDO(NLW_q0_reg_1_DOBDO_UNCONNECTED[15:0]),
        .DOPADOP(NLW_q0_reg_1_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_q0_reg_1_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(coeffs_ce0),
        .ENBWREN(coeffs_ce0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0
   (shift_reg0_address0,
    \ap_CS_fsm_reg[2] ,
    \i_0_reg_154_reg[0] ,
    q00,
    Q,
    ram_reg_0_15_0_0__62,
    \q0_reg[0] ,
    ram_reg_0_15_0_0__62_0,
    ram_reg_0_15_0_0__62_1,
    shift_reg0_ce0,
    ap_clk,
    \q0_reg[0]_0 ,
    \q0_reg[31] ,
    \q0_reg[31]_0 );
  output [3:0]shift_reg0_address0;
  output \ap_CS_fsm_reg[2] ;
  output \i_0_reg_154_reg[0] ;
  output [31:0]q00;
  input [3:0]Q;
  input [1:0]ram_reg_0_15_0_0__62;
  input [4:0]\q0_reg[0] ;
  input ram_reg_0_15_0_0__62_0;
  input [31:0]ram_reg_0_15_0_0__62_1;
  input shift_reg0_ce0;
  input ap_clk;
  input \q0_reg[0]_0 ;
  input \q0_reg[31] ;
  input [0:0]\q0_reg[31]_0 ;

  wire [3:0]Q;
  wire \ap_CS_fsm_reg[2] ;
  wire ap_clk;
  wire \i_0_reg_154_reg[0] ;
  wire [31:0]q00;
  wire [4:0]\q0_reg[0] ;
  wire \q0_reg[0]_0 ;
  wire \q0_reg[31] ;
  wire [0:0]\q0_reg[31]_0 ;
  wire [1:0]ram_reg_0_15_0_0__62;
  wire ram_reg_0_15_0_0__62_0;
  wire [31:0]ram_reg_0_15_0_0__62_1;
  wire [3:0]shift_reg0_address0;
  wire shift_reg0_ce0;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0_ram_2 fir_shift_reg0_ram_U
       (.Q(Q),
        .\ap_CS_fsm_reg[2] (\ap_CS_fsm_reg[2] ),
        .ap_clk(ap_clk),
        .\i_0_reg_154_reg[0] (\i_0_reg_154_reg[0] ),
        .q00(q00),
        .\q0_reg[0]_0 (\q0_reg[0] ),
        .\q0_reg[0]_1 (\q0_reg[0]_0 ),
        .\q0_reg[31]_0 (\q0_reg[31] ),
        .\q0_reg[31]_1 (\q0_reg[31]_0 ),
        .ram_reg_0_15_0_0__62_0(ram_reg_0_15_0_0__62),
        .ram_reg_0_15_0_0__62_1(ram_reg_0_15_0_0__62_0),
        .ram_reg_0_15_0_0__62_2(ram_reg_0_15_0_0__62_1),
        .shift_reg0_address0(shift_reg0_address0),
        .shift_reg0_ce0(shift_reg0_ce0));
endmodule

(* ORIG_REF_NAME = "fir_shift_reg0" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0_1
   (\zext_ln70_reg_313_reg[4] ,
    \i_0_reg_154_reg[2] ,
    B,
    D,
    Q,
    \q0_reg[31] ,
    \q0_reg[31]_0 ,
    ram_reg_0_15_0_0__62,
    shift_reg0_ce0,
    ap_clk,
    \q0_reg[31]_1 ,
    shift_reg0_address0,
    \q0_reg[0] );
  output [0:0]\zext_ln70_reg_313_reg[4] ;
  output \i_0_reg_154_reg[2] ;
  output [14:0]B;
  output [16:0]D;
  input [0:0]Q;
  input [1:0]\q0_reg[31] ;
  input [4:0]\q0_reg[31]_0 ;
  input [31:0]ram_reg_0_15_0_0__62;
  input shift_reg0_ce0;
  input ap_clk;
  input \q0_reg[31]_1 ;
  input [3:0]shift_reg0_address0;
  input \q0_reg[0] ;

  wire [14:0]B;
  wire [16:0]D;
  wire [0:0]Q;
  wire ap_clk;
  wire \i_0_reg_154_reg[2] ;
  wire \q0_reg[0] ;
  wire [1:0]\q0_reg[31] ;
  wire [4:0]\q0_reg[31]_0 ;
  wire \q0_reg[31]_1 ;
  wire [31:0]ram_reg_0_15_0_0__62;
  wire [3:0]shift_reg0_address0;
  wire shift_reg0_ce0;
  wire [0:0]\zext_ln70_reg_313_reg[4] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0_ram fir_shift_reg0_ram_U
       (.D({B,D}),
        .Q(Q),
        .ap_clk(ap_clk),
        .\i_0_reg_154_reg[2] (\i_0_reg_154_reg[2] ),
        .\q0_reg[0]_0 (\q0_reg[0] ),
        .\q0_reg[31]_0 (\q0_reg[31] ),
        .\q0_reg[31]_1 (\q0_reg[31]_0 ),
        .\q0_reg[31]_2 (\q0_reg[31]_1 ),
        .ram_reg_0_15_0_0__62_0(ram_reg_0_15_0_0__62),
        .shift_reg0_address0(shift_reg0_address0),
        .shift_reg0_ce0(shift_reg0_ce0),
        .\zext_ln70_reg_313_reg[4] (\zext_ln70_reg_313_reg[4] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0_ram
   (\zext_ln70_reg_313_reg[4] ,
    \i_0_reg_154_reg[2] ,
    D,
    Q,
    \q0_reg[31]_0 ,
    \q0_reg[31]_1 ,
    ram_reg_0_15_0_0__62_0,
    shift_reg0_ce0,
    ap_clk,
    \q0_reg[31]_2 ,
    shift_reg0_address0,
    \q0_reg[0]_0 );
  output \zext_ln70_reg_313_reg[4] ;
  output \i_0_reg_154_reg[2] ;
  output [31:0]D;
  input [0:0]Q;
  input [1:0]\q0_reg[31]_0 ;
  input [4:0]\q0_reg[31]_1 ;
  input [31:0]ram_reg_0_15_0_0__62_0;
  input shift_reg0_ce0;
  input ap_clk;
  input \q0_reg[31]_2 ;
  input [3:0]shift_reg0_address0;
  input \q0_reg[0]_0 ;

  wire [31:0]D;
  wire [0:0]Q;
  wire ap_clk;
  wire \i_0_reg_154_reg[2] ;
  wire \q0_reg[0]_0 ;
  wire [1:0]\q0_reg[31]_0 ;
  wire [4:0]\q0_reg[31]_1 ;
  wire \q0_reg[31]_2 ;
  wire \q0_reg_n_1_[0] ;
  wire \q0_reg_n_1_[10] ;
  wire \q0_reg_n_1_[11] ;
  wire \q0_reg_n_1_[12] ;
  wire \q0_reg_n_1_[13] ;
  wire \q0_reg_n_1_[14] ;
  wire \q0_reg_n_1_[15] ;
  wire \q0_reg_n_1_[16] ;
  wire \q0_reg_n_1_[17] ;
  wire \q0_reg_n_1_[18] ;
  wire \q0_reg_n_1_[19] ;
  wire \q0_reg_n_1_[1] ;
  wire \q0_reg_n_1_[20] ;
  wire \q0_reg_n_1_[21] ;
  wire \q0_reg_n_1_[22] ;
  wire \q0_reg_n_1_[23] ;
  wire \q0_reg_n_1_[24] ;
  wire \q0_reg_n_1_[25] ;
  wire \q0_reg_n_1_[26] ;
  wire \q0_reg_n_1_[27] ;
  wire \q0_reg_n_1_[28] ;
  wire \q0_reg_n_1_[29] ;
  wire \q0_reg_n_1_[2] ;
  wire \q0_reg_n_1_[30] ;
  wire \q0_reg_n_1_[31] ;
  wire \q0_reg_n_1_[3] ;
  wire \q0_reg_n_1_[4] ;
  wire \q0_reg_n_1_[5] ;
  wire \q0_reg_n_1_[6] ;
  wire \q0_reg_n_1_[7] ;
  wire \q0_reg_n_1_[8] ;
  wire \q0_reg_n_1_[9] ;
  wire ram_reg_0_15_0_0__0_n_1;
  wire ram_reg_0_15_0_0__10_n_1;
  wire ram_reg_0_15_0_0__11_i_1_n_1;
  wire ram_reg_0_15_0_0__11_n_1;
  wire ram_reg_0_15_0_0__12_n_1;
  wire ram_reg_0_15_0_0__13_i_1_n_1;
  wire ram_reg_0_15_0_0__13_n_1;
  wire ram_reg_0_15_0_0__14_n_1;
  wire ram_reg_0_15_0_0__15_i_1_n_1;
  wire ram_reg_0_15_0_0__15_n_1;
  wire ram_reg_0_15_0_0__16_n_1;
  wire ram_reg_0_15_0_0__17_i_1_n_1;
  wire ram_reg_0_15_0_0__17_n_1;
  wire ram_reg_0_15_0_0__18_n_1;
  wire ram_reg_0_15_0_0__19_i_1_n_1;
  wire ram_reg_0_15_0_0__19_n_1;
  wire ram_reg_0_15_0_0__1_i_1_n_1;
  wire ram_reg_0_15_0_0__1_n_1;
  wire ram_reg_0_15_0_0__20_n_1;
  wire ram_reg_0_15_0_0__21_i_1_n_1;
  wire ram_reg_0_15_0_0__21_n_1;
  wire ram_reg_0_15_0_0__22_n_1;
  wire ram_reg_0_15_0_0__23_i_1_n_1;
  wire ram_reg_0_15_0_0__23_n_1;
  wire ram_reg_0_15_0_0__24_n_1;
  wire ram_reg_0_15_0_0__25_i_1_n_1;
  wire ram_reg_0_15_0_0__25_n_1;
  wire ram_reg_0_15_0_0__26_n_1;
  wire ram_reg_0_15_0_0__27_i_1_n_1;
  wire ram_reg_0_15_0_0__27_n_1;
  wire ram_reg_0_15_0_0__28_n_1;
  wire ram_reg_0_15_0_0__29_i_1_n_1;
  wire ram_reg_0_15_0_0__29_n_1;
  wire ram_reg_0_15_0_0__2_n_1;
  wire ram_reg_0_15_0_0__30_n_1;
  wire ram_reg_0_15_0_0__31_i_1_n_1;
  wire ram_reg_0_15_0_0__31_n_1;
  wire ram_reg_0_15_0_0__32_n_1;
  wire ram_reg_0_15_0_0__33_i_1_n_1;
  wire ram_reg_0_15_0_0__33_n_1;
  wire ram_reg_0_15_0_0__34_n_1;
  wire ram_reg_0_15_0_0__35_i_1_n_1;
  wire ram_reg_0_15_0_0__35_n_1;
  wire ram_reg_0_15_0_0__36_n_1;
  wire ram_reg_0_15_0_0__37_i_1_n_1;
  wire ram_reg_0_15_0_0__37_n_1;
  wire ram_reg_0_15_0_0__38_n_1;
  wire ram_reg_0_15_0_0__39_i_1_n_1;
  wire ram_reg_0_15_0_0__39_n_1;
  wire ram_reg_0_15_0_0__3_i_1_n_1;
  wire ram_reg_0_15_0_0__3_n_1;
  wire ram_reg_0_15_0_0__40_n_1;
  wire ram_reg_0_15_0_0__41_i_1_n_1;
  wire ram_reg_0_15_0_0__41_n_1;
  wire ram_reg_0_15_0_0__42_n_1;
  wire ram_reg_0_15_0_0__43_i_1_n_1;
  wire ram_reg_0_15_0_0__43_n_1;
  wire ram_reg_0_15_0_0__44_n_1;
  wire ram_reg_0_15_0_0__45_i_1_n_1;
  wire ram_reg_0_15_0_0__45_n_1;
  wire ram_reg_0_15_0_0__46_n_1;
  wire ram_reg_0_15_0_0__47_i_1_n_1;
  wire ram_reg_0_15_0_0__47_n_1;
  wire ram_reg_0_15_0_0__48_n_1;
  wire ram_reg_0_15_0_0__49_i_1_n_1;
  wire ram_reg_0_15_0_0__49_n_1;
  wire ram_reg_0_15_0_0__4_n_1;
  wire ram_reg_0_15_0_0__50_n_1;
  wire ram_reg_0_15_0_0__51_i_1_n_1;
  wire ram_reg_0_15_0_0__51_n_1;
  wire ram_reg_0_15_0_0__52_n_1;
  wire ram_reg_0_15_0_0__53_i_1_n_1;
  wire ram_reg_0_15_0_0__53_n_1;
  wire ram_reg_0_15_0_0__54_n_1;
  wire ram_reg_0_15_0_0__55_i_1_n_1;
  wire ram_reg_0_15_0_0__55_n_1;
  wire ram_reg_0_15_0_0__56_n_1;
  wire ram_reg_0_15_0_0__57_i_1_n_1;
  wire ram_reg_0_15_0_0__57_n_1;
  wire ram_reg_0_15_0_0__58_n_1;
  wire ram_reg_0_15_0_0__59_i_1_n_1;
  wire ram_reg_0_15_0_0__59_n_1;
  wire ram_reg_0_15_0_0__5_i_1_n_1;
  wire ram_reg_0_15_0_0__5_n_1;
  wire ram_reg_0_15_0_0__60_n_1;
  wire ram_reg_0_15_0_0__61_i_1_n_1;
  wire ram_reg_0_15_0_0__61_n_1;
  wire [31:0]ram_reg_0_15_0_0__62_0;
  wire ram_reg_0_15_0_0__62_n_1;
  wire ram_reg_0_15_0_0__6_n_1;
  wire ram_reg_0_15_0_0__7_i_1_n_1;
  wire ram_reg_0_15_0_0__7_n_1;
  wire ram_reg_0_15_0_0__8_n_1;
  wire ram_reg_0_15_0_0__9_i_1_n_1;
  wire ram_reg_0_15_0_0__9_n_1;
  wire ram_reg_0_15_0_0_i_1_n_1;
  wire ram_reg_0_15_0_0_n_1;
  wire [3:0]shift_reg0_address0;
  wire shift_reg0_ce0;
  wire \zext_ln70_reg_313_reg[4] ;

  LUT3 #(
    .INIT(8'hB8)) 
    \q0[0]_i_1 
       (.I0(ram_reg_0_15_0_0__0_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0_n_1),
        .O(D[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[10]_i_1 
       (.I0(ram_reg_0_15_0_0__20_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__19_n_1),
        .O(D[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[11]_i_1 
       (.I0(ram_reg_0_15_0_0__22_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__21_n_1),
        .O(D[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[12]_i_1 
       (.I0(ram_reg_0_15_0_0__24_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__23_n_1),
        .O(D[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[13]_i_1 
       (.I0(ram_reg_0_15_0_0__26_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__25_n_1),
        .O(D[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[14]_i_1 
       (.I0(ram_reg_0_15_0_0__28_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__27_n_1),
        .O(D[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[15]_i_1 
       (.I0(ram_reg_0_15_0_0__30_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__29_n_1),
        .O(D[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[16]_i_1 
       (.I0(ram_reg_0_15_0_0__32_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__31_n_1),
        .O(D[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[17]_i_1 
       (.I0(ram_reg_0_15_0_0__34_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__33_n_1),
        .O(D[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[18]_i_1 
       (.I0(ram_reg_0_15_0_0__36_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__35_n_1),
        .O(D[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[19]_i_1 
       (.I0(ram_reg_0_15_0_0__38_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__37_n_1),
        .O(D[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[1]_i_1 
       (.I0(ram_reg_0_15_0_0__2_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__1_n_1),
        .O(D[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[20]_i_1 
       (.I0(ram_reg_0_15_0_0__40_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__39_n_1),
        .O(D[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[21]_i_1 
       (.I0(ram_reg_0_15_0_0__42_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__41_n_1),
        .O(D[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[22]_i_1 
       (.I0(ram_reg_0_15_0_0__44_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__43_n_1),
        .O(D[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[23]_i_1 
       (.I0(ram_reg_0_15_0_0__46_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__45_n_1),
        .O(D[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[24]_i_1 
       (.I0(ram_reg_0_15_0_0__48_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__47_n_1),
        .O(D[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[25]_i_1 
       (.I0(ram_reg_0_15_0_0__50_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__49_n_1),
        .O(D[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[26]_i_1 
       (.I0(ram_reg_0_15_0_0__52_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__51_n_1),
        .O(D[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[27]_i_1 
       (.I0(ram_reg_0_15_0_0__54_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__53_n_1),
        .O(D[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[28]_i_1 
       (.I0(ram_reg_0_15_0_0__56_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__55_n_1),
        .O(D[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[29]_i_1 
       (.I0(ram_reg_0_15_0_0__58_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__57_n_1),
        .O(D[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[2]_i_1 
       (.I0(ram_reg_0_15_0_0__4_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__3_n_1),
        .O(D[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[30]_i_1 
       (.I0(ram_reg_0_15_0_0__60_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__59_n_1),
        .O(D[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[31]_i_2 
       (.I0(ram_reg_0_15_0_0__62_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__61_n_1),
        .O(D[31]));
  LUT6 #(
    .INIT(64'hBBBB88BB88888B88)) 
    \q0[31]_i_3 
       (.I0(Q),
        .I1(\q0_reg[31]_0 [1]),
        .I2(\q0_reg[31]_0 [0]),
        .I3(\i_0_reg_154_reg[2] ),
        .I4(\q0_reg[31]_1 [3]),
        .I5(\q0_reg[31]_1 [4]),
        .O(\zext_ln70_reg_313_reg[4] ));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[3]_i_1 
       (.I0(ram_reg_0_15_0_0__6_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__5_n_1),
        .O(D[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[4]_i_1 
       (.I0(ram_reg_0_15_0_0__8_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__7_n_1),
        .O(D[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[5]_i_1 
       (.I0(ram_reg_0_15_0_0__10_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__9_n_1),
        .O(D[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[6]_i_1 
       (.I0(ram_reg_0_15_0_0__12_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__11_n_1),
        .O(D[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[7]_i_1 
       (.I0(ram_reg_0_15_0_0__14_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__13_n_1),
        .O(D[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[8]_i_1 
       (.I0(ram_reg_0_15_0_0__16_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__15_n_1),
        .O(D[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[9]_i_1 
       (.I0(ram_reg_0_15_0_0__18_n_1),
        .I1(\zext_ln70_reg_313_reg[4] ),
        .I2(ram_reg_0_15_0_0__17_n_1),
        .O(D[9]));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[0]),
        .Q(\q0_reg_n_1_[0] ),
        .R(1'b0));
  FDRE \q0_reg[10] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[10]),
        .Q(\q0_reg_n_1_[10] ),
        .R(1'b0));
  FDRE \q0_reg[11] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[11]),
        .Q(\q0_reg_n_1_[11] ),
        .R(1'b0));
  FDRE \q0_reg[12] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[12]),
        .Q(\q0_reg_n_1_[12] ),
        .R(1'b0));
  FDRE \q0_reg[13] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[13]),
        .Q(\q0_reg_n_1_[13] ),
        .R(1'b0));
  FDRE \q0_reg[14] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[14]),
        .Q(\q0_reg_n_1_[14] ),
        .R(1'b0));
  FDRE \q0_reg[15] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[15]),
        .Q(\q0_reg_n_1_[15] ),
        .R(1'b0));
  FDRE \q0_reg[16] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[16]),
        .Q(\q0_reg_n_1_[16] ),
        .R(1'b0));
  FDRE \q0_reg[17] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[17]),
        .Q(\q0_reg_n_1_[17] ),
        .R(1'b0));
  FDRE \q0_reg[18] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[18]),
        .Q(\q0_reg_n_1_[18] ),
        .R(1'b0));
  FDRE \q0_reg[19] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[19]),
        .Q(\q0_reg_n_1_[19] ),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[1]),
        .Q(\q0_reg_n_1_[1] ),
        .R(1'b0));
  FDRE \q0_reg[20] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[20]),
        .Q(\q0_reg_n_1_[20] ),
        .R(1'b0));
  FDRE \q0_reg[21] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[21]),
        .Q(\q0_reg_n_1_[21] ),
        .R(1'b0));
  FDRE \q0_reg[22] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[22]),
        .Q(\q0_reg_n_1_[22] ),
        .R(1'b0));
  FDRE \q0_reg[23] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[23]),
        .Q(\q0_reg_n_1_[23] ),
        .R(1'b0));
  FDRE \q0_reg[24] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[24]),
        .Q(\q0_reg_n_1_[24] ),
        .R(1'b0));
  FDRE \q0_reg[25] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[25]),
        .Q(\q0_reg_n_1_[25] ),
        .R(1'b0));
  FDRE \q0_reg[26] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[26]),
        .Q(\q0_reg_n_1_[26] ),
        .R(1'b0));
  FDRE \q0_reg[27] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[27]),
        .Q(\q0_reg_n_1_[27] ),
        .R(1'b0));
  FDRE \q0_reg[28] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[28]),
        .Q(\q0_reg_n_1_[28] ),
        .R(1'b0));
  FDRE \q0_reg[29] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[29]),
        .Q(\q0_reg_n_1_[29] ),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[2]),
        .Q(\q0_reg_n_1_[2] ),
        .R(1'b0));
  FDRE \q0_reg[30] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[30]),
        .Q(\q0_reg_n_1_[30] ),
        .R(1'b0));
  FDRE \q0_reg[31] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[31]),
        .Q(\q0_reg_n_1_[31] ),
        .R(1'b0));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[3]),
        .Q(\q0_reg_n_1_[3] ),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[4]),
        .Q(\q0_reg_n_1_[4] ),
        .R(1'b0));
  FDRE \q0_reg[5] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[5]),
        .Q(\q0_reg_n_1_[5] ),
        .R(1'b0));
  FDRE \q0_reg[6] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[6]),
        .Q(\q0_reg_n_1_[6] ),
        .R(1'b0));
  FDRE \q0_reg[7] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[7]),
        .Q(\q0_reg_n_1_[7] ),
        .R(1'b0));
  FDRE \q0_reg[8] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[8]),
        .Q(\q0_reg_n_1_[8] ),
        .R(1'b0));
  FDRE \q0_reg[9] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(D[9]),
        .Q(\q0_reg_n_1_[9] ),
        .R(1'b0));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "0" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0_i_1_n_1),
        .O(ram_reg_0_15_0_0_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "0" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0_i_1_n_1),
        .O(ram_reg_0_15_0_0__0_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "1" *) 
  (* ram_slice_end = "1" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__1
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__1_i_1_n_1),
        .O(ram_reg_0_15_0_0__1_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "5" *) 
  (* ram_slice_end = "5" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__10
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__9_i_1_n_1),
        .O(ram_reg_0_15_0_0__10_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "6" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__11
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__11_i_1_n_1),
        .O(ram_reg_0_15_0_0__11_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__11_i_1
       (.I0(\q0_reg_n_1_[6] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[6]),
        .O(ram_reg_0_15_0_0__11_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "6" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__12
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__11_i_1_n_1),
        .O(ram_reg_0_15_0_0__12_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "7" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__13
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__13_i_1_n_1),
        .O(ram_reg_0_15_0_0__13_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__13_i_1
       (.I0(\q0_reg_n_1_[7] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[7]),
        .O(ram_reg_0_15_0_0__13_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "7" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__14
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__13_i_1_n_1),
        .O(ram_reg_0_15_0_0__14_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "8" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__15
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__15_i_1_n_1),
        .O(ram_reg_0_15_0_0__15_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__15_i_1
       (.I0(\q0_reg_n_1_[8] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[8]),
        .O(ram_reg_0_15_0_0__15_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "8" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__16
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__15_i_1_n_1),
        .O(ram_reg_0_15_0_0__16_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "9" *) 
  (* ram_slice_end = "9" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__17
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__17_i_1_n_1),
        .O(ram_reg_0_15_0_0__17_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__17_i_1
       (.I0(\q0_reg_n_1_[9] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[9]),
        .O(ram_reg_0_15_0_0__17_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "9" *) 
  (* ram_slice_end = "9" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__18
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__17_i_1_n_1),
        .O(ram_reg_0_15_0_0__18_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "10" *) 
  (* ram_slice_end = "10" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__19
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__19_i_1_n_1),
        .O(ram_reg_0_15_0_0__19_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__19_i_1
       (.I0(\q0_reg_n_1_[10] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[10]),
        .O(ram_reg_0_15_0_0__19_i_1_n_1));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__1_i_1
       (.I0(\q0_reg_n_1_[1] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[1]),
        .O(ram_reg_0_15_0_0__1_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "1" *) 
  (* ram_slice_end = "1" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__2
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__1_i_1_n_1),
        .O(ram_reg_0_15_0_0__2_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "10" *) 
  (* ram_slice_end = "10" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__20
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__19_i_1_n_1),
        .O(ram_reg_0_15_0_0__20_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "11" *) 
  (* ram_slice_end = "11" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__21
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__21_i_1_n_1),
        .O(ram_reg_0_15_0_0__21_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__21_i_1
       (.I0(\q0_reg_n_1_[11] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[11]),
        .O(ram_reg_0_15_0_0__21_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "11" *) 
  (* ram_slice_end = "11" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__22
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__21_i_1_n_1),
        .O(ram_reg_0_15_0_0__22_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "12" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__23
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__23_i_1_n_1),
        .O(ram_reg_0_15_0_0__23_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__23_i_1
       (.I0(\q0_reg_n_1_[12] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[12]),
        .O(ram_reg_0_15_0_0__23_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "12" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__24
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__23_i_1_n_1),
        .O(ram_reg_0_15_0_0__24_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "13" *) 
  (* ram_slice_end = "13" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__25
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__25_i_1_n_1),
        .O(ram_reg_0_15_0_0__25_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__25_i_1
       (.I0(\q0_reg_n_1_[13] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[13]),
        .O(ram_reg_0_15_0_0__25_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "13" *) 
  (* ram_slice_end = "13" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__26
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__25_i_1_n_1),
        .O(ram_reg_0_15_0_0__26_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "14" *) 
  (* ram_slice_end = "14" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__27
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__27_i_1_n_1),
        .O(ram_reg_0_15_0_0__27_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__27_i_1
       (.I0(\q0_reg_n_1_[14] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[14]),
        .O(ram_reg_0_15_0_0__27_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "14" *) 
  (* ram_slice_end = "14" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__28
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__27_i_1_n_1),
        .O(ram_reg_0_15_0_0__28_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "15" *) 
  (* ram_slice_end = "15" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__29
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__29_i_1_n_1),
        .O(ram_reg_0_15_0_0__29_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__29_i_1
       (.I0(\q0_reg_n_1_[15] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[15]),
        .O(ram_reg_0_15_0_0__29_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "2" *) 
  (* ram_slice_end = "2" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__3
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__3_i_1_n_1),
        .O(ram_reg_0_15_0_0__3_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "15" *) 
  (* ram_slice_end = "15" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__30
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__29_i_1_n_1),
        .O(ram_reg_0_15_0_0__30_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "16" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__31
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__31_i_1_n_1),
        .O(ram_reg_0_15_0_0__31_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__31_i_1
       (.I0(\q0_reg_n_1_[16] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[16]),
        .O(ram_reg_0_15_0_0__31_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "16" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__32
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__31_i_1_n_1),
        .O(ram_reg_0_15_0_0__32_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "17" *) 
  (* ram_slice_end = "17" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__33
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__33_i_1_n_1),
        .O(ram_reg_0_15_0_0__33_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__33_i_1
       (.I0(\q0_reg_n_1_[17] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[17]),
        .O(ram_reg_0_15_0_0__33_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "17" *) 
  (* ram_slice_end = "17" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__34
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__33_i_1_n_1),
        .O(ram_reg_0_15_0_0__34_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "18" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__35
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__35_i_1_n_1),
        .O(ram_reg_0_15_0_0__35_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__35_i_1
       (.I0(\q0_reg_n_1_[18] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[18]),
        .O(ram_reg_0_15_0_0__35_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "18" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__36
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__35_i_1_n_1),
        .O(ram_reg_0_15_0_0__36_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "19" *) 
  (* ram_slice_end = "19" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__37
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__37_i_1_n_1),
        .O(ram_reg_0_15_0_0__37_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__37_i_1
       (.I0(\q0_reg_n_1_[19] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[19]),
        .O(ram_reg_0_15_0_0__37_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "19" *) 
  (* ram_slice_end = "19" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__38
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__37_i_1_n_1),
        .O(ram_reg_0_15_0_0__38_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "20" *) 
  (* ram_slice_end = "20" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__39
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__39_i_1_n_1),
        .O(ram_reg_0_15_0_0__39_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__39_i_1
       (.I0(\q0_reg_n_1_[20] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[20]),
        .O(ram_reg_0_15_0_0__39_i_1_n_1));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__3_i_1
       (.I0(\q0_reg_n_1_[2] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[2]),
        .O(ram_reg_0_15_0_0__3_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "2" *) 
  (* ram_slice_end = "2" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__4
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__3_i_1_n_1),
        .O(ram_reg_0_15_0_0__4_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "20" *) 
  (* ram_slice_end = "20" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__40
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__39_i_1_n_1),
        .O(ram_reg_0_15_0_0__40_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "21" *) 
  (* ram_slice_end = "21" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__41
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__41_i_1_n_1),
        .O(ram_reg_0_15_0_0__41_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__41_i_1
       (.I0(\q0_reg_n_1_[21] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[21]),
        .O(ram_reg_0_15_0_0__41_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "21" *) 
  (* ram_slice_end = "21" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__42
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__41_i_1_n_1),
        .O(ram_reg_0_15_0_0__42_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "22" *) 
  (* ram_slice_end = "22" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__43
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__43_i_1_n_1),
        .O(ram_reg_0_15_0_0__43_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__43_i_1
       (.I0(\q0_reg_n_1_[22] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[22]),
        .O(ram_reg_0_15_0_0__43_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "22" *) 
  (* ram_slice_end = "22" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__44
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__43_i_1_n_1),
        .O(ram_reg_0_15_0_0__44_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "23" *) 
  (* ram_slice_end = "23" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__45
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__45_i_1_n_1),
        .O(ram_reg_0_15_0_0__45_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__45_i_1
       (.I0(\q0_reg_n_1_[23] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[23]),
        .O(ram_reg_0_15_0_0__45_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "23" *) 
  (* ram_slice_end = "23" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__46
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__45_i_1_n_1),
        .O(ram_reg_0_15_0_0__46_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "24" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__47
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__47_i_1_n_1),
        .O(ram_reg_0_15_0_0__47_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__47_i_1
       (.I0(\q0_reg_n_1_[24] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[24]),
        .O(ram_reg_0_15_0_0__47_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "24" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__48
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__47_i_1_n_1),
        .O(ram_reg_0_15_0_0__48_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "25" *) 
  (* ram_slice_end = "25" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__49
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__49_i_1_n_1),
        .O(ram_reg_0_15_0_0__49_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__49_i_1
       (.I0(\q0_reg_n_1_[25] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[25]),
        .O(ram_reg_0_15_0_0__49_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "3" *) 
  (* ram_slice_end = "3" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__5
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__5_i_1_n_1),
        .O(ram_reg_0_15_0_0__5_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "25" *) 
  (* ram_slice_end = "25" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__50
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__49_i_1_n_1),
        .O(ram_reg_0_15_0_0__50_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "26" *) 
  (* ram_slice_end = "26" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__51
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__51_i_1_n_1),
        .O(ram_reg_0_15_0_0__51_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__51_i_1
       (.I0(\q0_reg_n_1_[26] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[26]),
        .O(ram_reg_0_15_0_0__51_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "26" *) 
  (* ram_slice_end = "26" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__52
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__51_i_1_n_1),
        .O(ram_reg_0_15_0_0__52_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "27" *) 
  (* ram_slice_end = "27" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__53
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__53_i_1_n_1),
        .O(ram_reg_0_15_0_0__53_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__53_i_1
       (.I0(\q0_reg_n_1_[27] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[27]),
        .O(ram_reg_0_15_0_0__53_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "27" *) 
  (* ram_slice_end = "27" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__54
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__53_i_1_n_1),
        .O(ram_reg_0_15_0_0__54_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "28" *) 
  (* ram_slice_end = "28" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__55
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__55_i_1_n_1),
        .O(ram_reg_0_15_0_0__55_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__55_i_1
       (.I0(\q0_reg_n_1_[28] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[28]),
        .O(ram_reg_0_15_0_0__55_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "28" *) 
  (* ram_slice_end = "28" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__56
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__55_i_1_n_1),
        .O(ram_reg_0_15_0_0__56_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "29" *) 
  (* ram_slice_end = "29" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__57
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__57_i_1_n_1),
        .O(ram_reg_0_15_0_0__57_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__57_i_1
       (.I0(\q0_reg_n_1_[29] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[29]),
        .O(ram_reg_0_15_0_0__57_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "29" *) 
  (* ram_slice_end = "29" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__58
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__57_i_1_n_1),
        .O(ram_reg_0_15_0_0__58_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "30" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__59
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__59_i_1_n_1),
        .O(ram_reg_0_15_0_0__59_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__59_i_1
       (.I0(\q0_reg_n_1_[30] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[30]),
        .O(ram_reg_0_15_0_0__59_i_1_n_1));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__5_i_1
       (.I0(\q0_reg_n_1_[3] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[3]),
        .O(ram_reg_0_15_0_0__5_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "3" *) 
  (* ram_slice_end = "3" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__6
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__5_i_1_n_1),
        .O(ram_reg_0_15_0_0__6_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "30" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__60
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__59_i_1_n_1),
        .O(ram_reg_0_15_0_0__60_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "31" *) 
  (* ram_slice_end = "31" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__61
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__61_i_1_n_1),
        .O(ram_reg_0_15_0_0__61_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__61_i_1
       (.I0(\q0_reg_n_1_[31] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[31]),
        .O(ram_reg_0_15_0_0__61_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "31" *) 
  (* ram_slice_end = "31" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__62
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__61_i_1_n_1),
        .O(ram_reg_0_15_0_0__62_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "4" *) 
  (* ram_slice_end = "4" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__7
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__7_i_1_n_1),
        .O(ram_reg_0_15_0_0__7_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__7_i_1
       (.I0(\q0_reg_n_1_[4] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[4]),
        .O(ram_reg_0_15_0_0__7_i_1_n_1));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "4" *) 
  (* ram_slice_end = "4" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__8
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__7_i_1_n_1),
        .O(ram_reg_0_15_0_0__8_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg1_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "5" *) 
  (* ram_slice_end = "5" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__9
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_15_0_0__9_i_1_n_1),
        .O(ram_reg_0_15_0_0__9_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__9_i_1
       (.I0(\q0_reg_n_1_[5] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[5]),
        .O(ram_reg_0_15_0_0__9_i_1_n_1));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0_i_1
       (.I0(\q0_reg_n_1_[0] ),
        .I1(\q0_reg[31]_0 [1]),
        .I2(ram_reg_0_15_0_0__62_0[0]),
        .O(ram_reg_0_15_0_0_i_1_n_1));
  LUT3 #(
    .INIT(8'h01)) 
    ram_reg_0_15_0_0_i_8
       (.I0(\q0_reg[31]_1 [2]),
        .I1(\q0_reg[31]_1 [1]),
        .I2(\q0_reg[31]_1 [0]),
        .O(\i_0_reg_154_reg[2] ));
endmodule

(* ORIG_REF_NAME = "fir_shift_reg0_ram" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fir_shift_reg0_ram_2
   (shift_reg0_address0,
    \ap_CS_fsm_reg[2] ,
    \i_0_reg_154_reg[0] ,
    q00,
    Q,
    ram_reg_0_15_0_0__62_0,
    \q0_reg[0]_0 ,
    ram_reg_0_15_0_0__62_1,
    ram_reg_0_15_0_0__62_2,
    shift_reg0_ce0,
    ap_clk,
    \q0_reg[0]_1 ,
    \q0_reg[31]_0 ,
    \q0_reg[31]_1 );
  output [3:0]shift_reg0_address0;
  output \ap_CS_fsm_reg[2] ;
  output \i_0_reg_154_reg[0] ;
  output [31:0]q00;
  input [3:0]Q;
  input [1:0]ram_reg_0_15_0_0__62_0;
  input [4:0]\q0_reg[0]_0 ;
  input ram_reg_0_15_0_0__62_1;
  input [31:0]ram_reg_0_15_0_0__62_2;
  input shift_reg0_ce0;
  input ap_clk;
  input \q0_reg[0]_1 ;
  input \q0_reg[31]_0 ;
  input [0:0]\q0_reg[31]_1 ;

  wire [3:0]Q;
  wire \ap_CS_fsm_reg[2] ;
  wire ap_clk;
  wire [31:0]d0;
  wire \i_0_reg_154_reg[0] ;
  wire [31:0]q0;
  wire [31:0]q00;
  wire [4:0]\q0_reg[0]_0 ;
  wire \q0_reg[0]_1 ;
  wire \q0_reg[31]_0 ;
  wire [0:0]\q0_reg[31]_1 ;
  wire ram_reg_0_15_0_0__0_n_1;
  wire ram_reg_0_15_0_0__10_n_1;
  wire ram_reg_0_15_0_0__11_n_1;
  wire ram_reg_0_15_0_0__12_n_1;
  wire ram_reg_0_15_0_0__13_n_1;
  wire ram_reg_0_15_0_0__14_n_1;
  wire ram_reg_0_15_0_0__15_n_1;
  wire ram_reg_0_15_0_0__16_n_1;
  wire ram_reg_0_15_0_0__17_n_1;
  wire ram_reg_0_15_0_0__18_n_1;
  wire ram_reg_0_15_0_0__19_n_1;
  wire ram_reg_0_15_0_0__1_n_1;
  wire ram_reg_0_15_0_0__20_n_1;
  wire ram_reg_0_15_0_0__21_n_1;
  wire ram_reg_0_15_0_0__22_n_1;
  wire ram_reg_0_15_0_0__23_n_1;
  wire ram_reg_0_15_0_0__24_n_1;
  wire ram_reg_0_15_0_0__25_n_1;
  wire ram_reg_0_15_0_0__26_n_1;
  wire ram_reg_0_15_0_0__27_n_1;
  wire ram_reg_0_15_0_0__28_n_1;
  wire ram_reg_0_15_0_0__29_n_1;
  wire ram_reg_0_15_0_0__2_n_1;
  wire ram_reg_0_15_0_0__30_n_1;
  wire ram_reg_0_15_0_0__31_n_1;
  wire ram_reg_0_15_0_0__32_n_1;
  wire ram_reg_0_15_0_0__33_n_1;
  wire ram_reg_0_15_0_0__34_n_1;
  wire ram_reg_0_15_0_0__35_n_1;
  wire ram_reg_0_15_0_0__36_n_1;
  wire ram_reg_0_15_0_0__37_n_1;
  wire ram_reg_0_15_0_0__38_n_1;
  wire ram_reg_0_15_0_0__39_n_1;
  wire ram_reg_0_15_0_0__3_n_1;
  wire ram_reg_0_15_0_0__40_n_1;
  wire ram_reg_0_15_0_0__41_n_1;
  wire ram_reg_0_15_0_0__42_n_1;
  wire ram_reg_0_15_0_0__43_n_1;
  wire ram_reg_0_15_0_0__44_n_1;
  wire ram_reg_0_15_0_0__45_n_1;
  wire ram_reg_0_15_0_0__46_n_1;
  wire ram_reg_0_15_0_0__47_n_1;
  wire ram_reg_0_15_0_0__48_n_1;
  wire ram_reg_0_15_0_0__49_n_1;
  wire ram_reg_0_15_0_0__4_n_1;
  wire ram_reg_0_15_0_0__50_n_1;
  wire ram_reg_0_15_0_0__51_n_1;
  wire ram_reg_0_15_0_0__52_n_1;
  wire ram_reg_0_15_0_0__53_n_1;
  wire ram_reg_0_15_0_0__54_n_1;
  wire ram_reg_0_15_0_0__55_n_1;
  wire ram_reg_0_15_0_0__56_n_1;
  wire ram_reg_0_15_0_0__57_n_1;
  wire ram_reg_0_15_0_0__58_n_1;
  wire ram_reg_0_15_0_0__59_n_1;
  wire ram_reg_0_15_0_0__5_n_1;
  wire ram_reg_0_15_0_0__60_n_1;
  wire ram_reg_0_15_0_0__61_n_1;
  wire [1:0]ram_reg_0_15_0_0__62_0;
  wire ram_reg_0_15_0_0__62_1;
  wire [31:0]ram_reg_0_15_0_0__62_2;
  wire ram_reg_0_15_0_0__62_n_1;
  wire ram_reg_0_15_0_0__6_n_1;
  wire ram_reg_0_15_0_0__7_n_1;
  wire ram_reg_0_15_0_0__8_n_1;
  wire ram_reg_0_15_0_0__9_n_1;
  wire ram_reg_0_15_0_0_n_1;
  wire [3:0]shift_reg0_address0;
  wire shift_reg0_ce0;

  LUT5 #(
    .INIT(32'h00000001)) 
    \ap_CS_fsm[2]_i_2 
       (.I0(\q0_reg[0]_0 [0]),
        .I1(\q0_reg[0]_0 [1]),
        .I2(\q0_reg[0]_0 [2]),
        .I3(\q0_reg[0]_0 [4]),
        .I4(\q0_reg[0]_0 [3]),
        .O(\i_0_reg_154_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[0]_i_1__0 
       (.I0(ram_reg_0_15_0_0__0_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0_n_1),
        .O(q00[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[10]_i_1__0 
       (.I0(ram_reg_0_15_0_0__20_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__19_n_1),
        .O(q00[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[11]_i_1__0 
       (.I0(ram_reg_0_15_0_0__22_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__21_n_1),
        .O(q00[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[12]_i_1__0 
       (.I0(ram_reg_0_15_0_0__24_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__23_n_1),
        .O(q00[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[13]_i_1__0 
       (.I0(ram_reg_0_15_0_0__26_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__25_n_1),
        .O(q00[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[14]_i_1__0 
       (.I0(ram_reg_0_15_0_0__28_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__27_n_1),
        .O(q00[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[15]_i_1__0 
       (.I0(ram_reg_0_15_0_0__30_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__29_n_1),
        .O(q00[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[16]_i_1__0 
       (.I0(ram_reg_0_15_0_0__32_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__31_n_1),
        .O(q00[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[17]_i_1__0 
       (.I0(ram_reg_0_15_0_0__34_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__33_n_1),
        .O(q00[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[18]_i_1__0 
       (.I0(ram_reg_0_15_0_0__36_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__35_n_1),
        .O(q00[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[19]_i_1__0 
       (.I0(ram_reg_0_15_0_0__38_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__37_n_1),
        .O(q00[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[1]_i_1__0 
       (.I0(ram_reg_0_15_0_0__2_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__1_n_1),
        .O(q00[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[20]_i_1__0 
       (.I0(ram_reg_0_15_0_0__40_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__39_n_1),
        .O(q00[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[21]_i_1__0 
       (.I0(ram_reg_0_15_0_0__42_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__41_n_1),
        .O(q00[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[22]_i_1__0 
       (.I0(ram_reg_0_15_0_0__44_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__43_n_1),
        .O(q00[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[23]_i_1__0 
       (.I0(ram_reg_0_15_0_0__46_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__45_n_1),
        .O(q00[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[24]_i_1__0 
       (.I0(ram_reg_0_15_0_0__48_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__47_n_1),
        .O(q00[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[25]_i_1__0 
       (.I0(ram_reg_0_15_0_0__50_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__49_n_1),
        .O(q00[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[26]_i_1__0 
       (.I0(ram_reg_0_15_0_0__52_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__51_n_1),
        .O(q00[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[27]_i_1__0 
       (.I0(ram_reg_0_15_0_0__54_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__53_n_1),
        .O(q00[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[28]_i_1__0 
       (.I0(ram_reg_0_15_0_0__56_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__55_n_1),
        .O(q00[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[29]_i_1__0 
       (.I0(ram_reg_0_15_0_0__58_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__57_n_1),
        .O(q00[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[2]_i_1__0 
       (.I0(ram_reg_0_15_0_0__4_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__3_n_1),
        .O(q00[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[30]_i_1__0 
       (.I0(ram_reg_0_15_0_0__60_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__59_n_1),
        .O(q00[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[31]_i_1 
       (.I0(ram_reg_0_15_0_0__62_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__61_n_1),
        .O(q00[31]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[3]_i_1__0 
       (.I0(ram_reg_0_15_0_0__6_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__5_n_1),
        .O(q00[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[4]_i_1__0 
       (.I0(ram_reg_0_15_0_0__8_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__7_n_1),
        .O(q00[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[5]_i_1__0 
       (.I0(ram_reg_0_15_0_0__10_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__9_n_1),
        .O(q00[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[6]_i_1__0 
       (.I0(ram_reg_0_15_0_0__12_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__11_n_1),
        .O(q00[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[7]_i_1__0 
       (.I0(ram_reg_0_15_0_0__14_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__13_n_1),
        .O(q00[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[8]_i_1__0 
       (.I0(ram_reg_0_15_0_0__16_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__15_n_1),
        .O(q00[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[9]_i_1__0 
       (.I0(ram_reg_0_15_0_0__18_n_1),
        .I1(\q0_reg[31]_1 ),
        .I2(ram_reg_0_15_0_0__17_n_1),
        .O(q00[9]));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[0]),
        .Q(q0[0]),
        .R(1'b0));
  FDRE \q0_reg[10] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[10]),
        .Q(q0[10]),
        .R(1'b0));
  FDRE \q0_reg[11] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[11]),
        .Q(q0[11]),
        .R(1'b0));
  FDRE \q0_reg[12] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[12]),
        .Q(q0[12]),
        .R(1'b0));
  FDRE \q0_reg[13] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[13]),
        .Q(q0[13]),
        .R(1'b0));
  FDRE \q0_reg[14] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[14]),
        .Q(q0[14]),
        .R(1'b0));
  FDRE \q0_reg[15] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[15]),
        .Q(q0[15]),
        .R(1'b0));
  FDRE \q0_reg[16] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[16]),
        .Q(q0[16]),
        .R(1'b0));
  FDRE \q0_reg[17] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[17]),
        .Q(q0[17]),
        .R(1'b0));
  FDRE \q0_reg[18] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[18]),
        .Q(q0[18]),
        .R(1'b0));
  FDRE \q0_reg[19] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[19]),
        .Q(q0[19]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[1]),
        .Q(q0[1]),
        .R(1'b0));
  FDRE \q0_reg[20] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[20]),
        .Q(q0[20]),
        .R(1'b0));
  FDRE \q0_reg[21] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[21]),
        .Q(q0[21]),
        .R(1'b0));
  FDRE \q0_reg[22] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[22]),
        .Q(q0[22]),
        .R(1'b0));
  FDRE \q0_reg[23] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[23]),
        .Q(q0[23]),
        .R(1'b0));
  FDRE \q0_reg[24] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[24]),
        .Q(q0[24]),
        .R(1'b0));
  FDRE \q0_reg[25] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[25]),
        .Q(q0[25]),
        .R(1'b0));
  FDRE \q0_reg[26] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[26]),
        .Q(q0[26]),
        .R(1'b0));
  FDRE \q0_reg[27] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[27]),
        .Q(q0[27]),
        .R(1'b0));
  FDRE \q0_reg[28] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[28]),
        .Q(q0[28]),
        .R(1'b0));
  FDRE \q0_reg[29] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[29]),
        .Q(q0[29]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[2]),
        .Q(q0[2]),
        .R(1'b0));
  FDRE \q0_reg[30] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[30]),
        .Q(q0[30]),
        .R(1'b0));
  FDRE \q0_reg[31] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[31]),
        .Q(q0[31]),
        .R(1'b0));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[3]),
        .Q(q0[3]),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[4]),
        .Q(q0[4]),
        .R(1'b0));
  FDRE \q0_reg[5] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[5]),
        .Q(q0[5]),
        .R(1'b0));
  FDRE \q0_reg[6] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[6]),
        .Q(q0[6]),
        .R(1'b0));
  FDRE \q0_reg[7] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[7]),
        .Q(q0[7]),
        .R(1'b0));
  FDRE \q0_reg[8] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[8]),
        .Q(q0[8]),
        .R(1'b0));
  FDRE \q0_reg[9] 
       (.C(ap_clk),
        .CE(shift_reg0_ce0),
        .D(q00[9]),
        .Q(q0[9]),
        .R(1'b0));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "0" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[0]),
        .O(ram_reg_0_15_0_0_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "0" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[0]),
        .O(ram_reg_0_15_0_0__0_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "1" *) 
  (* ram_slice_end = "1" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__1
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[1]),
        .O(ram_reg_0_15_0_0__1_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "5" *) 
  (* ram_slice_end = "5" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__10
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[5]),
        .O(ram_reg_0_15_0_0__10_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "6" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__11
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[6]),
        .O(ram_reg_0_15_0_0__11_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__11_i_1__0
       (.I0(q0[6]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[6]),
        .O(d0[6]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "6" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__12
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[6]),
        .O(ram_reg_0_15_0_0__12_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "7" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__13
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[7]),
        .O(ram_reg_0_15_0_0__13_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__13_i_1__0
       (.I0(q0[7]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[7]),
        .O(d0[7]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "7" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__14
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[7]),
        .O(ram_reg_0_15_0_0__14_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "8" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__15
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[8]),
        .O(ram_reg_0_15_0_0__15_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__15_i_1__0
       (.I0(q0[8]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[8]),
        .O(d0[8]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "8" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__16
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[8]),
        .O(ram_reg_0_15_0_0__16_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "9" *) 
  (* ram_slice_end = "9" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__17
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[9]),
        .O(ram_reg_0_15_0_0__17_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__17_i_1__0
       (.I0(q0[9]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[9]),
        .O(d0[9]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "9" *) 
  (* ram_slice_end = "9" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__18
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[9]),
        .O(ram_reg_0_15_0_0__18_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "10" *) 
  (* ram_slice_end = "10" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__19
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[10]),
        .O(ram_reg_0_15_0_0__19_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__19_i_1__0
       (.I0(q0[10]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[10]),
        .O(d0[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__1_i_1__0
       (.I0(q0[1]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[1]),
        .O(d0[1]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "1" *) 
  (* ram_slice_end = "1" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__2
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[1]),
        .O(ram_reg_0_15_0_0__2_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "10" *) 
  (* ram_slice_end = "10" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__20
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[10]),
        .O(ram_reg_0_15_0_0__20_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "11" *) 
  (* ram_slice_end = "11" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__21
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[11]),
        .O(ram_reg_0_15_0_0__21_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__21_i_1__0
       (.I0(q0[11]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[11]),
        .O(d0[11]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "11" *) 
  (* ram_slice_end = "11" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__22
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[11]),
        .O(ram_reg_0_15_0_0__22_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "12" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__23
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[12]),
        .O(ram_reg_0_15_0_0__23_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__23_i_1__0
       (.I0(q0[12]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[12]),
        .O(d0[12]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "12" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__24
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[12]),
        .O(ram_reg_0_15_0_0__24_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "13" *) 
  (* ram_slice_end = "13" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__25
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[13]),
        .O(ram_reg_0_15_0_0__25_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__25_i_1__0
       (.I0(q0[13]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[13]),
        .O(d0[13]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "13" *) 
  (* ram_slice_end = "13" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__26
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[13]),
        .O(ram_reg_0_15_0_0__26_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "14" *) 
  (* ram_slice_end = "14" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__27
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[14]),
        .O(ram_reg_0_15_0_0__27_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__27_i_1__0
       (.I0(q0[14]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[14]),
        .O(d0[14]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "14" *) 
  (* ram_slice_end = "14" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__28
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[14]),
        .O(ram_reg_0_15_0_0__28_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "15" *) 
  (* ram_slice_end = "15" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__29
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[15]),
        .O(ram_reg_0_15_0_0__29_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__29_i_1__0
       (.I0(q0[15]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[15]),
        .O(d0[15]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "2" *) 
  (* ram_slice_end = "2" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__3
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[2]),
        .O(ram_reg_0_15_0_0__3_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "15" *) 
  (* ram_slice_end = "15" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__30
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[15]),
        .O(ram_reg_0_15_0_0__30_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "16" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__31
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[16]),
        .O(ram_reg_0_15_0_0__31_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__31_i_1__0
       (.I0(q0[16]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[16]),
        .O(d0[16]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "16" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__32
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[16]),
        .O(ram_reg_0_15_0_0__32_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "17" *) 
  (* ram_slice_end = "17" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__33
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[17]),
        .O(ram_reg_0_15_0_0__33_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__33_i_1__0
       (.I0(q0[17]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[17]),
        .O(d0[17]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "17" *) 
  (* ram_slice_end = "17" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__34
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[17]),
        .O(ram_reg_0_15_0_0__34_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "18" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__35
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[18]),
        .O(ram_reg_0_15_0_0__35_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__35_i_1__0
       (.I0(q0[18]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[18]),
        .O(d0[18]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "18" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__36
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[18]),
        .O(ram_reg_0_15_0_0__36_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "19" *) 
  (* ram_slice_end = "19" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__37
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[19]),
        .O(ram_reg_0_15_0_0__37_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__37_i_1__0
       (.I0(q0[19]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[19]),
        .O(d0[19]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "19" *) 
  (* ram_slice_end = "19" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__38
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[19]),
        .O(ram_reg_0_15_0_0__38_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "20" *) 
  (* ram_slice_end = "20" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__39
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[20]),
        .O(ram_reg_0_15_0_0__39_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__39_i_1__0
       (.I0(q0[20]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[20]),
        .O(d0[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__3_i_1__0
       (.I0(q0[2]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[2]),
        .O(d0[2]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "2" *) 
  (* ram_slice_end = "2" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__4
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[2]),
        .O(ram_reg_0_15_0_0__4_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "20" *) 
  (* ram_slice_end = "20" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__40
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[20]),
        .O(ram_reg_0_15_0_0__40_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "21" *) 
  (* ram_slice_end = "21" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__41
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[21]),
        .O(ram_reg_0_15_0_0__41_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__41_i_1__0
       (.I0(q0[21]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[21]),
        .O(d0[21]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "21" *) 
  (* ram_slice_end = "21" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__42
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[21]),
        .O(ram_reg_0_15_0_0__42_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "22" *) 
  (* ram_slice_end = "22" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__43
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[22]),
        .O(ram_reg_0_15_0_0__43_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__43_i_1__0
       (.I0(q0[22]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[22]),
        .O(d0[22]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "22" *) 
  (* ram_slice_end = "22" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__44
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[22]),
        .O(ram_reg_0_15_0_0__44_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "23" *) 
  (* ram_slice_end = "23" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__45
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[23]),
        .O(ram_reg_0_15_0_0__45_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__45_i_1__0
       (.I0(q0[23]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[23]),
        .O(d0[23]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "23" *) 
  (* ram_slice_end = "23" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__46
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[23]),
        .O(ram_reg_0_15_0_0__46_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "24" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__47
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[24]),
        .O(ram_reg_0_15_0_0__47_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__47_i_1__0
       (.I0(q0[24]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[24]),
        .O(d0[24]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "24" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__48
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[24]),
        .O(ram_reg_0_15_0_0__48_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "25" *) 
  (* ram_slice_end = "25" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__49
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[25]),
        .O(ram_reg_0_15_0_0__49_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__49_i_1__0
       (.I0(q0[25]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[25]),
        .O(d0[25]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "3" *) 
  (* ram_slice_end = "3" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__5
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[3]),
        .O(ram_reg_0_15_0_0__5_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "25" *) 
  (* ram_slice_end = "25" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__50
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[25]),
        .O(ram_reg_0_15_0_0__50_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "26" *) 
  (* ram_slice_end = "26" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__51
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[26]),
        .O(ram_reg_0_15_0_0__51_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__51_i_1__0
       (.I0(q0[26]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[26]),
        .O(d0[26]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "26" *) 
  (* ram_slice_end = "26" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__52
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[26]),
        .O(ram_reg_0_15_0_0__52_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "27" *) 
  (* ram_slice_end = "27" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__53
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[27]),
        .O(ram_reg_0_15_0_0__53_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__53_i_1__0
       (.I0(q0[27]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[27]),
        .O(d0[27]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "27" *) 
  (* ram_slice_end = "27" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__54
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[27]),
        .O(ram_reg_0_15_0_0__54_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "28" *) 
  (* ram_slice_end = "28" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__55
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[28]),
        .O(ram_reg_0_15_0_0__55_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__55_i_1__0
       (.I0(q0[28]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[28]),
        .O(d0[28]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "28" *) 
  (* ram_slice_end = "28" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__56
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[28]),
        .O(ram_reg_0_15_0_0__56_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "29" *) 
  (* ram_slice_end = "29" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__57
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[29]),
        .O(ram_reg_0_15_0_0__57_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__57_i_1__0
       (.I0(q0[29]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[29]),
        .O(d0[29]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "29" *) 
  (* ram_slice_end = "29" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__58
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[29]),
        .O(ram_reg_0_15_0_0__58_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "30" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__59
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[30]),
        .O(ram_reg_0_15_0_0__59_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__59_i_1__0
       (.I0(q0[30]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[30]),
        .O(d0[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__5_i_1__0
       (.I0(q0[3]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[3]),
        .O(d0[3]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "3" *) 
  (* ram_slice_end = "3" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__6
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[3]),
        .O(ram_reg_0_15_0_0__6_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "30" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__60
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[30]),
        .O(ram_reg_0_15_0_0__60_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "31" *) 
  (* ram_slice_end = "31" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__61
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[31]),
        .O(ram_reg_0_15_0_0__61_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__61_i_1__0
       (.I0(q0[31]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[31]),
        .O(d0[31]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "31" *) 
  (* ram_slice_end = "31" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__62
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[31]),
        .O(ram_reg_0_15_0_0__62_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "4" *) 
  (* ram_slice_end = "4" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__7
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[4]),
        .O(ram_reg_0_15_0_0__7_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__7_i_1__0
       (.I0(q0[4]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[4]),
        .O(d0[4]));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "16" *) 
  (* ram_addr_end = "30" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "4" *) 
  (* ram_slice_end = "4" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__8
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[4]),
        .O(ram_reg_0_15_0_0__8_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[31]_0 ));
  (* RTL_RAM_BITS = "992" *) 
  (* RTL_RAM_NAME = "shift_reg0_U/fir_shift_reg0_ram_U/ram" *) 
  (* RTL_RAM_TYPE = "RAM_SP" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "5" *) 
  (* ram_slice_end = "5" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__9
       (.A0(shift_reg0_address0[0]),
        .A1(shift_reg0_address0[1]),
        .A2(shift_reg0_address0[2]),
        .A3(shift_reg0_address0[3]),
        .A4(1'b0),
        .D(d0[5]),
        .O(ram_reg_0_15_0_0__9_n_1),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0__9_i_1__0
       (.I0(q0[5]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[5]),
        .O(d0[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_15_0_0_i_1__0
       (.I0(q0[0]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(ram_reg_0_15_0_0__62_2[0]),
        .O(d0[0]));
  LUT4 #(
    .INIT(16'h88B8)) 
    ram_reg_0_15_0_0_i_3
       (.I0(Q[0]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(\ap_CS_fsm_reg[2] ),
        .I3(\q0_reg[0]_0 [0]),
        .O(shift_reg0_address0[0]));
  LUT5 #(
    .INIT(32'hB88888B8)) 
    ram_reg_0_15_0_0_i_4
       (.I0(Q[1]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(\ap_CS_fsm_reg[2] ),
        .I3(\q0_reg[0]_0 [1]),
        .I4(\q0_reg[0]_0 [0]),
        .O(shift_reg0_address0[1]));
  LUT6 #(
    .INIT(64'hB8B8B888888888B8)) 
    ram_reg_0_15_0_0_i_5
       (.I0(Q[2]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(\ap_CS_fsm_reg[2] ),
        .I3(\q0_reg[0]_0 [0]),
        .I4(\q0_reg[0]_0 [1]),
        .I5(\q0_reg[0]_0 [2]),
        .O(shift_reg0_address0[2]));
  LUT6 #(
    .INIT(64'h8888BBBBB8BB8888)) 
    ram_reg_0_15_0_0_i_6
       (.I0(Q[3]),
        .I1(ram_reg_0_15_0_0__62_0[1]),
        .I2(\q0_reg[0]_0 [4]),
        .I3(ram_reg_0_15_0_0__62_0[0]),
        .I4(ram_reg_0_15_0_0__62_1),
        .I5(\q0_reg[0]_0 [3]),
        .O(shift_reg0_address0[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    ram_reg_0_15_0_0_i_7
       (.I0(ram_reg_0_15_0_0__62_0[0]),
        .I1(\q0_reg[0]_0 [3]),
        .I2(\q0_reg[0]_0 [4]),
        .I3(\q0_reg[0]_0 [2]),
        .I4(\q0_reg[0]_0 [1]),
        .I5(\q0_reg[0]_0 [0]),
        .O(\ap_CS_fsm_reg[2] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ibuf
   (coeffs_ce0,
    \ireg_reg[32]_0 ,
    ap_rst_n_0,
    \ireg_reg[32]_1 ,
    count,
    D,
    \ap_CS_fsm_reg[4] ,
    \ap_CS_fsm_reg[3] ,
    \ap_CS_fsm_reg[3]_0 ,
    shift_reg0_ce0,
    Q,
    ap_rst_n,
    \q0_reg[0] ,
    \count_reg[0] ,
    y_TREADY,
    \count_reg[0]_0 ,
    \trunc_ln1_reg_329_reg[0] ,
    x_TVALID_int,
    \odata_reg[31] ,
    O,
    \ireg_reg[19]_0 ,
    \ireg_reg[15]_0 ,
    \ireg_reg[11]_0 ,
    \ireg_reg[7]_0 ,
    \ireg_reg[3]_0 ,
    \ireg_reg[0]_0 ,
    shift_reg0_address0,
    SR,
    ap_clk);
  output coeffs_ce0;
  output [0:0]\ireg_reg[32]_0 ;
  output ap_rst_n_0;
  output \ireg_reg[32]_1 ;
  output [0:0]count;
  output [2:0]D;
  output [24:0]\ap_CS_fsm_reg[4] ;
  output \ap_CS_fsm_reg[3] ;
  output \ap_CS_fsm_reg[3]_0 ;
  output shift_reg0_ce0;
  input [4:0]Q;
  input ap_rst_n;
  input \q0_reg[0] ;
  input \count_reg[0] ;
  input y_TREADY;
  input \count_reg[0]_0 ;
  input \trunc_ln1_reg_329_reg[0] ;
  input x_TVALID_int;
  input [23:0]\odata_reg[31] ;
  input [3:0]O;
  input [3:0]\ireg_reg[19]_0 ;
  input [3:0]\ireg_reg[15]_0 ;
  input [3:0]\ireg_reg[11]_0 ;
  input [3:0]\ireg_reg[7]_0 ;
  input [3:0]\ireg_reg[3]_0 ;
  input [0:0]\ireg_reg[0]_0 ;
  input [0:0]shift_reg0_address0;
  input [0:0]SR;
  input ap_clk;

  wire [2:0]D;
  wire [3:0]O;
  wire [4:0]Q;
  wire [0:0]SR;
  wire \ap_CS_fsm[0]_i_2_n_1 ;
  wire \ap_CS_fsm[5]_i_2_n_1 ;
  wire \ap_CS_fsm_reg[3] ;
  wire \ap_CS_fsm_reg[3]_0 ;
  wire [24:0]\ap_CS_fsm_reg[4] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_0;
  wire coeffs_ce0;
  wire [0:0]count;
  wire \count[0]_i_2_n_1 ;
  wire \count_reg[0] ;
  wire \count_reg[0]_0 ;
  wire ireg01_out;
  wire [0:0]\ireg_reg[0]_0 ;
  wire [3:0]\ireg_reg[11]_0 ;
  wire [3:0]\ireg_reg[15]_0 ;
  wire [3:0]\ireg_reg[19]_0 ;
  wire [0:0]\ireg_reg[32]_0 ;
  wire \ireg_reg[32]_1 ;
  wire [3:0]\ireg_reg[3]_0 ;
  wire [3:0]\ireg_reg[7]_0 ;
  wire \ireg_reg_n_1_[0] ;
  wire \ireg_reg_n_1_[10] ;
  wire \ireg_reg_n_1_[11] ;
  wire \ireg_reg_n_1_[12] ;
  wire \ireg_reg_n_1_[13] ;
  wire \ireg_reg_n_1_[14] ;
  wire \ireg_reg_n_1_[15] ;
  wire \ireg_reg_n_1_[16] ;
  wire \ireg_reg_n_1_[17] ;
  wire \ireg_reg_n_1_[18] ;
  wire \ireg_reg_n_1_[19] ;
  wire \ireg_reg_n_1_[1] ;
  wire \ireg_reg_n_1_[20] ;
  wire \ireg_reg_n_1_[21] ;
  wire \ireg_reg_n_1_[22] ;
  wire \ireg_reg_n_1_[2] ;
  wire \ireg_reg_n_1_[31] ;
  wire \ireg_reg_n_1_[3] ;
  wire \ireg_reg_n_1_[4] ;
  wire \ireg_reg_n_1_[5] ;
  wire \ireg_reg_n_1_[6] ;
  wire \ireg_reg_n_1_[7] ;
  wire \ireg_reg_n_1_[8] ;
  wire \ireg_reg_n_1_[9] ;
  wire [23:0]\odata_reg[31] ;
  wire \q0_reg[0] ;
  wire [0:0]shift_reg0_address0;
  wire shift_reg0_ce0;
  wire \trunc_ln1_reg_329_reg[0] ;
  wire x_TVALID_int;
  wire [31:0]y_TDATA_int;
  wire y_TREADY;
  wire y_TVALID_int;

  LUT3 #(
    .INIT(8'hBA)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(\ap_CS_fsm[0]_i_2_n_1 ),
        .I1(x_TVALID_int),
        .I2(Q[0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000D50000000000)) 
    \ap_CS_fsm[0]_i_2 
       (.I0(\count_reg[0]_0 ),
        .I1(\count_reg[0] ),
        .I2(y_TREADY),
        .I3(ap_rst_n),
        .I4(\ireg_reg[32]_0 ),
        .I5(Q[4]),
        .O(\ap_CS_fsm[0]_i_2_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(\ireg_reg[32]_1 ),
        .I1(\ireg_reg[32]_0 ),
        .I2(Q[3]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hAEEEFFFFAAAA0000)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(Q[3]),
        .I1(\count_reg[0]_0 ),
        .I2(\count_reg[0] ),
        .I3(y_TREADY),
        .I4(\ap_CS_fsm[5]_i_2_n_1 ),
        .I5(Q[4]),
        .O(D[2]));
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[5]_i_2 
       (.I0(ap_rst_n),
        .I1(\ireg_reg[32]_0 ),
        .O(\ap_CS_fsm[5]_i_2_n_1 ));
  LUT6 #(
    .INIT(64'hAAA2AAAA88808880)) 
    \count[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\count_reg[0] ),
        .I2(\count[0]_i_2_n_1 ),
        .I3(\ireg_reg[32]_1 ),
        .I4(y_TREADY),
        .I5(\count_reg[0]_0 ),
        .O(ap_rst_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \count[0]_i_2 
       (.I0(Q[3]),
        .I1(\ireg_reg[32]_0 ),
        .I2(ap_rst_n),
        .O(\count[0]_i_2_n_1 ));
  LUT6 #(
    .INIT(64'hDDDFDFDFDDDDDDDD)) 
    \count[1]_i_1 
       (.I0(\count_reg[0]_0 ),
        .I1(y_TREADY),
        .I2(\ireg_reg[32]_1 ),
        .I3(\ap_CS_fsm[5]_i_2_n_1 ),
        .I4(Q[3]),
        .I5(\count_reg[0] ),
        .O(count));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[0]_i_1 
       (.I0(\odata_reg[31] [0]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [0]),
        .O(y_TDATA_int[0]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[10]_i_1 
       (.I0(\odata_reg[31] [10]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [2]),
        .O(y_TDATA_int[10]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[11]_i_1 
       (.I0(\odata_reg[31] [11]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [3]),
        .O(y_TDATA_int[11]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[12]_i_1 
       (.I0(\odata_reg[31] [12]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [0]),
        .O(y_TDATA_int[12]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[13]_i_1 
       (.I0(\odata_reg[31] [13]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [1]),
        .O(y_TDATA_int[13]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[14]_i_1 
       (.I0(\odata_reg[31] [14]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [2]),
        .O(y_TDATA_int[14]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[15]_i_1 
       (.I0(\odata_reg[31] [15]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [3]),
        .O(y_TDATA_int[15]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[16]_i_1 
       (.I0(\odata_reg[31] [16]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [0]),
        .O(y_TDATA_int[16]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[17]_i_1 
       (.I0(\odata_reg[31] [17]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [1]),
        .O(y_TDATA_int[17]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[18]_i_1 
       (.I0(\odata_reg[31] [18]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [2]),
        .O(y_TDATA_int[18]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[19]_i_1 
       (.I0(\odata_reg[31] [19]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [3]),
        .O(y_TDATA_int[19]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[1]_i_1 
       (.I0(\odata_reg[31] [1]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [1]),
        .O(y_TDATA_int[1]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[20]_i_1 
       (.I0(\odata_reg[31] [20]),
        .I1(Q[3]),
        .I2(O[0]),
        .O(y_TDATA_int[20]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[21]_i_1 
       (.I0(\odata_reg[31] [21]),
        .I1(Q[3]),
        .I2(O[1]),
        .O(y_TDATA_int[21]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[22]_i_1 
       (.I0(\odata_reg[31] [22]),
        .I1(Q[3]),
        .I2(O[2]),
        .O(y_TDATA_int[22]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[2]_i_1 
       (.I0(\odata_reg[31] [2]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [2]),
        .O(y_TDATA_int[2]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[31]_i_1 
       (.I0(\odata_reg[31] [23]),
        .I1(Q[3]),
        .I2(O[3]),
        .O(y_TDATA_int[31]));
  LUT3 #(
    .INIT(8'h04)) 
    \ireg[32]_i_2__0 
       (.I0(\ireg_reg[32]_0 ),
        .I1(\ireg_reg[0]_0 ),
        .I2(y_TREADY),
        .O(ireg01_out));
  LUT2 #(
    .INIT(4'hE)) 
    \ireg[32]_i_3 
       (.I0(\ireg_reg[32]_1 ),
        .I1(Q[3]),
        .O(y_TVALID_int));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[3]_i_1 
       (.I0(\odata_reg[31] [3]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [3]),
        .O(y_TDATA_int[3]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[4]_i_1 
       (.I0(\odata_reg[31] [4]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [0]),
        .O(y_TDATA_int[4]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[5]_i_1 
       (.I0(\odata_reg[31] [5]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [1]),
        .O(y_TDATA_int[5]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[6]_i_1 
       (.I0(\odata_reg[31] [6]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [2]),
        .O(y_TDATA_int[6]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[7]_i_1 
       (.I0(\odata_reg[31] [7]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [3]),
        .O(y_TDATA_int[7]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[8]_i_1 
       (.I0(\odata_reg[31] [8]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [0]),
        .O(y_TDATA_int[8]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ireg[9]_i_1 
       (.I0(\odata_reg[31] [9]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [1]),
        .O(y_TDATA_int[9]));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[0] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[0]),
        .Q(\ireg_reg_n_1_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[10] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[10]),
        .Q(\ireg_reg_n_1_[10] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[11] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[11]),
        .Q(\ireg_reg_n_1_[11] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[12] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[12]),
        .Q(\ireg_reg_n_1_[12] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[13] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[13]),
        .Q(\ireg_reg_n_1_[13] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[14] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[14]),
        .Q(\ireg_reg_n_1_[14] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[15] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[15]),
        .Q(\ireg_reg_n_1_[15] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[16] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[16]),
        .Q(\ireg_reg_n_1_[16] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[17] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[17]),
        .Q(\ireg_reg_n_1_[17] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[18] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[18]),
        .Q(\ireg_reg_n_1_[18] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[19] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[19]),
        .Q(\ireg_reg_n_1_[19] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[1] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[1]),
        .Q(\ireg_reg_n_1_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[20] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[20]),
        .Q(\ireg_reg_n_1_[20] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[21] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[21]),
        .Q(\ireg_reg_n_1_[21] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[22] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[22]),
        .Q(\ireg_reg_n_1_[22] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[2] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[2]),
        .Q(\ireg_reg_n_1_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[31] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[31]),
        .Q(\ireg_reg_n_1_[31] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[32] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TVALID_int),
        .Q(\ireg_reg[32]_0 ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[3] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[3]),
        .Q(\ireg_reg_n_1_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[4] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[4]),
        .Q(\ireg_reg_n_1_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[5] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[5]),
        .Q(\ireg_reg_n_1_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[6] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[6]),
        .Q(\ireg_reg_n_1_[6] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[7] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[7]),
        .Q(\ireg_reg_n_1_[7] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[8] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[8]),
        .Q(\ireg_reg_n_1_[8] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[9] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(y_TDATA_int[9]),
        .Q(\ireg_reg_n_1_[9] ),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[0]_i_1 
       (.I0(\odata_reg[31] [0]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [0]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[0] ),
        .O(\ap_CS_fsm_reg[4] [0]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[10]_i_1 
       (.I0(\odata_reg[31] [10]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [2]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[10] ),
        .O(\ap_CS_fsm_reg[4] [10]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[11]_i_1 
       (.I0(\odata_reg[31] [11]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [3]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[11] ),
        .O(\ap_CS_fsm_reg[4] [11]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[12]_i_1 
       (.I0(\odata_reg[31] [12]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [0]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[12] ),
        .O(\ap_CS_fsm_reg[4] [12]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[13]_i_1 
       (.I0(\odata_reg[31] [13]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [1]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[13] ),
        .O(\ap_CS_fsm_reg[4] [13]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[14]_i_1 
       (.I0(\odata_reg[31] [14]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [2]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[14] ),
        .O(\ap_CS_fsm_reg[4] [14]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[15]_i_1 
       (.I0(\odata_reg[31] [15]),
        .I1(Q[3]),
        .I2(\ireg_reg[15]_0 [3]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[15] ),
        .O(\ap_CS_fsm_reg[4] [15]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[16]_i_1 
       (.I0(\odata_reg[31] [16]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [0]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[16] ),
        .O(\ap_CS_fsm_reg[4] [16]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[17]_i_1 
       (.I0(\odata_reg[31] [17]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [1]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[17] ),
        .O(\ap_CS_fsm_reg[4] [17]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[18]_i_1 
       (.I0(\odata_reg[31] [18]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [2]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[18] ),
        .O(\ap_CS_fsm_reg[4] [18]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[19]_i_1 
       (.I0(\odata_reg[31] [19]),
        .I1(Q[3]),
        .I2(\ireg_reg[19]_0 [3]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[19] ),
        .O(\ap_CS_fsm_reg[4] [19]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[1]_i_1 
       (.I0(\odata_reg[31] [1]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [1]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[1] ),
        .O(\ap_CS_fsm_reg[4] [1]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[20]_i_1 
       (.I0(\odata_reg[31] [20]),
        .I1(Q[3]),
        .I2(O[0]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[20] ),
        .O(\ap_CS_fsm_reg[4] [20]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[21]_i_1 
       (.I0(\odata_reg[31] [21]),
        .I1(Q[3]),
        .I2(O[1]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[21] ),
        .O(\ap_CS_fsm_reg[4] [21]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[22]_i_1 
       (.I0(\odata_reg[31] [22]),
        .I1(Q[3]),
        .I2(O[2]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[22] ),
        .O(\ap_CS_fsm_reg[4] [22]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[2]_i_1 
       (.I0(\odata_reg[31] [2]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [2]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[2] ),
        .O(\ap_CS_fsm_reg[4] [2]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[31]_i_3 
       (.I0(\odata_reg[31] [23]),
        .I1(Q[3]),
        .I2(O[3]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[31] ),
        .O(\ap_CS_fsm_reg[4] [23]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \odata[32]_i_1 
       (.I0(Q[3]),
        .I1(\ireg_reg[32]_0 ),
        .I2(\ireg_reg[32]_1 ),
        .O(\ap_CS_fsm_reg[4] [24]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[3]_i_1 
       (.I0(\odata_reg[31] [3]),
        .I1(Q[3]),
        .I2(\ireg_reg[3]_0 [3]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[3] ),
        .O(\ap_CS_fsm_reg[4] [3]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[4]_i_1 
       (.I0(\odata_reg[31] [4]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [0]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[4] ),
        .O(\ap_CS_fsm_reg[4] [4]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[5]_i_1 
       (.I0(\odata_reg[31] [5]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [1]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[5] ),
        .O(\ap_CS_fsm_reg[4] [5]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[6]_i_1 
       (.I0(\odata_reg[31] [6]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [2]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[6] ),
        .O(\ap_CS_fsm_reg[4] [6]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[7]_i_1 
       (.I0(\odata_reg[31] [7]),
        .I1(Q[3]),
        .I2(\ireg_reg[7]_0 [3]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[7] ),
        .O(\ap_CS_fsm_reg[4] [7]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[8]_i_1 
       (.I0(\odata_reg[31] [8]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [0]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[8] ),
        .O(\ap_CS_fsm_reg[4] [8]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'hFFB800B8)) 
    \odata[9]_i_1 
       (.I0(\odata_reg[31] [9]),
        .I1(Q[3]),
        .I2(\ireg_reg[11]_0 [1]),
        .I3(\ireg_reg[32]_0 ),
        .I4(\ireg_reg_n_1_[9] ),
        .O(\ap_CS_fsm_reg[4] [9]));
  LUT5 #(
    .INIT(32'hFFFF08AA)) 
    \q0[31]_i_1__0 
       (.I0(Q[1]),
        .I1(ap_rst_n),
        .I2(\ireg_reg[32]_0 ),
        .I3(\q0_reg[0] ),
        .I4(Q[2]),
        .O(shift_reg0_ce0));
  LUT4 #(
    .INIT(16'h08AA)) 
    q0_reg_0_i_1
       (.I0(Q[1]),
        .I1(ap_rst_n),
        .I2(\ireg_reg[32]_0 ),
        .I3(\q0_reg[0] ),
        .O(coeffs_ce0));
  LUT3 #(
    .INIT(8'hE0)) 
    ram_reg_0_15_0_0__0_i_1
       (.I0(\ireg_reg[32]_1 ),
        .I1(Q[2]),
        .I2(shift_reg0_address0),
        .O(\ap_CS_fsm_reg[3]_0 ));
  LUT3 #(
    .INIT(8'h0E)) 
    ram_reg_0_15_0_0_i_2
       (.I0(\ireg_reg[32]_1 ),
        .I1(Q[2]),
        .I2(shift_reg0_address0),
        .O(\ap_CS_fsm_reg[3] ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \trunc_ln1_reg_329[23]_i_1 
       (.I0(\ireg_reg[32]_0 ),
        .I1(ap_rst_n),
        .I2(\trunc_ln1_reg_329_reg[0] ),
        .O(\ireg_reg[32]_1 ));
endmodule

(* ORIG_REF_NAME = "ibuf" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ibuf_3
   (x_TREADY,
    Q,
    \ireg_reg[32]_0 ,
    \x_TDATA[0] ,
    \x_TDATA[1] ,
    \x_TDATA[2] ,
    \x_TDATA[3] ,
    \x_TDATA[4] ,
    \x_TDATA[5] ,
    \x_TDATA[6] ,
    \x_TDATA[7] ,
    \x_TDATA[8] ,
    \x_TDATA[9] ,
    \x_TDATA[10] ,
    \x_TDATA[11] ,
    \x_TDATA[12] ,
    \x_TDATA[13] ,
    \x_TDATA[14] ,
    \x_TDATA[15] ,
    \x_TDATA[16] ,
    \x_TDATA[17] ,
    \x_TDATA[18] ,
    \x_TDATA[19] ,
    \x_TDATA[20] ,
    \x_TDATA[21] ,
    \x_TDATA[22] ,
    \x_TDATA[23] ,
    \x_TDATA[24] ,
    \x_TDATA[25] ,
    \x_TDATA[26] ,
    \x_TDATA[27] ,
    \x_TDATA[28] ,
    \x_TDATA[29] ,
    \x_TDATA[30] ,
    \x_TDATA[31] ,
    \ireg_reg[32]_1 ,
    ap_rst_n,
    \ireg_reg[0]_0 ,
    \ireg_reg[0]_1 ,
    SR,
    ap_clk);
  output x_TREADY;
  output [0:0]Q;
  output \ireg_reg[32]_0 ;
  output \x_TDATA[0] ;
  output \x_TDATA[1] ;
  output \x_TDATA[2] ;
  output \x_TDATA[3] ;
  output \x_TDATA[4] ;
  output \x_TDATA[5] ;
  output \x_TDATA[6] ;
  output \x_TDATA[7] ;
  output \x_TDATA[8] ;
  output \x_TDATA[9] ;
  output \x_TDATA[10] ;
  output \x_TDATA[11] ;
  output \x_TDATA[12] ;
  output \x_TDATA[13] ;
  output \x_TDATA[14] ;
  output \x_TDATA[15] ;
  output \x_TDATA[16] ;
  output \x_TDATA[17] ;
  output \x_TDATA[18] ;
  output \x_TDATA[19] ;
  output \x_TDATA[20] ;
  output \x_TDATA[21] ;
  output \x_TDATA[22] ;
  output \x_TDATA[23] ;
  output \x_TDATA[24] ;
  output \x_TDATA[25] ;
  output \x_TDATA[26] ;
  output \x_TDATA[27] ;
  output \x_TDATA[28] ;
  output \x_TDATA[29] ;
  output \x_TDATA[30] ;
  output \x_TDATA[31] ;
  input [32:0]\ireg_reg[32]_1 ;
  input ap_rst_n;
  input [1:0]\ireg_reg[0]_0 ;
  input \ireg_reg[0]_1 ;
  input [0:0]SR;
  input ap_clk;

  wire [0:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_rst_n;
  wire ireg01_out;
  wire [1:0]\ireg_reg[0]_0 ;
  wire \ireg_reg[0]_1 ;
  wire \ireg_reg[32]_0 ;
  wire [32:0]\ireg_reg[32]_1 ;
  wire \ireg_reg_n_1_[0] ;
  wire \ireg_reg_n_1_[10] ;
  wire \ireg_reg_n_1_[11] ;
  wire \ireg_reg_n_1_[12] ;
  wire \ireg_reg_n_1_[13] ;
  wire \ireg_reg_n_1_[14] ;
  wire \ireg_reg_n_1_[15] ;
  wire \ireg_reg_n_1_[16] ;
  wire \ireg_reg_n_1_[17] ;
  wire \ireg_reg_n_1_[18] ;
  wire \ireg_reg_n_1_[19] ;
  wire \ireg_reg_n_1_[1] ;
  wire \ireg_reg_n_1_[20] ;
  wire \ireg_reg_n_1_[21] ;
  wire \ireg_reg_n_1_[22] ;
  wire \ireg_reg_n_1_[23] ;
  wire \ireg_reg_n_1_[24] ;
  wire \ireg_reg_n_1_[25] ;
  wire \ireg_reg_n_1_[26] ;
  wire \ireg_reg_n_1_[27] ;
  wire \ireg_reg_n_1_[28] ;
  wire \ireg_reg_n_1_[29] ;
  wire \ireg_reg_n_1_[2] ;
  wire \ireg_reg_n_1_[30] ;
  wire \ireg_reg_n_1_[31] ;
  wire \ireg_reg_n_1_[3] ;
  wire \ireg_reg_n_1_[4] ;
  wire \ireg_reg_n_1_[5] ;
  wire \ireg_reg_n_1_[6] ;
  wire \ireg_reg_n_1_[7] ;
  wire \ireg_reg_n_1_[8] ;
  wire \ireg_reg_n_1_[9] ;
  wire \x_TDATA[0] ;
  wire \x_TDATA[10] ;
  wire \x_TDATA[11] ;
  wire \x_TDATA[12] ;
  wire \x_TDATA[13] ;
  wire \x_TDATA[14] ;
  wire \x_TDATA[15] ;
  wire \x_TDATA[16] ;
  wire \x_TDATA[17] ;
  wire \x_TDATA[18] ;
  wire \x_TDATA[19] ;
  wire \x_TDATA[1] ;
  wire \x_TDATA[20] ;
  wire \x_TDATA[21] ;
  wire \x_TDATA[22] ;
  wire \x_TDATA[23] ;
  wire \x_TDATA[24] ;
  wire \x_TDATA[25] ;
  wire \x_TDATA[26] ;
  wire \x_TDATA[27] ;
  wire \x_TDATA[28] ;
  wire \x_TDATA[29] ;
  wire \x_TDATA[2] ;
  wire \x_TDATA[30] ;
  wire \x_TDATA[31] ;
  wire \x_TDATA[3] ;
  wire \x_TDATA[4] ;
  wire \x_TDATA[5] ;
  wire \x_TDATA[6] ;
  wire \x_TDATA[7] ;
  wire \x_TDATA[8] ;
  wire \x_TDATA[9] ;
  wire x_TREADY;

  LUT4 #(
    .INIT(16'h0010)) 
    \ireg[32]_i_2 
       (.I0(Q),
        .I1(\ireg_reg[0]_0 [0]),
        .I2(\ireg_reg[0]_1 ),
        .I3(\ireg_reg[0]_0 [1]),
        .O(ireg01_out));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[0] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [0]),
        .Q(\ireg_reg_n_1_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[10] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [10]),
        .Q(\ireg_reg_n_1_[10] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[11] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [11]),
        .Q(\ireg_reg_n_1_[11] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[12] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [12]),
        .Q(\ireg_reg_n_1_[12] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[13] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [13]),
        .Q(\ireg_reg_n_1_[13] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[14] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [14]),
        .Q(\ireg_reg_n_1_[14] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[15] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [15]),
        .Q(\ireg_reg_n_1_[15] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[16] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [16]),
        .Q(\ireg_reg_n_1_[16] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[17] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [17]),
        .Q(\ireg_reg_n_1_[17] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[18] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [18]),
        .Q(\ireg_reg_n_1_[18] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[19] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [19]),
        .Q(\ireg_reg_n_1_[19] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[1] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [1]),
        .Q(\ireg_reg_n_1_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[20] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [20]),
        .Q(\ireg_reg_n_1_[20] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[21] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [21]),
        .Q(\ireg_reg_n_1_[21] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[22] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [22]),
        .Q(\ireg_reg_n_1_[22] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[23] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [23]),
        .Q(\ireg_reg_n_1_[23] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[24] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [24]),
        .Q(\ireg_reg_n_1_[24] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[25] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [25]),
        .Q(\ireg_reg_n_1_[25] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[26] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [26]),
        .Q(\ireg_reg_n_1_[26] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[27] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [27]),
        .Q(\ireg_reg_n_1_[27] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[28] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [28]),
        .Q(\ireg_reg_n_1_[28] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[29] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [29]),
        .Q(\ireg_reg_n_1_[29] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[2] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [2]),
        .Q(\ireg_reg_n_1_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[30] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [30]),
        .Q(\ireg_reg_n_1_[30] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[31] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [31]),
        .Q(\ireg_reg_n_1_[31] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[32] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [32]),
        .Q(Q),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[3] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [3]),
        .Q(\ireg_reg_n_1_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[4] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [4]),
        .Q(\ireg_reg_n_1_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[5] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [5]),
        .Q(\ireg_reg_n_1_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[6] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [6]),
        .Q(\ireg_reg_n_1_[6] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[7] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [7]),
        .Q(\ireg_reg_n_1_[7] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[8] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [8]),
        .Q(\ireg_reg_n_1_[8] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \ireg_reg[9] 
       (.C(ap_clk),
        .CE(ireg01_out),
        .D(\ireg_reg[32]_1 [9]),
        .Q(\ireg_reg_n_1_[9] ),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__0__0_i_1
       (.I0(\ireg_reg[32]_1 [16]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[16] ),
        .O(\x_TDATA[16] ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__10_i_1
       (.I0(\ireg_reg[32]_1 [6]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[6] ),
        .O(\x_TDATA[6] ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__11_i_1
       (.I0(\ireg_reg[32]_1 [5]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[5] ),
        .O(\x_TDATA[5] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__12_i_1
       (.I0(\ireg_reg[32]_1 [4]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[4] ),
        .O(\x_TDATA[4] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__13_i_1
       (.I0(\ireg_reg[32]_1 [3]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[3] ),
        .O(\x_TDATA[3] ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__14_i_1
       (.I0(\ireg_reg[32]_1 [2]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[2] ),
        .O(\x_TDATA[2] ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__15_i_1
       (.I0(\ireg_reg[32]_1 [1]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[1] ),
        .O(\x_TDATA[1] ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__16_i_1
       (.I0(\ireg_reg[32]_1 [0]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[0] ),
        .O(\x_TDATA[0] ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__1__0_i_1
       (.I0(\ireg_reg[32]_1 [15]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[15] ),
        .O(\x_TDATA[15] ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__2__0_i_1
       (.I0(\ireg_reg[32]_1 [14]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[14] ),
        .O(\x_TDATA[14] ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__3_i_1
       (.I0(\ireg_reg[32]_1 [13]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[13] ),
        .O(\x_TDATA[13] ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__4_i_1
       (.I0(\ireg_reg[32]_1 [12]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[12] ),
        .O(\x_TDATA[12] ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__5_i_1
       (.I0(\ireg_reg[32]_1 [11]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[11] ),
        .O(\x_TDATA[11] ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__6_i_1
       (.I0(\ireg_reg[32]_1 [10]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[10] ),
        .O(\x_TDATA[10] ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__7_i_1
       (.I0(\ireg_reg[32]_1 [9]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[9] ),
        .O(\x_TDATA[9] ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__8_i_1
       (.I0(\ireg_reg[32]_1 [8]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[8] ),
        .O(\x_TDATA[8] ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    mul_ln80_fu_191_p2__9_i_1
       (.I0(\ireg_reg[32]_1 [7]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[7] ),
        .O(\x_TDATA[7] ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \odata[32]_i_2 
       (.I0(Q),
        .I1(\ireg_reg[32]_1 [32]),
        .O(\ireg_reg[32]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__0_i_1
       (.I0(\ireg_reg[32]_1 [30]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[30] ),
        .O(\x_TDATA[30] ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__10_i_1
       (.I0(\ireg_reg[32]_1 [20]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[20] ),
        .O(\x_TDATA[20] ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__11_i_1
       (.I0(\ireg_reg[32]_1 [19]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[19] ),
        .O(\x_TDATA[19] ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__12_i_1
       (.I0(\ireg_reg[32]_1 [18]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[18] ),
        .O(\x_TDATA[18] ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__13_i_1
       (.I0(\ireg_reg[32]_1 [17]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[17] ),
        .O(\x_TDATA[17] ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__1_i_1
       (.I0(\ireg_reg[32]_1 [29]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[29] ),
        .O(\x_TDATA[29] ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__2_i_1
       (.I0(\ireg_reg[32]_1 [28]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[28] ),
        .O(\x_TDATA[28] ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__3_i_1
       (.I0(\ireg_reg[32]_1 [27]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[27] ),
        .O(\x_TDATA[27] ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__4_i_1
       (.I0(\ireg_reg[32]_1 [26]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[26] ),
        .O(\x_TDATA[26] ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__5_i_1
       (.I0(\ireg_reg[32]_1 [25]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[25] ),
        .O(\x_TDATA[25] ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__6_i_1
       (.I0(\ireg_reg[32]_1 [24]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[24] ),
        .O(\x_TDATA[24] ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__7_i_1
       (.I0(\ireg_reg[32]_1 [23]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[23] ),
        .O(\x_TDATA[23] ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__8_i_1
       (.I0(\ireg_reg[32]_1 [22]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[22] ),
        .O(\x_TDATA[22] ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata__9_i_1
       (.I0(\ireg_reg[32]_1 [21]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[21] ),
        .O(\x_TDATA[21] ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    odata_i_1
       (.I0(\ireg_reg[32]_1 [31]),
        .I1(Q),
        .I2(\ireg_reg_n_1_[31] ),
        .O(\x_TDATA[31] ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h20)) 
    x_TREADY_INST_0
       (.I0(\ireg_reg[32]_1 [32]),
        .I1(Q),
        .I2(ap_rst_n),
        .O(x_TREADY));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_obuf
   (SR,
    Q,
    mul_ln80_fu_191_p2__2,
    mul_ln80_fu_191_p2__2_0,
    mul_ln80_fu_191_p2__2_1,
    mul_ln80_fu_191_p2__2_2,
    mul_ln80_fu_191_p2__2_3,
    O,
    \odata_reg[32]_0 ,
    ap_rst_n,
    y_TREADY,
    \odata_reg[31]_i_4_0 ,
    mul_ln80_fu_191_p2__21,
    P,
    \ireg_reg[0] ,
    D,
    ap_clk);
  output [0:0]SR;
  output [24:0]Q;
  output [3:0]mul_ln80_fu_191_p2__2;
  output [3:0]mul_ln80_fu_191_p2__2_0;
  output [3:0]mul_ln80_fu_191_p2__2_1;
  output [3:0]mul_ln80_fu_191_p2__2_2;
  output [3:0]mul_ln80_fu_191_p2__2_3;
  output [3:0]O;
  output [0:0]\odata_reg[32]_0 ;
  input ap_rst_n;
  input y_TREADY;
  input [63:0]\odata_reg[31]_i_4_0 ;
  input [47:0]mul_ln80_fu_191_p2__21;
  input [15:0]P;
  input [0:0]\ireg_reg[0] ;
  input [24:0]D;
  input ap_clk;

  wire [24:0]D;
  wire [3:0]O;
  wire [15:0]P;
  wire [24:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_rst_n;
  wire [0:0]\ireg_reg[0] ;
  wire [3:0]mul_ln80_fu_191_p2__2;
  wire [47:0]mul_ln80_fu_191_p2__21;
  wire [3:0]mul_ln80_fu_191_p2__2_0;
  wire [3:0]mul_ln80_fu_191_p2__2_1;
  wire [3:0]mul_ln80_fu_191_p2__2_2;
  wire [3:0]mul_ln80_fu_191_p2__2_3;
  wire \odata[11]_i_4_n_1 ;
  wire \odata[11]_i_5_n_1 ;
  wire \odata[11]_i_6_n_1 ;
  wire \odata[11]_i_7_n_1 ;
  wire \odata[15]_i_4_n_1 ;
  wire \odata[15]_i_5_n_1 ;
  wire \odata[15]_i_6_n_1 ;
  wire \odata[15]_i_7_n_1 ;
  wire \odata[19]_i_4_n_1 ;
  wire \odata[19]_i_5_n_1 ;
  wire \odata[19]_i_6_n_1 ;
  wire \odata[19]_i_7_n_1 ;
  wire \odata[31]_i_2_n_1 ;
  wire \odata[31]_i_6_n_1 ;
  wire \odata[31]_i_7_n_1 ;
  wire \odata[31]_i_8_n_1 ;
  wire \odata[31]_i_9_n_1 ;
  wire \odata[3]_i_11_n_1 ;
  wire \odata[3]_i_12_n_1 ;
  wire \odata[3]_i_13_n_1 ;
  wire \odata[3]_i_14_n_1 ;
  wire \odata[3]_i_21_n_1 ;
  wire \odata[3]_i_22_n_1 ;
  wire \odata[3]_i_23_n_1 ;
  wire \odata[3]_i_24_n_1 ;
  wire \odata[3]_i_31_n_1 ;
  wire \odata[3]_i_32_n_1 ;
  wire \odata[3]_i_33_n_1 ;
  wire \odata[3]_i_34_n_1 ;
  wire \odata[3]_i_41_n_1 ;
  wire \odata[3]_i_42_n_1 ;
  wire \odata[3]_i_43_n_1 ;
  wire \odata[3]_i_44_n_1 ;
  wire \odata[3]_i_51_n_1 ;
  wire \odata[3]_i_52_n_1 ;
  wire \odata[3]_i_53_n_1 ;
  wire \odata[3]_i_54_n_1 ;
  wire \odata[3]_i_5_n_1 ;
  wire \odata[3]_i_61_n_1 ;
  wire \odata[3]_i_62_n_1 ;
  wire \odata[3]_i_63_n_1 ;
  wire \odata[3]_i_64_n_1 ;
  wire \odata[3]_i_6_n_1 ;
  wire \odata[3]_i_70_n_1 ;
  wire \odata[3]_i_71_n_1 ;
  wire \odata[3]_i_72_n_1 ;
  wire \odata[3]_i_73_n_1 ;
  wire \odata[3]_i_78_n_1 ;
  wire \odata[3]_i_79_n_1 ;
  wire \odata[3]_i_7_n_1 ;
  wire \odata[3]_i_80_n_1 ;
  wire \odata[3]_i_81_n_1 ;
  wire \odata[3]_i_83_n_1 ;
  wire \odata[3]_i_84_n_1 ;
  wire \odata[3]_i_85_n_1 ;
  wire \odata[3]_i_86_n_1 ;
  wire \odata[3]_i_87_n_1 ;
  wire \odata[3]_i_88_n_1 ;
  wire \odata[3]_i_89_n_1 ;
  wire \odata[3]_i_8_n_1 ;
  wire \odata[3]_i_90_n_1 ;
  wire \odata[7]_i_4_n_1 ;
  wire \odata[7]_i_5_n_1 ;
  wire \odata[7]_i_6_n_1 ;
  wire \odata[7]_i_7_n_1 ;
  wire \odata_reg[11]_i_2_n_1 ;
  wire \odata_reg[11]_i_2_n_2 ;
  wire \odata_reg[11]_i_2_n_3 ;
  wire \odata_reg[11]_i_2_n_4 ;
  wire \odata_reg[15]_i_2_n_1 ;
  wire \odata_reg[15]_i_2_n_2 ;
  wire \odata_reg[15]_i_2_n_3 ;
  wire \odata_reg[15]_i_2_n_4 ;
  wire \odata_reg[19]_i_2_n_1 ;
  wire \odata_reg[19]_i_2_n_2 ;
  wire \odata_reg[19]_i_2_n_3 ;
  wire \odata_reg[19]_i_2_n_4 ;
  wire [63:0]\odata_reg[31]_i_4_0 ;
  wire \odata_reg[31]_i_4_n_2 ;
  wire \odata_reg[31]_i_4_n_3 ;
  wire \odata_reg[31]_i_4_n_4 ;
  wire [0:0]\odata_reg[32]_0 ;
  wire \odata_reg[3]_i_19_n_1 ;
  wire \odata_reg[3]_i_19_n_2 ;
  wire \odata_reg[3]_i_19_n_3 ;
  wire \odata_reg[3]_i_19_n_4 ;
  wire \odata_reg[3]_i_29_n_1 ;
  wire \odata_reg[3]_i_29_n_2 ;
  wire \odata_reg[3]_i_29_n_3 ;
  wire \odata_reg[3]_i_29_n_4 ;
  wire \odata_reg[3]_i_2_n_1 ;
  wire \odata_reg[3]_i_2_n_2 ;
  wire \odata_reg[3]_i_2_n_3 ;
  wire \odata_reg[3]_i_2_n_4 ;
  wire \odata_reg[3]_i_39_n_1 ;
  wire \odata_reg[3]_i_39_n_2 ;
  wire \odata_reg[3]_i_39_n_3 ;
  wire \odata_reg[3]_i_39_n_4 ;
  wire \odata_reg[3]_i_3_n_1 ;
  wire \odata_reg[3]_i_3_n_2 ;
  wire \odata_reg[3]_i_3_n_3 ;
  wire \odata_reg[3]_i_3_n_4 ;
  wire \odata_reg[3]_i_49_n_1 ;
  wire \odata_reg[3]_i_49_n_2 ;
  wire \odata_reg[3]_i_49_n_3 ;
  wire \odata_reg[3]_i_49_n_4 ;
  wire \odata_reg[3]_i_59_n_1 ;
  wire \odata_reg[3]_i_59_n_2 ;
  wire \odata_reg[3]_i_59_n_3 ;
  wire \odata_reg[3]_i_59_n_4 ;
  wire \odata_reg[3]_i_69_n_1 ;
  wire \odata_reg[3]_i_69_n_2 ;
  wire \odata_reg[3]_i_69_n_3 ;
  wire \odata_reg[3]_i_69_n_4 ;
  wire \odata_reg[3]_i_77_n_1 ;
  wire \odata_reg[3]_i_77_n_2 ;
  wire \odata_reg[3]_i_77_n_3 ;
  wire \odata_reg[3]_i_77_n_4 ;
  wire \odata_reg[3]_i_82_n_1 ;
  wire \odata_reg[3]_i_82_n_2 ;
  wire \odata_reg[3]_i_82_n_3 ;
  wire \odata_reg[3]_i_82_n_4 ;
  wire \odata_reg[3]_i_9_n_1 ;
  wire \odata_reg[3]_i_9_n_2 ;
  wire \odata_reg[3]_i_9_n_3 ;
  wire \odata_reg[3]_i_9_n_4 ;
  wire \odata_reg[7]_i_2_n_1 ;
  wire \odata_reg[7]_i_2_n_2 ;
  wire \odata_reg[7]_i_2_n_3 ;
  wire \odata_reg[7]_i_2_n_4 ;
  wire y_TREADY;
  wire [3:3]\NLW_odata_reg[31]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_19_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_29_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_39_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_49_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_59_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_69_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_77_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_82_O_UNCONNECTED ;
  wire [3:0]\NLW_odata_reg[3]_i_9_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'hD0FF)) 
    \ireg[32]_i_1 
       (.I0(Q[24]),
        .I1(y_TREADY),
        .I2(\ireg_reg[0] ),
        .I3(ap_rst_n),
        .O(\odata_reg[32]_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_4 
       (.I0(mul_ln80_fu_191_p2__21[35]),
        .I1(\odata_reg[31]_i_4_0 [51]),
        .O(\odata[11]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_5 
       (.I0(mul_ln80_fu_191_p2__21[34]),
        .I1(\odata_reg[31]_i_4_0 [50]),
        .O(\odata[11]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_6 
       (.I0(mul_ln80_fu_191_p2__21[33]),
        .I1(\odata_reg[31]_i_4_0 [49]),
        .O(\odata[11]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[11]_i_7 
       (.I0(mul_ln80_fu_191_p2__21[32]),
        .I1(\odata_reg[31]_i_4_0 [48]),
        .O(\odata[11]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_4 
       (.I0(mul_ln80_fu_191_p2__21[39]),
        .I1(\odata_reg[31]_i_4_0 [55]),
        .O(\odata[15]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_5 
       (.I0(mul_ln80_fu_191_p2__21[38]),
        .I1(\odata_reg[31]_i_4_0 [54]),
        .O(\odata[15]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_6 
       (.I0(mul_ln80_fu_191_p2__21[37]),
        .I1(\odata_reg[31]_i_4_0 [53]),
        .O(\odata[15]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[15]_i_7 
       (.I0(mul_ln80_fu_191_p2__21[36]),
        .I1(\odata_reg[31]_i_4_0 [52]),
        .O(\odata[15]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_4 
       (.I0(mul_ln80_fu_191_p2__21[43]),
        .I1(\odata_reg[31]_i_4_0 [59]),
        .O(\odata[19]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_5 
       (.I0(mul_ln80_fu_191_p2__21[42]),
        .I1(\odata_reg[31]_i_4_0 [58]),
        .O(\odata[19]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_6 
       (.I0(mul_ln80_fu_191_p2__21[41]),
        .I1(\odata_reg[31]_i_4_0 [57]),
        .O(\odata[19]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[19]_i_7 
       (.I0(mul_ln80_fu_191_p2__21[40]),
        .I1(\odata_reg[31]_i_4_0 [56]),
        .O(\odata[19]_i_7_n_1 ));
  LUT1 #(
    .INIT(2'h1)) 
    \odata[31]_i_1 
       (.I0(ap_rst_n),
        .O(SR));
  LUT2 #(
    .INIT(4'hB)) 
    \odata[31]_i_2 
       (.I0(y_TREADY),
        .I1(Q[24]),
        .O(\odata[31]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_6 
       (.I0(\odata_reg[31]_i_4_0 [63]),
        .I1(mul_ln80_fu_191_p2__21[47]),
        .O(\odata[31]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_7 
       (.I0(mul_ln80_fu_191_p2__21[46]),
        .I1(\odata_reg[31]_i_4_0 [62]),
        .O(\odata[31]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_8 
       (.I0(mul_ln80_fu_191_p2__21[45]),
        .I1(\odata_reg[31]_i_4_0 [61]),
        .O(\odata[31]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[31]_i_9 
       (.I0(mul_ln80_fu_191_p2__21[44]),
        .I1(\odata_reg[31]_i_4_0 [60]),
        .O(\odata[31]_i_9_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_11 
       (.I0(mul_ln80_fu_191_p2__21[23]),
        .I1(\odata_reg[31]_i_4_0 [39]),
        .O(\odata[3]_i_11_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_12 
       (.I0(mul_ln80_fu_191_p2__21[22]),
        .I1(\odata_reg[31]_i_4_0 [38]),
        .O(\odata[3]_i_12_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_13 
       (.I0(mul_ln80_fu_191_p2__21[21]),
        .I1(\odata_reg[31]_i_4_0 [37]),
        .O(\odata[3]_i_13_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_14 
       (.I0(mul_ln80_fu_191_p2__21[20]),
        .I1(\odata_reg[31]_i_4_0 [36]),
        .O(\odata[3]_i_14_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_21 
       (.I0(mul_ln80_fu_191_p2__21[19]),
        .I1(\odata_reg[31]_i_4_0 [35]),
        .O(\odata[3]_i_21_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_22 
       (.I0(mul_ln80_fu_191_p2__21[18]),
        .I1(\odata_reg[31]_i_4_0 [34]),
        .O(\odata[3]_i_22_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_23 
       (.I0(mul_ln80_fu_191_p2__21[17]),
        .I1(\odata_reg[31]_i_4_0 [33]),
        .O(\odata[3]_i_23_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_24 
       (.I0(mul_ln80_fu_191_p2__21[16]),
        .I1(\odata_reg[31]_i_4_0 [32]),
        .O(\odata[3]_i_24_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_31 
       (.I0(mul_ln80_fu_191_p2__21[15]),
        .I1(\odata_reg[31]_i_4_0 [31]),
        .O(\odata[3]_i_31_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_32 
       (.I0(mul_ln80_fu_191_p2__21[14]),
        .I1(\odata_reg[31]_i_4_0 [30]),
        .O(\odata[3]_i_32_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_33 
       (.I0(mul_ln80_fu_191_p2__21[13]),
        .I1(\odata_reg[31]_i_4_0 [29]),
        .O(\odata[3]_i_33_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_34 
       (.I0(mul_ln80_fu_191_p2__21[12]),
        .I1(\odata_reg[31]_i_4_0 [28]),
        .O(\odata[3]_i_34_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_41 
       (.I0(mul_ln80_fu_191_p2__21[11]),
        .I1(\odata_reg[31]_i_4_0 [27]),
        .O(\odata[3]_i_41_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_42 
       (.I0(mul_ln80_fu_191_p2__21[10]),
        .I1(\odata_reg[31]_i_4_0 [26]),
        .O(\odata[3]_i_42_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_43 
       (.I0(mul_ln80_fu_191_p2__21[9]),
        .I1(\odata_reg[31]_i_4_0 [25]),
        .O(\odata[3]_i_43_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_44 
       (.I0(mul_ln80_fu_191_p2__21[8]),
        .I1(\odata_reg[31]_i_4_0 [24]),
        .O(\odata[3]_i_44_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_5 
       (.I0(mul_ln80_fu_191_p2__21[27]),
        .I1(\odata_reg[31]_i_4_0 [43]),
        .O(\odata[3]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_51 
       (.I0(mul_ln80_fu_191_p2__21[7]),
        .I1(\odata_reg[31]_i_4_0 [23]),
        .O(\odata[3]_i_51_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_52 
       (.I0(mul_ln80_fu_191_p2__21[6]),
        .I1(\odata_reg[31]_i_4_0 [22]),
        .O(\odata[3]_i_52_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_53 
       (.I0(mul_ln80_fu_191_p2__21[5]),
        .I1(\odata_reg[31]_i_4_0 [21]),
        .O(\odata[3]_i_53_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_54 
       (.I0(mul_ln80_fu_191_p2__21[4]),
        .I1(\odata_reg[31]_i_4_0 [20]),
        .O(\odata[3]_i_54_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_6 
       (.I0(mul_ln80_fu_191_p2__21[26]),
        .I1(\odata_reg[31]_i_4_0 [42]),
        .O(\odata[3]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_61 
       (.I0(mul_ln80_fu_191_p2__21[3]),
        .I1(\odata_reg[31]_i_4_0 [19]),
        .O(\odata[3]_i_61_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_62 
       (.I0(mul_ln80_fu_191_p2__21[2]),
        .I1(\odata_reg[31]_i_4_0 [18]),
        .O(\odata[3]_i_62_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_63 
       (.I0(mul_ln80_fu_191_p2__21[1]),
        .I1(\odata_reg[31]_i_4_0 [17]),
        .O(\odata[3]_i_63_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_64 
       (.I0(mul_ln80_fu_191_p2__21[0]),
        .I1(\odata_reg[31]_i_4_0 [16]),
        .O(\odata[3]_i_64_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_7 
       (.I0(mul_ln80_fu_191_p2__21[25]),
        .I1(\odata_reg[31]_i_4_0 [41]),
        .O(\odata[3]_i_7_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_70 
       (.I0(P[15]),
        .I1(\odata_reg[31]_i_4_0 [15]),
        .O(\odata[3]_i_70_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_71 
       (.I0(P[14]),
        .I1(\odata_reg[31]_i_4_0 [14]),
        .O(\odata[3]_i_71_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_72 
       (.I0(P[13]),
        .I1(\odata_reg[31]_i_4_0 [13]),
        .O(\odata[3]_i_72_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_73 
       (.I0(P[12]),
        .I1(\odata_reg[31]_i_4_0 [12]),
        .O(\odata[3]_i_73_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_78 
       (.I0(P[11]),
        .I1(\odata_reg[31]_i_4_0 [11]),
        .O(\odata[3]_i_78_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_79 
       (.I0(P[10]),
        .I1(\odata_reg[31]_i_4_0 [10]),
        .O(\odata[3]_i_79_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_8 
       (.I0(mul_ln80_fu_191_p2__21[24]),
        .I1(\odata_reg[31]_i_4_0 [40]),
        .O(\odata[3]_i_8_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_80 
       (.I0(P[9]),
        .I1(\odata_reg[31]_i_4_0 [9]),
        .O(\odata[3]_i_80_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_81 
       (.I0(P[8]),
        .I1(\odata_reg[31]_i_4_0 [8]),
        .O(\odata[3]_i_81_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_83 
       (.I0(P[7]),
        .I1(\odata_reg[31]_i_4_0 [7]),
        .O(\odata[3]_i_83_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_84 
       (.I0(P[6]),
        .I1(\odata_reg[31]_i_4_0 [6]),
        .O(\odata[3]_i_84_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_85 
       (.I0(P[5]),
        .I1(\odata_reg[31]_i_4_0 [5]),
        .O(\odata[3]_i_85_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_86 
       (.I0(P[4]),
        .I1(\odata_reg[31]_i_4_0 [4]),
        .O(\odata[3]_i_86_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_87 
       (.I0(P[3]),
        .I1(\odata_reg[31]_i_4_0 [3]),
        .O(\odata[3]_i_87_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_88 
       (.I0(P[2]),
        .I1(\odata_reg[31]_i_4_0 [2]),
        .O(\odata[3]_i_88_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_89 
       (.I0(P[1]),
        .I1(\odata_reg[31]_i_4_0 [1]),
        .O(\odata[3]_i_89_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[3]_i_90 
       (.I0(P[0]),
        .I1(\odata_reg[31]_i_4_0 [0]),
        .O(\odata[3]_i_90_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_4 
       (.I0(mul_ln80_fu_191_p2__21[31]),
        .I1(\odata_reg[31]_i_4_0 [47]),
        .O(\odata[7]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_5 
       (.I0(mul_ln80_fu_191_p2__21[30]),
        .I1(\odata_reg[31]_i_4_0 [46]),
        .O(\odata[7]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_6 
       (.I0(mul_ln80_fu_191_p2__21[29]),
        .I1(\odata_reg[31]_i_4_0 [45]),
        .O(\odata[7]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \odata[7]_i_7 
       (.I0(mul_ln80_fu_191_p2__21[28]),
        .I1(\odata_reg[31]_i_4_0 [44]),
        .O(\odata[7]_i_7_n_1 ));
  FDRE \odata_reg[0] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \odata_reg[10] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE \odata_reg[11] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[11]_i_2 
       (.CI(\odata_reg[7]_i_2_n_1 ),
        .CO({\odata_reg[11]_i_2_n_1 ,\odata_reg[11]_i_2_n_2 ,\odata_reg[11]_i_2_n_3 ,\odata_reg[11]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[35:32]),
        .O(mul_ln80_fu_191_p2__2_1),
        .S({\odata[11]_i_4_n_1 ,\odata[11]_i_5_n_1 ,\odata[11]_i_6_n_1 ,\odata[11]_i_7_n_1 }));
  FDRE \odata_reg[12] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE \odata_reg[13] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE \odata_reg[14] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE \odata_reg[15] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[15]_i_2 
       (.CI(\odata_reg[11]_i_2_n_1 ),
        .CO({\odata_reg[15]_i_2_n_1 ,\odata_reg[15]_i_2_n_2 ,\odata_reg[15]_i_2_n_3 ,\odata_reg[15]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[39:36]),
        .O(mul_ln80_fu_191_p2__2_2),
        .S({\odata[15]_i_4_n_1 ,\odata[15]_i_5_n_1 ,\odata[15]_i_6_n_1 ,\odata[15]_i_7_n_1 }));
  FDRE \odata_reg[16] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE \odata_reg[17] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE \odata_reg[18] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE \odata_reg[19] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[19]),
        .Q(Q[19]),
        .R(SR));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[19]_i_2 
       (.CI(\odata_reg[15]_i_2_n_1 ),
        .CO({\odata_reg[19]_i_2_n_1 ,\odata_reg[19]_i_2_n_2 ,\odata_reg[19]_i_2_n_3 ,\odata_reg[19]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[43:40]),
        .O(mul_ln80_fu_191_p2__2_3),
        .S({\odata[19]_i_4_n_1 ,\odata[19]_i_5_n_1 ,\odata[19]_i_6_n_1 ,\odata[19]_i_7_n_1 }));
  FDRE \odata_reg[1] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \odata_reg[20] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE \odata_reg[21] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE \odata_reg[22] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE \odata_reg[2] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE \odata_reg[31] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[23]),
        .Q(Q[23]),
        .R(SR));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[31]_i_4 
       (.CI(\odata_reg[19]_i_2_n_1 ),
        .CO({\NLW_odata_reg[31]_i_4_CO_UNCONNECTED [3],\odata_reg[31]_i_4_n_2 ,\odata_reg[31]_i_4_n_3 ,\odata_reg[31]_i_4_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln80_fu_191_p2__21[46:44]}),
        .O(O),
        .S({\odata[31]_i_6_n_1 ,\odata[31]_i_7_n_1 ,\odata[31]_i_8_n_1 ,\odata[31]_i_9_n_1 }));
  FDRE \odata_reg[32] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[24]),
        .Q(Q[24]),
        .R(SR));
  FDRE \odata_reg[3] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_19 
       (.CI(\odata_reg[3]_i_29_n_1 ),
        .CO({\odata_reg[3]_i_19_n_1 ,\odata_reg[3]_i_19_n_2 ,\odata_reg[3]_i_19_n_3 ,\odata_reg[3]_i_19_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[15:12]),
        .O(\NLW_odata_reg[3]_i_19_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_31_n_1 ,\odata[3]_i_32_n_1 ,\odata[3]_i_33_n_1 ,\odata[3]_i_34_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_2 
       (.CI(\odata_reg[3]_i_3_n_1 ),
        .CO({\odata_reg[3]_i_2_n_1 ,\odata_reg[3]_i_2_n_2 ,\odata_reg[3]_i_2_n_3 ,\odata_reg[3]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[27:24]),
        .O(mul_ln80_fu_191_p2__2),
        .S({\odata[3]_i_5_n_1 ,\odata[3]_i_6_n_1 ,\odata[3]_i_7_n_1 ,\odata[3]_i_8_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_29 
       (.CI(\odata_reg[3]_i_39_n_1 ),
        .CO({\odata_reg[3]_i_29_n_1 ,\odata_reg[3]_i_29_n_2 ,\odata_reg[3]_i_29_n_3 ,\odata_reg[3]_i_29_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[11:8]),
        .O(\NLW_odata_reg[3]_i_29_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_41_n_1 ,\odata[3]_i_42_n_1 ,\odata[3]_i_43_n_1 ,\odata[3]_i_44_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_3 
       (.CI(\odata_reg[3]_i_9_n_1 ),
        .CO({\odata_reg[3]_i_3_n_1 ,\odata_reg[3]_i_3_n_2 ,\odata_reg[3]_i_3_n_3 ,\odata_reg[3]_i_3_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[23:20]),
        .O(\NLW_odata_reg[3]_i_3_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_11_n_1 ,\odata[3]_i_12_n_1 ,\odata[3]_i_13_n_1 ,\odata[3]_i_14_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_39 
       (.CI(\odata_reg[3]_i_49_n_1 ),
        .CO({\odata_reg[3]_i_39_n_1 ,\odata_reg[3]_i_39_n_2 ,\odata_reg[3]_i_39_n_3 ,\odata_reg[3]_i_39_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[7:4]),
        .O(\NLW_odata_reg[3]_i_39_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_51_n_1 ,\odata[3]_i_52_n_1 ,\odata[3]_i_53_n_1 ,\odata[3]_i_54_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_49 
       (.CI(\odata_reg[3]_i_59_n_1 ),
        .CO({\odata_reg[3]_i_49_n_1 ,\odata_reg[3]_i_49_n_2 ,\odata_reg[3]_i_49_n_3 ,\odata_reg[3]_i_49_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[3:0]),
        .O(\NLW_odata_reg[3]_i_49_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_61_n_1 ,\odata[3]_i_62_n_1 ,\odata[3]_i_63_n_1 ,\odata[3]_i_64_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_59 
       (.CI(\odata_reg[3]_i_69_n_1 ),
        .CO({\odata_reg[3]_i_59_n_1 ,\odata_reg[3]_i_59_n_2 ,\odata_reg[3]_i_59_n_3 ,\odata_reg[3]_i_59_n_4 }),
        .CYINIT(1'b0),
        .DI(P[15:12]),
        .O(\NLW_odata_reg[3]_i_59_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_70_n_1 ,\odata[3]_i_71_n_1 ,\odata[3]_i_72_n_1 ,\odata[3]_i_73_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_69 
       (.CI(\odata_reg[3]_i_77_n_1 ),
        .CO({\odata_reg[3]_i_69_n_1 ,\odata_reg[3]_i_69_n_2 ,\odata_reg[3]_i_69_n_3 ,\odata_reg[3]_i_69_n_4 }),
        .CYINIT(1'b0),
        .DI(P[11:8]),
        .O(\NLW_odata_reg[3]_i_69_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_78_n_1 ,\odata[3]_i_79_n_1 ,\odata[3]_i_80_n_1 ,\odata[3]_i_81_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_77 
       (.CI(\odata_reg[3]_i_82_n_1 ),
        .CO({\odata_reg[3]_i_77_n_1 ,\odata_reg[3]_i_77_n_2 ,\odata_reg[3]_i_77_n_3 ,\odata_reg[3]_i_77_n_4 }),
        .CYINIT(1'b0),
        .DI(P[7:4]),
        .O(\NLW_odata_reg[3]_i_77_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_83_n_1 ,\odata[3]_i_84_n_1 ,\odata[3]_i_85_n_1 ,\odata[3]_i_86_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_82 
       (.CI(1'b0),
        .CO({\odata_reg[3]_i_82_n_1 ,\odata_reg[3]_i_82_n_2 ,\odata_reg[3]_i_82_n_3 ,\odata_reg[3]_i_82_n_4 }),
        .CYINIT(1'b0),
        .DI(P[3:0]),
        .O(\NLW_odata_reg[3]_i_82_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_87_n_1 ,\odata[3]_i_88_n_1 ,\odata[3]_i_89_n_1 ,\odata[3]_i_90_n_1 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[3]_i_9 
       (.CI(\odata_reg[3]_i_19_n_1 ),
        .CO({\odata_reg[3]_i_9_n_1 ,\odata_reg[3]_i_9_n_2 ,\odata_reg[3]_i_9_n_3 ,\odata_reg[3]_i_9_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[19:16]),
        .O(\NLW_odata_reg[3]_i_9_O_UNCONNECTED [3:0]),
        .S({\odata[3]_i_21_n_1 ,\odata[3]_i_22_n_1 ,\odata[3]_i_23_n_1 ,\odata[3]_i_24_n_1 }));
  FDRE \odata_reg[4] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE \odata_reg[5] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE \odata_reg[6] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE \odata_reg[7] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \odata_reg[7]_i_2 
       (.CI(\odata_reg[3]_i_2_n_1 ),
        .CO({\odata_reg[7]_i_2_n_1 ,\odata_reg[7]_i_2_n_2 ,\odata_reg[7]_i_2_n_3 ,\odata_reg[7]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln80_fu_191_p2__21[31:28]),
        .O(mul_ln80_fu_191_p2__2_0),
        .S({\odata[7]_i_4_n_1 ,\odata[7]_i_5_n_1 ,\odata[7]_i_6_n_1 ,\odata[7]_i_7_n_1 }));
  FDRE \odata_reg[8] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE \odata_reg[9] 
       (.C(ap_clk),
        .CE(\odata[31]_i_2_n_1 ),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "obuf" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_obuf_4
   (\odata_reg[32]_0 ,
    \ap_CS_fsm_reg[1] ,
    D,
    ap_NS_fsm11_out,
    \ap_CS_fsm_reg[1]_0 ,
    ack_out1,
    \odata_reg[32]_1 ,
    \ap_CS_fsm_reg[0] ,
    SR,
    \odata_reg[32]_2 ,
    ap_clk,
    odata_reg__13_0,
    odata_reg__12_0,
    odata_reg__11_0,
    odata_reg__10_0,
    odata_reg__9_0,
    odata_reg__8_0,
    odata_reg__7_0,
    odata_reg__6_0,
    odata_reg__5_0,
    odata_reg__4_0,
    odata_reg__3_0,
    odata_reg__2_0,
    odata_reg__1_0,
    odata_reg__0_0,
    odata_reg_0,
    Q,
    \ireg_reg[0] ,
    ap_rst_n,
    \ap_CS_fsm_reg[2] ,
    \ap_CS_fsm_reg[2]_0 );
  output \odata_reg[32]_0 ;
  output \ap_CS_fsm_reg[1] ;
  output [14:0]D;
  output ap_NS_fsm11_out;
  output [0:0]\ap_CS_fsm_reg[1]_0 ;
  output ack_out1;
  output [1:0]\odata_reg[32]_1 ;
  output [0:0]\ap_CS_fsm_reg[0] ;
  input [0:0]SR;
  input \odata_reg[32]_2 ;
  input ap_clk;
  input odata_reg__13_0;
  input odata_reg__12_0;
  input odata_reg__11_0;
  input odata_reg__10_0;
  input odata_reg__9_0;
  input odata_reg__8_0;
  input odata_reg__7_0;
  input odata_reg__6_0;
  input odata_reg__5_0;
  input odata_reg__4_0;
  input odata_reg__3_0;
  input odata_reg__2_0;
  input odata_reg__1_0;
  input odata_reg__0_0;
  input odata_reg_0;
  input [3:0]Q;
  input [0:0]\ireg_reg[0] ;
  input ap_rst_n;
  input \ap_CS_fsm_reg[2] ;
  input [0:0]\ap_CS_fsm_reg[2]_0 ;

  wire [14:0]D;
  wire [3:0]Q;
  wire [0:0]SR;
  wire ack_out1;
  wire [0:0]\ap_CS_fsm_reg[0] ;
  wire \ap_CS_fsm_reg[1] ;
  wire [0:0]\ap_CS_fsm_reg[1]_0 ;
  wire \ap_CS_fsm_reg[2] ;
  wire [0:0]\ap_CS_fsm_reg[2]_0 ;
  wire ap_NS_fsm11_out;
  wire ap_clk;
  wire ap_rst_n;
  wire [0:0]\ireg_reg[0] ;
  wire \odata_reg[32]_0 ;
  wire [1:0]\odata_reg[32]_1 ;
  wire \odata_reg[32]_2 ;
  wire odata_reg_0;
  wire odata_reg__0_0;
  wire odata_reg__10_0;
  wire odata_reg__11_0;
  wire odata_reg__12_0;
  wire odata_reg__13_0;
  wire odata_reg__1_0;
  wire odata_reg__2_0;
  wire odata_reg__3_0;
  wire odata_reg__4_0;
  wire odata_reg__5_0;
  wire odata_reg__6_0;
  wire odata_reg__7_0;
  wire odata_reg__8_0;
  wire odata_reg__9_0;

  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(Q[0]),
        .I1(\odata_reg[32]_0 ),
        .I2(Q[1]),
        .O(\odata_reg[32]_1 [0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(\odata_reg[32]_0 ),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[2] ),
        .I3(\ap_CS_fsm_reg[2]_0 ),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\odata_reg[32]_1 [1]));
  LUT3 #(
    .INIT(8'h08)) 
    \i_0_reg_154[4]_i_1 
       (.I0(Q[1]),
        .I1(\odata_reg[32]_0 ),
        .I2(Q[3]),
        .O(\ap_CS_fsm_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hFB00FFFF)) 
    \ireg[32]_i_1__0 
       (.I0(Q[0]),
        .I1(\odata_reg[32]_0 ),
        .I2(Q[1]),
        .I3(\ireg_reg[0] ),
        .I4(ap_rst_n),
        .O(\ap_CS_fsm_reg[0] ));
  LUT3 #(
    .INIT(8'hFB)) 
    \odata[32]_i_1__0 
       (.I0(Q[1]),
        .I1(\odata_reg[32]_0 ),
        .I2(Q[0]),
        .O(\ap_CS_fsm_reg[1] ));
  FDRE odata_reg
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg_0),
        .Q(D[14]),
        .R(SR));
  FDRE \odata_reg[32] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(\odata_reg[32]_2 ),
        .Q(\odata_reg[32]_0 ),
        .R(SR));
  FDRE odata_reg__0
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__0_0),
        .Q(D[13]),
        .R(SR));
  FDRE odata_reg__1
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__1_0),
        .Q(D[12]),
        .R(SR));
  FDRE odata_reg__10
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__10_0),
        .Q(D[3]),
        .R(SR));
  FDRE odata_reg__11
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__11_0),
        .Q(D[2]),
        .R(SR));
  FDRE odata_reg__12
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__12_0),
        .Q(D[1]),
        .R(SR));
  FDRE odata_reg__13
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__13_0),
        .Q(D[0]),
        .R(SR));
  FDRE odata_reg__2
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__2_0),
        .Q(D[11]),
        .R(SR));
  FDRE odata_reg__3
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__3_0),
        .Q(D[10]),
        .R(SR));
  FDRE odata_reg__4
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__4_0),
        .Q(D[9]),
        .R(SR));
  FDRE odata_reg__5
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__5_0),
        .Q(D[8]),
        .R(SR));
  FDRE odata_reg__6
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__6_0),
        .Q(D[7]),
        .R(SR));
  FDRE odata_reg__7
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__7_0),
        .Q(D[6]),
        .R(SR));
  FDRE odata_reg__8
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__8_0),
        .Q(D[5]),
        .R(SR));
  FDRE odata_reg__9
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[1] ),
        .D(odata_reg__9_0),
        .Q(D[4]),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    \x0_reg_283[31]_i_1 
       (.I0(\odata_reg[32]_0 ),
        .I1(Q[0]),
        .O(ap_NS_fsm11_out));
  LUT2 #(
    .INIT(4'h8)) 
    \x1_reg_289[31]_i_1 
       (.I0(\odata_reg[32]_0 ),
        .I1(Q[1]),
        .O(ack_out1));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_regslice_both
   (x_TVALID_int,
    \ap_CS_fsm_reg[1] ,
    D,
    x_TREADY,
    ap_NS_fsm11_out,
    \ap_CS_fsm_reg[1]_0 ,
    ack_out1,
    \odata_reg[32] ,
    \x_TDATA[0] ,
    \x_TDATA[1] ,
    \x_TDATA[2] ,
    \x_TDATA[3] ,
    \x_TDATA[4] ,
    \x_TDATA[5] ,
    \x_TDATA[6] ,
    \x_TDATA[7] ,
    \x_TDATA[8] ,
    \x_TDATA[9] ,
    \x_TDATA[10] ,
    \x_TDATA[11] ,
    \x_TDATA[12] ,
    \x_TDATA[13] ,
    \x_TDATA[14] ,
    \x_TDATA[15] ,
    \x_TDATA[16] ,
    SR,
    ap_clk,
    \ireg_reg[32] ,
    ap_rst_n,
    Q,
    \ap_CS_fsm_reg[2] ,
    \ap_CS_fsm_reg[2]_0 );
  output x_TVALID_int;
  output \ap_CS_fsm_reg[1] ;
  output [14:0]D;
  output x_TREADY;
  output ap_NS_fsm11_out;
  output [0:0]\ap_CS_fsm_reg[1]_0 ;
  output ack_out1;
  output [1:0]\odata_reg[32] ;
  output \x_TDATA[0] ;
  output \x_TDATA[1] ;
  output \x_TDATA[2] ;
  output \x_TDATA[3] ;
  output \x_TDATA[4] ;
  output \x_TDATA[5] ;
  output \x_TDATA[6] ;
  output \x_TDATA[7] ;
  output \x_TDATA[8] ;
  output \x_TDATA[9] ;
  output \x_TDATA[10] ;
  output \x_TDATA[11] ;
  output \x_TDATA[12] ;
  output \x_TDATA[13] ;
  output \x_TDATA[14] ;
  output \x_TDATA[15] ;
  output \x_TDATA[16] ;
  input [0:0]SR;
  input ap_clk;
  input [32:0]\ireg_reg[32] ;
  input ap_rst_n;
  input [3:0]Q;
  input \ap_CS_fsm_reg[2] ;
  input [0:0]\ap_CS_fsm_reg[2]_0 ;

  wire [14:0]D;
  wire [3:0]Q;
  wire [0:0]SR;
  wire ack_out1;
  wire \ap_CS_fsm_reg[1] ;
  wire [0:0]\ap_CS_fsm_reg[1]_0 ;
  wire \ap_CS_fsm_reg[2] ;
  wire [0:0]\ap_CS_fsm_reg[2]_0 ;
  wire ap_NS_fsm11_out;
  wire ap_clk;
  wire ap_rst_n;
  wire ibuf_inst_n_21;
  wire ibuf_inst_n_22;
  wire ibuf_inst_n_23;
  wire ibuf_inst_n_24;
  wire ibuf_inst_n_25;
  wire ibuf_inst_n_26;
  wire ibuf_inst_n_27;
  wire ibuf_inst_n_28;
  wire ibuf_inst_n_29;
  wire ibuf_inst_n_3;
  wire ibuf_inst_n_30;
  wire ibuf_inst_n_31;
  wire ibuf_inst_n_32;
  wire ibuf_inst_n_33;
  wire ibuf_inst_n_34;
  wire ibuf_inst_n_35;
  wire [32:0]\ireg_reg[32] ;
  wire obuf_inst_n_23;
  wire [1:0]\odata_reg[32] ;
  wire p_0_in;
  wire \x_TDATA[0] ;
  wire \x_TDATA[10] ;
  wire \x_TDATA[11] ;
  wire \x_TDATA[12] ;
  wire \x_TDATA[13] ;
  wire \x_TDATA[14] ;
  wire \x_TDATA[15] ;
  wire \x_TDATA[16] ;
  wire \x_TDATA[1] ;
  wire \x_TDATA[2] ;
  wire \x_TDATA[3] ;
  wire \x_TDATA[4] ;
  wire \x_TDATA[5] ;
  wire \x_TDATA[6] ;
  wire \x_TDATA[7] ;
  wire \x_TDATA[8] ;
  wire \x_TDATA[9] ;
  wire x_TREADY;
  wire x_TVALID_int;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ibuf_3 ibuf_inst
       (.Q(p_0_in),
        .SR(obuf_inst_n_23),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .\ireg_reg[0]_0 (Q[1:0]),
        .\ireg_reg[0]_1 (x_TVALID_int),
        .\ireg_reg[32]_0 (ibuf_inst_n_3),
        .\ireg_reg[32]_1 (\ireg_reg[32] ),
        .\x_TDATA[0] (\x_TDATA[0] ),
        .\x_TDATA[10] (\x_TDATA[10] ),
        .\x_TDATA[11] (\x_TDATA[11] ),
        .\x_TDATA[12] (\x_TDATA[12] ),
        .\x_TDATA[13] (\x_TDATA[13] ),
        .\x_TDATA[14] (\x_TDATA[14] ),
        .\x_TDATA[15] (\x_TDATA[15] ),
        .\x_TDATA[16] (\x_TDATA[16] ),
        .\x_TDATA[17] (ibuf_inst_n_21),
        .\x_TDATA[18] (ibuf_inst_n_22),
        .\x_TDATA[19] (ibuf_inst_n_23),
        .\x_TDATA[1] (\x_TDATA[1] ),
        .\x_TDATA[20] (ibuf_inst_n_24),
        .\x_TDATA[21] (ibuf_inst_n_25),
        .\x_TDATA[22] (ibuf_inst_n_26),
        .\x_TDATA[23] (ibuf_inst_n_27),
        .\x_TDATA[24] (ibuf_inst_n_28),
        .\x_TDATA[25] (ibuf_inst_n_29),
        .\x_TDATA[26] (ibuf_inst_n_30),
        .\x_TDATA[27] (ibuf_inst_n_31),
        .\x_TDATA[28] (ibuf_inst_n_32),
        .\x_TDATA[29] (ibuf_inst_n_33),
        .\x_TDATA[2] (\x_TDATA[2] ),
        .\x_TDATA[30] (ibuf_inst_n_34),
        .\x_TDATA[31] (ibuf_inst_n_35),
        .\x_TDATA[3] (\x_TDATA[3] ),
        .\x_TDATA[4] (\x_TDATA[4] ),
        .\x_TDATA[5] (\x_TDATA[5] ),
        .\x_TDATA[6] (\x_TDATA[6] ),
        .\x_TDATA[7] (\x_TDATA[7] ),
        .\x_TDATA[8] (\x_TDATA[8] ),
        .\x_TDATA[9] (\x_TDATA[9] ),
        .x_TREADY(x_TREADY));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_obuf_4 obuf_inst
       (.D(D),
        .Q(Q),
        .SR(SR),
        .ack_out1(ack_out1),
        .\ap_CS_fsm_reg[0] (obuf_inst_n_23),
        .\ap_CS_fsm_reg[1] (\ap_CS_fsm_reg[1] ),
        .\ap_CS_fsm_reg[1]_0 (\ap_CS_fsm_reg[1]_0 ),
        .\ap_CS_fsm_reg[2] (\ap_CS_fsm_reg[2] ),
        .\ap_CS_fsm_reg[2]_0 (\ap_CS_fsm_reg[2]_0 ),
        .ap_NS_fsm11_out(ap_NS_fsm11_out),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .\ireg_reg[0] (p_0_in),
        .\odata_reg[32]_0 (x_TVALID_int),
        .\odata_reg[32]_1 (\odata_reg[32] ),
        .\odata_reg[32]_2 (ibuf_inst_n_3),
        .odata_reg_0(ibuf_inst_n_35),
        .odata_reg__0_0(ibuf_inst_n_34),
        .odata_reg__10_0(ibuf_inst_n_24),
        .odata_reg__11_0(ibuf_inst_n_23),
        .odata_reg__12_0(ibuf_inst_n_22),
        .odata_reg__13_0(ibuf_inst_n_21),
        .odata_reg__1_0(ibuf_inst_n_33),
        .odata_reg__2_0(ibuf_inst_n_32),
        .odata_reg__3_0(ibuf_inst_n_31),
        .odata_reg__4_0(ibuf_inst_n_30),
        .odata_reg__5_0(ibuf_inst_n_29),
        .odata_reg__6_0(ibuf_inst_n_28),
        .odata_reg__7_0(ibuf_inst_n_27),
        .odata_reg__8_0(ibuf_inst_n_26),
        .odata_reg__9_0(ibuf_inst_n_25));
endmodule

(* ORIG_REF_NAME = "regslice_both" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_regslice_both_0
   (SR,
    coeffs_ce0,
    \ireg_reg[32] ,
    ap_NS_fsm13_out,
    D,
    \odata_reg[32] ,
    \ap_CS_fsm_reg[3] ,
    \ap_CS_fsm_reg[3]_0 ,
    shift_reg0_ce0,
    ap_clk,
    Q,
    ap_rst_n,
    \q0_reg[0] ,
    y_TREADY,
    \trunc_ln1_reg_329_reg[0] ,
    x_TVALID_int,
    \odata_reg[31] ,
    \odata_reg[31]_i_4 ,
    mul_ln80_fu_191_p2__21,
    P,
    shift_reg0_address0);
  output [0:0]SR;
  output coeffs_ce0;
  output [0:0]\ireg_reg[32] ;
  output ap_NS_fsm13_out;
  output [2:0]D;
  output [24:0]\odata_reg[32] ;
  output \ap_CS_fsm_reg[3] ;
  output \ap_CS_fsm_reg[3]_0 ;
  output shift_reg0_ce0;
  input ap_clk;
  input [4:0]Q;
  input ap_rst_n;
  input \q0_reg[0] ;
  input y_TREADY;
  input \trunc_ln1_reg_329_reg[0] ;
  input x_TVALID_int;
  input [23:0]\odata_reg[31] ;
  input [63:0]\odata_reg[31]_i_4 ;
  input [47:0]mul_ln80_fu_191_p2__21;
  input [15:0]P;
  input [0:0]shift_reg0_address0;

  wire [2:0]D;
  wire [15:0]P;
  wire [4:0]Q;
  wire [0:0]SR;
  wire \ap_CS_fsm_reg[3] ;
  wire \ap_CS_fsm_reg[3]_0 ;
  wire ap_NS_fsm13_out;
  wire ap_clk;
  wire ap_rst_n;
  wire [32:0]cdata;
  wire coeffs_ce0;
  wire [1:1]count;
  wire \count_reg_n_1_[0] ;
  wire \count_reg_n_1_[1] ;
  wire ibuf_inst_n_3;
  wire [0:0]\ireg_reg[32] ;
  wire [47:0]mul_ln80_fu_191_p2__21;
  wire obuf_inst_n_27;
  wire obuf_inst_n_28;
  wire obuf_inst_n_29;
  wire obuf_inst_n_30;
  wire obuf_inst_n_31;
  wire obuf_inst_n_32;
  wire obuf_inst_n_33;
  wire obuf_inst_n_34;
  wire obuf_inst_n_35;
  wire obuf_inst_n_36;
  wire obuf_inst_n_37;
  wire obuf_inst_n_38;
  wire obuf_inst_n_39;
  wire obuf_inst_n_40;
  wire obuf_inst_n_41;
  wire obuf_inst_n_42;
  wire obuf_inst_n_43;
  wire obuf_inst_n_44;
  wire obuf_inst_n_45;
  wire obuf_inst_n_46;
  wire obuf_inst_n_48;
  wire obuf_inst_n_49;
  wire obuf_inst_n_50;
  wire obuf_inst_n_51;
  wire [23:0]\odata_reg[31] ;
  wire [63:0]\odata_reg[31]_i_4 ;
  wire [24:0]\odata_reg[32] ;
  wire p_0_in0;
  wire \q0_reg[0] ;
  wire [0:0]shift_reg0_address0;
  wire shift_reg0_ce0;
  wire \trunc_ln1_reg_329_reg[0] ;
  wire x_TVALID_int;
  wire y_TREADY;

  FDRE \count_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ibuf_inst_n_3),
        .Q(\count_reg_n_1_[0] ),
        .R(1'b0));
  FDRE \count_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(count),
        .Q(\count_reg_n_1_[1] ),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ibuf ibuf_inst
       (.D(D),
        .O({p_0_in0,obuf_inst_n_48,obuf_inst_n_49,obuf_inst_n_50}),
        .Q(Q),
        .SR(obuf_inst_n_51),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .\ap_CS_fsm_reg[3]_0 (\ap_CS_fsm_reg[3]_0 ),
        .\ap_CS_fsm_reg[4] ({cdata[32:31],cdata[22:0]}),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_0(ibuf_inst_n_3),
        .coeffs_ce0(coeffs_ce0),
        .count(count),
        .\count_reg[0] (\count_reg_n_1_[1] ),
        .\count_reg[0]_0 (\count_reg_n_1_[0] ),
        .\ireg_reg[0]_0 (\odata_reg[32] [24]),
        .\ireg_reg[11]_0 ({obuf_inst_n_35,obuf_inst_n_36,obuf_inst_n_37,obuf_inst_n_38}),
        .\ireg_reg[15]_0 ({obuf_inst_n_39,obuf_inst_n_40,obuf_inst_n_41,obuf_inst_n_42}),
        .\ireg_reg[19]_0 ({obuf_inst_n_43,obuf_inst_n_44,obuf_inst_n_45,obuf_inst_n_46}),
        .\ireg_reg[32]_0 (\ireg_reg[32] ),
        .\ireg_reg[32]_1 (ap_NS_fsm13_out),
        .\ireg_reg[3]_0 ({obuf_inst_n_27,obuf_inst_n_28,obuf_inst_n_29,obuf_inst_n_30}),
        .\ireg_reg[7]_0 ({obuf_inst_n_31,obuf_inst_n_32,obuf_inst_n_33,obuf_inst_n_34}),
        .\odata_reg[31] (\odata_reg[31] ),
        .\q0_reg[0] (\q0_reg[0] ),
        .shift_reg0_address0(shift_reg0_address0),
        .shift_reg0_ce0(shift_reg0_ce0),
        .\trunc_ln1_reg_329_reg[0] (\trunc_ln1_reg_329_reg[0] ),
        .x_TVALID_int(x_TVALID_int),
        .y_TREADY(y_TREADY));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_obuf obuf_inst
       (.D({cdata[32:31],cdata[22:0]}),
        .O({p_0_in0,obuf_inst_n_48,obuf_inst_n_49,obuf_inst_n_50}),
        .P(P),
        .Q(\odata_reg[32] ),
        .SR(SR),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .\ireg_reg[0] (\ireg_reg[32] ),
        .mul_ln80_fu_191_p2__2({obuf_inst_n_27,obuf_inst_n_28,obuf_inst_n_29,obuf_inst_n_30}),
        .mul_ln80_fu_191_p2__21(mul_ln80_fu_191_p2__21),
        .mul_ln80_fu_191_p2__2_0({obuf_inst_n_31,obuf_inst_n_32,obuf_inst_n_33,obuf_inst_n_34}),
        .mul_ln80_fu_191_p2__2_1({obuf_inst_n_35,obuf_inst_n_36,obuf_inst_n_37,obuf_inst_n_38}),
        .mul_ln80_fu_191_p2__2_2({obuf_inst_n_39,obuf_inst_n_40,obuf_inst_n_41,obuf_inst_n_42}),
        .mul_ln80_fu_191_p2__2_3({obuf_inst_n_43,obuf_inst_n_44,obuf_inst_n_45,obuf_inst_n_46}),
        .\odata_reg[31]_i_4_0 (\odata_reg[31]_i_4 ),
        .\odata_reg[32]_0 (obuf_inst_n_51),
        .y_TREADY(y_TREADY));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
