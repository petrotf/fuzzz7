# FuzzZ7

### Configuration Initiale

Afin de faire fonctionner notre projet, vous aurez besoin de :
* Une carte Zybo Z7-20
* Le logiciel Vitis 2020.1
* Un câble mini-Jack
* Une source audio (instrument, portable, baladeur, ...)
* Un casque ou des écouteurs
    
Branchez la carte Zybo Z7-20 à votre PC et allumez-la. Branchez votre source audio par l’intermédiaire du câble mini-Jack dans l’entrée *Line In* de la carte. Branchez votre casque ou vos écouteurs dans la sortie *HPH Out* de la carte.
Lancer le logiciel Vitis et cliquez sur *New* puis *Platform Project*.

Donner un nom à ce projet puis cliquez sur *Next*. 
Cliquez alors sur *Browse...* et naviguez dans le répertoire *FuzzZ7_hw* du projet et trouvez le fichier *design_1_wrapper.xsa*. Sélectionnez *standalone* pour *Operating system* et *ps7_cortexa9_0* pour *Processor* et cocher la case *Generate boot components*. Enfin, cliquez sur *Finish*.

Faites un clic droit sur le nom de votre projet et cliquez sur *Build Project* pour compiler. 

Cliquez sur *New* et sélectionnez cette fois *Application Project*. Cliquez ensuite sur *Next*. Sélectionnez la plate-forme que vous venez de compiler, puis cliquez sur *Next*. Donner un nom à l’application puis cliquez sur *Next*. Sélectionnez *standalone_domain* puis cliquez sur *Next*. Sélectionnez *Empty application* et cliquez sur *Finish*. Faites un clic droit sur le dossier *src* de votre application nouvellement créée et sélectionnez *Import Sources...*. Cliquez ici sur *Browse...*, naviguez dans le répertoire du projet et sélectionnez le répertoire *FuzzZ7_src*, puis validez. Cochez la case *src* ainsi que la case *Overwrite existing resources* without warning et finalisez en cliquant sur *Finish*. Faites un clic droit sur le nom du projet nouvellement créé et cliquez sur *Build Project* pour compiler. Il faut maintenant ouvrir un terminal série pour voir les messages de la carte. Cliquez sur *Window* puis *Show view...*. Recherchez *Terminal* et cliquez sur *Open*. Cliquez sur *Open a Terminal* dans la fenêtre ajoutée et renseignez les informations suivantes : 
* Choose Terminal : Serial Terminal
* Port : COM6
* Baud Rate : 115200
* Data Bits : 8
* Parity : None
* Stop Bits : 1
* Flow Control : None
* Timeout (sec) : 5
* Encoding : Default (ISO-8859-1)

Faites un clic droit sur le projet et sélectionnez *Run As* puis *Run Configurations...*.

Faites un clic droit sur *Single Application Debug* pour créer une nouvelle configuration de lancement et cliquer sur *Run*. Félicitations, le code tourne maintenant sur la carte. Un message d’accueil s’affiche dans le terminal série (N’hésitez pas à relancer si le son ne fonctionne pas la première fois).

### Utilisation

Pour activer ou désactiver un filtre, utiliser les interrupteurs. Pour configurer le fonctionnement de chacun des filtres, appuyez sur le *Btn3* pour entrer en mode configuration, puis fiez-vous aux messages qui s’affichent dans le terminal. Notez que ce mode de configuration est accessible à tout moment et ne stoppe pas le fonctionnement normal de la carte.

Le filtre Reverb n’est pour le moment pas encore implémenté. Les trois filtres disponibles actuellement sont paramétrables.
Le filtre FIR possède trois jeux de coefficients entre lesquels vous pouvez jongler dans les paramètres.
Ces trois paramétrages possibles permettent d’obtenir au choix un filtre passe-bas, un filtre passe-haut et un filtre passe-bande.
Le filtre Delay possède un Delay à durée variable. Il est par défaut réglé à 1.16 secondes (sa valeur maximum) mais peut descendre jusqu’à 0.116 secondes minimum par pas de 0.116 secondes. Le filtre de Distorsion possède un Gain et un Threshold variables. Le Gain est par défaut de 2.0 mais peut être augmenté ou diminué par pas de 0.5 avec un minimum de 1.0 et un maximum de 4.0. Le Threshold est par défaut adapté à une entrée type téléphone portable, mais doit être adapté en fonction de l’amplitude du signal. Le Threshold peut être augmenté et diminué par pas de 10000.
