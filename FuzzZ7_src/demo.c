/************************************************************************/
/*   Le code est repris du code du software Digilent en partie          */
/*   Le reste a été modifié par nos soins                               */
/*																		*/
/************************************************************************/
/*	Contributeurs: Lilian BILLOD, Jimmy LEFEVRE					        */
/*																		*/
/************************************************************************/
/*  Description: 														*/
/*																		*/
/*		Ce fichier contient le code principal de notre FuzzZ7    		*/
/*		Le main et les handlers pour les interruptions de l'AF.   		*/
/*		Ainsi que le code de tous les filtres SW						*/
/*																		*/
/************************************************************************/

/************************************************************************/
/*																		*/
/*	demo.c	--	Zybo DMA Demo				 						    */
/*																		*/
/************************************************************************/
/*	Author: Sam Lowe											        */
/*	Copyright 2015, Digilent Inc.										*/
/************************************************************************/
/*  Module Description: 												*/
/*																		*/
/*		This file contains code for running a demonstration of the		*/
/*		DMA audio inputs and outputs on the Zybo.		     			*/
/*																		*/
/*																		*/
/************************************************************************/
/*  Notes:																*/
/*																		*/
/*		None										                    */
/*																		*/
/************************************************************************/
/*  Revision History:													*/
/* 																		*/
/*		9/6/2016(SamL): Created		      								*/
/*																		*/
/************************************************************************/




#include "demo.h"

#include "xparameters.h"
#include "xil_exception.h"
#include "xdebug.h"
#include "xiic.h"
#include "xtime_l.h"

#include "xaudioformatter.h"
#include "audio.h"
#include "userio.h"
#include "iic.h"
#include "xgpio.h"
#include "xi2stx.h"
#include "xi2srx.h"
#include "xllfifo.h"
#include "intc.h"
#include "platform.h"

#ifdef XPAR_INTC_0_DEVICE_ID
#include "xintc.h"
#include "microblaze_sleep.h"
#else
#include "xscugic.h"
#include "sleep.h"
#include "xil_cache.h"
#endif

/************************** Constant Definitions *****************************/

/*
 * Device hardware build related constants.
 */

// Audio constants

// ADC/DAC sampling rate in Hz
#define AUDIO_SAMPLING_RATE	  44100

/* Timeout loop counter for reset
 */

#define TEST_START_VALUE	0x0


/**************************** Type Definitions *******************************/

typedef int32_t b32;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef uint32_t u32;
typedef float f32;
typedef double f64;
typedef uintptr_t uptr;
typedef intptr_t sptr;

/***************** Macros (Inline Functions) Definitions *********************/

#define global static
#define function static

// Permet de choisir entre la Fir en HW ou en SW
#define HW_FIR 0

/************************** Function Prototypes ******************************/
#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif


/************************** Variable Definitions *****************************/
/*
 * Device instance definitions
 */

static XIic Iic;



//
// Interrupt vector table
#ifdef XPAR_INTC_0_DEVICE_ID
const ivt_t ivt[] = {
    //IIC
    {XPAR_AXI_INTC_0_AXI_IIC_0_IIC2INTC_IRPT_INTR, (XInterruptHandler)XIic_InterruptHandler, &Iic}
};

#endif


// Timeout loop counter for reset
#define RESET_TIMEOUT_COUNTER	10000


global volatile b32 GlobalTransferError;
global volatile b32 GlobalTXDone;
global volatile b32 GlobalRXDone;


global XScuGic Intc;
global XI2s_Tx I2sTx;
global XI2s_Rx I2sRx;
global XLlFifo FirFifoInstance;
global XAudioFormatter AudioFormatter;

Filter_Params filter_params;
Filter_Type filter_order[MAX_FILTER_NB];

#define bool32 int

global s32 S2MMPeriodNo;
global s32 MM2SPeriodNo;
global s32 DelayPeriodNo;

#define I2S_RX_TIME_OUT 500000
#define I2S_TX_TIME_OUT 500000

#define AF_FS		AUDIO_SAMPLING_RATE /* kHz */
#define AF_MCLK		(768 * AF_FS)
#define AF_S2MM_TIMEOUT 0x80000000

#define AUDIO_IN_BUFFER_BASE 0x20000000
#define DELAY_BUFFER_BASE 0x30000000
#define DELAY_PERIODS 200

#define PERIODS 8
#define BYTES_PER_PERIOD 256

#define AUDIO_OUT_BUFFER_BASE (AUDIO_IN_BUFFER_BASE+PERIODS*BYTES_PER_PERIOD)
#define AUDIO_TRANSITION_BUFFER_1_BASE (AUDIO_IN_BUFFER_BASE+2*PERIODS*BYTES_PER_PERIOD)
#define AUDIO_TRANSITION_BUFFER_2_BASE (AUDIO_IN_BUFFER_BASE+3*PERIODS*BYTES_PER_PERIOD)

#define COEF_NB 31


XAudioFormatterHwParams af_hw_params = {
		.buf_addr=AUDIO_IN_BUFFER_BASE,
		.active_ch=2,
		.bits_per_sample=BIT_DEPTH_24,
		.periods=PERIODS,
		.bytes_per_period=BYTES_PER_PERIOD};

XAudioFormatterHwParams af_hw_params_MM2S = {
		.buf_addr=AUDIO_OUT_BUFFER_BASE,
		.active_ch=2,
		.bits_per_sample=BIT_DEPTH_24,
		.periods=PERIODS,
		.bytes_per_period=BYTES_PER_PERIOD};


/**
 *
 * Handler d'interruption du la partie réception de l'audio Formatter
 * On lance le filtrage des échantillons qui viennent d'arriver
 * On fait du double buffering pour pouvoir chainer les filtres
 * Le résultat est copié dans le buffer de sortie
 *
*******************************************************************************/

void XS2MMAFCallback(void *data)
{
	// Il faut invalider le cache car la DMA vient de modifier le contenu de la mémoire
	Xil_DCacheInvalidateRange(AUDIO_IN_BUFFER_BASE,BYTES_PER_PERIOD*PERIODS); 
	s32 filtersDone = 0;
	s32 i = 0;
	void * currentBufferBase = AUDIO_IN_BUFFER_BASE;
	void * nextBufferBase = AUDIO_TRANSITION_BUFFER_1_BASE;
	if (filter_params.numberOfActiveFilters) {
		while (filtersDone < filter_params.numberOfActiveFilters && i < MAX_FILTER_NB) {
			if (filter_order[i] == No_Filter) {

			}
			else {
				if (filtersDone == filter_params.numberOfActiveFilters - 1) {
					nextBufferBase = AUDIO_OUT_BUFFER_BASE;
					filter(nextBufferBase+S2MMPeriodNo*BYTES_PER_PERIOD,
						currentBufferBase+S2MMPeriodNo*BYTES_PER_PERIOD,
						BYTES_PER_PERIOD,filter_order[i]);
				}
				else {
					filter(nextBufferBase+S2MMPeriodNo*BYTES_PER_PERIOD,
						currentBufferBase+S2MMPeriodNo*BYTES_PER_PERIOD,
						BYTES_PER_PERIOD,filter_order[i]);

					// Gestion du double buffering
					if (nextBufferBase  == AUDIO_TRANSITION_BUFFER_1_BASE) {
						nextBufferBase = AUDIO_TRANSITION_BUFFER_2_BASE;
						currentBufferBase = AUDIO_TRANSITION_BUFFER_1_BASE;
					}
					else {
						nextBufferBase = AUDIO_TRANSITION_BUFFER_1_BASE;
						currentBufferBase = AUDIO_TRANSITION_BUFFER_2_BASE;
					}
				}
				filtersDone++;
			}
			i++;
		}
	}
	else {
		nextBufferBase = AUDIO_OUT_BUFFER_BASE;
		filter(nextBufferBase+S2MMPeriodNo*BYTES_PER_PERIOD,
				currentBufferBase+S2MMPeriodNo*BYTES_PER_PERIOD,
				BYTES_PER_PERIOD,No_Filter);
	}

	// On copie les échantillons qui viennent d'arriver dans le buffer de Delay.
	memcpy(DELAY_BUFFER_BASE+DelayPeriodNo*PERIODS*BYTES_PER_PERIOD+S2MMPeriodNo*BYTES_PER_PERIOD,
			af_hw_params.buf_addr+S2MMPeriodNo*BYTES_PER_PERIOD,
			BYTES_PER_PERIOD);
		
	if (S2MMPeriodNo == PERIODS-1) {
		DelayPeriodNo = (DelayPeriodNo + 1)%DELAY_PERIODS;
	}
	// Le buffer de sortie vient d'être modifié, il faut flush le contenu du cache pour que la DMA puisse accéder aux modifications
	Xil_DCacheFlushRange(af_hw_params_MM2S.buf_addr+S2MMPeriodNo*BYTES_PER_PERIOD,
						BYTES_PER_PERIOD);
	S2MMPeriodNo = (S2MMPeriodNo+1)%PERIODS;
}

global f64 c_HP[] = {-0.1160,-0.0189,-0.0202,-0.0215,-0.0228,-0.0241,-0.0253,-0.0264,-0.0274,-0.0283,-0.0291,-0.0298,-0.0304,-0.0309,-0.0311,-0.0313,0.9686,-0.0313,-0.0311,-0.0309,-0.0304,-0.0298,-0.0291,-0.0283,-0.0274,-0.0264,-0.0253,-0.0241,-0.0228,-0.0215,-0.0202,-0.0189,-0.1160};
global f64 c_LP[] = {0.146898, 0.012694, 0.013225, 0.013608, 0.014001, 0.014365, 0.014687, 0.015018, 0.015291, 0.015577, 0.015791, 0.016015, 0.016153, 0.016294, 0.016342, 0.016390, 0.016342, 0.016294, 0.016153, 0.016015, 0.015791, 0.015577, 0.015291, 0.015018, 0.014687, 0.014365, 0.014001, 0.013608, 0.013225, 0.012694, 0.146898};
global f64 c_BP[] = {-0.250936, -0.036102, -0.033610, -0.034389, -0.028177, -0.026395, -0.016387, -0.012988, 0.000561, 0.003071, 0.020918, 0.018049, 0.050045, 0.028620, 0.257586, 0.587996, 0.257586, 0.028620, 0.050045, 0.018049, 0.020918, 0.003071, 0.000561, -0.012988, -0.016387, -0.026395, -0.028177, -0.034389, -0.033610, -0.036102, -0.250936};
global s32 shift_regL[COEF_NB];
global s32 shift_regR[COEF_NB];

// Filtres

s32 distortion(s32 sample) {
    s32 absSample = abs(sample);
    s32 signSample = (s32)((float) sample / absSample);
    f64 over = absSample - (filter_params.Threshold/filter_params.Gain);
    if (sample > filter_params.Threshold) sample = filter_params.Threshold;
    else if (sample < -filter_params.Threshold) sample = -filter_params.Threshold;
    else if (over > 0){
        f64 ratio = over / (filter_params.Threshold - filter_params.Threshold/filter_params.Gain);
        sample = (0.9*filter_params.Threshold + ratio*0.1*filter_params.Threshold)* signSample;
    }
    else sample= sample*filter_params.Gain*0.9;
    return sample;
}

s32 fir (s32 shift_reg[COEF_NB],s32 x,f64 c[COEF_NB]) {
  f64 acc;
  s32 data;
  s32 i;

  acc=0.0;
  for (i=COEF_NB-1;i>=0;i--) {
	if (i==0) {
		shift_reg[0]=x;
     	data = x;
    } else {
			shift_reg[i]=shift_reg[i-1];
			data = shift_reg[i];
    }
    acc+=data*c[i];
  }
  return ((s32)(acc));
}


// Cette fonction copie n bytes de src vers dest en réalisant le filtre spécifié dans filterType
void filter(void * dest, void * src, s32 n, Filter_Type filterType) {

	if (filterType == No_Filter) {
		memcpy(dest,src,n);
		return;
	}

	s32 current_sample;
	s32 delayed_sample;
	f64 * coeffs;

	switch (filterType) {
		case (FIR_Filter) :
			switch (filter_params.firFilterType) {
				case (LP_FILTER) :
					coeffs=c_LP;
					break;
				case (HP_FILTER) :
					coeffs=c_HP;
					break;
				case (BP_FILTER) :
					coeffs=c_BP;
					break;
				default:
					break;
			}
#if HW_FIR
			for (s32 i = 0;i<n/4;i+=2) {
				 current_sample = (*((s32*)(src)+i));
				 if (current_sample & 0x00800000) {
					 current_sample = current_sample | 0xFF000000;
				 }

				XLlFifo_TxPutWord(&FirFifoInstance, current_sample);

				 current_sample = *((s32*)(src)+i+1);
				 if (current_sample & 0x00800000) {
				 			 current_sample = current_sample | 0xFF000000;
				 		 }
				 XLlFifo_TxPutWord(&FirFifoInstance, current_sample);
			}
			XLlFifo_iTxSetLen(&FirFifoInstance, 256);
			for (s32 i = 0; i < n/4; i+=2) {
				while((XLlFifo_iRxOccupancy(&FirFifoInstance))<2) {}
				u32 processed_sample = XLlFifo_RxGetWord(&FirFifoInstance);
				*(((s32*)(dest))+i) =0x00FFFFFF & processed_sample;
				processed_sample = XLlFifo_RxGetWord(&FirFifoInstance);
				*(((s32*)(dest))+i+1)=0x00FFFFFF & processed_sample;
			}
#else
			for (s32 i = 0;i<n/4;i+=2) {
							 current_sample = (*((s32*)(src)+i));
							 if (current_sample & 0x00800000) {
								 current_sample = current_sample | 0xFF000000;
							 }

							 *(((s32*)(dest))+i) =0x00FFFFFF & fir(shift_regL,current_sample,coeffs);

							 current_sample = *((s32*)(src)+i+1);
							 if (current_sample & 0x00800000) {
							 			 current_sample = current_sample | 0xFF000000;
							 		 }
							 *(((s32*)(dest))+i+1) =0x00FFFFFF & fir(shift_regR,current_sample,coeffs);
						}
#endif
			break;
		case (Delay_Filter) :
			for (s32 i = 0;i<n/4;i+=2) {
				 current_sample = (*((s32*)(src)+i));
				 if (current_sample & 0x00800000) {
					 current_sample = current_sample | 0xFF000000;
				 }
				 delayed_sample = (*((s32*)(DELAY_BUFFER_BASE+((((DelayPeriodNo+(DELAY_PERIODS/10)*(filter_params.DelayAmount))%DELAY_PERIODS)*PERIODS+S2MMPeriodNo)*BYTES_PER_PERIOD))+i));
				 if (delayed_sample & 0x00800000) {
					 delayed_sample = delayed_sample | 0xFF000000;
				 }

				 *(((s32*)(dest))+i)=0x00FFFFFF & ((current_sample + delayed_sample/2));

				 current_sample = *((s32*)(src)+i+1);
				 if (current_sample & 0x00800000) {
				 			 current_sample = current_sample | 0xFF000000;
				 		 }
				 delayed_sample = (*((s32*)(DELAY_BUFFER_BASE+((((DelayPeriodNo+(DELAY_PERIODS/10)*(filter_params.DelayAmount))%DELAY_PERIODS)*PERIODS+S2MMPeriodNo)*BYTES_PER_PERIOD))+i+1));
				 if (delayed_sample & 0x00800000) {
					 delayed_sample = delayed_sample | 0xFF000000;
				 }
				 *(((s32*)(dest))+i+1)=0x00FFFFFF & ((current_sample + delayed_sample/2));
			}
				break;
		case (Reverb_Filter) :
			// Not implemented yet
			memcpy(dest,src,n);
				break;
		case (Disto_Filter) :
				for (s32 i = 0;i<n/4;i+=2) {
						 current_sample = (*((s32*)(src)+i));
						 if (current_sample & 0x00800000) {
							 current_sample = current_sample | 0xFF000000;
						 }
						 *(((s32*)(dest))+i) =0x00FFFFFF & distortion(current_sample);

						 current_sample = *((s32*)(src)+i+1);
						 if (current_sample & 0x00800000) {
						 			 current_sample = current_sample | 0xFF000000;
						 		 }
						*(((s32*)(dest))+i+1)=0x00FFFFFF & distortion(current_sample);
				}
				break;
		default:
			xil_printf("Default case error\r\n");
			break;
	}
}

/*****************************************************************************/
/**
 *
 * This function is called from the interrupt handler of audio formatter core.
 * After the first MM2S interrupt is received the interrupt_flag is set here.
 *
 * @return
 *
 * @note	This function assumes a Microblaze or ARM system and no
 *	operating system is used.
 *
*******************************************************************************/
void XMM2SAFCallback(void *data)
{
	MM2SPeriodNo = (MM2SPeriodNo+1)%PERIODS;
}

#define AF_DEVICE_ID XPAR_XAUDIOFORMATTER_0_DEVICE_ID
#define AF_S2MM_INTERRUPT_ID XPAR_FABRIC_AUDIO_FORMATTER_0_IRQ_S2MM_INTR
#define AF_MM2S_INTERRUPT_ID XPAR_FABRIC_AUDIO_FORMATTER_0_IRQ_MM2S_INTR

// Initialisation de l'AF
u32 InitializeAudioFormatter(XAudioFormatter *AFInstancePtr)
{
	u32 Status;
	/*
	 * Lookup and Initialize the audio formatter so that it's ready to use.
	 */
	Status = XAudioFormatter_Initialize(AFInstancePtr, AF_DEVICE_ID);
	if (Status != XST_SUCCESS)
		return XST_FAILURE;

	if (AFInstancePtr->s2mm_presence == 1) {
		Status = XScuGic_Connect(&Intc, AF_S2MM_INTERRUPT_ID,
			(XInterruptHandler)XAudioFormatterS2MMIntrHandler,
			(void *)AFInstancePtr);
		if (Status == XST_SUCCESS) {
			XScuGic_Enable(&Intc, AF_S2MM_INTERRUPT_ID);
		} else {
			xil_printf("Failed to register AF interrupt handler");
			return XST_FAILURE;
		}
		AFInstancePtr->ChannelId = XAudioFormatter_S2MM;
		XAudioFormatter_SetS2MMCallback(AFInstancePtr,
			XAudioFormatter_IOC_Handler, XS2MMAFCallback,
			(void *)AFInstancePtr);
		XAudioFormatter_InterruptEnable(AFInstancePtr,
			XAUD_CTRL_IOC_IRQ_MASK);
		XAudioFormatterSetS2MMTimeOut(AFInstancePtr, AF_S2MM_TIMEOUT);
		XAudioFormatterSetHwParams(AFInstancePtr, &af_hw_params);
		XAudioFormatterDMAStart(AFInstancePtr);
	}
	if (AFInstancePtr->mm2s_presence == 1) {
		Status = XScuGic_Connect(&Intc, AF_MM2S_INTERRUPT_ID,
			(XInterruptHandler)XAudioFormatterMM2SIntrHandler,
			(void *)AFInstancePtr);
		if (Status == XST_SUCCESS) {
			XScuGic_Enable(&Intc, AF_MM2S_INTERRUPT_ID);
		} else {
			xil_printf("Failed to register AF interrupt handler");
			return XST_FAILURE;
		}
		AFInstancePtr->ChannelId = XAudioFormatter_MM2S;
		XAudioFormatter_SetMM2SCallback(AFInstancePtr,
			XAudioFormatter_IOC_Handler, XMM2SAFCallback,
			(void *)AFInstancePtr);
		XAudioFormatter_InterruptEnable(AFInstancePtr,
			XAUD_CTRL_IOC_IRQ_MASK);
		XAudioFormatterSetFsMultiplier(AFInstancePtr, AF_MCLK, AF_FS);
		XAudioFormatterSetHwParams(AFInstancePtr, &af_hw_params_MM2S);
		XAudioFormatterDMAStart(AFInstancePtr);
	}
	return Status;
}

static s32 GPIOINT;

/*****************************************************************************/
/**
*
* Fonction Main
*
* Cette fonction initialise tous les composants du système.
* Elle met à zéro tous les buffers
* Elle renseigne les paramètres de base des filtres
* Elle affiche le message d'accueil
*
******************************************************************************/
int main(void)
{
    s32 Status;
    
    init_platform();

    filter_params.DelayAmount=1;
    filter_params.Gain = 2.0;
    filter_params.Threshold = 80000;

    // Reset all buffers
    s32 * buffer = (s32*)AUDIO_IN_BUFFER_BASE;
    memset(buffer,0,BYTES_PER_PERIOD*PERIODS*4);
    memset(DELAY_BUFFER_BASE,0,BYTES_PER_PERIOD*PERIODS*DELAY_PERIODS);
    Xil_DCacheFlushRange(AUDIO_IN_BUFFER_BASE, 4*BYTES_PER_PERIOD*PERIODS);

    /* IO initialization */
    XGpio input,output;

    s32 button_data = 0;
    s32 switch_data = 0;

    XGpio_Initialize(&input,XPAR_AXI_GPIO_0_DEVICE_ID);
    XGpio_Initialize(&output,XPAR_AXI_GPIO_1_DEVICE_ID);

    XGpio_SetDataDirection(&input,1,0xF);
    XGpio_SetDataDirection(&input,2,0xF);

    XGpio_SetDataDirection(&output,1,0x0);

    s32 swState = XGpio_DiscreteRead(&input,2);
    checkSwState(swState);

	// Fifo initialization
	XLlFifo_Config *Config;
	Config = XLlFfio_LookupConfig(XPAR_AXI_FIFO_0_DEVICE_ID);
	XLlFifo_CfgInitialize(&FirFifoInstance, Config, Config->BaseAddress);

	XGpio_InterruptEnable(&input, 0x3);
    XGpio_InterruptGlobalEnable(&input);

    /* Initialize IRQ */

    xil_printf("Initializing ...\r\n");

    Status = SetupInterruptSystem(&Intc);
      	if (Status == XST_FAILURE) {
    	    xil_printf("IRQ init failed.\n\r");
    	    return XST_FAILURE;
    	}

    Status = XScuGic_Connect(&Intc, XPAR_FABRIC_AXI_GPIO_0_IP2INTC_IRPT_INTR,
      						(XInterruptHandler)UserIOHandler,
      						(void *)(&input));
    if (Status == XST_FAILURE) {
      	xil_printf("\nXGPIO Intr Init Failed\n");
      	return XST_FAILURE;
    }
    else {
      	XScuGic_Enable(&Intc, XPAR_FABRIC_AXI_GPIO_0_IP2INTC_IRPT_INTR);
    }

    Status = InitializeAudioFormatter(&AudioFormatter);
    if (Status == XST_FAILURE) {
    	xil_printf("\nAudio Formatter Init failed\n");
    	return XST_FAILURE;
    }

 	Xil_ExceptionEnable();
    
    // Initialize IIC controller
    Status = InitIic(&Iic);
    if(Status != XST_SUCCESS) {
        xil_printf("Error initializing I2C controller");
        return XST_FAILURE;
    }
    
    //Initialize Audio I2S
    Status = InitAudio(&I2sRx,&I2sTx);
    if(Status != XST_SUCCESS) {
        xil_printf("Audio initializing ERROR");
        return XST_FAILURE;
    }

    xil_printf("----------------------------------------------------------\r\n");
    xil_printf("Zybo Z7-20 Fir by HLS Audio RT Demo\r\n");
    xil_printf("----------------------------------------------------------\r\n");
    while (1) {}

    return XST_SUCCESS;
}
