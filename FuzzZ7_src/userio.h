#define bool32 int

typedef enum {
    IDLE,
    FIR_CHOICE,
    DELAY_CHOICE,
    REVERB_CHOICE,
    DISTO_CHOICE,
    LP_CHOICE,
    HP_CHOICE,
    BP_CHOICE,
    DELAY_PLUS,
    DELAY_MINUS,
	GAIN_CHOICE,
	THRESHOLD_CHOICE,
	GAIN_PLUS,
	GAIN_MINUS,
	THRESHOLD_PLUS,
	THRESHOLD_MINUS
} UserIO_State;

void UserIOHandler(void * data);
void checkSwState(int swState);
