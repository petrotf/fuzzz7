#include "userio.h"
#include "demo.h"
#include "xgpio.h"

#define global static
#define b32 int
#define s32 int

global s32 swState;
global s32 btnState;

b32 DelayUp;
b32 ReverbUp;
b32 DistoUp;
b32 FIRUp;

b32 ConfigStartDown;
b32 NextDown;
b32 ExitDown;
b32 EnterDown;

extern Filter_Params filter_params;
extern Filter_Type filter_order[MAX_FILTER_NB];
global UserIO_State userIO_State;

// Vérifie l'état actuel des interrupteurs
void checkSwState(int swState) {
    int numberOfActiveFilters = 0;

    FIRUp = swState & 0b1000;
    if (FIRUp) {
        filter_order[1] = FIR_Filter;
        numberOfActiveFilters++;
    } else {
        filter_order[1] = No_Filter;
    }
    
    DelayUp = swState & 0b0100;
    if (DelayUp) {
        filter_order[0] = Delay_Filter;
        numberOfActiveFilters++;
    } else {
        filter_order[0] = No_Filter;
    }
    
    ReverbUp = swState & 0b0010;
    if (ReverbUp) {
        filter_order[2] = Reverb_Filter;
        numberOfActiveFilters++;
    } else {
        filter_order[2] = No_Filter;
    }
    
    DistoUp = swState & 0b0001;
    if (DistoUp) {
        filter_order[3] = Disto_Filter;
        numberOfActiveFilters++;
    } else {
        filter_order[3] = No_Filter;
    }

    filter_params.numberOfActiveFilters = numberOfActiveFilters;
}

// Machine à états pour la configuration des filtres
void UserIOHandler(void * data) {

	XGpio * input =(XGpio*)data;

	XGpio_InterruptGlobalDisable(input);
	XGpio_InterruptGetStatus(input);
	XGpio_InterruptClear(input, 0x3);

	swState = XGpio_DiscreteRead(input,2);
    btnState = XGpio_DiscreteRead(input,1);

    checkSwState(swState);
    
    ConfigStartDown = btnState & 0b1000;
    NextDown = btnState & 0b0100;
    ExitDown = btnState & 0b0010;
    EnterDown = btnState & 0b0001;

    switch (userIO_State) {
        case (IDLE):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Enabled \r\n");
            	xil_printf("Config FIR ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State=FIR_CHOICE;
            }
            break;
        case (FIR_CHOICE):
            if (ConfigStartDown || ExitDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State=IDLE;
            }
            else if (EnterDown) {
            	xil_printf("Set FIR Filter to LP ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = LP_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Config Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = DELAY_CHOICE;
            }
            break;
        case (DELAY_CHOICE):
            if (ConfigStartDown || ExitDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State=IDLE;
            }
            else if (EnterDown) {
            	xil_printf("Increase Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State=DELAY_PLUS;
            }
            else if (NextDown) {
            	xil_printf("Config Reverb ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State=REVERB_CHOICE;
            }
            break;
        case (REVERB_CHOICE):
            if (ConfigStartDown || ExitDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (NextDown) {
            	xil_printf("Config Disto ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = DISTO_CHOICE;
            }
            else if (EnterDown) {
            	xil_printf("Not Yet Implemented\r\n");
            	xil_printf("Config Reverb ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            }
            break;
        case (DISTO_CHOICE):
            if (ConfigStartDown || ExitDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (NextDown) {
            	xil_printf("Config FIR ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = FIR_CHOICE;
            }
            else if (EnterDown) {
            	xil_printf("Configure Gain Amount ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            	userIO_State = GAIN_CHOICE;
            }
            break;
        case (LP_CHOICE):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Config FIR ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = FIR_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Set FIR Filter to HP ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = HP_CHOICE;
            }
            else if (EnterDown) {
            	xil_printf("LP Filter Set !\r\n");
            	xil_printf("Set FIR Filter to LP ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                filter_params.firFilterType = LP_FILTER;
            }
            break;
        case (HP_CHOICE):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Config FIR ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = FIR_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Set FIR Filter to BP ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = BP_CHOICE;
            }
            else if (EnterDown) {
            	xil_printf("HP Filter Set !\r\n");
            	xil_printf("Set FIR Filter to HP ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                filter_params.firFilterType = HP_FILTER;
            }
            break;
        case (BP_CHOICE):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Config FIR ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = FIR_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Set FIR Filter to LP ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = LP_CHOICE;
            }
            else if (EnterDown) {
            	xil_printf("BP Filter Set !\r\n");
            	xil_printf("Set FIR Filter to BP ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                filter_params.firFilterType = BP_FILTER;
            }
            break;
        case (DELAY_PLUS):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Config Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = DELAY_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Decrease Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = DELAY_MINUS;
            }
            else if (EnterDown) {

                if (filter_params.DelayAmount > 1) {
                    filter_params.DelayAmount--;
                    xil_printf("Delay Increased \r\n");
                } else {
                	xil_printf("Delay Already at Max Level\r\n");
                }
                xil_printf("Increase Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            }
            break;
        case (DELAY_MINUS):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Config Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = DELAY_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Increase Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = DELAY_PLUS;
            }
            else if (EnterDown) {
                if (filter_params.DelayAmount < 9) {
                    filter_params.DelayAmount++;
                    xil_printf("Delay Decreased \r\n");
                }
                else {
                	xil_printf("Delay Already at Min Level\r\n");
                }
                xil_printf("Decrease Delay ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            }
            break;
        case (GAIN_CHOICE):
                    if (ConfigStartDown) {
                    	xil_printf("Config Mode Disabled \r\n");
                        userIO_State = IDLE;
                    }
                    else if (ExitDown) {
                    	xil_printf("Config Disto ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                        userIO_State = DISTO_CHOICE;
                    }
                    else if (NextDown) {
                    	xil_printf("Configure Threshold Amount ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                        userIO_State = THRESHOLD_CHOICE;
                    }
                    else if (EnterDown) {
                        xil_printf("Increase Gain ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                        userIO_State = GAIN_PLUS;
                    }
                    break;
        case (THRESHOLD_CHOICE):
                    if (ConfigStartDown) {
                    	xil_printf("Config Mode Disabled \r\n");
                        userIO_State = IDLE;
                    }
                    else if (ExitDown) {
                      	xil_printf("Config Disto ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                        userIO_State = DISTO_CHOICE;
                    }
                    else if (NextDown) {
                       	xil_printf("Configure Gain Amount ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                        userIO_State = GAIN_CHOICE;
                    }
                    else if (EnterDown) {
                        xil_printf("Increase Threshold ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                        userIO_State = THRESHOLD_PLUS;
                    }
                    break;
        case (GAIN_PLUS):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Configure Gain Amount ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = GAIN_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Decrease Gain ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = GAIN_MINUS;
            }
            else if (EnterDown) {

                if (filter_params.Gain < 4) {
                    filter_params.Gain += 0.5;
                    xil_printf("Gain Increased \r\n");
                } else {
                	xil_printf("Gain Already at Max Level\r\n");
                }
                xil_printf("Increase Gain ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            }
            break;
        case (GAIN_MINUS):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Configure Gain Amount ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = GAIN_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Increase Gain ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = GAIN_PLUS;
            }
            else if (EnterDown) {
                if (filter_params.Gain > 1) {
                    filter_params.Gain -= 0.5;
                    xil_printf("Gain Decreased \r\n");
                }
                else {
                	xil_printf("Gain Already at Min Level\r\n");
                }
                xil_printf("Decrease Gain ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            }
            break;
        case (THRESHOLD_PLUS):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Configure Threshold Amount ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = THRESHOLD_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Decrease Threshold ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = THRESHOLD_MINUS;
            }
            else if (EnterDown) {

                if (filter_params.Threshold < 800000) {
                    filter_params.Threshold += 10000;
                    xil_printf("Threshold Increased to %d\r\n", filter_params.Threshold);
                } else {
                	xil_printf("Threshold Already at Max Level\r\n");
                }
                xil_printf("Increase Threshold ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            }
            break;
        case (THRESHOLD_MINUS):
            if (ConfigStartDown) {
            	xil_printf("Config Mode Disabled \r\n");
                userIO_State = IDLE;
            }
            else if (ExitDown) {
            	xil_printf("Configure Threshold Amount ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = THRESHOLD_CHOICE;
            }
            else if (NextDown) {
            	xil_printf("Increase Threshold ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
                userIO_State = THRESHOLD_PLUS;
            }
            else if (EnterDown) {
                if (filter_params.Threshold > 10000) {
                    filter_params.Threshold -= 10000;
                    xil_printf("Threshold Decreased to %d\r\n", filter_params.Threshold);
                }
                else {
                	xil_printf("Threshold Already at Min Level\r\n");
                }
                xil_printf("Decrease Threshold ? 1.Config 2.Next 3.Exit 4.Enter \r\n");
            }
            break;
        default:
            xil_printf("ERREUR ETAT INTERNE \r\n");
            break;
    }
    XGpio_InterruptGlobalEnable(input);
}
