#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "assets.h"
#include "FIR.h"

static Sound_Asset load_wav(string loaded) {

    Sound_Asset result = {};

    s16 *read_data = 0;
    //string loaded = os_platform.read_entire_file( name );
    WAV_File_Header *file_header = (WAV_File_Header *) loaded.data;

    assert((file_header->chunk_id == WAV_ID_RIFF) &&
           (file_header->wave_id == WAV_ID_WAVE));

    WAV_Reader reader;
    for (reader.at = (u8 *) (file_header + 1);
         reader.at < (((u8 *) (file_header + 1)) + file_header->chunk_size - 4);
         reader.at += (((reader.h->size + 1) & ~1) + sizeof(WAV_Chunk_Header))) {

        if (reader.h->id == WAV_ID_FMT) {
            assert((reader.h->id == WAV_ID_FMT) &&
                   ((reader.h->size == 16) ||
                    (reader.h->size == 18) ||
                    (reader.h->size == 40)));

            WAV_Fmt *format = (WAV_Fmt *) (reader.h + 1);

            assert((format->format_tag == 1) || // PCM...
                   ((format->format_tag == 0xFFFE) && (format->sub_format[0] == 1)));
            assert(format->bits_per_sample == 16); // ...ie. signed 16-bit.
            assert(format->data_block_byte_count == (format->channel_count * 2));

            result.channelCount = format->channel_count;
        } else if (reader.h->id == WAV_ID_DATA) {
            read_data = (s16 *) (reader.h + 1);
            result.samplesPerChannel = reader.h->size / (result.channelCount * sizeof(s16));
        }
    }
    assert(read_data && (result.channelCount < 3));
    result.samples = read_data;


    return result;
}


#define PI 3.14159265359

//Choice of the FIR filter to use
#define FILTER HP1200C255

#define FILTER_SIZE sizeof(FILTER)/sizeof(FILTER[0])

int16_t *filterFIFO;

// number of samples to read per loop
#define SAMPLES_TOTAL 44100

// Parameters to generate a sinusoidal signal
#define SIGNAL_FREQUENCY 400
#define SIGNAL_AMPLITUDE 4000

// FIR Filter function
void firFilter(int16_t *samples, double *coeffs, int16_t *output) {
    *output = 0;
    double tempOutput = *output;
    for (int i = 0; i < FILTER_SIZE; ++i) {
        tempOutput += samples[FILTER_SIZE - 1 - i] * coeffs[i];
    }
    *output = (int16_t) tempOutput;
}

// FIR FIlter to use with wav sample (can be adapted but is the same as firFilter right now)
void firFilterWav(int16_t *samples, double *coeffs, int16_t *output) {
    *output = 0;
    double tempOutput = *output;
    for (int i = 0; i < FILTER_SIZE; ++i) {
        tempOutput += samples[FILTER_SIZE - 1 - i] * coeffs[i];
    }
    *output = (int16_t) tempOutput;
}

// Distortion filter with only 'hard-clipping'
void distortionFilterWav(int16_t *samples, int16_t *output) {
    int16_t sample = samples[0];
    float GAIN = 2.0;
    int threshold = 6000;
    //Hard Clipping
    sample = sample * GAIN;
    if (sample > threshold) sample = threshold;
    else if (sample < -threshold) sample = -threshold;
    *output = (int16_t) sample/GAIN;
}

// Hybrid Distortion filter using hard and soft clipping
void distortionFilterWav2(int16_t *samples, int16_t *output) {
    int16_t sample = samples[0];
    float GAIN = 2.0;
    int threshold = 1000;
    int16_t absSample = abs(sample);
    int16_t signSample = (int16_t)((float) sample / absSample);
    double over = absSample - (threshold/GAIN);

    if (sample > threshold) sample = threshold;
    else if (sample < -threshold) sample = -threshold;
    else if (over > 0){
        double ratio = over / (threshold - threshold/GAIN);
        sample = (0.9*threshold + ratio*0.1*threshold)* signSample;
    }
    else sample= sample*GAIN*0.9;
    *output = (int16_t) sample;
}


string readWavFile(FILE *input_wav) {

    input_wav = fopen("flute.wav", "rb");

    if (input_wav == 0) {
        printf("couldn't open sample.wav");
        exit(EXIT_FAILURE);
    }

    fseek(input_wav, 0, SEEK_END);
    u32 fsize = ftell(input_wav);
    fseek(input_wav, 0, SEEK_SET);  /* same as rewind(f); */

    uint8_t *data = malloc(fsize);

    fread(data, 1, fsize, input_wav);

    fclose(input_wav);

    string wavData;
    wavData.data = data;
    wavData.length = fsize;
    return wavData;
}


int main(void) {

    FILE *input_fid;
    FILE *output_fid;
    // open the input waveform file
    // Only used in this program to store the generated waveform or translated wav sample
    input_fid = fopen("input.pcm", "wb");
    if (input_fid == 0) {
        printf("couldn't open input.pcm");
        exit(EXIT_FAILURE);
    }

    // open the output waveform file
    // Store the filtered signal
    output_fid = fopen("outputFixed.pcm", "wb");
    if (output_fid == 0) {
        printf("couldn't open outputFixed.pcm");
        exit(EXIT_FAILURE);
    }


    int16_t input[SAMPLES_TOTAL];
    // Generation of the sinusoide as an array of SAMPLES_TOTAL length
    for (int i = 0; i < SAMPLES_TOTAL; i++) {
        input[i] = SIGNAL_AMPLITUDE * sin(2 * PI * SIGNAL_FREQUENCY * i / SAMPLES_TOTAL);
    }

    // Opening and reading of the wav file sample
    FILE *input_wav;
    string wavData = readWavFile(input_wav);
    Sound_Asset sound = load_wav(wavData);
    printf("Samples : %d\t, Channels %d\n", sound.samplesPerChannel, sound.channelCount);
    int16_t output[sound.samplesPerChannel - FILTER_SIZE + 1];

    //Sin Filtering Loop
    /*
    for (int i = 0; i < sound.samplesPerChannel - FILTER_SIZE + 1; i++) {
        filterFIFO = &(sound.samples[sound.samplesPerChannel - FILTER_SIZE - i]);
        firFilter(filterFIFO, FILTER, &(output[i]));
    }
    */

    //Wav Filtering Loop
    for (int i = 0; i < sound.samplesPerChannel - FILTER_SIZE + 1; i++) {
        filterFIFO = &(sound.samples[i]);
        //firFilterWav(filterFIFO, FILTER, &(output[i]));
        distortionFilterWav2(filterFIFO, &(output[i]));
    }

    // Saving the input signal
    fwrite(sound.samples, 2, sound.samplesPerChannel, input_fid);
    // Saving the output signal
    fwrite(output, 2, sound.samplesPerChannel - FILTER_SIZE + 1, output_fid);

    fclose(input_fid);
    fclose(output_fid);

    return 0;
}