
#define FOUR_BYTES_TO_U32(a, b, c, d) (((u32)a << 0) | ((u32)b << 8) | ((u32)c << 16) | ((u32)d << 24))

typedef uint8_t u8;
typedef size_t usize;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;

typedef struct {
    u8 *data;
    usize length;
} string;


enum Asset_UID {
    ASSET_UID_player_mesh,
    ASSET_UID_ball_mesh,
    ASSET_UID_partner_mesh,

    ASSET_UID_COUNT,
};

//
// WAV
//

typedef struct {
    u32 chunk_id;
    u32 chunk_size;
    u32 wave_id;
} WAV_File_Header;

enum WAV_Chunk_ID {
    WAV_ID_RIFF = FOUR_BYTES_TO_U32('R', 'I', 'F', 'F'),
    WAV_ID_WAVE = FOUR_BYTES_TO_U32('W', 'A', 'V', 'E'),
    WAV_ID_FMT = FOUR_BYTES_TO_U32('f', 'm', 't', ' '),
    WAV_ID_DATA = FOUR_BYTES_TO_U32('d', 'a', 't', 'a'),
};

typedef struct {
    u16 format_tag;
    u16 channel_count;
    u32 samples_per_sec;
    u32 avg_bytes_per_sec;
    u16 data_block_byte_count;
    u16 bits_per_sample;
    u16 extension_size;
    u16 valid_bits_per_sample;
    u32 channel_mask;
    u8 sub_format[16];
} WAV_Fmt;

typedef struct {
    u32 id;
    u32 size;
} WAV_Chunk_Header;

typedef union {
    u8 *at;
    WAV_Chunk_Header *h;
} WAV_Reader;

//
// Internal assets
//

typedef struct {
    s16 *samples;
    u32 samplesPerChannel;
    u16 channelCount;
} Sound_Asset;



/*

#define UVGA_CODING_CHOICE_RUN_LENGTH_BITS 16
#define UVGA_CODING_CHOICE_BITS_PER_SAMPLE_BITS 5
#define UVGA_CODING_CHOICE_BITS ((UVGA_CODING_CHOICE_RUN_LENGTH_BITS) + (UVGA_CODING_CHOICE_BITS_PER_SAMPLE_BITS))
struct UVGA_Coding_Choice {
    s32 run_length;
    s32 bits_per_sample;
};


#pragma pack(push, 1)
// Uninterleaved Variable-bit Gradient Audio
struct UVGA_Header {
    u32 sample_rate;
    u32 samples_per_chunk;
    u16 chunks_per_channel;
    u8 channel_count;
    u8 bytes_per_sample;
    // u64 chunk_offsets_in_file[chunk_count];
};

struct FACS_Header {
    u32 sample_rate;
    u32 chunk_size;
    u16 chunk_count;
    u8 channel_count;
    u8 bytes_per_sample;
    u16 encoding_flags;
};

//
// TA
//

struct TA_Header {
    u16 texture_count;
    u16 atlas_width;
    u16 atlas_height;
    u8 texture_encoding;
    // @Todo: Change TA data to
    // texture_framing framings[texture_count];
    // u32 *pixels[atlas_width * atlas_height];
};

//
// Datapack
//

struct Datapack_Asset_Info {
    union {
        struct {
            u32 location_low;
            u16 location_high;
            u8 padding;
            u8 compression; // @Cleanup: Remove this.
        };
        usize all;
    };
};
struct Datapack_Header {
    u32 version;
    // Asset count and IDs are implicit.
    // To get an asset's length, use locations[id + 1] - locations[id].
    // datapack_asset_info asset_table[asset_count];
};
#pragma pack(pop)

struct Datapack_Handle {
    void *file_handle;
    Datapack_Asset_Info *assets;
};

struct Asset_Location {
    void *file_handle;
    u64 offset;
};

struct Asset_Metadata {
    Asset_Location location;
    u64 size;
};

static inline u64 get_offset_in_file(Datapack_Handle *pack, u32 asset_uid) {
    return pack->assets[asset_uid].all & (((u64)1 << 48) - 1);
}
static Asset_Location get_asset_location(Datapack_Handle *pack, u32 asset_uid) {
    ASSERT(asset_uid < ASSET_UID_COUNT);
    Asset_Location result;

    result.file_handle = pack->file_handle;
    result.offset = get_offset_in_file(pack, asset_uid);

    return result;
}
static Asset_Metadata get_asset_metadata(Datapack_Handle *pack, u32 asset_uid) {
    ASSERT(asset_uid < ASSET_UID_COUNT);
    Asset_Metadata result;

    result.location = get_asset_location(pack, asset_uid);
    result.size = get_offset_in_file(pack, asset_uid + 1) - result.location.offset;

    return result;
}

struct Asset_Storage {
    Datapack_Handle datapack;
};

//
// Synth Input
//

#define SYNTH_INPUT_VERSION 2

#pragma pack(push, 1)
struct Synth_Input_Note {
    s32 start;
    s32 duration; // @Compression

    f32 velocity; // @Compression: Normalized 0 -> 1 in integer?
    u8 midi_key; // @Compression: midi_key on 7 bits?
};
struct Synth_Input_Pitch_Bend {
    s32 center_sample; // @Compression
    s32 radius;        // @Compression

    f32 factor;
};
struct Synth_Input_Track {
    u16 instrument;
    u32 note_count;
    u32 pitch_bend_count;
    // Synth_Input_Note       notes[note_count];
    // Synth_Input_Pitch_Bend pitch_bends[pitch_bend_count];
};
struct Synth_Input_Header {
    u32 version;
    u32 ticks_per_second;
    u16 track_count;
    // [] Synth_Input_Track;
};
#pragma pack(pop)

// Synth1 instrument presets are simple text files; maybe we could read them and translate them to a
// format we like?
*/

/*
// Maybe???
struct Synth_Instrument {
    struct {
        u8 waveform;
        f32 volume;
    } oscillators[3];
    
    f32 delay;
    f32 delay_feedback;
    
    u8 filter_type;
    f32 cutoff;
    f32 resonance;
};
*/

//
// Meshes
//
/*
#pragma pack(push, 1)
#define SERIALIZED_EDIT_MESH_VERSION 1
struct Serialized_Edit_Mesh { // ;Serialized
    u16 version;

    v2 center_point;
    v2 offset;
    v2 scale;
    v2 rot;
    u8 layer_count;

    // v4 layer_colors[layer_count];
    // u16 layer_running_vert_total[layer_count];

    // v2 verts[layer_running_vert_total[layer_count-1]];
    // u16 indices[sum(index_count_or_zero(layer_counts))]; where layer_counts is running_total[i] - running_total[i-1]
};
#define SERIALIZED_RENDER_MESH_VERSION 1
struct Serialized_Render_Mesh { // ;Serialized
    u16 version;
    u16 vertex_count;

    v2 default_offset;
    v2 default_scale;
    v2 default_rotation;

    // Render_Vertex verts[vertex_count];
    // u16 indices[(vertex_count - 2) * 3];
};
#pragma pack(pop)
 */