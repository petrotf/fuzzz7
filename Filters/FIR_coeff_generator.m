% Saisir ici l'ordre du filtre (le nombre de coeffs -1 )
order = 254; 
% Saisir ici la taille voulue en bit des coefficients
sizeBits=16;
% Ici, les valeurs des fréquences de coupure compris entre 0 et 1 avec
% 0=0Hz et 1=Fe/2
f = [0 0.02494331065759637188208616780045 0.02947845804988662131519274376417 1];
% Ici saisir le gain voulu aux fréquences de coupure saisies plus haut
a = [0 0 1 1];

% Génération des coefficients, affichage du spectre obtenu
b = firpm(order,f,a);
[h,w] = freqz(b,1,512);
plot(f,a,w/pi,abs(h))
legend('Ideal','firpm Design')
xlabel 'Radian Frequency (\omega/\pi)', ylabel 'Magnitude'

% Manipulation des coefficients pour les transformer en entiers
Max = max(abs(b));
float=[];
format shortG
for i=1:order+1;
   float(i) = b(i)*(2^(sizeBits-1)-1)/Max;
end
entiers = round(float);

% Affichage des coefficients au format entier et virgule flottant
entiers = sprintf('%.0f, ',entiers)
float = sprintf('%.8f, ',b)

%[h1,w1] = freqz(entiers,1,512);
%plot(f,a*33900,w1/pi,abs(h1));

